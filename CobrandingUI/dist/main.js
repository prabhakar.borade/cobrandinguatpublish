(self["webpackChunkco_branding_ui"] = self["webpackChunkco_branding_ui"] || []).push([["main"],{

/***/ 8255:
/*!*******************************************************!*\
  !*** ./$_lazy_route_resources/ lazy namespace object ***!
  \*******************************************************/
/***/ ((module) => {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(() => {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = () => ([]);
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 8255;
module.exports = webpackEmptyAsyncContext;

/***/ }),

/***/ 9284:
/*!********************************************************!*\
  !*** ./src/app/@shared/collaterals.files.directive.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CollateralsDetails": () => (/* binding */ CollateralsDetails)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);

class CollateralsDetails {
    constructor() { }
}
CollateralsDetails.ɵfac = function CollateralsDetails_Factory(t) { return new (t || CollateralsDetails)(); };
CollateralsDetails.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: CollateralsDetails, factory: CollateralsDetails.ɵfac });


/***/ }),

/***/ 3341:
/*!*************************************!*\
  !*** ./src/app/@shared/constant.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ValidImageFileExtensions": () => (/* binding */ ValidImageFileExtensions),
/* harmony export */   "ValidDocumentFileExtensions": () => (/* binding */ ValidDocumentFileExtensions),
/* harmony export */   "ValidVideoFileExtensions": () => (/* binding */ ValidVideoFileExtensions)
/* harmony export */ });
const ValidImageFileExtensions = [".jpg", ".jpeg", ".png"];
const ValidDocumentFileExtensions = [".pdf"];
const ValidVideoFileExtensions = [".mp4"];


/***/ }),

/***/ 2871:
/*!********************************************************!*\
  !*** ./src/app/@shared/http/api-prefix.interceptor.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ApiPrefixInterceptor": () => (/* binding */ ApiPrefixInterceptor)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);

class ApiPrefixInterceptor {
    constructor() { }
    intercept(request, next) {
        var credentials = localStorage.getItem('credentials');
        if (credentials != null) {
            this.token = JSON.parse(credentials).token;
            request = request.clone({
                headers: request.headers.set('Authorization', 'Bearer ' + this.token),
                url: request.url,
            });
        }
        else {
            request = request.clone({
                url: request.url,
            });
        }
        return next.handle(request);
    }
}
ApiPrefixInterceptor.ɵfac = function ApiPrefixInterceptor_Factory(t) { return new (t || ApiPrefixInterceptor)(); };
ApiPrefixInterceptor.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ApiPrefixInterceptor, factory: ApiPrefixInterceptor.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 431:
/*!***********************************************************!*\
  !*** ./src/app/@shared/http/error-handler.interceptor.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ErrorHandlerInterceptor": () => (/* binding */ ErrorHandlerInterceptor)
/* harmony export */ });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var _logger_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../logger.service */ 7308);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




const log = new _logger_service__WEBPACK_IMPORTED_MODULE_1__.Logger('ErrorHandlerInterceptor');
/**
 * Adds a default error handler to all requests.
 */
class ErrorHandlerInterceptor {
    intercept(request, next) {
        return next.handle(request).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)((error) => this.errorHandler(error)));
    }
    // Customize the default error handler here if needed
    errorHandler(response) {
        if (!_env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.production) {
            // Do something with the error
            //log.error('Request error', response);
        }
        throw response;
    }
}
ErrorHandlerInterceptor.ɵfac = function ErrorHandlerInterceptor_Factory(t) { return new (t || ErrorHandlerInterceptor)(); };
ErrorHandlerInterceptor.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ token: ErrorHandlerInterceptor, factory: ErrorHandlerInterceptor.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 2842:
/*!**********************************!*\
  !*** ./src/app/@shared/index.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedModule": () => (/* reexport safe */ _shared_module__WEBPACK_IMPORTED_MODULE_0__.SharedModule),
/* harmony export */   "ApiPrefixInterceptor": () => (/* reexport safe */ _http_api_prefix_interceptor__WEBPACK_IMPORTED_MODULE_1__.ApiPrefixInterceptor),
/* harmony export */   "ErrorHandlerInterceptor": () => (/* reexport safe */ _http_error_handler_interceptor__WEBPACK_IMPORTED_MODULE_2__.ErrorHandlerInterceptor),
/* harmony export */   "LoaderComponent": () => (/* reexport safe */ _loader_loader_component__WEBPACK_IMPORTED_MODULE_3__.LoaderComponent),
/* harmony export */   "RouteReusableStrategy": () => (/* reexport safe */ _route_reusable_strategy__WEBPACK_IMPORTED_MODULE_4__.RouteReusableStrategy),
/* harmony export */   "LogLevel": () => (/* reexport safe */ _logger_service__WEBPACK_IMPORTED_MODULE_5__.LogLevel),
/* harmony export */   "Logger": () => (/* reexport safe */ _logger_service__WEBPACK_IMPORTED_MODULE_5__.Logger),
/* harmony export */   "UntilDestroy": () => (/* reexport safe */ _ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_6__.UntilDestroy),
/* harmony export */   "untilDestroyed": () => (/* reexport safe */ _ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_6__.untilDestroyed)
/* harmony export */ });
/* harmony import */ var _shared_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shared.module */ 2234);
/* harmony import */ var _http_api_prefix_interceptor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./http/api-prefix.interceptor */ 2871);
/* harmony import */ var _http_error_handler_interceptor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./http/error-handler.interceptor */ 431);
/* harmony import */ var _loader_loader_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loader/loader.component */ 9967);
/* harmony import */ var _route_reusable_strategy__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./route-reusable-strategy */ 9015);
/* harmony import */ var _logger_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./logger.service */ 7308);
/* harmony import */ var _ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngneat/until-destroy */ 6857);









/***/ }),

/***/ 9967:
/*!****************************************************!*\
  !*** ./src/app/@shared/loader/loader.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoaderComponent": () => (/* binding */ LoaderComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);

class LoaderComponent {
    constructor() {
        this.isLoading = false;
    }
    ngOnInit() { }
}
LoaderComponent.ɵfac = function LoaderComponent_Factory(t) { return new (t || LoaderComponent)(); };
LoaderComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LoaderComponent, selectors: [["app-loader"]], inputs: { isLoading: "isLoading", message: "message" }, decls: 6, vars: 1, consts: [[1, "text-xs-center", "loader", 3, "hidden"], [1, "fa", "fa-spinner", "fa-spin", "fa-3x"]], template: function LoaderComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", !ctx.isLoading);
    } }, styles: [".fa[_ngcontent-%COMP%] {\n  vertical-align: middle;\n}\n\n.loader[_ngcontent-%COMP%] {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  z-index: 9999999999;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHNCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUNGIiwiZmlsZSI6ImxvYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mYSB7XHJcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG5cclxuLmxvYWRlciB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICB6LWluZGV4OiA5OTk5OTk5OTk5O1xyXG59XHJcbiJdfQ== */"] });


/***/ }),

/***/ 7308:
/*!*******************************************!*\
  !*** ./src/app/@shared/logger.service.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LogLevel": () => (/* binding */ LogLevel),
/* harmony export */   "Logger": () => (/* binding */ Logger)
/* harmony export */ });
/**
 * Simple logger system with the possibility of registering custom outputs.
 *
 * 4 different log levels are provided, with corresponding methods:
 * - debug   : for debug information
 * - info    : for informative status of the application (success, ...)
 * - warning : for non-critical errors that do not prevent normal application behavior
 * - error   : for critical errors that prevent normal application behavior
 *
 * Example usage:
 * ```
 * import { Logger } from 'app/core/logger.service';
 *
 * const log = new Logger('myFile');
 * ...
 * log.debug('something happened');
 * ```
 *
 * To disable debug and info logs in production, add this snippet to your root component:
 * ```
 * export class AppComponent implements OnInit {
 *   ngOnInit() {
 *     if (environment.production) {
 *       Logger.enableProductionMode();
 *     }
 *     ...
 *   }
 * }
 *
 * If you want to process logs through other outputs than console, you can add LogOutput functions to Logger.outputs.
 */
/**
 * The possible log levels.
 * LogLevel.Off is never emitted and only used with Logger.level property to disable logs.
 */
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["Off"] = 0] = "Off";
    LogLevel[LogLevel["Error"] = 1] = "Error";
    LogLevel[LogLevel["Warning"] = 2] = "Warning";
    LogLevel[LogLevel["Info"] = 3] = "Info";
    LogLevel[LogLevel["Debug"] = 4] = "Debug";
})(LogLevel || (LogLevel = {}));
class Logger {
    constructor(source) {
        this.source = source;
    }
    /**
     * Enables production mode.
     * Sets logging level to LogLevel.Warning.
     */
    static enableProductionMode() {
        Logger.level = LogLevel.Warning;
    }
    /**
     * Logs messages or objects  with the debug level.
     * Works the same as console.log().
     */
    debug(...objects) {
        this.log(console.log, LogLevel.Debug, objects);
    }
    /**
     * Logs messages or objects  with the info level.
     * Works the same as console.log().
     */
    info(...objects) {
        this.log(console.info, LogLevel.Info, objects);
    }
    /**
     * Logs messages or objects  with the warning level.
     * Works the same as console.log().
     */
    warn(...objects) {
        this.log(console.warn, LogLevel.Warning, objects);
    }
    /**
     * Logs messages or objects  with the error level.
     * Works the same as console.log().
     */
    error(...objects) {
        this.log(console.error, LogLevel.Error, objects);
    }
    log(func, level, objects) {
        if (level <= Logger.level) {
            const log = this.source ? ['[' + this.source + ']'].concat(objects) : objects;
            func.apply(console, log);
            Logger.outputs.forEach((output) => output.apply(output, [this.source, level, ...objects]));
        }
    }
}
/**
 * Current logging level.
 * Set it to LogLevel.Off to disable logs completely.
 */
Logger.level = LogLevel.Debug;
/**
 * Additional log outputs.
 */
Logger.outputs = [];


/***/ }),

/***/ 4454:
/*!*******************************************************!*\
  !*** ./src/app/@shared/pagination/pager.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagerComponent": () => (/* binding */ PagerComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);


class PagerComponent {
    constructor() {
        this._itemCount = 0;
        this._pageSize = 0;
        this._pageCount = 0;
        this._pageIndex = 0;
        this.pageIndexChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.pageSize = 1;
    }
    get itemCount() {
        return this._itemCount;
    }
    set itemCount(value) {
        this._itemCount = value;
        this.updatePageCount();
    }
    get pageSize() {
        return this._pageSize;
    }
    set pageSize(value) {
        this._pageSize = value;
        this.updatePageCount();
    }
    updatePageCount() {
        this._pageCount = Math.ceil(this.itemCount / this.pageSize);
    }
    get pageIndex() {
        return this._pageIndex;
    }
    set pageIndex(value) {
        this._pageIndex = value;
        this.pageIndexChange.emit(this.pageIndex);
    }
    get canMoveToNextPage() {
        return this.pageIndex < this._pageCount - 1 ? true : false;
    }
    get canMoveToPreviousPage() {
        return this.pageIndex >= 1 ? true : false;
    }
    moveToNextPage() {
        if (this.canMoveToNextPage) {
            this.pageIndex++;
        }
    }
    moveToPreviousPage() {
        if (this.canMoveToPreviousPage) {
            this.pageIndex--;
        }
    }
    moveToLastPage() {
        this.pageIndex = this._pageCount - 1;
    }
    moveToFirstPage() {
        this.pageIndex = 0;
    }
}
PagerComponent.ɵfac = function PagerComponent_Factory(t) { return new (t || PagerComponent)(); };
PagerComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PagerComponent, selectors: [["pager"]], inputs: { itemCount: "itemCount", pageSize: "pageSize", pageIndex: "pageIndex" }, outputs: { pageIndexChange: "pageIndexChange" }, decls: 18, vars: 4, consts: [["role", "group"], [1, "btn", "paginator-button", 3, "disabled", "click"], [1, "fa", "fa-angle-left", "paginator-icon"], ["disabled", "", 1, "btn", 2, "width", "100px"], [1, "fa", "fa-angle-right", "paginator-icon"]], template: function PagerComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PagerComponent_Template_button_click_3_listener() { return ctx.moveToPreviousPage(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PagerComponent_Template_button_click_11_listener() { return ctx.moveToNextPage(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.canMoveToPreviousPage);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", ctx.pageIndex + 1, " / ", ctx._pageCount, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.canMoveToNextPage);
    } }, encapsulation: 2 });


/***/ }),

/***/ 2738:
/*!********************************************************!*\
  !*** ./src/app/@shared/progress/progress.component.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressComponent": () => (/* binding */ ProgressComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);

class ProgressComponent {
    constructor() {
        this.progress = 0;
    }
    ngOnInit() { }
}
ProgressComponent.ɵfac = function ProgressComponent_Factory(t) { return new (t || ProgressComponent)(); };
ProgressComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ProgressComponent, selectors: [["app-progress"]], inputs: { progress: "progress" }, decls: 5, vars: 2, consts: [[1, "progress-cont"], [1, "progress"]], template: function ProgressComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("width", ctx.progress + "%");
    } }, styles: [".progress-cont[_ngcontent-%COMP%] {\n  height: 7px;\n  width: 100%;\n  border-radius: 4px;\n  background-color: #d0d0d0;\n  position: relative;\n}\n.progress-cont[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%] {\n  width: 0;\n  height: 100%;\n  position: absolute;\n  z-index: 1;\n  top: 0;\n  left: 0;\n  border-radius: 4px;\n  background-color: #4c97cb;\n  transition: 0.5s all;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2dyZXNzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUFDRjtBQUNFO0VBQ0UsUUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxvQkFBQTtBQUNKIiwiZmlsZSI6InByb2dyZXNzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnByb2dyZXNzLWNvbnQge1xyXG4gIGhlaWdodDogN3B4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDBkMGQwO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgLnByb2dyZXNzIHtcclxuICAgIHdpZHRoOiAwO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGM5N2NiO1xyXG4gICAgdHJhbnNpdGlvbjogMC41cyBhbGw7XHJcbiAgfVxyXG59XHJcbiJdfQ== */"] });


/***/ }),

/***/ 9015:
/*!****************************************************!*\
  !*** ./src/app/@shared/route-reusable-strategy.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RouteReusableStrategy": () => (/* binding */ RouteReusableStrategy)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


/**
 * A route strategy allowing for explicit route reuse.
 * Used as a workaround for https://github.com/angular/angular/issues/18374
 * To reuse a given route, add `data: { reuse: true }` to the route definition.
 */
class RouteReusableStrategy extends _angular_router__WEBPACK_IMPORTED_MODULE_0__.RouteReuseStrategy {
    shouldDetach(route) {
        return false;
    }
    store(route, detachedTree) { }
    shouldAttach(route) {
        return false;
    }
    retrieve(route) {
        return null;
    }
    shouldReuseRoute(future, curr) {
        var _a, _b, _c;
        // Reuse the route if the RouteConfig is the same, or if both routes use the
        // same component, because the latter can have different RouteConfigs.
        return (future.routeConfig === curr.routeConfig ||
            Boolean(((_a = future.routeConfig) === null || _a === void 0 ? void 0 : _a.component) && ((_b = future.routeConfig) === null || _b === void 0 ? void 0 : _b.component) === ((_c = curr.routeConfig) === null || _c === void 0 ? void 0 : _c.component)));
    }
}
RouteReusableStrategy.ɵfac = /*@__PURE__*/ function () { let ɵRouteReusableStrategy_BaseFactory; return function RouteReusableStrategy_Factory(t) { return (ɵRouteReusableStrategy_BaseFactory || (ɵRouteReusableStrategy_BaseFactory = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](RouteReusableStrategy)))(t || RouteReusableStrategy); }; }();
RouteReusableStrategy.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: RouteReusableStrategy, factory: RouteReusableStrategy.ɵfac });


/***/ }),

/***/ 2234:
/*!******************************************!*\
  !*** ./src/app/@shared/shared.module.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedModule": () => (/* binding */ SharedModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _loader_loader_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loader/loader.component */ 9967);
/* harmony import */ var _progress_progress_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress/progress.component */ 2738);
/* harmony import */ var _pagination_pager_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pagination/pager.component */ 4454);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);





class SharedModule {
}
SharedModule.ɵfac = function SharedModule_Factory(t) { return new (t || SharedModule)(); };
SharedModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: SharedModule });
SharedModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](SharedModule, { declarations: [_loader_loader_component__WEBPACK_IMPORTED_MODULE_0__.LoaderComponent, _progress_progress_component__WEBPACK_IMPORTED_MODULE_1__.ProgressComponent, _pagination_pager_component__WEBPACK_IMPORTED_MODULE_2__.PagerComponent], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule], exports: [_loader_loader_component__WEBPACK_IMPORTED_MODULE_0__.LoaderComponent, _progress_progress_component__WEBPACK_IMPORTED_MODULE_1__.ProgressComponent, _pagination_pager_component__WEBPACK_IMPORTED_MODULE_2__.PagerComponent] }); })();


/***/ }),

/***/ 158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/shell/shell.service */ 6042);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);




const routes = [
    _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_0__.Shell.childRoutes([
        { path: 'co-branding', loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ./co-branding/co-branding.module */ 3883)).then((m) => m.CobrandingModule) },
        { path: 'my-profile', loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ./my-profile/my-profile.module */ 4907)).then((m) => m.MyProfileModule) }
    ]),
    // Fallback when no prior route is matched
    { path: '**', redirectTo: '', pathMatch: 'full' },
];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ providers: [], imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__.PreloadAllModules })], _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule] }); })();


/***/ }),

/***/ 5041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 6682);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5435);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 8002);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 3190);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser */ 9075);









const log = new _shared__WEBPACK_IMPORTED_MODULE_1__.Logger('App');
let AppComponent = class AppComponent {
    constructor(router, activatedRoute, titleService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.titleService = titleService;
    }
    ngOnInit() {
        // Setup logger
        if (_env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.production) {
            _shared__WEBPACK_IMPORTED_MODULE_1__.Logger.enableProductionMode();
        }
        //log.debug('init');
        const onNavigationEnd = this.router.events.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.filter)((event) => event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__.NavigationEnd));
        // Change page title on navigation or language change, based on route data
        (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.merge)(onNavigationEnd)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(() => {
            let route = this.activatedRoute;
            while (route.firstChild) {
                route = route.firstChild;
            }
            return route;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.filter)((route) => route.outlet === 'primary'), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.switchMap)((route) => route.data), (0,_shared__WEBPACK_IMPORTED_MODULE_1__.untilDestroyed)(this))
            .subscribe((event) => {
            const title = event.title;
            if (title) {
                this.titleService.setTitle(title);
            }
        });
    }
    ngOnDestroy() { }
};
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__.Title)); };
AppComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 2, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](0, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterOutlet], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"] });
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_shared__WEBPACK_IMPORTED_MODULE_1__.UntilDestroy)()
], AppComponent);



/***/ }),

/***/ 6747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser */ 9075);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ 2664);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _app_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/auth */ 6282);
/* harmony import */ var _collateral_collateral_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./collateral/collateral.module */ 9408);
/* harmony import */ var _shell_shell_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shell/shell.module */ 5506);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ 5041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ 158);
/* harmony import */ var _co_branding_co_branding_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./co-branding/co-branding.module */ 3883);
/* harmony import */ var _my_profile_my_profile_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./my-profile/my-profile.module */ 4907);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-toastr */ 9699);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/platform-browser/animations */ 5835);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);

















class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__.AppComponent] });
AppModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineInjector"]({ providers: [
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__.HTTP_INTERCEPTORS,
            useClass: _shared__WEBPACK_IMPORTED_MODULE_0__.ApiPrefixInterceptor,
            multi: true,
        },
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__.HTTP_INTERCEPTORS,
            useClass: _shared__WEBPACK_IMPORTED_MODULE_0__.ErrorHandlerInterceptor,
            multi: true,
        },
        {
            provide: _angular_router__WEBPACK_IMPORTED_MODULE_10__.RouteReuseStrategy,
            useClass: _shared__WEBPACK_IMPORTED_MODULE_0__.RouteReusableStrategy,
        },
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__.BrowserModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__.FormsModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__.HttpClientModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_10__.RouterModule,
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__.NgbModule,
            _shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule,
            _shell_shell_module__WEBPACK_IMPORTED_MODULE_3__.ShellModule,
            _collateral_collateral_module__WEBPACK_IMPORTED_MODULE_2__.CollateralModule,
            _co_branding_co_branding_module__WEBPACK_IMPORTED_MODULE_6__.CobrandingModule,
            _my_profile_my_profile_module__WEBPACK_IMPORTED_MODULE_7__.MyProfileModule,
            _app_auth__WEBPACK_IMPORTED_MODULE_1__.AuthModule,
            ngx_toastr__WEBPACK_IMPORTED_MODULE_14__.ToastrModule.forRoot({
                timeOut: 10000,
                preventDuplicates: true
            }),
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__.BrowserAnimationsModule,
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__.AppRoutingModule, // must be imported as the last module as it contains the fallback route
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__.AppComponent], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__.BrowserModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_12__.FormsModule,
        _angular_common_http__WEBPACK_IMPORTED_MODULE_9__.HttpClientModule,
        _angular_router__WEBPACK_IMPORTED_MODULE_10__.RouterModule,
        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__.NgbModule,
        _shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule,
        _shell_shell_module__WEBPACK_IMPORTED_MODULE_3__.ShellModule,
        _collateral_collateral_module__WEBPACK_IMPORTED_MODULE_2__.CollateralModule,
        _co_branding_co_branding_module__WEBPACK_IMPORTED_MODULE_6__.CobrandingModule,
        _my_profile_my_profile_module__WEBPACK_IMPORTED_MODULE_7__.MyProfileModule,
        _app_auth__WEBPACK_IMPORTED_MODULE_1__.AuthModule, ngx_toastr__WEBPACK_IMPORTED_MODULE_14__.ToastrModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__.BrowserAnimationsModule,
        _app_routing_module__WEBPACK_IMPORTED_MODULE_5__.AppRoutingModule] }); })();


/***/ }),

/***/ 4504:
/*!*********************************************************!*\
  !*** ./src/app/auth/AdminLogin/adminLogin.component.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdminLoginComponent": () => (/* binding */ AdminLoginComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 8939);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../authentication.service */ 7517);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .. */ 6282);










const log = new _shared__WEBPACK_IMPORTED_MODULE_1__.Logger('Login');
let AdminLoginComponent = class AdminLoginComponent {
    constructor(router, route, formBuilder, authenticationService, credentialsService) {
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.authenticationService = authenticationService;
        this.credentialsService = credentialsService;
        this.version = _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.version;
        this.isLoading = false;
        this.createForm();
    }
    ngOnInit() { }
    login() {
        this.isLoading = true;
        /*
        //Test data
        this.credentialsService.setCredentials(
          {
            userLoggedIn: 'sdf',
            username: 'sand',
            token: 'sdfs879-jkshdjkfh0809dfskjk89s7dfsj',
          },
          true
        );
        this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });*/
        const login$ = this.authenticationService.loginAdmin(this.loginForm.value);
        login$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.finalize)(() => {
            this.loginForm.markAsPristine();
            this.isLoading = false;
        }), (0,_shared__WEBPACK_IMPORTED_MODULE_1__.untilDestroyed)(this))
            .subscribe((credentials) => {
            console.log("activity id", credentials);
            //log.debug(`${credentials.username} successfully logged in`);
            this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
        }, (error) => {
            //log.debug(`Login error: ${error}`);
            this.error = error;
        });
    }
    createForm() {
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required],
            remember: true,
            roleId: 1,
        });
    }
};
AdminLoginComponent.ɵfac = function AdminLoginComponent_Factory(t) { return new (t || AdminLoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](_authentication_service__WEBPACK_IMPORTED_MODULE_2__.AuthenticationService), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdirectiveInject"](___WEBPACK_IMPORTED_MODULE_3__.CredentialsService)); };
AdminLoginComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({ type: AdminLoginComponent, selectors: [["app-admin-login"]], decls: 75, vars: 8, consts: [[1, "login-container", "bg-light"], [1, "login-box"], [1, "text-primary"], [1, "fab", "fa-artstation"], [1, "container"], [1, "mx-auto", "mt-3", "card", "col-md-4"], [1, "card-body"], [1, "mb-4", "text-center", "card-title", "text-primary"], ["novalidate", "", 1, "mt-4", 3, "formGroup", "ngSubmit"], [1, "alert", "alert-danger", 3, "hidden"], [1, "m-3"], [1, "my-3", "d-block"], ["type", "text", "formControlName", "username", "autocomplete", "off", 1, "form-control", 3, "placeholder"], ["hidden", ""], [1, "text-danger", 3, "hidden"], [1, "mb-3", "d-block"], ["type", "password", "formControlName", "password", "autocomplete", "false", "required", "", 1, "form-control", 3, "placeholder"], [1, "m-3", "mt-4"], ["type", "submit", 1, "btn", "btn-primary", "w-100", 3, "disabled"], [1, "fas", "fa-cog", "fa-spin", 3, "hidden"], [1, "ms-1", "fas", "fa-arrow-right", "btn-icon"], [1, "text-center"], ["routerLink", "/login"]], template: function AdminLoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](4, "h2", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](5, "i", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](6, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](7, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](8, "CoBrandz");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](9, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](10, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](11, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](12, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](13, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](14, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](15, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](16, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](17, "ADMIN LOGIN");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](18, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](19, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](20, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](21, "form", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵlistener"]("ngSubmit", function AdminLoginComponent_Template_form_ngSubmit_21_listener() { return ctx.login(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](22, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](23, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](24, "Username or password incorrect.");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](25, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](26, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](27, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](28, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](29, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](30, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](31, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](32, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](33, "USERNAME");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](34, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](35, "small", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](36, "\n                  Username is required\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](37, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](38, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](39, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](40, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](41, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](42, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](43, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](44, "PASSWORD");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](45, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](46, "small", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](47, "\n                  Password is required\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](48, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](49, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](50, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](51, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](52, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](53, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](54, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](55, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](56, "i", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](57, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](58, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](59, "LOGIN");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](60, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](61, "i", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](62, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](63, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](64, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](65, "\n\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](66, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementStart"](67, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](68, "Sign in as Distributor");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](69, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](70, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](71, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](72, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](73, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵtext"](74, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("formGroup", ctx.loginForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("hidden", !ctx.error || ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("placeholder", "USERNAME");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("hidden", ctx.loginForm.controls.username.valid || ctx.loginForm.controls.username.untouched);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("placeholder", "PASSWORD");
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("hidden", ctx.loginForm.controls.password.valid || ctx.loginForm.controls.password.untouched);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("disabled", ctx.loginForm.invalid || ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵproperty"]("hidden", !ctx.isLoading);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControlName, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.RequiredValidator, _angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterLinkWithHref], styles: [".login-container[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.login-box[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n  min-height: 100%;\n}\n.ng-invalid.ng-touched[_ngcontent-%COMP%]:not(form) {\n  border-left: 4px solid theme-color(\"danger\");\n}\n.container[_ngcontent-%COMP%] {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRoZW1lXFx0aGVtZS12YXJpYWJsZXMuc2NzcyIsImFkbWluTG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBQUE7QUNHQTtFQUNFLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBQUNGO0FBRUE7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBQ0Y7QUFFQTtFQUNFLDRDQUFBO0FBQ0Y7QUFHRTtFQUNFLFdBQUE7QUFBSiIsImZpbGUiOiJhZG1pbkxvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuICogQXBwbGljYXRpb24gZ2xvYmFsIHZhcmlhYmxlcy5cclxuICovXHJcblxyXG4vLyBTZXQgRm9udCBBd2Vzb21lIGZvbnQgcGF0aFxyXG4kZmEtZm9udC1wYXRoOiBcIn5AZm9ydGF3ZXNvbWUvZm9udGF3ZXNvbWUtZnJlZS93ZWJmb250c1wiO1xyXG5cclxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbi8vIEJvb3RzdHJhcCB2YXJpYWJsZXNcclxuLy9cclxuLy8gT3ZlcnJpZGUgQm9vdHN0cmFwIHZhcmlhYmxlcyBoZXJlIHRvIHN1aXRlIHlvdXIgdGhlbWUuXHJcbi8vIENvcHkgdmFyaWFibGVzIHlvdSB3YW50IHRvIGN1c3RvbWl6ZSBmcm9tIG5vZGVfbW9kdWxlcy9ib290c3RyYXAvc2Nzcy9fdmFyaWFibGVzLnNjc3NcclxuXHJcbi8vXHJcbi8vIENvbG9yIHN5c3RlbVxyXG4vL1xyXG5cclxuJHdoaXRlOiAjZmZmO1xyXG4kZ3JheS0xMDA6ICNmOGY5ZmE7XHJcbiRncmF5LTIwMDogI2U5ZWNlZjtcclxuJGdyYXktMzAwOiAjZGVlMmU2O1xyXG4kZ3JheS00MDA6ICNjZWQ0ZGE7XHJcbiRncmF5LTUwMDogI2FkYjViZDtcclxuJGdyYXktNjAwOiAjODY4ZTk2O1xyXG4kZ3JheS03MDA6ICM0OTUwNTc7XHJcbiRncmF5LTgwMDogIzM0M2E0MDtcclxuJGdyYXktOTAwOiAjMjEyNTI5O1xyXG4kZ3JheS1iZzogI2U1ZTVlNTtcclxuJGJsYWNrOiAjNDg0ODQ4O1xyXG5cclxuJHByaW1hcnk6ICNhMTE4NWE7XHJcbiRzZWNvbmRhcnk6ICM3MTcxNzE7XHJcbiRibHVlOiAjMDA3M2RkO1xyXG4kaW5kaWdvOiAjNjYxMGYyO1xyXG4kcHVycGxlOiAjNmY0MmMxO1xyXG4kcGluazogI2U4M2U4YztcclxuJHJlZDogI2RjMzU0NTtcclxuJG9yYW5nZTogI2ZkN2UxNDtcclxuJHllbGxvdzogI2ZmYzEwNztcclxuJGdyZWVuOiAjMjhhNzQ1O1xyXG4kdGVhbDogIzIwYzk5NztcclxuJGN5YW46ICMxN2EyYjg7XHJcblxyXG4kdGhlbWUtY29sb3JzOiAoXHJcbiAgcHJpbWFyeTogJHByaW1hcnksXHJcbiAgc2Vjb25kYXJ5OiAkZ3JheS02MDAsXHJcbiAgc3VjY2VzczogJGdyZWVuLFxyXG4gIGluZm86ICRjeWFuLFxyXG4gIHdhcm5pbmc6ICR5ZWxsb3csXHJcbiAgZGFuZ2VyOiAkcmVkLFxyXG4gIGxpZ2h0OiAkZ3JheS0xMDAsXHJcbiAgZGFyazogJGdyYXktODAwLFxyXG4gIGJsYWNrOiAkYmxhY2ssXHJcbik7XHJcblxyXG4vLyBVc2UgQm9vdHN0cmFwIGRlZmF1bHRzIGZvciBvdGhlciB2YXJpYWJsZXMsIGltcG9ydGVkIGhlcmUgc28gd2UgY2FuIGFjY2VzcyBhbGwgYXBwIHZhcmlhYmxlcyBpbiBvbmUgcGxhY2Ugd2hlbiB1c2VkXHJcbi8vIGluIGNvbXBvbmVudHMuXHJcbkBpbXBvcnQgXCJ+Ym9vdHN0cmFwL3Njc3MvX2Z1bmN0aW9uc1wiO1xyXG5AaW1wb3J0IFwifmJvb3RzdHJhcC9zY3NzL192YXJpYWJsZXNcIjtcclxuIiwiQGltcG9ydCBcInNyYy90aGVtZS90aGVtZS12YXJpYWJsZXNcIjtcclxuQGltcG9ydCBcIn5ib290c3RyYXAvc2Nzcy9taXhpbnMvX2JyZWFrcG9pbnRzXCI7XHJcblxyXG4ubG9naW4tY29udGFpbmVyIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwO1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcblxyXG4ubG9naW4tYm94IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxuICBtaW4taGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4ubmctaW52YWxpZC5uZy10b3VjaGVkOm5vdChmb3JtKSB7XHJcbiAgYm9yZGVyLWxlZnQ6IDRweCBzb2xpZCB0aGVtZS1jb2xvcihcImRhbmdlclwiKTtcclxufVxyXG5cclxuQGluY2x1ZGUgbWVkaWEtYnJlYWtwb2ludC1kb3duKHhzKSB7XHJcbiAgLmNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuIl19 */"] });
AdminLoginComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_shared__WEBPACK_IMPORTED_MODULE_1__.UntilDestroy)()
], AdminLoginComponent);



/***/ }),

/***/ 2276:
/*!*********************************************!*\
  !*** ./src/app/auth/auth-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthRoutingModule": () => (/* binding */ AuthRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _AdminLogin_adminLogin_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AdminLogin/adminLogin.component */ 4504);
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.component */ 8728);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);





const routes = [
    { path: 'login', component: _login_component__WEBPACK_IMPORTED_MODULE_1__.LoginComponent, data: { title: 'Login' } },
    { path: 'admin-login', component: _AdminLogin_adminLogin_component__WEBPACK_IMPORTED_MODULE_0__.AdminLoginComponent, data: { title: 'Admin-Login' } },
];
class AuthRoutingModule {
}
AuthRoutingModule.ɵfac = function AuthRoutingModule_Factory(t) { return new (t || AuthRoutingModule)(); };
AuthRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: AuthRoutingModule });
AuthRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ providers: [], imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AuthRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule] }); })();


/***/ }),

/***/ 1674:
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthModule": () => (/* binding */ AuthModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ 2664);
/* harmony import */ var _auth_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth-routing.module */ 2276);
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.component */ 8728);
/* harmony import */ var _AdminLogin_adminLogin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AdminLogin/adminLogin.component */ 4504);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);







class AuthModule {
}
AuthModule.ɵfac = function AuthModule_Factory(t) { return new (t || AuthModule)(); };
AuthModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: AuthModule });
AuthModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__.NgbModule, _auth_routing_module__WEBPACK_IMPORTED_MODULE_0__.AuthRoutingModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](AuthModule, { declarations: [_login_component__WEBPACK_IMPORTED_MODULE_1__.LoginComponent, _AdminLogin_adminLogin_component__WEBPACK_IMPORTED_MODULE_2__.AdminLoginComponent], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__.NgbModule, _auth_routing_module__WEBPACK_IMPORTED_MODULE_0__.AuthRoutingModule] }); })();


/***/ }),

/***/ 1395:
/*!**********************************************!*\
  !*** ./src/app/auth/authentication.guard.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthenticationGuard": () => (/* binding */ AuthenticationGuard)
/* harmony export */ });
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _credentials_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./credentials.service */ 2459);




const log = new _shared__WEBPACK_IMPORTED_MODULE_0__.Logger('AuthenticationGuard');
class AuthenticationGuard {
    constructor(router, credentialsService) {
        this.router = router;
        this.credentialsService = credentialsService;
    }
    canActivate(route, state) {
        if (this.credentialsService.isAuthenticated()) {
            return true;
        }
        //log.debug('Not authenticated, redirecting and adding redirect url...');
        this.router.navigate(['/login'], { queryParams: { redirect: state.url }, replaceUrl: true });
        return false;
    }
}
AuthenticationGuard.ɵfac = function AuthenticationGuard_Factory(t) { return new (t || AuthenticationGuard)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_credentials_service__WEBPACK_IMPORTED_MODULE_1__.CredentialsService)); };
AuthenticationGuard.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: AuthenticationGuard, factory: AuthenticationGuard.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 7517:
/*!************************************************!*\
  !*** ./src/app/auth/authentication.service.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthenticationService": () => (/* binding */ AuthenticationService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 8002);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _credentials_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./credentials.service */ 2459);
/* harmony import */ var _app_rest_user_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/rest/user-api.service */ 4962);





/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
class AuthenticationService {
    constructor(credentialsService, userApiService) {
        this.credentialsService = credentialsService;
        this.userApiService = userApiService;
    }
    /**
     * Authenticates the user.
     * @param context The login parameters.
     * @return The user credentials.
     */
    loginAdmin(context) {
        return this.userApiService
            .loginAdmin({ username: context.username, password: context.password, roleId: context.roleId })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)((o) => {
            const data = {
                userLoggedIn: o.data,
                username: o.data.username,
                token: o.data.token,
            };
            this.credentialsService.setCredentials(data, context.remember);
            return data;
        }));
        // Replace by proper authentication call
    }
    login(context) {
        return this.userApiService
            .login({ username: context.username, panNumber: context.panNumber, roleId: context.roleId })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)((o) => {
            const data = {
                userLoggedIn: o.data,
                username: o.data.username,
                token: o.data.token,
            };
            this.credentialsService.setCredentials(data, context.remember);
            return data;
        }));
        // Replace by proper authentication call
    }
    /**
     * Logs out the user and clear credentials.
     * @return True if the user was logged out successfully.
     */
    logout() {
        // Customize credentials invalidation here
        this.credentialsService.setCredentials();
        return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(true);
    }
}
AuthenticationService.ɵfac = function AuthenticationService_Factory(t) { return new (t || AuthenticationService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_credentials_service__WEBPACK_IMPORTED_MODULE_0__.CredentialsService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_app_rest_user_api_service__WEBPACK_IMPORTED_MODULE_1__.UserApiService)); };
AuthenticationService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: AuthenticationService, factory: AuthenticationService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 2459:
/*!*********************************************!*\
  !*** ./src/app/auth/credentials.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CredentialsService": () => (/* binding */ CredentialsService)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);

const credentialsKey = 'credentials';
/**
 * Provides storage for authentication credentials.
 * The Credentials interface should be replaced with proper implementation.
 */
class CredentialsService {
    constructor() {
        this._credentials = null;
        const savedCredentials = sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey);
        if (savedCredentials) {
            this._credentials = JSON.parse(savedCredentials);
        }
    }
    /**
     * Checks is the user is authenticated.
     * @return True if the user is authenticated.
     */
    isAuthenticated() {
        return !!this.credentials;
    }
    /**
     * Gets the user credentials.
     * @return The user credentials or null if the user is not authenticated.
     */
    get credentials() {
        return this._credentials;
    }
    /**
     * Sets the user credentials.
     * The credentials may be persisted across sessions by setting the `remember` parameter to true.
     * Otherwise, the credentials are only persisted for the current session.
     * @param credentials The user credentials.
     * @param remember True to remember credentials across sessions.
     */
    setCredentials(credentials, remember) {
        this._credentials = credentials || null;
        if (credentials) {
            const storage = remember ? localStorage : sessionStorage;
            storage.setItem(credentialsKey, JSON.stringify(credentials));
        }
        else {
            sessionStorage.removeItem(credentialsKey);
            localStorage.removeItem(credentialsKey);
        }
    }
}
CredentialsService.ɵfac = function CredentialsService_Factory(t) { return new (t || CredentialsService)(); };
CredentialsService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: CredentialsService, factory: CredentialsService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 6282:
/*!*******************************!*\
  !*** ./src/app/auth/index.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthModule": () => (/* reexport safe */ _auth_module__WEBPACK_IMPORTED_MODULE_0__.AuthModule),
/* harmony export */   "AuthenticationService": () => (/* reexport safe */ _authentication_service__WEBPACK_IMPORTED_MODULE_1__.AuthenticationService),
/* harmony export */   "CredentialsService": () => (/* reexport safe */ _credentials_service__WEBPACK_IMPORTED_MODULE_2__.CredentialsService),
/* harmony export */   "AuthenticationGuard": () => (/* reexport safe */ _authentication_guard__WEBPACK_IMPORTED_MODULE_3__.AuthenticationGuard)
/* harmony export */ });
/* harmony import */ var _auth_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.module */ 1674);
/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authentication.service */ 7517);
/* harmony import */ var _credentials_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./credentials.service */ 2459);
/* harmony import */ var _authentication_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./authentication.guard */ 1395);






/***/ }),

/***/ 8728:
/*!*****************************************!*\
  !*** ./src/app/auth/login.component.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginComponent": () => (/* binding */ LoginComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 8939);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authentication.service */ 7517);








const log = new _shared__WEBPACK_IMPORTED_MODULE_0__.Logger('Login');
let LoginComponent = class LoginComponent {
    constructor(router, route, formBuilder, authenticationService) {
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.authenticationService = authenticationService;
        this.isLoading = false;
        this.createForm();
    }
    ngOnInit() { }
    login() {
        this.isLoading = true;
        const login$ = this.authenticationService.login(this.loginForm.value);
        login$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.finalize)(() => {
            this.loginForm.markAsPristine();
            this.isLoading = false;
        }), (0,_shared__WEBPACK_IMPORTED_MODULE_0__.untilDestroyed)(this))
            .subscribe((credentials) => {
            //log.debug(`${credentials.username} successfully logged in`);
            this.router.navigate([this.route.snapshot.queryParams.redirect || '/'], { replaceUrl: true });
        }, (error) => {
            //log.debug(`Login error: ${error}`);
            this.error = error;
        });
    }
    createForm() {
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required],
            panNumber: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required],
            remember: true,
            roleId: 2,
        });
    }
};
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_authentication_service__WEBPACK_IMPORTED_MODULE_1__.AuthenticationService)); };
LoginComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: LoginComponent, selectors: [["app-login"]], decls: 81, vars: 8, consts: [[1, "login-container", "bg-light"], [1, "login-box"], [1, "text-primary"], [1, "fab", "fa-artstation"], [1, "container"], [1, "mx-auto", "mt-3", "card", "col-md-4"], [1, "card-body"], [1, "mb-4", "text-center", "card-title", "text-primary"], ["novalidate", "", 1, "mt-4", 3, "formGroup", "ngSubmit"], [1, "alert", "alert-danger", 3, "hidden"], [1, "m-3"], [1, "my-3", "mb-4", "d-block"], ["type", "text", "formControlName", "username", "autocomplete", "off", 1, "form-control", 3, "placeholder"], ["hidden", ""], [1, "text-danger", 3, "hidden"], [1, "mb-3", "d-block"], ["type", "text", "formControlName", "panNumber", "autocomplete", "off", "required", "", 1, "form-control", 3, "placeholder"], [1, "m-3", "mt-4"], ["type", "submit", 1, "btn", "btn-primary", "w-100", 3, "disabled"], [1, "fas", "fa-cog", "fa-spin", 3, "hidden"], [1, "ms-1", "fas", "fa-arrow-right", "btn-icon"], [1, "text-center"], ["routerLink", "/admin-login"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "h2", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](5, "i", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](6, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](8, "CoBrandz");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](9, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](11, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](13, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](15, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](17, "SIGN IN");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](18, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](19, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](20, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "form", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function LoginComponent_Template_form_ngSubmit_21_listener() { return ctx.login(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](22, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](24, "ARN Number Or Pan Number incorrect.");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](25, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](27, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](28, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](29, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](30, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](31, "ARN");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](32, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](33, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](34, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](35, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](36, "ARN Number");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](37, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](38, "small", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](39, "\n                  ARN Number is required\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](40, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](41, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](42, "label", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](43, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](44, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](45, "PAN");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](46, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](47, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](48, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](49, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](50, "PAN Number");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](51, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](52, "small", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](53, "\n                  PAN Number is required\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](54, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](55, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](56, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](57, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](58, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](59, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](60, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](61, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](62, "i", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](63, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](64, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](65, "LOGIN");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](66, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](67, "i", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](68, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](69, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](70, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](71, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](72, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](73, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](74, "Sign in as Admin");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](75, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](76, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](77, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](78, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](79, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](80, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.loginForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("hidden", !ctx.error || ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("placeholder", "Enter ARN Number");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("hidden", ctx.loginForm.controls.username.valid || ctx.loginForm.controls.username.untouched);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("placeholder", "Enter PAN Number");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("hidden", ctx.loginForm.controls.panNumber.valid || ctx.loginForm.controls.panNumber.untouched);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx.loginForm.invalid || ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("hidden", !ctx.isLoading);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControlName, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.RequiredValidator, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterLinkWithHref], styles: [".login-container[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.login-box[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n  min-height: 100%;\n}\n.ng-invalid.ng-touched[_ngcontent-%COMP%]:not(form) {\n  border-left: 4px solid theme-color(\"danger\");\n}\n.container[_ngcontent-%COMP%] {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcdGhlbWVcXHRoZW1lLXZhcmlhYmxlcy5zY3NzIiwibG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBQUE7QUNHQTtFQUNFLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBQUNGO0FBRUE7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBQ0Y7QUFFQTtFQUNFLDRDQUFBO0FBQ0Y7QUFHRTtFQUNFLFdBQUE7QUFBSiIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiAqIEFwcGxpY2F0aW9uIGdsb2JhbCB2YXJpYWJsZXMuXHJcbiAqL1xyXG5cclxuLy8gU2V0IEZvbnQgQXdlc29tZSBmb250IHBhdGhcclxuJGZhLWZvbnQtcGF0aDogXCJ+QGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLWZyZWUvd2ViZm9udHNcIjtcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBCb290c3RyYXAgdmFyaWFibGVzXHJcbi8vXHJcbi8vIE92ZXJyaWRlIEJvb3RzdHJhcCB2YXJpYWJsZXMgaGVyZSB0byBzdWl0ZSB5b3VyIHRoZW1lLlxyXG4vLyBDb3B5IHZhcmlhYmxlcyB5b3Ugd2FudCB0byBjdXN0b21pemUgZnJvbSBub2RlX21vZHVsZXMvYm9vdHN0cmFwL3Njc3MvX3ZhcmlhYmxlcy5zY3NzXHJcblxyXG4vL1xyXG4vLyBDb2xvciBzeXN0ZW1cclxuLy9cclxuXHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXktMTAwOiAjZjhmOWZhO1xyXG4kZ3JheS0yMDA6ICNlOWVjZWY7XHJcbiRncmF5LTMwMDogI2RlZTJlNjtcclxuJGdyYXktNDAwOiAjY2VkNGRhO1xyXG4kZ3JheS01MDA6ICNhZGI1YmQ7XHJcbiRncmF5LTYwMDogIzg2OGU5NjtcclxuJGdyYXktNzAwOiAjNDk1MDU3O1xyXG4kZ3JheS04MDA6ICMzNDNhNDA7XHJcbiRncmF5LTkwMDogIzIxMjUyOTtcclxuJGdyYXktYmc6ICNlNWU1ZTU7XHJcbiRibGFjazogIzQ4NDg0ODtcclxuXHJcbiRwcmltYXJ5OiAjYTExODVhO1xyXG4kc2Vjb25kYXJ5OiAjNzE3MTcxO1xyXG4kYmx1ZTogIzAwNzNkZDtcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG5cclxuJHRoZW1lLWNvbG9yczogKFxyXG4gIHByaW1hcnk6ICRwcmltYXJ5LFxyXG4gIHNlY29uZGFyeTogJGdyYXktNjAwLFxyXG4gIHN1Y2Nlc3M6ICRncmVlbixcclxuICBpbmZvOiAkY3lhbixcclxuICB3YXJuaW5nOiAkeWVsbG93LFxyXG4gIGRhbmdlcjogJHJlZCxcclxuICBsaWdodDogJGdyYXktMTAwLFxyXG4gIGRhcms6ICRncmF5LTgwMCxcclxuICBibGFjazogJGJsYWNrLFxyXG4pO1xyXG5cclxuLy8gVXNlIEJvb3RzdHJhcCBkZWZhdWx0cyBmb3Igb3RoZXIgdmFyaWFibGVzLCBpbXBvcnRlZCBoZXJlIHNvIHdlIGNhbiBhY2Nlc3MgYWxsIGFwcCB2YXJpYWJsZXMgaW4gb25lIHBsYWNlIHdoZW4gdXNlZFxyXG4vLyBpbiBjb21wb25lbnRzLlxyXG5AaW1wb3J0IFwifmJvb3RzdHJhcC9zY3NzL19mdW5jdGlvbnNcIjtcclxuQGltcG9ydCBcIn5ib290c3RyYXAvc2Nzcy9fdmFyaWFibGVzXCI7XHJcbiIsIkBpbXBvcnQgXCJzcmMvdGhlbWUvdGhlbWUtdmFyaWFibGVzXCI7XHJcbkBpbXBvcnQgXCJ+Ym9vdHN0cmFwL3Njc3MvbWl4aW5zL19icmVha3BvaW50c1wiO1xyXG5cclxuLmxvZ2luLWNvbnRhaW5lciB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxufVxyXG5cclxuLmxvZ2luLWJveCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWluLWhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLm5nLWludmFsaWQubmctdG91Y2hlZDpub3QoZm9ybSkge1xyXG4gIGJvcmRlci1sZWZ0OiA0cHggc29saWQgdGhlbWUtY29sb3IoXCJkYW5nZXJcIik7XHJcbn1cclxuXHJcbkBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtZG93bih4cykge1xyXG4gIC5jb250YWluZXIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcbiJdfQ== */"] });
LoginComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_shared__WEBPACK_IMPORTED_MODULE_0__.UntilDestroy)()
], LoginComponent);



/***/ }),

/***/ 7368:
/*!***********************************************************!*\
  !*** ./src/app/co-branding/co-branding-routing.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CobrandingRoutingModule": () => (/* binding */ CobrandingRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/shell/shell.service */ 6042);
/* harmony import */ var _co_branding_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./co-branding.component */ 828);
/* harmony import */ var _define_cobranding_define_cobranding_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./define-cobranding/define-cobranding.component */ 7394);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);






const routes = [
    _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_0__.Shell.childRoutes([
        { path: '', redirectTo: '/co-branding', pathMatch: 'full' },
        { path: 'co-branding', component: _co_branding_component__WEBPACK_IMPORTED_MODULE_1__.CobrandingComponent, data: { title: 'Co-Branding' } },
        { path: 'co-branding/define', component: _define_cobranding_define_cobranding_component__WEBPACK_IMPORTED_MODULE_2__.DefineCobrandingComponent, data: { title: 'Define-Cobranding' } },
    ]),
];
class CobrandingRoutingModule {
}
CobrandingRoutingModule.ɵfac = function CobrandingRoutingModule_Factory(t) { return new (t || CobrandingRoutingModule)(); };
CobrandingRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: CobrandingRoutingModule });
CobrandingRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ providers: [], imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](CobrandingRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule] }); })();


/***/ }),

/***/ 828:
/*!******************************************************!*\
  !*** ./src/app/co-branding/co-branding.component.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CobrandingComponent": () => (/* binding */ CobrandingComponent)
/* harmony export */ });
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ 2664);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ 2340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _app_rest_product_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/rest/product-api.service */ 3173);
/* harmony import */ var _app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/rest/files-api.service */ 8226);
/* harmony import */ var _app_rest_cobrand_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/rest/cobrand-api.service */ 1436);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ 9699);
/* harmony import */ var _shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../@shared/loader/loader.component */ 9967);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ng-select/ng-select */ 6640);
/* harmony import */ var _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../@shared/pagination/pager.component */ 4454);














const _c0 = ["downloadFileModal"];
const _c1 = ["filterModal"];
const _c2 = ["infoFileModal"];
function CobrandingComponent_span_31_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "span", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "i", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_span_31_Template_i_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r11.resetFilter(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
const _c3 = function (a0, a1) { return { productId: a0, productName: a1 }; };
function CobrandingComponent_ng_option_107_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const product_r13 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](2, _c3, product_r13.id, product_r13.name));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](product_r13.name);
} }
const _c4 = function (a0, a1) { return { subproductId: a0, subproductName: a1 }; };
function CobrandingComponent_ng_option_133_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const subproduct_r14 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](2, _c4, subproduct_r14.id, subproduct_r14.name));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](subproduct_r14.name);
} }
function CobrandingComponent_div_165_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "h3", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "No Co-Brand Created");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CobrandingComponent_pager_171_Template(rf, ctx) { if (rf & 1) {
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "pager", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("pageIndexChange", function CobrandingComponent_pager_171_Template_pager_pageIndexChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r16); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r15.pageIndex = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("itemCount", ctx_r4.coBrandListLength)("pageIndex", ctx_r4.pageIndex)("pageSize", ctx_r4.pageSize);
} }
function CobrandingComponent_div_174_Template(rf, ctx) { if (rf & 1) {
    const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "span", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "i", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "ul", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_div_174_Template_button_click_14_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r18); const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r17.dateFilter("Last Modified"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n              Last Modified\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](18, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](20, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_div_174_Template_button_click_20_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r18); const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r19.sortFilter("ASC"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "A - Z");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](26, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_div_174_Template_button_click_26_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r18); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r20.sortFilter("DESC"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "Z - A");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx_r5.sortType);
} }
function CobrandingComponent_div_179_img_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](0, "img", 69);
} if (rf & 2) {
    const cobrand_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", ctx_r23.apiEndPoint + cobrand_r21.cobrandfilemaps[0].file.thumnailPath, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsanitizeUrl"]);
} }
function CobrandingComponent_div_179_img_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](0, "img", 70);
} }
function CobrandingComponent_div_179_i_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "i", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CobrandingComponent_div_179_li_20_Template(rf, ctx) { if (rf & 1) {
    const _r31 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_div_179_li_20_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r31); const cobrand_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit; const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r29.infoCoBrandClick(cobrand_r21.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                Info\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CobrandingComponent_div_179_li_28_Template(rf, ctx) { if (rf & 1) {
    const _r34 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_div_179_li_28_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r34); const cobrand_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit; const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r32.deleteCoBrandById(cobrand_r21.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                Delete\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CobrandingComponent_div_179_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](4, CobrandingComponent_div_179_img_4_Template, 1, 1, "img", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](6, CobrandingComponent_div_179_img_6_Template, 1, 0, "img", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](9, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](11, "label", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, " \u00A0\n\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](16, CobrandingComponent_div_179_i_16_Template, 2, 0, "i", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](18, "ul", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](20, CobrandingComponent_div_179_li_20_Template, 5, 0, "li", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "button", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_div_179_Template_button_click_24_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r36); const cobrand_r21 = restoredCtx.$implicit; const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r35.getFilesByCoBrandId(cobrand_r21.collateralId); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n                Download\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](28, CobrandingComponent_div_179_li_28_Template, 5, 0, "li", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](32, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](37, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](39, "label", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](41, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const cobrand_r21 = ctx.$implicit;
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", cobrand_r21.isProcessed == true);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", cobrand_r21.isProcessed == false);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](cobrand_r21.collateralName);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", cobrand_r21.isProcessed == true);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx_r6.userRole == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx_r6.userRole == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate2"]("", cobrand_r21.categoryName, " | ", cobrand_r21.subcategoryName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](41, 9, cobrand_r21.updatedAt, "MMM dd, YYYY | HH:mm"));
} }
function CobrandingComponent_ng_template_184_div_14_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r45 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "img", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "a", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_ng_template_184_div_14_div_2_Template_a_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r45); const y_r42 = restoredCtx.$implicit; const ctx_r44 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r44.downloadFile(y_r42.file.path); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](7, "i", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](9, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const y_r42 = ctx.$implicit;
    const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", ctx_r41.apiEndPoint + y_r42.file.thumnailPath, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsanitizeUrl"]);
} }
function CobrandingComponent_ng_template_184_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](2, CobrandingComponent_ng_template_184_div_14_div_2_Template, 12, 1, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const x_r39 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", x_r39.cobrandfilemaps);
} }
function CobrandingComponent_ng_template_184_Template(rf, ctx) { if (rf & 1) {
    const _r47 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](0, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "div", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "h4", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "Download Co-Brand Files");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_ng_template_184_Template_button_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r47); const modal_r37 = restoredCtx.$implicit; return modal_r37.dismiss("Cross click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](8, "i", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](14, CobrandingComponent_ng_template_184_div_14_Template, 4, 1, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n");
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r8.downloadFileList);
} }
function CobrandingComponent_ng_template_187_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "Co-Brand Name :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "Product Tpye :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](13, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](14, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](16, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "Sub Product Tpye :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](20, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](21, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](23, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Number Of Files :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](27, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](28, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](30, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](31, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "Created At :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](34, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](35, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](36, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const info_r50 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r50.collateralName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r50.categoryName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r50.subcategoryName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r50.cobrandfilemaps.length, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](34, 5, info_r50.updatedAt, "MMM dd, YYYY | HH:mm"), "");
} }
function CobrandingComponent_ng_template_187_Template(rf, ctx) { if (rf & 1) {
    const _r53 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](0, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "div", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "h4", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "Co-Brand Information");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_ng_template_187_Template_button_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r53); const modal_r48 = restoredCtx.$implicit; return modal_r48.dismiss("Cross click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](8, "i", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](14, CobrandingComponent_ng_template_187_div_14_Template, 38, 8, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n");
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r10.cobrandInfoList);
} }
const log = new _shared__WEBPACK_IMPORTED_MODULE_0__.Logger('Cobrands');
class CobrandingComponent {
    constructor(modalService, formBuilder, productService, filesService, cobrandService, toastr //private webSocketService:WebSocketService
    ) {
        this.modalService = modalService;
        this.formBuilder = formBuilder;
        this.productService = productService;
        this.filesService = filesService;
        this.cobrandService = cobrandService;
        this.toastr = toastr;
        this.isLoading = false;
        this.popUpCloseResult = '';
        this.searchText = '';
        this.searchOptions = 'Search By Co-Brand Name';
        this.productCategory$ = [];
        this.subProductCategory$ = [];
        this.sortType = 'Last Modified';
        this.apiEndPoint = '';
        this.coBrandList = [];
        this.coBrandListLength = 0;
        this.cobrandInfoList = [];
        this.downloadFileList = [];
        this.downloadFileListLength = 0;
        this.isFilterOpened = false;
        //this.socket = io('http://127.0.0.1:1339/');
        var credentials = localStorage.getItem('credentials') || '';
        this.userRole = JSON.parse(credentials).userLoggedIn.role;
        this.loggedUserId = JSON.parse(credentials).userLoggedIn.id;
        this.apiEndPoint = _environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.serverUrl;
        this.pageIndex = 0;
        this.pageSize = 12;
        this.createForm();
    }
    createForm() {
        this.ApplyFilterForm = this.formBuilder.group({
            imageTypeFilter: [''],
            videoTypeFilter: [''],
            documentTypeFilter: [''],
            selectedProductCategoryFilter: [''],
            selectedSubProductCategoryFilter: [''],
        });
    }
    ngOnInit() {
        // this.webSocketService.socket.on('processing', (data: any) => {
        //   console.log(data);
        // });
        this.ApplyFilterForm.reset();
        this.getAllCobrands();
    }
    onProductChange(event) {
        this.getSubProductCategory(event);
    }
    getProductCategory() {
        this.isLoading = true;
        this.productService.getProductCategory().subscribe((data) => {
            this.isLoading = false;
            this.productCategory$ = data;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    getSubProductCategory(ProductId) {
        this.isLoading = true;
        this.productService.getSubProductCategory({ ProductId: ProductId }).subscribe((data) => {
            this.isLoading = false;
            this.subProductCategory$ = data;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    // Popup start
    openDownloadPopUp(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.popUpCloseResult = `Closed with: ${result}`;
        }, (reason) => {
            this.popUpCloseResult = `Dismissed ${this.getPopUpDismissReason(reason)}`;
        });
    }
    openFilterPopUp(content) {
        this.isFilterOpened = !this.isFilterOpened;
        if (this.isFilterOpened == true) {
            this.ApplyFilterForm.reset();
            this.getProductCategory();
            this.subProductCategory$ = [];
            this.searchText = '';
        }
        // this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
        //   (result) => {
        //     this.popUpCloseResult = `Closed with: ${result}`;
        //   },
        //   (reason) => {
        //     this.popUpCloseResult = `Dismissed ${this.getPopUpDismissReason(reason)}`;
        //   }
        // );
    }
    closeFilter() {
        const dropDownId = document.getElementById('dropdownMenuClickable');
        dropDownId === null || dropDownId === void 0 ? void 0 : dropDownId.click();
    }
    openInfoPopUp(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.popUpCloseResult = `Closed with: ${result}`;
        }, (reason) => {
            this.popUpCloseResult = `Dismissed ${this.getPopUpDismissReason(reason)}`;
        });
    }
    getPopUpDismissReason(reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__.ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__.ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return `with: ${reason}`;
        }
    }
    //Popup close
    getAllCobrands() {
        this.isLoading = true;
        const data = {
            query: {
                createdBy: this.loggedUserId,
            },
            options: {
                sort: [['id', 'DESC']],
            },
        };
        this.cobrandService.getAllCoBrandingList(data).subscribe((data) => {
            this.isLoading = false;
            this.coBrandList = data.data;
            this.coBrandListLength = this.coBrandList.length;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    getFilesByCoBrandId(collateralId) {
        let list = this.coBrandList;
        list.forEach((element) => {
            if (element.collateralId == collateralId) {
                this.downloadFileList = [element];
            }
        });
        this.downloadFileListLength = this.downloadFileList.length;
        this.openDownloadPopUp(this.downloadFileModal);
    }
    deleteCoBrandById(cobrandId) {
        this.isLoading = true;
        let imgNames = [];
        let thumbNames = [];
        this.coBrandList.forEach((element) => {
            if (element.id == cobrandId) {
                for (let i = 0; i < element.cobrandfilemaps.length; i++) {
                    const item = element.cobrandfilemaps[i];
                    imgNames.push(item.file.imageName);
                    thumbNames.push(item.file.thumnailName);
                }
            }
        });
        this.cobrandService.deleteCobrand({ id: cobrandId, imgList: imgNames, thumbList: thumbNames }).subscribe((data) => {
            this.isLoading = false;
            imgNames.splice(0, imgNames.length);
            thumbNames.splice(0, thumbNames.length);
            this.toastr.success('Deleted successfully!');
            this.getAllCobrands();
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    downloadFile(fileName) {
        let filename = fileName.split('/').pop();
        window.open(this.filesService.downloadFileByNameCobrand + filename, '_blank');
    }
    isFilterClick() {
        this.openFilterPopUp(this.filterModal);
    }
    applyFilter() {
        this.isLoading = true;
        const imagetype = this.ApplyFilterForm.controls.imageTypeFilter.value == true ? 'Image' : null;
        const videotype = this.ApplyFilterForm.controls.videoTypeFilter.value == true ? 'Video' : null;
        const documenttype = this.ApplyFilterForm.controls.documentTypeFilter.value == true ? 'Document' : null;
        const categoryid = this.ApplyFilterForm.controls.selectedProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedProductCategoryFilter.value.productId)
            : null;
        const subcategoryid = this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value.subproductId)
            : null;
        const categoryname = this.ApplyFilterForm.controls.selectedProductCategoryFilter.value != null
            ? this.ApplyFilterForm.controls.selectedProductCategoryFilter.value.productName
            : 'All';
        const subcategoryname = this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value != null
            ? this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value.subproductName
            : 'All';
        const collateralType = [];
        const categoryId = [];
        const subcategoryId = [];
        if (imagetype != null) {
            collateralType.push(imagetype);
        }
        if (videotype != null) {
            collateralType.push(videotype);
        }
        if (documenttype != null) {
            collateralType.push(documenttype);
        }
        if (categoryid != null) {
            categoryId.push(categoryid);
        }
        if (subcategoryid != null) {
            subcategoryId.push(subcategoryid);
        }
        const data = {
            filter: [{ collateralType: collateralType }, { categoryId: categoryId }, { subcategoryId: subcategoryId }],
        };
        this.cobrandService.getCoBrandsByFilter(data).subscribe((data) => {
            this.isLoading = false;
            this.coBrandList = data.data;
            this.coBrandListLength = this.coBrandList.length;
            this.searchOptions =
                'type:' +
                    (collateralType.length != 0 ? collateralType : 'All') +
                    '; cat:' +
                    categoryname +
                    '; sub-cat:' +
                    subcategoryname;
            this.closeFilter();
            //this.modalService.dismissAll('Dismissed after fetching data');
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    resetFilter() {
        this.searchText = '';
        this.searchOptions = 'Search By Co-Brand Name';
        //this.modalService.dismissAll('Dismissed after reset data');
        this.ApplyFilterForm.reset();
        this.getAllCobrands();
        this.closeFilter();
    }
    dateFilter(type) {
        this.isLoading = true;
        if (type == 'Last Modified') {
            this.sortType = 'Last Modified';
            this.getAllCobrands();
        }
    }
    sortFilter(order) {
        this.isLoading = true;
        let data;
        if (order == 'ASC') {
            this.sortType = 'A - Z';
            data = {
                query: {
                    createdBy: this.loggedUserId,
                },
                options: {
                    sort: [['collateralName', 'ASC']],
                },
            };
        }
        if (order == 'DESC') {
            this.sortType = 'Z - A';
            data = {
                query: {
                    createdBy: this.loggedUserId,
                },
                options: {
                    sort: [['collateralName', 'DESC']],
                },
            };
        }
        this.cobrandService.getAllCoBrandingList(data).subscribe((data) => {
            this.isLoading = false;
            this.coBrandList = data.data;
            this.coBrandListLength = this.coBrandList.length;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    onSearchCoBrands() {
        this.isLoading = true;
        const imagetype = this.ApplyFilterForm.controls.imageTypeFilter.value == true ? 'Image' : null;
        const videotype = this.ApplyFilterForm.controls.videoTypeFilter.value == true ? 'Video' : null;
        const documenttype = this.ApplyFilterForm.controls.documentTypeFilter.value == true ? 'Document' : null;
        const categoryid = this.ApplyFilterForm.controls.selectedProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedProductCategoryFilter.value.productId)
            : null;
        const subcategoryid = this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value.subproductId)
            : null;
        const categoryname = this.ApplyFilterForm.controls.selectedProductCategoryFilter.value != null
            ? this.ApplyFilterForm.controls.selectedProductCategoryFilter.value.productName
            : 'All';
        const subcategoryname = this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value != null
            ? this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value.subproductName
            : 'All';
        let searchedText = this.searchText;
        const collateralType = [];
        const categoryId = [];
        const subcategoryId = [];
        if (imagetype != null) {
            collateralType.push(imagetype);
        }
        if (videotype != null) {
            collateralType.push(videotype);
        }
        if (documenttype != null) {
            collateralType.push(documenttype);
        }
        if (categoryid != null) {
            categoryId.push(categoryid);
        }
        if (subcategoryid != null) {
            subcategoryId.push(subcategoryid);
        }
        const data = {
            filter: [{ collateralType: collateralType }, { categoryId: categoryId }, { subcategoryId: subcategoryId }],
            search: [{ collateralName: searchedText }],
        };
        this.cobrandService.getCoBrandsByFilter(data).subscribe((data) => {
            this.isLoading = false;
            this.coBrandList = data.data;
            this.coBrandListLength = this.coBrandList.length;
            this.searchOptions =
                'type:' +
                    (collateralType.length != 0 ? collateralType : 'All') +
                    '; cat:' +
                    categoryname +
                    '; sub-cat:' +
                    subcategoryname;
            this.modalService.dismissAll('Dismissed after fetching data');
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    infoCoBrandClick(cobrandId) {
        this.coBrandList.forEach((element) => {
            if (element.id == cobrandId) {
                this.cobrandInfoList = [element];
            }
        });
        this.openInfoPopUp(this.infoFileModal);
    }
}
CobrandingComponent.ɵfac = function CobrandingComponent_Factory(t) { return new (t || CobrandingComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__.NgbModal), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_rest_product_api_service__WEBPACK_IMPORTED_MODULE_2__.ProductService), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_3__.FilesService), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_rest_cobrand_api_service__WEBPACK_IMPORTED_MODULE_4__.CobrandService), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_10__.ToastrService)); };
CobrandingComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineComponent"]({ type: CobrandingComponent, selectors: [["app-co-branding"]], viewQuery: function CobrandingComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵviewQuery"](_c0, 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵviewQuery"](_c1, 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵviewQuery"](_c2, 7);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵloadQuery"]()) && (ctx.downloadFileModal = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵloadQuery"]()) && (ctx.filterModal = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵloadQuery"]()) && (ctx.infoFileModal = _t.first);
    } }, decls: 191, vars: 15, consts: [[1, "container-fluid"], [1, "text-center", "jumbotron"], [3, "isLoading"], [1, "mx-5", "mt-5", "page-title"], [1, "text-black", "d-inline"], [1, "mx-5", "mt-4"], [1, "row"], [1, "col-md-5"], [1, "mb-3", "input-group", "drop-shadow", 2, "z-index", "99"], [1, "input-group-prepend", "text-primary"], [1, "input-group-text"], [1, "fas", "fa-search"], ["type", "text", 1, "form-control", "text-primary", 3, "ngModel", "placeholder", "ngModelChange", "keyup.enter"], ["class", "input-group-text", 4, "ngIf"], [1, "input-group-append", "text-primary"], ["id", "dropdownMenuClickable", "data-bs-toggle", "dropdown", "data-bs-auto-close", "false", "aria-expanded", "false", 1, "fas", "fa-sliders-h", 3, "click"], ["aria-labelledby", "dropdownMenuClickable", 1, "dropdown-menu", "dropdown-menu-lg-end", "filter-box"], [1, "modal-body"], [3, "formGroup", "ngSubmit"], [1, "col-md-10"], [1, "font-weight-bold", "text-black", "d-block"], [1, "col-md-2"], ["type", "button", "aria-label", "Close", 1, "btn", "btn-link", 3, "click"], ["aria-hidden", "true", 1, "fas", "fa-times"], [1, "row", 2, "padding-left", "3%"], [1, "col-md-3", "form-check", "form-check-inline"], ["mdbCheckbox", "", "type", "checkbox", "id", "inlineCheckbox1", "value", "Image", "formControlName", "imageTypeFilter", 1, "form-check-input"], ["for", "inlineCheckbox1", 1, "form-check-label"], ["mdbCheckbox", "", "type", "checkbox", "id", "inlineCheckbox2", "value", "Video", "formControlName", "videoTypeFilter", 1, "form-check-input"], ["for", "inlineCheckbox2", 1, "form-check-label"], ["mdbCheckbox", "", "type", "checkbox", "id", "inlineCheckbox3", "value", "Document", "formControlName", "documentTypeFilter", 1, "form-check-input"], ["for", "inlineCheckbox3", 1, "form-check-label"], [1, "my-4"], [1, "mb-3", "input-group", "input-control"], ["formControlName", "selectedProductCategoryFilter", 1, "form-control", 3, "change"], [3, "value", 4, "ngFor", "ngForOf"], [1, "fas", "fa-caret-down"], ["formControlName", "selectedSubProductCategoryFilter", 1, "form-control"], [1, "modal-footer"], ["type", "button", 1, "btn", "btn-link", 3, "click"], ["type", "button", 1, "btn", "btn-primary", 3, "click"], ["class", "row", 4, "ngIf"], [1, "col-md-6"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange", 4, "ngIf"], ["class", "col-md-6 d-flex align-items-center justify-content-end", "style", "padding-right: 5%", 4, "ngIf"], ["class", "col-md-2 card card-compact box-padding", 4, "ngFor", "ngForOf"], ["data-backdrop", "static", "data-keyboard", "false"], ["downloadFileModal", ""], ["infoFileModal", ""], [1, "fas", "fa-times", 3, "click"], [3, "value"], [2, "text-align", "center"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange"], [1, "col-md-6", "d-flex", "align-items-center", "justify-content-end", 2, "padding-right", "5%"], [1, "btn-group"], [1, "sort-type"], ["type", "button", "data-bs-toggle", "dropdown", "aria-expanded", "false", 1, "fas", "fa-caret-down"], [1, "dropdown-menu", "dropdown-menu-end"], ["type", "button", 1, "dropdown-item", "co-menu-items", 3, "click"], [1, "col-md-2", "card", "card-compact", "box-padding"], [1, "card-img"], [3, "src", 4, "ngIf"], ["src", "assets/inprocess.png", 4, "ngIf"], [1, "card-info"], [1, "co-name"], ["type", "button", "class", "fas fa-ellipsis-v co-menu", "data-bs-toggle", "dropdown", "aria-expanded", "false", 4, "ngIf"], [4, "ngIf"], [1, "co-type-name"], [1, "co-date"], [3, "src"], ["src", "assets/inprocess.png"], ["type", "button", "data-bs-toggle", "dropdown", "aria-expanded", "false", 1, "fas", "fa-ellipsis-v", "co-menu"], [1, "modal-header"], ["id", "modal-basic-title", 1, "modal-title"], ["class", "my-4", 4, "ngFor", "ngForOf"], [3, "click"], [1, "fas", "fa-download"]], template: function CobrandingComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "app-loader", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](9, "h2", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "Co-Branded Assets");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](19, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](21, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](23, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](24, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("ngModelChange", function CobrandingComponent_Template_input_ngModelChange_27_listener($event) { return ctx.searchText = $event; })("keyup.enter", function CobrandingComponent_Template_input_keyup_enter_27_listener() { return ctx.onSearchCoBrands(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](29, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](31, CobrandingComponent_span_31_Template, 4, 0, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](36, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_Template_i_click_38_listener() { return ctx.isFilterClick(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](40, "ul", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](46, "form", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("ngSubmit", function CobrandingComponent_Template_form_ngSubmit_46_listener() { return ctx.applyFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](47, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](48, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](50, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](51, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](52, "h6", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](53, "Document Type");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](56, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](58, "button", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_Template_button_click_58_listener() { return ctx.closeFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](60, "i", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](61, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](62, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](64, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](65, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](66, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](67, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](69, "input", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](70, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](71, "label", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](72, "Image");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](74, "\n\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](75, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](77, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](78, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](79, "label", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](80, "Video");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](81, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](82, "\n\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](83, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](85, "input", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](87, "label", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](88, "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](89, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](90, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](91, "\n\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](92, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](93, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](94, "h6", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](95, "Fund Category");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](96, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](97, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](98, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](99, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](100, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](101, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](102, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](103, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](104, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](105, "ng-select", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("change", function CobrandingComponent_Template_ng_select_change_105_listener($event) { return ctx.onProductChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](106, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](107, CobrandingComponent_ng_option_107_Template, 2, 5, "ng-option", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](108, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](109, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](110, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](111, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](112, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](113, "i", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](114, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](115, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](116, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](117, "\n\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](118, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](119, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](120, "h6", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](121, "Fund Sub Category");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](122, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](123, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](124, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](125, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](126, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](127, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](128, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](129, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](130, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](131, "ng-select", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](132, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](133, CobrandingComponent_ng_option_133_Template, 2, 5, "ng-option", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](134, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](135, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](136, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](137, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](138, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](139, "i", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](140, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](141, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](142, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](143, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](144, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](145, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](146, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](147, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](148, "button", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_Template_button_click_148_listener() { return ctx.resetFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](149, "Reset");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](150, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](151, "button", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CobrandingComponent_Template_button_click_151_listener() { return ctx.applyFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](152, "Apply");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](153, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](154, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](155, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](156, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](157, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](158, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](159, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](160, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](161, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](162, "\n\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](163, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](164, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](165, CobrandingComponent_div_165_Template, 5, 0, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](166, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](167, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](168, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](169, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](170, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](171, CobrandingComponent_pager_171_Template, 2, 3, "pager", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](172, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](173, "\n\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](174, CobrandingComponent_div_174_Template, 32, 1, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](175, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](176, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](177, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](178, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](179, CobrandingComponent_div_179_Template, 44, 12, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](180, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](181, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](182, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](183, "\n\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](184, CobrandingComponent_ng_template_184_Template, 17, 1, "ng-template", 46, 47, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](186, "\n\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](187, CobrandingComponent_ng_template_187_Template, 17, 1, "ng-template", 46, 48, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](189, "\n\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](190, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("isLoading", ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](23);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngModel", ctx.searchText)("placeholder", ctx.searchOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.searchText != "");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("formGroup", ctx.ApplyFilterForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](61);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx.productCategory$);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](26);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx.subProductCategory$);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](32);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.coBrandListLength == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.coBrandListLength >= ctx.pageSize);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.coBrandListLength > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind3"](180, 11, ctx.coBrandList, ctx.pageIndex * ctx.pageSize, (ctx.pageIndex + 1) * ctx.pageSize));
    } }, directives: [_shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_5__.LoaderComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.NgModel, _angular_common__WEBPACK_IMPORTED_MODULE_11__.NgIf, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.CheckboxControlValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormControlName, _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_12__.NgSelectComponent, _angular_common__WEBPACK_IMPORTED_MODULE_11__.NgForOf, _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_12__["ɵr"], _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_6__.PagerComponent], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_11__.SlicePipe, _angular_common__WEBPACK_IMPORTED_MODULE_11__.DatePipe], styles: [".logo[_ngcontent-%COMP%] {\n  width: 100px;\n}\n\n.filter-box[_ngcontent-%COMP%] {\n  width: 94%;\n  margin-top: 2% !important;\n  margin-right: 3% !important;\n}\n\n.box-padding[_ngcontent-%COMP%] {\n  margin-left: 15px;\n  margin-right: 15px;\n}\n\n.sort-type[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 23px;\n  letter-spacing: 0.02em;\n  color: #717171;\n  padding-right: 5px;\n}\n\n.sort-pointer[_ngcontent-%COMP%] {\n  padding-top: 4px;\n  color: #717171;\n}\n\n.co-name[_ngcontent-%COMP%] {\n  width: 184px;\n  height: 18px;\n  left: 124px;\n  top: 558px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 18px;\n  color: #484848;\n}\n\n.co-type-name[_ngcontent-%COMP%] {\n  width: 184px;\n  height: 18px;\n  left: 446px;\n  top: 576px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 18px;\n  color: #adadad;\n}\n\n.co-date[_ngcontent-%COMP%] {\n  width: 138px;\n  height: 15px;\n  left: 446px;\n  top: 599px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 12px;\n  line-height: 15px;\n  color: #353030;\n}\n\n.co-menu[_ngcontent-%COMP%] {\n  justify-content: flex-end !important;\n}\n\n.co-menu-items[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 18px;\n  letter-spacing: 0.02em;\n  color: #484848;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvLWJyYW5kaW5nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQUNGOztBQUVBO0VBQ0UsVUFBQTtFQUNBLHlCQUFBO0VBQ0EsMkJBQUE7QUFDRjs7QUFFQTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFFQTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7QUFDRjs7QUFFQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBRUE7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUNGOztBQUVBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFDRjs7QUFFQTtFQUNFLG9DQUFBO0FBQ0Y7O0FBRUE7RUFDRSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7QUFDRiIsImZpbGUiOiJjby1icmFuZGluZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dvIHtcclxuICB3aWR0aDogMTAwcHg7XHJcbn1cclxuXHJcbi5maWx0ZXItYm94IHtcclxuICB3aWR0aDogOTQlO1xyXG4gIG1hcmdpbi10b3A6IDIlICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiAzJSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYm94LXBhZGRpbmcge1xyXG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gIG1hcmdpbi1yaWdodDogMTVweDtcclxufVxyXG5cclxuLnNvcnQtdHlwZSB7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDIzcHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDJlbTtcclxuICBjb2xvcjogIzcxNzE3MTtcclxuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbi5zb3J0LXBvaW50ZXIge1xyXG4gIHBhZGRpbmctdG9wOiA0cHg7XHJcbiAgY29sb3I6ICM3MTcxNzE7XHJcbn1cclxuXHJcbi5jby1uYW1lIHtcclxuICB3aWR0aDogMTg0cHg7XHJcbiAgaGVpZ2h0OiAxOHB4O1xyXG4gIGxlZnQ6IDEyNHB4O1xyXG4gIHRvcDogNTU4cHg7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgY29sb3I6ICM0ODQ4NDg7XHJcbn1cclxuXHJcbi5jby10eXBlLW5hbWUge1xyXG4gIHdpZHRoOiAxODRweDtcclxuICBoZWlnaHQ6IDE4cHg7XHJcbiAgbGVmdDogNDQ2cHg7XHJcbiAgdG9wOiA1NzZweDtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIGNvbG9yOiAjYWRhZGFkO1xyXG59XHJcblxyXG4uY28tZGF0ZSB7XHJcbiAgd2lkdGg6IDEzOHB4O1xyXG4gIGhlaWdodDogMTVweDtcclxuICBsZWZ0OiA0NDZweDtcclxuICB0b3A6IDU5OXB4O1xyXG4gIGZvbnQtZmFtaWx5OiBSYWpkaGFuaTtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgbGluZS1oZWlnaHQ6IDE1cHg7XHJcbiAgY29sb3I6ICMzNTMwMzA7XHJcbn1cclxuXHJcbi5jby1tZW51IHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jby1tZW51LWl0ZW1zIHtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjAyZW07XHJcbiAgY29sb3I6ICM0ODQ4NDg7XHJcbn1cclxuIl19 */"] });


/***/ }),

/***/ 3883:
/*!***************************************************!*\
  !*** ./src/app/co-branding/co-branding.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CobrandingModule": () => (/* binding */ CobrandingModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _co_branding_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./co-branding-routing.module */ 7368);
/* harmony import */ var _co_branding_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./co-branding.component */ 828);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _define_cobranding_define_cobranding_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./define-cobranding/define-cobranding.component */ 7394);
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-select/ng-select */ 6640);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-pdf-viewer */ 3621);
/* harmony import */ var ngx_drag_resize__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-drag-resize */ 9898);
/* harmony import */ var _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular-slider/ngx-slider */ 4555);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);











class CobrandingModule {
}
CobrandingModule.ɵfac = function CobrandingModule_Factory(t) { return new (t || CobrandingModule)(); };
CobrandingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({ type: CobrandingModule });
CobrandingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({ imports: [[
            _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_5__.NgxSliderModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _shared__WEBPACK_IMPORTED_MODULE_2__.SharedModule,
            _co_branding_routing_module__WEBPACK_IMPORTED_MODULE_0__.CobrandingRoutingModule,
            _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__.NgSelectModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__.ReactiveFormsModule,
            ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_9__.PdfViewerModule,
            ngx_drag_resize__WEBPACK_IMPORTED_MODULE_10__.NgxDragResizeModule,
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](CobrandingModule, { declarations: [_co_branding_component__WEBPACK_IMPORTED_MODULE_1__.CobrandingComponent, _define_cobranding_define_cobranding_component__WEBPACK_IMPORTED_MODULE_3__.DefineCobrandingComponent], imports: [_angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_5__.NgxSliderModule,
        _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
        _shared__WEBPACK_IMPORTED_MODULE_2__.SharedModule,
        _co_branding_routing_module__WEBPACK_IMPORTED_MODULE_0__.CobrandingRoutingModule,
        _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__.NgSelectModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormsModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_8__.ReactiveFormsModule,
        ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_9__.PdfViewerModule,
        ngx_drag_resize__WEBPACK_IMPORTED_MODULE_10__.NgxDragResizeModule] }); })();


/***/ }),

/***/ 7394:
/*!******************************************************************************!*\
  !*** ./src/app/co-branding/define-cobranding/define-cobranding.component.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DefineCobrandingComponent": () => (/* binding */ DefineCobrandingComponent)
/* harmony export */ });
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ 6738);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ngx_drag_resize__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-drag-resize */ 9898);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ 2340);
/* harmony import */ var _app_shared_constant__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/@shared/constant */ 3341);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/rest/collateral-api.service */ 2690);
/* harmony import */ var _app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/rest/files-api.service */ 8226);
/* harmony import */ var _app_rest_cobrand_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @app/rest/cobrand-api.service */ 1436);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-toastr */ 9699);
/* harmony import */ var _shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../@shared/loader/loader.component */ 9967);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../@shared/pagination/pager.component */ 4454);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ng2-pdf-viewer */ 3621);

















function DefineCobrandingComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_16_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r5.previousPage(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](4, "i", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](7, "button", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](10, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_16_Template_button_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r7.nextPage(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](12, "i", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("disabled", ctx_r0.page === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate2"]("", ctx_r0.page, " / ", ctx_r0.totalPages, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("disabled", ctx_r0.totalPages === 1 || ctx_r0.page === ctx_r0.totalPages);
} }
function DefineCobrandingComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "pager", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("pageIndexChange", function DefineCobrandingComponent_div_18_Template_pager_pageIndexChange_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r8.pageIndex = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("itemCount", ctx_r1.fileDetailsListLength)("pageIndex", ctx_r1.pageIndex)("pageSize", ctx_r1.pageSize);
} }
function DefineCobrandingComponent_div_50_div_2_img_10_Template(rf, ctx) { if (rf & 1) {
    const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "img", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("load", function DefineCobrandingComponent_div_50_div_2_img_10_Template_img_load_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r18); const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r17.onImageLoad(ctx_r17.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "image-id-" + ctx_r13.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r13.apiEndPoint + file_r11.file.path, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function DefineCobrandingComponent_div_50_div_2_img_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](0, "img", 30);
} if (rf & 2) {
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "logo-area-" + ctx_r14.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r14.fileName, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function DefineCobrandingComponent_div_50_div_2_div_62_Template(rf, ctx) { if (rf & 1) {
    const _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "label", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "input", 33, 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("change", function DefineCobrandingComponent_div_50_div_2_div_62_Template_input_change_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r22); const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r21.fileBrowseHandler($event.target); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](10, "i", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, " Upload file ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} }
function DefineCobrandingComponent_div_50_div_2_div_64_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "i", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_50_div_2_div_64_Template_i_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r24); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r23.deleteFile(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"]("\n              ", ctx_r16.files[0].name, "\n            ");
} }
const _c0 = function () { return { standalone: true }; };
function DefineCobrandingComponent_div_50_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](8, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](10, DefineCobrandingComponent_div_50_div_2_img_10_Template, 1, 2, "img", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](14, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](15, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](16, DefineCobrandingComponent_div_50_div_2_img_16_Template, 1, 2, "img", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](17, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](18, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](19, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](20, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](21, "canvas", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](22, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](23, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](24, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](25, "canvas", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](27, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](28, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](29, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](30, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](31, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](32, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](33, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](34, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](35, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](36, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](37, "label", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](38, "Logo Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](39, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](40, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](41, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](42, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](43, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](44, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](45, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](46, "Witdh : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](47, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](48, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](49);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](50, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](51, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](52, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](53, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](54, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](55, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](56, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](57, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](58);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](59, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](60, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](61, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](62, DefineCobrandingComponent_div_50_div_2_div_62_Template, 15, 0, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](63, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](64, DefineCobrandingComponent_div_50_div_2_div_64_Template, 4, 1, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](65, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](66, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](67, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](68, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](69, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](70, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](71, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](72, "label", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](73, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](74, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](75, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](76, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](77, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](78, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](79, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](80, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](81, "Witdh : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](82, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](83, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](84);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](85, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](86, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](87, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](88, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](89, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](90, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](91, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](92, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](93);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](94, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](95, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](96, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](97, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](98, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](99, "Max Character : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](100, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](101, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](102);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](103, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](104, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](105, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](106, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](107, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](108, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](109, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](110, "textarea", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("ngModelChange", function DefineCobrandingComponent_div_50_div_2_Template_textarea_ngModelChange_110_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r26); const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r25.textAreaValue = $event; })("ngModelChange", function DefineCobrandingComponent_div_50_div_2_Template_textarea_ngModelChange_110_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r26); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r27.textChanged($event, ctx_r27.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](111, "                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](112, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](113, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](114, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](115, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](116, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](117, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](118, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](119, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](120, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](121, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](122, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_50_div_2_Template_button_click_122_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r26); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r28.submitCobrand(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](123, "SAVE AND GENERATE");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](124, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](125, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](126, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](127, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r11 = ctx.$implicit;
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", file_r11.file.mimetype == "image/jpeg" || file_r11.file.mimetype == "image/jpg" || file_r11.file.mimetype == "image/png");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r10.files.length > 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "text-area-" + ctx_r10.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "textCanvas-" + ctx_r10.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r11.watermarkDetails[0].logo.logoWidthcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r11.watermarkDetails[0].logo.logoHeightcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r10.files.length == 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r10.files.length > 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r11.watermarkDetails[0].text.textAreaWidthcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r11.watermarkDetails[0].text.textAreaHeightcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r11.watermarkDetails[0].text.maxCharacter);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngModel", ctx_r10.textAreaValue)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpureFunction0"](13, _c0));
} }
function DefineCobrandingComponent_div_50_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](2, DefineCobrandingComponent_div_50_div_2_Template, 128, 14, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](4, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpipeBind3"](3, 1, ctx_r2.fileDetailsList, ctx_r2.pageIndex * ctx_r2.pageSize, (ctx_r2.pageIndex + 1) * ctx_r2.pageSize));
} }
function DefineCobrandingComponent_div_52_div_2_div_2_img_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](0, "img", 30);
} if (rf & 2) {
    const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "logo-area-" + ctx_r35.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r35.fileName, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function DefineCobrandingComponent_div_52_div_2_div_2_div_59_Template(rf, ctx) { if (rf & 1) {
    const _r40 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "input", 43, 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("change", function DefineCobrandingComponent_div_52_div_2_div_2_div_59_Template_input_change_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r40); const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4); return ctx_r39.fileBrowseHandler($event.target); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](10, "i", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, " Upload file ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} }
function DefineCobrandingComponent_div_52_div_2_div_2_div_61_Template(rf, ctx) { if (rf & 1) {
    const _r42 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "i", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_52_div_2_div_2_div_61_Template_i_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r42); const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4); return ctx_r41.deleteFile(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"]("\n                ", ctx_r37.files[0].name, "\n              ");
} }
function DefineCobrandingComponent_div_52_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r44 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](8, "pdf-viewer", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("page-rendered", function DefineCobrandingComponent_div_52_div_2_div_2_Template_pdf_viewer_page_rendered_8_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r44); const ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r43.pdfPageRendered($event); })("after-load-complete", function DefineCobrandingComponent_div_52_div_2_div_2_Template_pdf_viewer_after_load_complete_8_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r44); const ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r45.afterLoadComplete($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](10, "\n\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](11, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](13, DefineCobrandingComponent_div_52_div_2_div_2_img_13_Template, 1, 2, "img", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](15, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](16, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](17, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](18, "canvas", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](19, "\n                        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](20, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](21, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](22, "canvas", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](23, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](24, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](25, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](26, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](27, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](28, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](29, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](30, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](31, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](32, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](33, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](34, "label", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](35, "Logo Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](36, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](37, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](38, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](39, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](40, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](41, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](42, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](43, "Witdh : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](44, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](45, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](46);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](47, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](48, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](49, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](50, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](51, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](52, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](53, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](54, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](55);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](56, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](57, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](58, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](59, DefineCobrandingComponent_div_52_div_2_div_2_div_59_Template, 15, 0, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](60, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](61, DefineCobrandingComponent_div_52_div_2_div_2_div_61_Template, 4, 1, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](62, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](63, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](64, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](65, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](66, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](67, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](68, "label", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](69, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](70, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](71, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](72, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](73, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](74, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](75, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](76, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](77, "Witdh : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](78, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](79, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](80);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](81, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](82, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](83, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](84, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](85, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](86, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](87, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](88, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](89);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](90, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](91, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](92, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](93, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](94, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](95, "Max Character : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](96, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](97, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](98);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](99, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](100, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](101, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](102, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](103, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](104, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](105, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](106, "textarea", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("ngModelChange", function DefineCobrandingComponent_div_52_div_2_div_2_Template_textarea_ngModelChange_106_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r44); const ctx_r46 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r46.textAreaValue = $event; })("ngModelChange", function DefineCobrandingComponent_div_52_div_2_div_2_Template_textarea_ngModelChange_106_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r44); const ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r47.textChanged($event, ctx_r47.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](107, "                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](108, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](109, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](110, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](111, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](112, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](113, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](114, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](115, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](116, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](117, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](118, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_52_div_2_div_2_Template_button_click_118_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r44); const ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r48.submitCobrand(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](119, "SAVE AND GENERATE");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](120, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](121, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](122, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](123, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const pageData_r33 = ctx.$implicit;
    const document_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r32.apiEndPoint + document_r30.file.path)("show-all", false)("page", ctx_r32.page)("original-size", false)("render-text", false)("zoom-scale", "page-width");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r32.files.length > 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "text-area-" + ctx_r32.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "textCanvas-" + ctx_r32.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](pageData_r33.logoDetails.width);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](pageData_r33.logoDetails.height);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r32.files.length == 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r32.files.length > 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](pageData_r33.textAreaDetails.width);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](pageData_r33.textAreaDetails.height);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](pageData_r33.textAreaDetails.maxCharacter);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngModel", ctx_r32.textAreaValue)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpureFunction0"](18, _c0));
} }
function DefineCobrandingComponent_div_52_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](2, DefineCobrandingComponent_div_52_div_2_div_2_Template, 124, 19, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](4, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpipeBind3"](3, 1, ctx_r29.documentPageData, ctx_r29.pageIndex * ctx_r29.pageSize, (ctx_r29.pageIndex + 1) * ctx_r29.pageSize));
} }
function DefineCobrandingComponent_div_52_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](2, DefineCobrandingComponent_div_52_div_2_Template, 5, 5, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngForOf", ctx_r3.fileDetailsList);
} }
function DefineCobrandingComponent_div_54_div_2_img_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](0, "img", 53);
} if (rf & 2) {
    const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "logo-area-" + ctx_r53.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r53.fileName, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function DefineCobrandingComponent_div_54_div_2_div_80_Template(rf, ctx) { if (rf & 1) {
    const _r58 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "label", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "input", 33, 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("change", function DefineCobrandingComponent_div_54_div_2_div_80_Template_input_change_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r58); const ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r57.fileBrowseHandler($event.target); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](10, "i", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, " Upload file ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} }
function DefineCobrandingComponent_div_54_div_2_div_82_Template(rf, ctx) { if (rf & 1) {
    const _r60 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "i", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_54_div_2_div_82_Template_i_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r60); const ctx_r59 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r59.deleteFile(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"]("\n              ", ctx_r55.files[0].name, "\n            ");
} }
function DefineCobrandingComponent_div_54_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r62 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](8, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](10, "video", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](12, "source", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](15, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](16, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](17, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](18, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](19, DefineCobrandingComponent_div_54_div_2_img_19_Template, 1, 2, "img", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](20, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](21, "span", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](22, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](23, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](24, "canvas", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](25, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](27, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](28, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](29, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](30, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](31, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](32, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](33, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](34, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](35, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](36, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](37, "label", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](38, "Logo Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](39, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](40, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](41, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](42, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](43, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](44, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](45, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](46, "Witdh : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](47, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](48, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](49);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](50, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](51, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](52, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](53, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](54, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](55, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](56, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](57, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](58);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](59, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](60, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](61, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](62, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](63, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](64, "Start Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](65, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](66, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](67);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](68, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](69, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](70, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](71, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](72, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](73, "End Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](74, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](75, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](76);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](77, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](78, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](79, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](80, DefineCobrandingComponent_div_54_div_2_div_80_Template, 15, 0, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](81, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](82, DefineCobrandingComponent_div_54_div_2_div_82_Template, 4, 1, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](83, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](84, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](85, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](86, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](87, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](88, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](89, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](90, "label", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](91, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](92, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](93, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](94, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](95, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](96, "div", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](97, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](98, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](99, "Witdh : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](100, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](101, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](102);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](103, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](104, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](105, "div", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](106, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](107, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](108, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](109, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](110, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](111);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](112, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](113, "\n\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](114, "div", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](115, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](116, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](117, "Max Character : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](118, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](119, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](120);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](121, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](122, "\n\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](123, "div", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](124, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](125, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](126, "Start Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](127, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](128, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](129);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](130, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](131, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](132, "div", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](133, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](134, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](135, "End Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](136, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](137, "label", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](138);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](139, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](140, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](141, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](142, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](143, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](144, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](145, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](146, "textarea", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("ngModelChange", function DefineCobrandingComponent_div_54_div_2_Template_textarea_ngModelChange_146_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r62); const ctx_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r61.textAreaValue = $event; })("ngModelChange", function DefineCobrandingComponent_div_54_div_2_Template_textarea_ngModelChange_146_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r62); const ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r63.textChanged($event, ctx_r63.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](147, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](148, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](149, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](150, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](151, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](152, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](153, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](154, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](155, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](156, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](157, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function DefineCobrandingComponent_div_54_div_2_Template_button_click_157_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r62); const ctx_r64 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r64.submitCobrand(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](158, "SAVE AND GENERATE");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](159, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](160, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](161, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](162, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r51 = ctx.$implicit;
    const ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r50.apiEndPoint + file_r51.file.path, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r50.files.length > 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "text-area-" + ctx_r50.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("id", "textCanvas-" + ctx_r50.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("height", file_r51.watermarkDetails[0].text.textAreaHeightcordinate / ctx_r50.videoAspectRatio);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpropertyInterpolate"]("width", file_r51.watermarkDetails[0].text.textAreaWidthcordinate / ctx_r50.videoAspectRatio);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].logo.logoWidthcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].logo.logoHeightcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].logo.videoDetails.startTime);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].logo.videoDetails.endTime);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r50.files.length == 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r50.files.length > 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].text.textAreaWidthcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].text.textAreaHeightcordinate);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].text.maxCharacter);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].text.videoDetails.startTime);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](file_r51.watermarkDetails[0].text.videoDetails.endTime);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngModel", ctx_r50.textAreaValue)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpureFunction0"](19, _c0));
} }
function DefineCobrandingComponent_div_54_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](2, DefineCobrandingComponent_div_54_div_2_Template, 163, 20, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngForOf", ctx_r4.fileDetailsList);
} }
const log = new _shared__WEBPACK_IMPORTED_MODULE_0__.Logger('Define-Cobranding');
class DefineCobrandingComponent {
    //@ViewChild('textCanvas', { static: true }) textCanvas!: ElementRef;
    constructor(router, route, formBuilder, collateralService, fileService, cobrandService, toastr) {
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.collateralService = collateralService;
        this.fileService = fileService;
        this.cobrandService = cobrandService;
        this.toastr = toastr;
        this.handleType = ngx_drag_resize__WEBPACK_IMPORTED_MODULE_10__.NgxResizeHandleType;
        this.isEditable = false;
        this.mergedCobrandImage = false;
        this.isLoading = false;
        this.textAreaValue = '';
        this.fileDetailsList = [];
        this.fileDetailsListLength = 0;
        this.collateralDetails = [];
        this.documentPageData = [];
        this.apiEndPoint = '';
        this.cobrandDetailList = [];
        this.imageAspectRatio = 0;
        this.pdfAspectRatio = 0;
        this.videoAspectRatio = 0;
        this.files = [];
        this.formData = new FormData();
        this.totalPages = 0;
        this.page = 1;
        this.isLoaded = false;
        this.imageLoaded = false;
        this.finalCanvasList = [];
        this.apiEndPoint = _environments_environment__WEBPACK_IMPORTED_MODULE_2__.environment.serverUrl;
        this.pageIndex = 0;
        this.pageSize = 1;
        this.route.queryParamMap.subscribe((params) => {
            this.queryparams = Object.assign(Object.assign({}, params.keys), params);
            var encodedString = this.queryparams.params._d;
            this.collateralId = atob(encodedString);
        });
        this.createForm();
    }
    createForm() {
        this.CoBrandForm = this.formBuilder.group({
            textAreaValue: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_11__.Validators.required],
        });
    }
    ngOnInit() {
        this.getCollateralById(this.collateralId);
    }
    ngDoCheck() {
        if (this.collateralFileType == 'Image') {
            this.DOMSetTextAreaStyle();
        }
        else if (this.collateralFileType == 'Document') {
            this.DOMSetLogoAreaStyle();
            this.DOMSetTextAreaStyle();
        }
        else if (this.collateralFileType == 'Video') {
            if (this.files.length > 0 && this.textAreaValue != '') {
                this.setVideoPlayLogoText();
            }
            if (this.files.length > 0 && this.textAreaValue == '') {
                this.setVideoPlayLogo();
            }
            if (this.textAreaValue != '' && this.files.length == 0) {
                this.setVideoPlayText();
            }
        }
    }
    setVideoPlayLogo() {
        this.videoLogo = document.getElementById('drag-resize-area');
        this.videoLogo.ontimeupdate = (event) => {
            var _a;
            let videoLogoStartTime = this.fileDetailsList[0].watermarkDetails[0].logo.videoDetails.startTime;
            let videoLogoEndTime = this.fileDetailsList[0].watermarkDetails[0].logo.videoDetails.endTime;
            if (this.videoLogo.currentTime >= videoLogoStartTime && this.videoLogo.currentTime <= videoLogoEndTime) {
                this.DOMSetLogoAreaStyle();
            }
            else {
                let divId = 'logo-area-' + this.pageIndex;
                var styleData = 'display: none;';
                (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
            }
        };
    }
    setVideoPlayText() {
        this.videoText = document.getElementById('drag-resize-area');
        let videoTextStartTime = this.fileDetailsList[0].watermarkDetails[0].text.videoDetails.startTime;
        let videoTextEndTime = this.fileDetailsList[0].watermarkDetails[0].text.videoDetails.endTime;
        this.videoText.ontimeupdate = (event) => {
            var _a;
            if (this.videoText.currentTime >= videoTextStartTime && this.videoText.currentTime <= videoTextEndTime) {
                this.DOMSetTextAreaStyle();
            }
            else {
                let divId = 'text-area-' + this.pageIndex;
                var styleData = 'display: none;';
                (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
            }
        };
    }
    setVideoPlayLogoText() {
        this.videoText = document.getElementById('drag-resize-area');
        let videoTextStartTime = this.fileDetailsList[0].watermarkDetails[0].text.videoDetails.startTime;
        let videoTextEndTime = this.fileDetailsList[0].watermarkDetails[0].text.videoDetails.endTime;
        let videoLogoStartTime = this.fileDetailsList[0].watermarkDetails[0].logo.videoDetails.startTime;
        let videoLogoEndTime = this.fileDetailsList[0].watermarkDetails[0].logo.videoDetails.endTime;
        this.videoText.ontimeupdate = (event) => {
            var _a, _b;
            if (this.files.length > 0) {
                if (this.videoLogo.currentTime >= videoLogoStartTime && this.videoLogo.currentTime <= videoLogoEndTime) {
                    this.DOMSetLogoAreaStyle();
                }
                else {
                    let divId = 'logo-area-' + this.pageIndex;
                    var styleData = 'display: none;';
                    (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
                }
            }
            if (this.videoText.currentTime >= videoTextStartTime && this.videoText.currentTime <= videoTextEndTime) {
                this.DOMSetTextAreaStyle();
            }
            else {
                let divId = 'text-area-' + this.pageIndex;
                var styleData = 'display: none;';
                (_b = document.getElementById(divId)) === null || _b === void 0 ? void 0 : _b.setAttribute('style', styleData);
            }
        };
    }
    getCollateralById(id) {
        this.isLoading = true;
        const data = {
            query: {
                id: id,
            },
        };
        this.collateralService.getAllCollateralList(data).subscribe((data) => {
            this.isLoading = false;
            this.collateralDetails = data.data[0];
            this.fileDetailsList = data.data[0].collateralfilemaps;
            this.fileDetailsListLength = data.data[0].collateralfilemaps.length;
            this.collateralName = data.data[0].collateralName;
            this.collateralCategory = data.data[0].categoryName;
            this.collateralSubCategory = data.data[0].subcategoryName;
            this.collateralDate = moment__WEBPACK_IMPORTED_MODULE_1__(data.data[0].updatedAt).format('MMM DD, YYYY | HH:mm');
            this.collateralFileType = data.data[0].collateralType;
            this.documentPageData = this.fileDetailsList[0].watermarkDetails;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    //Calculate Aspect ratio start
    getPdfCanvasHtWd() {
        var _a;
        const pageWidth = this.documentPageData[this.pageIndex].width;
        const pageHeight = this.documentPageData[this.pageIndex].height;
        const pageRotation = this.documentPageData[this.pageIndex].rotation;
        const pdfViewer = document.getElementsByTagName('pdf-viewer')[0].getElementsByClassName('canvasWrapper')[0];
        this.pdfCanvasHeight = pdfViewer.scrollHeight;
        this.pdfCanvasWidth = pdfViewer.scrollWidth;
        if (pageRotation == 0 || pageRotation == 180) {
            this.pdfAspectRatio = pageWidth / this.pdfCanvasWidth;
        }
        else {
            this.pdfAspectRatio = pageHeight / this.pdfCanvasWidth;
        }
        let divId = 'pdfCanvas';
        var styleData = 'width: ' + this.pdfCanvasWidth + 'px;height: ' + this.pdfCanvasHeight + 'px; margin-left: 0px;';
        (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
        document
            .getElementsByTagName('pdf-viewer')[0]
            .getElementsByClassName('page')[0]
            .setAttribute('style', 'margin-left: 0px;' + styleData);
    }
    pdfPageRendered(e) {
        console.log('(page-rendered)', e);
        this.getPdfCanvasHtWd();
    }
    calculatePDFRatio(page) {
        const pageWidth = this.documentPageData[page].width;
        const pageHeight = this.documentPageData[page].height;
        const pageRotation = this.documentPageData[page].rotation;
        const dragResizeDiv = document.getElementById('drag-resize-area');
        if (dragResizeDiv != undefined) {
            const divWidth = dragResizeDiv.getBoundingClientRect().width;
            this.dargResizedivWidth = dragResizeDiv.getBoundingClientRect().width;
            if (pageRotation == 0 || pageRotation == 180) {
                let divHeight = (pageHeight * this.dargResizedivWidth) / pageWidth;
                var styleData = 'height: ' + divHeight + 'px;';
                dragResizeDiv.setAttribute('style', styleData);
            }
            else {
                let divHeight = pageWidth / this.pdfAspectRatio;
                var styleData = 'height: ' + divHeight + 'px;';
                dragResizeDiv.setAttribute('style', styleData);
            }
        }
    }
    onImageLoad(pageIndex) {
        this.imageLoaded = true;
        var xW = document.getElementById('image-id-' + pageIndex);
        let naturalWidth = xW.naturalWidth;
        var yW = document.getElementById('image-id-' + pageIndex);
        let convertedWidth = yW.width;
        this.imageAspectRatio = naturalWidth / convertedWidth;
        this.textChanged('event', pageIndex);
        //this.DOMSetTextAreaStyle();
        if (this.fileName != undefined) {
            // if (this.textAreaValue != '') {
            //   this.textChanged('DOmSetTextareaStyle',this.pageIndex);
            // }
            this.DOMSetLogoAreaStyle();
            this.DOMSetTextAreaStyle();
        }
    }
    calculateVideoRatio() {
        var _a;
        let videoWidth = (_a = document.getElementById('drag-resize-area')) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect().width;
        let videoElement = document.getElementById('drag-resize-area');
        let videoFramWidth = videoElement.videoWidth;
        if (videoWidth != undefined) {
            this.videoAspectRatio = videoFramWidth / videoWidth;
        }
    }
    //Calculate Aspect ratio End
    DOMSetLogoAreaStyle() {
        var _a, _b, _c;
        if (this.collateralFileType == 'Image') {
            const item = this.fileDetailsList[this.pageIndex].watermarkDetails[0].logo;
            let divId = 'logo-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.logoXcordinate / this.imageAspectRatio +
                'px;width: ' +
                item.logoWidthcordinate / this.imageAspectRatio +
                'px;top: ' +
                item.logoYcordinate / this.imageAspectRatio +
                'px;height: ' +
                item.logoHeightcordinate / this.imageAspectRatio +
                'px;';
            (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
        }
        else if (this.collateralFileType == 'Document') {
            const item = this.documentPageData[this.pageIndex].logoDetails;
            let divId = 'logo-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.pdfAspectRatio +
                'px;width: ' +
                item.width / this.pdfAspectRatio +
                'px;top: ' +
                item.y / this.pdfAspectRatio +
                'px;height: ' +
                item.height / this.pdfAspectRatio +
                'px;';
            (_b = document.getElementById(divId)) === null || _b === void 0 ? void 0 : _b.setAttribute('style', styleData);
        }
        else if (this.collateralFileType == 'Video') {
            const item = this.fileDetailsList[this.pageIndex].watermarkDetails[0].logo;
            let divId = 'logo-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                (item.logoXcordinate / this.videoAspectRatio + 10) +
                'px;width: ' +
                item.logoWidthcordinate / this.videoAspectRatio +
                'px;top: ' +
                item.logoYcordinate / this.videoAspectRatio +
                'px;height: ' +
                item.logoHeightcordinate / this.videoAspectRatio +
                'px;';
            (_c = document.getElementById(divId)) === null || _c === void 0 ? void 0 : _c.setAttribute('style', styleData);
        }
    }
    DOMSetTextAreaStyle() {
        var _a, _b, _c;
        if (this.collateralFileType == 'Image') {
            const item = this.fileDetailsList[this.pageIndex].watermarkDetails[0].text;
            let divId = 'text-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.textAreaXcordinate / this.imageAspectRatio +
                'px;width: ' +
                item.textAreaWidthcordinate / this.imageAspectRatio +
                'px;top: ' +
                item.textAreaYcordinate / this.imageAspectRatio +
                'px;height: ' +
                item.textAreaHeightcordinate / this.imageAspectRatio +
                'px;';
            (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
            if (this.textAreaValue != '' && this.imageLoaded == true) {
                this.textChanged('event', this.pageIndex);
            }
        }
        else if (this.collateralFileType == 'Document') {
            const item = this.documentPageData[this.pageIndex].textAreaDetails;
            let divId = 'text-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.pdfAspectRatio +
                'px;width: ' +
                item.width / this.pdfAspectRatio +
                'px;top: ' +
                item.y / this.pdfAspectRatio +
                'px;height: ' +
                item.height / this.pdfAspectRatio +
                'px;';
            (_b = document.getElementById(divId)) === null || _b === void 0 ? void 0 : _b.setAttribute('style', styleData);
        }
        else if (this.collateralFileType == 'Video') {
            const item = this.fileDetailsList[this.pageIndex].watermarkDetails[0].text;
            let divId = 'text-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                (item.textAreaXcordinate / this.videoAspectRatio + 10) +
                'px;width: ' +
                item.textAreaWidthcordinate / this.videoAspectRatio +
                'px;top: ' +
                item.textAreaYcordinate / this.videoAspectRatio +
                'px;height: ' +
                item.textAreaHeightcordinate / this.videoAspectRatio +
                'px;';
            (_c = document.getElementById(divId)) === null || _c === void 0 ? void 0 : _c.setAttribute('style', styleData);
        }
    }
    //File Upload Start
    fileBrowseHandler(files) {
        this.uploadLogo(files.files);
    }
    uploadLogo(files) {
        this.formData = new FormData();
        for (const item of files) {
            var extn = item.name.substr(item.name.lastIndexOf('.'), item.name.length).toLowerCase();
            if (!_app_shared_constant__WEBPACK_IMPORTED_MODULE_3__.ValidImageFileExtensions.includes(extn)) {
                this.toastr.error('Please select valid jpg/png image file.');
                break;
            }
            this.files.push(item);
            this.formData.append('files', item);
            this.fileService.uploadCobrandFiles(this.formData).subscribe((data) => {
                this.isLoading = false;
                this.fileName = this.apiEndPoint + 'temp/' + data.data.split('\\').pop();
                if (this.collateralFileType == 'Image') {
                    this.onImageLoad(this.pageIndex);
                }
                if (this.collateralFileType == 'Video') {
                    this.calculateVideoRatio();
                }
            }, (error) => {
                this.toastr.error('Upload File Unsuccessful, Please try again');
                console.log(error);
            });
        }
    }
    deleteFile() {
        this.files.splice(0);
    }
    //File Upload End
    //Text Canvas Start
    textChanged(event, index) {
        if (this.collateralFileType == 'Image') {
            let fontStyle = this.fileDetailsList[index].watermarkDetails[0].text.textAreaFontText;
            let fontSize = this.fileDetailsList[index].watermarkDetails[0].text.textAreaFontSize + 'px';
            let isBold = this.fileDetailsList[index].watermarkDetails[0].text.textAreaBold == true ? 'bold' : '';
            let isItalic = this.fileDetailsList[index].watermarkDetails[0].text.textAreaItalic == true ? 'italic' : '';
            var canvas = document.getElementById('textCanvas-' + index);
            var context = canvas.getContext('2d');
            canvas.width =
                this.fileDetailsList[index].watermarkDetails[0].text.textAreaWidthcordinate / this.imageAspectRatio;
            canvas.height =
                this.fileDetailsList[index].watermarkDetails[0].text.textAreaHeightcordinate / this.imageAspectRatio;
            context.font = isItalic + ' ' + isBold + ' ' + fontSize + ' ' + fontStyle;
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillText(this.textAreaValue, 5, parseInt(fontSize));
            this.imageURL = canvas.toDataURL('image');
            this.imageLoaded = false;
        }
        if (this.collateralFileType == 'Document') {
            let fontStyle = this.documentPageData[index].textAreaDetails.font;
            let fontSize = this.documentPageData[index].textAreaDetails.fontSize + 'px';
            let isBold = this.documentPageData[index].textAreaDetails.bold == true ? 'bold' : '';
            let isItalic = this.documentPageData[index].textAreaDetails.italic == true ? 'italic' : '';
            var canvas = document.getElementById('textCanvas-' + index);
            var context = canvas.getContext('2d');
            canvas.width = this.documentPageData[index].textAreaDetails.width / this.pdfAspectRatio;
            canvas.height = this.documentPageData[index].textAreaDetails.height / this.pdfAspectRatio;
            context.font = isItalic + ' ' + isBold + ' ' + fontSize + ' ' + fontStyle;
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillText(this.textAreaValue, 5, parseInt(fontSize));
            this.imageURL = canvas.toDataURL('image');
        }
        if (this.collateralFileType == 'Video') {
            this.calculateVideoRatio();
            let fontStyle = this.fileDetailsList[index].watermarkDetails[0].text.textAreaFontText;
            let fontSize = this.fileDetailsList[index].watermarkDetails[0].text.textAreaFontSize + 'px';
            let isBold = this.fileDetailsList[index].watermarkDetails[0].text.textAreaBold == true ? 'bold' : '';
            let isItalic = this.fileDetailsList[index].watermarkDetails[0].text.textAreaItalic == true ? 'italic' : '';
            var canvas = document.getElementById('textCanvas-' + index);
            var context = canvas.getContext('2d');
            context.font = isItalic + ' ' + isBold + ' ' + fontSize + ' ' + fontStyle;
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillText(this.textAreaValue, 5, parseInt(fontSize));
            this.imageURL = canvas.toDataURL('image');
        }
    }
    createMultipleCanvas() {
        if (this.collateralFileType == 'Image') {
            this.fileDetailsList.forEach((element) => {
                let fontStyle = element.watermarkDetails[0].text.textAreaFontText;
                let fontSize = element.watermarkDetails[0].text.textAreaFontSize + 'px';
                let isBold = element.watermarkDetails[0].text.textAreaBold == true ? 'bold' : '';
                let isItalic = element.watermarkDetails[0].text.textAreaItalic == true ? 'italic' : '';
                var canvas = document.getElementById('createCanvas');
                var context = canvas.getContext('2d');
                canvas.width = element.watermarkDetails[0].text.textAreaWidthcordinate / this.imageAspectRatio;
                canvas.height = element.watermarkDetails[0].text.textAreaHeightcordinate / this.imageAspectRatio;
                context.font = isItalic + ' ' + isBold + ' ' + fontSize + ' ' + fontStyle;
                context.clearRect(0, 0, canvas.width, canvas.height);
                // context!.clearRect(
                //   0,
                //   0,
                //   element.watermarkDetails[0].text.textAreaWidthcordinate / this.imageAspectRatio,
                //   element.watermarkDetails[0].text.textAreaHeightcordinate / this.imageAspectRatio
                // );
                context.fillText(this.textAreaValue, 5, parseInt(fontSize));
                this.imageURL = canvas.toDataURL('image');
                this.finalCanvasList.push(this.imageURL);
            });
            console.log(this.finalCanvasList);
        }
        if (this.collateralFileType == 'Document') {
            this.documentPageData.forEach((element) => {
                let fontStyle = element.textAreaDetails.font;
                let fontSize = element.textAreaDetails.fontSize + 'px';
                let isBold = element.textAreaDetails.bold == true ? 'bold' : '';
                let isItalic = element.textAreaDetails.italic == true ? 'italic' : '';
                var canvas = document.getElementById('createCanvas');
                var context = canvas.getContext('2d');
                canvas.width = element.textAreaDetails.width / this.pdfAspectRatio;
                canvas.height = element.textAreaDetails.height / this.pdfAspectRatio;
                context.font = isItalic + ' ' + isBold + ' ' + fontSize + ' ' + fontStyle;
                context.clearRect(0, 0, canvas.width, canvas.height);
                context.fillText(this.textAreaValue, 5, parseInt(fontSize));
                this.imageURL = canvas.toDataURL('image');
                this.finalCanvasList.push(this.imageURL);
            });
            console.log(this.finalCanvasList);
        }
    }
    //Text Canvas End
    submitCobrand() {
        if (this.files.length == 0) {
            this.toastr.error('Please upload logo file and then try again');
        }
        else {
            this.isLoading = true;
            this.createMultipleCanvas();
            const data = [];
            if (this.collateralFileType == 'Image') {
                this.fileDetailsList.forEach((element) => {
                    data.push({
                        collateralDetails: this.collateralDetails,
                        cobrandDetails: {
                            mainImage: this.apiEndPoint + element.file.path,
                            logoImage: this.fileName,
                            textImage: this.finalCanvasList[this.fileDetailsList.indexOf(element)],
                            Ximg: element.watermarkDetails[0].logo.logoXcordinate,
                            Yimg: element.watermarkDetails[0].logo.logoYcordinate,
                            logoWidth: element.watermarkDetails[0].logo.logoWidthcordinate,
                            logoHeight: element.watermarkDetails[0].logo.logoHeightcordinate,
                            Xtext: element.watermarkDetails[0].text.textAreaXcordinate,
                            Ytext: element.watermarkDetails[0].text.textAreaYcordinate,
                            textWidth: element.watermarkDetails[0].text.textAreaWidthcordinate,
                            textHeight: element.watermarkDetails[0].text.textAreaHeightcordinate,
                        },
                    });
                });
            }
            if (this.collateralFileType == 'Document') {
                this.fileDetailsList.forEach((element) => {
                    data.push({
                        collateralDetails: this.collateralDetails,
                        cobrandDetails: {
                            mainImage: this.apiEndPoint + element.file.path,
                            logoImage: this.fileName,
                            textImage: this.finalCanvasList,
                            watermarkDetails: element.watermarkDetails,
                        },
                    });
                });
            }
            if (this.collateralFileType == 'Video') {
                this.fileDetailsList.forEach((element) => {
                    data.push({
                        collateralDetails: this.collateralDetails,
                        cobrandDetails: {
                            mainImage: this.apiEndPoint + element.file.path,
                            logoImage: this.fileName,
                            textImage: this.imageURL,
                            watermarkDetails: element.watermarkDetails,
                        },
                    });
                });
            }
            this.cobrandService.createCobrandFiles(data).subscribe((data) => {
                this.isLoading = false;
                this.router.navigate(['/co-branding'], { replaceUrl: false });
                // this.cobrandDetailList = data.data.data;
                // this.mergedCobrandImage = true;
            }, (error) => {
                this.isLoading = false;
                console.log(error);
            });
        }
    }
    DownloadFile(fileName) {
        let filename = fileName.split('/').pop();
        window.open(this.fileService.downloadFileByNameCobrand + filename, '_blank');
    }
    DownloadLater() {
        this.router.navigate(['/co-branding'], { replaceUrl: false });
    }
    //PDF Viewer start
    nextPage() {
        this.page += 1;
        this.pageIndex += 1;
    }
    previousPage() {
        this.page -= 1;
        this.pageIndex -= 1;
    }
    afterLoadComplete(pdfData) {
        this.totalPages = pdfData.numPages;
        this.calculatePDFRatio(this.page - 1);
        this.isLoaded = true;
        this.textChanged('event', this.pageIndex);
    }
}
DefineCobrandingComponent.ɵfac = function DefineCobrandingComponent_Factory(t) { return new (t || DefineCobrandingComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_12__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_12__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_4__.CollateralService), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_5__.FilesService), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_app_rest_cobrand_api_service__WEBPACK_IMPORTED_MODULE_6__.CobrandService), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_13__.ToastrService)); };
DefineCobrandingComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdefineComponent"]({ type: DefineCobrandingComponent, selectors: [["app-define-cobranding"]], decls: 61, vars: 11, consts: [[1, "container-fluid"], [1, "jumbotron", "text-center"], [3, "isLoading"], [3, "formGroup"], [1, "row"], [1, "col-md-6"], [4, "ngIf"], [1, "col-md-12"], [1, "cobrand-name"], [1, "cobrand-catergory"], [1, "cobrand-date", "d-flex", "align-items-center", "justify-content-end"], [1, "btn", "paginator-button", 3, "disabled", "click"], [1, "fas", "fa-angle-left", "paginator-icon"], ["disabled", "", 1, "btn", 2, "width", "100px"], [1, "fas", "fa-angle-right", "paginator-icon"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange"], ["class", "row", 4, "ngFor", "ngForOf"], [1, "area"], [1, "single-file"], ["width", "100%", 3, "id", "src", "load", 4, "ngIf"], [3, "id", "src", 4, "ngIf"], [3, "id"], ["id", "createCanvas", 2, "display", "none"], [1, "heading"], [1, "sub-heading"], [1, "sub-value"], ["class", "col-md-12", 4, "ngIf"], ["id", "text", "type", "text", "placeholder", "Enter Text Here", "mdbInput", "", "maxlength", "file.watermarkDetails[0].text.maxCharacter", 1, "md-textarea", "form-control", 3, "ngModel", "ngModelOptions", "ngModelChange"], ["type", "button", 1, "btn", "btn-primary", 3, "click"], ["width", "100%", 3, "id", "src", "load"], [3, "id", "src"], [1, "btn", "btn-outline-primary", "btn-dotted"], ["for", "fileDropRef"], ["type", "file", "id", "fileDropRef", "name", "fileData", "hidden", "", 3, "change"], ["fileDropRef", ""], [1, "fas", "fa-plus"], [1, "far", "fa-trash-alt", "icon-color", 3, "click"], [4, "ngFor", "ngForOf"], ["id", "pdf-outer-div-id", 1, "pdf-outer-div"], ["id", "drag-resize-area", 1, "pdf-position"], ["id", "pdfCanvas", 3, "src", "show-all", "page", "original-size", "render-text", "zoom-scale", "page-rendered", "after-load-complete"], ["id", "text", "type", "text", "placeholder", "Enter Text Here", "mdbInput", "", 1, "md-textarea", "form-control", 3, "ngModel", "ngModelOptions", "ngModelChange"], ["for", "fileDropRefDoc"], ["type", "file", "id", "fileDropRefDoc", "name", "fileData", "hidden", "", 3, "change"], ["fileDropRefDoc", ""], [2, "margin", "10px 10px 10px 10px"], ["id", "drag-resize-area", "width", "100%", "controls", ""], ["type", "video/mp4", 3, "src"], ["style", "display: none", 3, "id", "src", 4, "ngIf"], [2, "display", "none", 3, "id"], [3, "id", "height", "width"], [1, "col-md-4"], [1, "col-md-3"], [2, "display", "none", 3, "id", "src"]], template: function DefineCobrandingComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](4, "app-loader", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](8, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](10, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](12, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](14, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](15, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](16, DefineCobrandingComponent_div_16_Template, 15, 4, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](17, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](18, DefineCobrandingComponent_div_18_Template, 4, 3, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](19, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](20, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](21, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](22, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](23, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](24, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](25, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](26, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](27, "label", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](28);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](29, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](30, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](31, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](32, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](33, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](34, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](35, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](36, "label", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](37);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](38, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](39, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](40, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](41, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](42, "label", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](43);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](44, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](45, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](46, "\n\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](47, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](48, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](49, "\n\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](50, DefineCobrandingComponent_div_50_Template, 5, 5, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](51, "\n\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](52, DefineCobrandingComponent_div_52_Template, 4, 1, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](53, "\n\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](54, DefineCobrandingComponent_div_54_Template, 4, 1, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](55, "\n\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](56, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](57, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](58, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](59, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](60, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("isLoading", ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("formGroup", ctx.CoBrandForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Image" && ctx.fileDetailsListLength >= 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate"](ctx.collateralName);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate2"](" ", ctx.collateralCategory, " | ", ctx.collateralSubCategory, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"](" ", ctx.collateralDate, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Image");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Video");
    } }, directives: [_shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_7__.LoaderComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormGroupDirective, _angular_common__WEBPACK_IMPORTED_MODULE_14__.NgIf, _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_8__.PagerComponent, _angular_common__WEBPACK_IMPORTED_MODULE_14__.NgForOf, _angular_forms__WEBPACK_IMPORTED_MODULE_11__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_11__.MaxLengthValidator, _angular_forms__WEBPACK_IMPORTED_MODULE_11__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_11__.NgModel, ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_15__.PdfViewerComponent], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_14__.SlicePipe], styles: [".logo[_ngcontent-%COMP%] {\n  width: 100px;\n}\n\n.pdf-position[_ngcontent-%COMP%] {\n  position: relative;\n  width: 100%;\n  height: 100%;\n}\n\n.pdf-outer-div[_ngcontent-%COMP%] {\n  height: 720px;\n  width: 100%;\n}\n\n.disable-div[_ngcontent-%COMP%] {\n  pointer-events: none;\n}\n\n.enable-div[_ngcontent-%COMP%] {\n  pointer-events: all;\n}\n\n.app-title[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\n\n.area[_ngcontent-%COMP%] {\n  position: relative;\n  height: 200px;\n  width: 100%;\n  border-radius: 3px;\n  background: rgba(211, 211, 211, 0.25);\n}\n\n.area.large[_ngcontent-%COMP%] {\n  height: 500px;\n}\n\n.rect[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 100px;\n  border: dashed;\n  box-shadow: inset 0 0 1px darkgrey, 0 0 2px rgba(0, 0, 0, 0.8);\n  background: #f9f9f9;\n  opacity: 0.5;\n}\n\n.rect.medium[_ngcontent-%COMP%] {\n  height: 200px;\n  width: 200px;\n}\n\n.rect.large[_ngcontent-%COMP%] {\n  height: 250px;\n  width: 250px;\n}\n\n.move[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n\n.handle[_ngcontent-%COMP%] {\n  padding: 8px;\n  display: inline-flex;\n}\n\n.content-inside[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n[data-resize-handle][_ngcontent-%COMP%] {\n  position: absolute;\n  -webkit-user-select: none;\n          user-select: none;\n  z-index: 3;\n}\n\n[data-resize-handle=Top][_ngcontent-%COMP%], [data-resize-handle=Right][_ngcontent-%COMP%] {\n  top: 0;\n  right: 0;\n}\n\n[data-resize-handle=Bottom][_ngcontent-%COMP%], [data-resize-handle=Left][_ngcontent-%COMP%] {\n  bottom: 0;\n  left: 0;\n}\n\n[data-resize-handle=Top][_ngcontent-%COMP%], [data-resize-handle=Bottom][_ngcontent-%COMP%] {\n  width: 100%;\n  height: 10px;\n  cursor: ns-resize;\n}\n\n[data-resize-handle=Right][_ngcontent-%COMP%], [data-resize-handle=Left][_ngcontent-%COMP%] {\n  height: 100%;\n  width: 10px;\n  cursor: ew-resize;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%], [data-resize-handle=TopRight][_ngcontent-%COMP%], [data-resize-handle=BottomLeft][_ngcontent-%COMP%], [data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  width: 10px;\n  height: 10px;\n  z-index: 4;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%], [data-resize-handle=TopRight][_ngcontent-%COMP%] {\n  top: 0;\n}\n\n[data-resize-handle=BottomLeft][_ngcontent-%COMP%], [data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  bottom: 0;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%], [data-resize-handle=BottomLeft][_ngcontent-%COMP%] {\n  left: 0;\n}\n\n[data-resize-handle=TopRight][_ngcontent-%COMP%], [data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  right: 0;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%] {\n  cursor: nw-resize;\n}\n\n[data-resize-handle=TopRight][_ngcontent-%COMP%] {\n  cursor: ne-resize;\n}\n\n[data-resize-handle=BottomLeft][_ngcontent-%COMP%] {\n  cursor: sw-resize;\n}\n\n[data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  cursor: se-resize;\n}\n\n.offset-small[_ngcontent-%COMP%], .offset-large[_ngcontent-%COMP%] {\n  position: absolute;\n}\n\n.offset-small[_ngcontent-%COMP%] {\n  top: 16px;\n  left: 16px;\n}\n\n.offset-large[_ngcontent-%COMP%] {\n  left: 100px;\n  top: 50px;\n}\n\n.offset-center-medium[_ngcontent-%COMP%] {\n  left: calc(50% - 100px);\n  top: calc(50% - 100px);\n}\n\n.cobrand-name[_ngcontent-%COMP%] {\n  height: 36px;\n  left: 1015px;\n  top: 102px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 28px;\n  line-height: 36px;\n  letter-spacing: 0.02em;\n  color: #484848;\n}\n\n.cobrand-catergory[_ngcontent-%COMP%] {\n  width: 152px;\n  height: 18px;\n  left: 1015px;\n  top: 150px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 14px;\n  line-height: 18px;\n  letter-spacing: 0.02em;\n  color: #adadad;\n}\n\n.cobrand-date[_ngcontent-%COMP%] {\n  height: 15px;\n  top: 152px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 12px;\n  line-height: 15px;\n  color: #353030;\n  padding-right: 40px;\n  padding-top: 10px;\n}\n\n.heading[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 18px;\n  line-height: 23px;\n  color: #000000;\n}\n\n.sub-heading[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 18px;\n  color: #bbbbbb;\n}\n\n.sub-value[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 18px;\n  color: #383333;\n}\n\n.paginator-icon[_ngcontent-%COMP%] {\n  color: #484848;\n}\n\n.paginator-button[_ngcontent-%COMP%] {\n  color: #fff;\n  background-color: white;\n  border-color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRlZmluZS1jb2JyYW5kaW5nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQUNGOztBQWFBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQVZGOztBQWFBO0VBQ0UsYUFBQTtFQUNBLFdBQUE7QUFWRjs7QUFhQTtFQUNFLG9CQUFBO0FBVkY7O0FBYUE7RUFDRSxtQkFBQTtBQVZGOztBQWdCRTtFQUNFLGNBQUE7QUFiSjs7QUFpQkE7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQ0FBQTtBQWRGOztBQWdCRTtFQUNFLGFBQUE7QUFkSjs7QUFrQkE7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUVBLGNBQUE7RUFDQSw4REFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQWhCRjs7QUFrQkU7RUFDRSxhQTlCVTtFQStCVixZQS9CVTtBQWVkOztBQW1CRTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBakJKOztBQXFCQTtFQUNFLGVBQUE7QUFsQkY7O0FBcUJBO0VBQ0UsWUFBQTtFQUNBLG9CQUFBO0FBbEJGOztBQXFCQTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBbEJGOztBQXVCQTtFQUNFLGtCQUFBO0VBQ0EseUJBQUE7VUFBQSxpQkFBQTtFQUNBLFVBQUE7QUFwQkY7O0FBdUJBOztFQUVFLE1BQUE7RUFDQSxRQUFBO0FBcEJGOztBQXVCQTs7RUFFRSxTQUFBO0VBQ0EsT0FBQTtBQXBCRjs7QUF1QkE7O0VBRUUsV0FBQTtFQUNBLFlBdkJpQjtFQXdCakIsaUJBQUE7QUFwQkY7O0FBdUJBOztFQUVFLFlBQUE7RUFDQSxXQTlCaUI7RUErQmpCLGlCQUFBO0FBcEJGOztBQXVCQTs7OztFQUlFLFdBdENpQjtFQXVDakIsWUF2Q2lCO0VBd0NqQixVQUFBO0FBcEJGOztBQXVCQTs7RUFFRSxNQUFBO0FBcEJGOztBQXVCQTs7RUFFRSxTQUFBO0FBcEJGOztBQXVCQTs7RUFFRSxPQUFBO0FBcEJGOztBQXVCQTs7RUFFRSxRQUFBO0FBcEJGOztBQXVCQTtFQUNFLGlCQUFBO0FBcEJGOztBQXVCQTtFQUNFLGlCQUFBO0FBcEJGOztBQXVCQTtFQUNFLGlCQUFBO0FBcEJGOztBQXVCQTtFQUNFLGlCQUFBO0FBcEJGOztBQXVCQTs7RUFFRSxrQkFBQTtBQXBCRjs7QUF1QkE7RUFDRSxTQUFBO0VBQ0EsVUFBQTtBQXBCRjs7QUF1QkE7RUFDRSxXQUFBO0VBQ0EsU0FBQTtBQXBCRjs7QUF1QkE7RUFDRSx1QkFBQTtFQUNBLHNCQUFBO0FBcEJGOztBQXVCQTtFQUVFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtBQXJCRjs7QUF3QkE7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FBckJGOztBQXdCQTtFQUVFLFlBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUF0QkY7O0FBeUJBO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQXRCRjs7QUF5QkE7RUFDRSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBdEJGOztBQXlCQTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUF0QkY7O0FBeUJBO0VBQ0UsY0FBQTtBQXRCRjs7QUF5QkE7RUFDRSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQXRCRiIsImZpbGUiOiJkZWZpbmUtY29icmFuZGluZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dvIHtcclxuICB3aWR0aDogMTAwcHg7XHJcbn1cclxuXHJcbi8vSW1hZ2UgY2FudmFzIHN0YXJ0XHJcblxyXG4vLyBjYW52YXN7XHJcbi8vICAgYm9yZGVyOiAxcHggYmxhY2sgc29saWQ7XHJcbi8vIH1cclxuLy8gI3RleHRDYW52YXN7XHJcblxyXG4vLyB9XHJcblxyXG4vL0ltYWdlIGNhbnZhcyBlbmRzXHJcblxyXG4ucGRmLXBvc2l0aW9uIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4ucGRmLW91dGVyLWRpdiB7XHJcbiAgaGVpZ2h0OiA3MjBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmRpc2FibGUtZGl2IHtcclxuICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxufVxyXG5cclxuLmVuYWJsZS1kaXYge1xyXG4gIHBvaW50ZXItZXZlbnRzOiBhbGw7XHJcbn1cclxuXHJcbiRtZWRpdW0tcmVjdDogMjAwcHg7XHJcblxyXG4uYXBwLXRpdGxlIHtcclxuICBhIHtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gIH1cclxufVxyXG5cclxuLmFyZWEge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDIxMSwgMjExLCAyMTEsIDAuMjUpO1xyXG5cclxuICAmLmxhcmdlIHtcclxuICAgIGhlaWdodDogNTAwcHg7XHJcbiAgfVxyXG59XHJcblxyXG4ucmVjdCB7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgLy8gYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gIGJvcmRlcjogZGFzaGVkO1xyXG4gIGJveC1zaGFkb3c6IGluc2V0IDAgMCAxcHggZGFya2dyZXksIDAgMCAycHggcmdiYSgwLCAwLCAwLCAwLjgpO1xyXG4gIGJhY2tncm91bmQ6ICNmOWY5Zjk7XHJcbiAgb3BhY2l0eTogMC41O1xyXG5cclxuICAmLm1lZGl1bSB7XHJcbiAgICBoZWlnaHQ6ICRtZWRpdW0tcmVjdDtcclxuICAgIHdpZHRoOiAkbWVkaXVtLXJlY3Q7XHJcbiAgfVxyXG5cclxuICAmLmxhcmdlIHtcclxuICAgIGhlaWdodDogMjUwcHg7XHJcbiAgICB3aWR0aDogMjUwcHg7XHJcbiAgfVxyXG59XHJcblxyXG4ubW92ZSB7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uaGFuZGxlIHtcclxuICBwYWRkaW5nOiA4cHg7XHJcbiAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbn1cclxuXHJcbi5jb250ZW50LWluc2lkZSB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4kaGFuZGxlLXRoaWNrbmVzczogMTBweDtcclxuXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGVdIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgei1pbmRleDogMztcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcFwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlJpZ2h0XCJdIHtcclxuICB0b3A6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJCb3R0b21cIl0sXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJMZWZ0XCJdIHtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcFwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkJvdHRvbVwiXSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAkaGFuZGxlLXRoaWNrbmVzcztcclxuICBjdXJzb3I6IG5zLXJlc2l6ZTtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlJpZ2h0XCJdLFxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiTGVmdFwiXSB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHdpZHRoOiAkaGFuZGxlLXRoaWNrbmVzcztcclxuICBjdXJzb3I6IGV3LXJlc2l6ZTtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcExlZnRcIl0sXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJUb3BSaWdodFwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkJvdHRvbUxlZnRcIl0sXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJCb3R0b21SaWdodFwiXSB7XHJcbiAgd2lkdGg6ICRoYW5kbGUtdGhpY2tuZXNzO1xyXG4gIGhlaWdodDogJGhhbmRsZS10aGlja25lc3M7XHJcbiAgei1pbmRleDogNDtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcExlZnRcIl0sXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJUb3BSaWdodFwiXSB7XHJcbiAgdG9wOiAwO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiQm90dG9tTGVmdFwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkJvdHRvbVJpZ2h0XCJdIHtcclxuICBib3R0b206IDA7XHJcbn1cclxuXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJUb3BMZWZ0XCJdLFxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiQm90dG9tTGVmdFwiXSB7XHJcbiAgbGVmdDogMDtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcFJpZ2h0XCJdLFxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiQm90dG9tUmlnaHRcIl0ge1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiVG9wTGVmdFwiXSB7XHJcbiAgY3Vyc29yOiBudy1yZXNpemU7XHJcbn1cclxuXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJUb3BSaWdodFwiXSB7XHJcbiAgY3Vyc29yOiBuZS1yZXNpemU7XHJcbn1cclxuXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJCb3R0b21MZWZ0XCJdIHtcclxuICBjdXJzb3I6IHN3LXJlc2l6ZTtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkJvdHRvbVJpZ2h0XCJdIHtcclxuICBjdXJzb3I6IHNlLXJlc2l6ZTtcclxufVxyXG5cclxuLm9mZnNldC1zbWFsbCxcclxuLm9mZnNldC1sYXJnZSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcblxyXG4ub2Zmc2V0LXNtYWxsIHtcclxuICB0b3A6IDE2cHg7XHJcbiAgbGVmdDogMTZweDtcclxufVxyXG5cclxuLm9mZnNldC1sYXJnZSB7XHJcbiAgbGVmdDogMTAwcHg7XHJcbiAgdG9wOiA1MHB4O1xyXG59XHJcblxyXG4ub2Zmc2V0LWNlbnRlci1tZWRpdW0ge1xyXG4gIGxlZnQ6IGNhbGMoNTAlIC0gI3skbWVkaXVtLXJlY3QgLyAyfSk7XHJcbiAgdG9wOiBjYWxjKDUwJSAtICN7JG1lZGl1bS1yZWN0IC8gMn0pO1xyXG59XHJcblxyXG4uY29icmFuZC1uYW1lIHtcclxuICAvL3dpZHRoOiAyMDRweDtcclxuICBoZWlnaHQ6IDM2cHg7XHJcbiAgbGVmdDogMTAxNXB4O1xyXG4gIHRvcDogMTAycHg7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDI4cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDM2cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDJlbTtcclxuICBjb2xvcjogIzQ4NDg0ODtcclxufVxyXG5cclxuLmNvYnJhbmQtY2F0ZXJnb3J5IHtcclxuICB3aWR0aDogMTUycHg7XHJcbiAgaGVpZ2h0OiAxOHB4O1xyXG4gIGxlZnQ6IDEwMTVweDtcclxuICB0b3A6IDE1MHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBSYWpkaGFuaTtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDJlbTtcclxuICBjb2xvcjogI2FkYWRhZDtcclxufVxyXG5cclxuLmNvYnJhbmQtZGF0ZSB7XHJcbiAgLy93aWR0aDogMTAxcHg7XHJcbiAgaGVpZ2h0OiAxNXB4O1xyXG4gIHRvcDogMTUycHg7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBsaW5lLWhlaWdodDogMTVweDtcclxuICBjb2xvcjogIzM1MzAzMDtcclxuICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uaGVhZGluZyB7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBsaW5lLWhlaWdodDogMjNweDtcclxuICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG5cclxuLnN1Yi1oZWFkaW5nIHtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIGNvbG9yOiAjYmJiYmJiO1xyXG59XHJcblxyXG4uc3ViLXZhbHVlIHtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIGNvbG9yOiAjMzgzMzMzO1xyXG59XHJcblxyXG4ucGFnaW5hdG9yLWljb24ge1xyXG4gIGNvbG9yOiAjNDg0ODQ4O1xyXG59XHJcblxyXG4ucGFnaW5hdG9yLWJ1dHRvbiB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxufVxyXG4iXX0= */"] });


/***/ }),

/***/ 3058:
/*!*********************************************************!*\
  !*** ./src/app/collateral/collateral-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CollateralRoutingModule": () => (/* binding */ CollateralRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _collateral_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./collateral.component */ 8099);
/* harmony import */ var _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @app/shell/shell.service */ 6042);
/* harmony import */ var _upload_collateral_upload_collateral_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./upload-collateral/upload-collateral.component */ 2662);
/* harmony import */ var _define_collateral_define_collateral_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./define-collateral/define-collateral.component */ 9750);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);







const routes = [
    _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_1__.Shell.childRoutes([
        { path: '', redirectTo: '/collateral', pathMatch: 'full' },
        { path: 'collateral', component: _collateral_component__WEBPACK_IMPORTED_MODULE_0__.CollateralComponent, data: { title: 'Collateral' } },
        { path: 'collateral/upload', component: _upload_collateral_upload_collateral_component__WEBPACK_IMPORTED_MODULE_2__.UploadCollateralComponent, data: { title: 'Upload-Collateral' } },
        { path: 'collateral/define', component: _define_collateral_define_collateral_component__WEBPACK_IMPORTED_MODULE_3__.DefineCollateralComponent, data: { title: 'Define-Collateral' } },
    ]),
];
class CollateralRoutingModule {
}
CollateralRoutingModule.ɵfac = function CollateralRoutingModule_Factory(t) { return new (t || CollateralRoutingModule)(); };
CollateralRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({ type: CollateralRoutingModule });
CollateralRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({ providers: [], imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](CollateralRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule] }); })();


/***/ }),

/***/ 8099:
/*!****************************************************!*\
  !*** ./src/app/collateral/collateral.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CollateralComponent": () => (/* binding */ CollateralComponent)
/* harmony export */ });
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ 2664);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../environments/environment */ 2340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/rest/collateral-api.service */ 2690);
/* harmony import */ var _app_rest_product_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/rest/product-api.service */ 3173);
/* harmony import */ var _app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/rest/files-api.service */ 8226);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ 9699);
/* harmony import */ var _shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../@shared/loader/loader.component */ 9967);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-select/ng-select */ 6640);
/* harmony import */ var _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../@shared/pagination/pager.component */ 4454);
















const _c0 = ["downloadFileModal"];
const _c1 = ["filterModal"];
const _c2 = ["infoFileModal"];
function CollateralComponent_button_12_Template(rf, ctx) { if (rf & 1) {
    const _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "button", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_button_12_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r15); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵreference"](192); return ctx_r14.openNewCollateralPopUp(_r8); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      New ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](2, "i", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CollateralComponent_span_33_Template(rf, ctx) { if (rf & 1) {
    const _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "i", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_span_33_Template_i_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r17); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r16.resetFilter(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
const _c3 = function (a0, a1) { return { productId: a0, productName: a1 }; };
function CollateralComponent_ng_option_110_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const product_r18 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](2, _c3, product_r18.id, product_r18.name));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](product_r18.name);
} }
const _c4 = function (a0, a1) { return { subproductId: a0, subproductName: a1 }; };
function CollateralComponent_ng_option_136_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const subproduct_r19 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](2, _c4, subproduct_r19.id, subproduct_r19.name));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](subproduct_r19.name);
} }
function CollateralComponent_div_171_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "h3", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "No Collateral Created");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CollateralComponent_pager_177_Template(rf, ctx) { if (rf & 1) {
    const _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "pager", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("pageIndexChange", function CollateralComponent_pager_177_Template_pager_pageIndexChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r21); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r20.pageIndex = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("itemCount", ctx_r5.collateralListLength)("pageIndex", ctx_r5.pageIndex)("pageSize", ctx_r5.pageSize);
} }
function CollateralComponent_div_180_Template(rf, ctx) { if (rf & 1) {
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "span", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "i", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "ul", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "button", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_div_180_Template_button_click_14_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r23); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r22.dateFilter("Last Modified"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n                Last Modified\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](18, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](20, "button", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_div_180_Template_button_click_20_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r23); const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r24.sortFilter("ASC"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "A - Z");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](26, "button", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_div_180_Template_button_click_26_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r23); const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r25.sortFilter("DESC"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "Z - A");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx_r6.sortType);
} }
function CollateralComponent_div_185_li_19_Template(rf, ctx) { if (rf & 1) {
    const _r33 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_div_185_li_19_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r33); const collateral_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit; const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r31.createNewCoBrand(collateral_r26.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                  Create Co-Brand Asset\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CollateralComponent_div_185_li_21_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_div_185_li_21_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r36); const collateral_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit; const ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r34.infoCollateralClick(collateral_r26.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                  Info\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CollateralComponent_div_185_li_29_Template(rf, ctx) { if (rf & 1) {
    const _r39 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_div_185_li_29_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r39); const collateral_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit; const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r37.deleteCollateralById(collateral_r26.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                  Delete\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function CollateralComponent_div_185_Template(rf, ctx) { if (rf & 1) {
    const _r41 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "img", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "div", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](9, "label", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "i", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "ul", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](19, CollateralComponent_div_185_li_19_Template, 5, 0, "li", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](21, CollateralComponent_div_185_li_21_Template, 5, 0, "li", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](23, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](25, "button", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_div_185_Template_button_click_25_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r41); const collateral_r26 = restoredCtx.$implicit; const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r40.getFilesByCollateralId(collateral_r26.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                  Download\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](29, CollateralComponent_div_185_li_29_Template, 5, 0, "li", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](33, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](34, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](35, "label", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](38, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](40, "label", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](42, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const collateral_r26 = ctx.$implicit;
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", ctx_r7.apiEndPoint + collateral_r26.collateralfilemaps[0].file.thumnailPath, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](collateral_r26.collateralName);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx_r7.userRole == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx_r7.userRole == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx_r7.userRole == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate2"]("", collateral_r26.categoryName, " | ", collateral_r26.subcategoryName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](42, 8, collateral_r26.updatedAt, "MMM dd, YYYY | HH:mm"));
} }
function CollateralComponent_ng_template_191_ng_option_61_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const product_r45 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](2, _c3, product_r45.id, product_r45.name));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](product_r45.name);
} }
function CollateralComponent_ng_template_191_ng_option_87_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const subproduct_r46 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](2, _c4, subproduct_r46.id, subproduct_r46.name));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](subproduct_r46.name);
} }
function CollateralComponent_ng_template_191_Template(rf, ctx) { if (rf & 1) {
    const _r48 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](0, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "div", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "h4", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "Create Marketing Collateral");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "button", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_ng_template_191_Template_button_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r48); const modal_r42 = restoredCtx.$implicit; return modal_r42.dismiss("Cross click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](8, "i", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "form", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("ngSubmit", function CollateralComponent_ng_template_191_Template_form_ngSubmit_14_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r48); const ctx_r49 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r49.proceed(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](16, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](19, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](21, "h6", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "Document Type *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "div", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](26, "div", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](28, "input", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](30, "label", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "Image");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](33, "input", 79);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](34, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](35, "label", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "Video");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](38, "input", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](40, "label", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "Document");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "\n\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](46, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](47, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](48, "h6", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "Fund Category *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](51, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](52, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](53, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](55, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](56, "i", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](58, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](59, "ng-select", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("change", function CollateralComponent_ng_template_191_Template_ng_select_change_59_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r48); const ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r50.onProductChange($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](60, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](61, CollateralComponent_ng_template_191_ng_option_61_Template, 2, 5, "ng-option", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](62, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](64, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](65, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](66, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](67, "i", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](69, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](70, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](71, "\n\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](72, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](74, "h6", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](75, "Fund Sub Category *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](77, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](78, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](79, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](80, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](81, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](82, "i", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](83, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](85, "ng-select", 84);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](87, CollateralComponent_ng_template_191_ng_option_87_Template, 2, 5, "ng-option", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](88, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](89, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](90, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](91, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](92, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](93, "i", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](94, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](95, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](96, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](97, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](98, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](99, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](100, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](101, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](102, "button", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_ng_template_191_Template_button_click_102_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r48); const modal_r42 = restoredCtx.$implicit; return modal_r42.close("Save click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](103, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](104, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](105, "button", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_ng_template_191_Template_button_click_105_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r48); const ctx_r52 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r52.proceed(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](106, "\n      Proceed ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](107, "i", 85);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](108, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](109, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](110, "\n");
} if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("formGroup", ctx_r9.CreateCollateralForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("hidden", !ctx_r9.error || ctx_r9.isLoading);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx_r9.error);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r9.productCategory$);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](26);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r9.subProductCategory$);
} }
function CollateralComponent_ng_template_194_div_14_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "img", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "a", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_ng_template_194_div_14_div_2_Template_a_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r61); const y_r58 = restoredCtx.$implicit; const ctx_r60 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r60.downloadFile(y_r58.file.imageName); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](7, "i", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const y_r58 = ctx.$implicit;
    const ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", ctx_r57.apiEndPoint + y_r58.file.thumnailPath, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsanitizeUrl"]);
} }
function CollateralComponent_ng_template_194_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](2, CollateralComponent_ng_template_194_div_14_div_2_Template, 10, 1, "div", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const x_r55 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", x_r55.collateralfilemaps);
} }
function CollateralComponent_ng_template_194_Template(rf, ctx) { if (rf & 1) {
    const _r63 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](0, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "div", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "h4", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "Download Collateral Files");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "button", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_ng_template_194_Template_button_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r63); const modal_r53 = restoredCtx.$implicit; return modal_r53.dismiss("Cross click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](8, "i", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](14, CollateralComponent_ng_template_194_div_14_Template, 4, 1, "div", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n");
} if (rf & 2) {
    const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r11.downloadFileList);
} }
function CollateralComponent_ng_template_197_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "Collateral Name :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "Collateral Type :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](13, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](14, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](16, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "Product Tpye :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](20, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](21, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](23, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Sub Product Tpye :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](27, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](28, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](30, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](31, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "Number Of Files :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](34, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](35, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "Created At :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](41, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](42, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](43, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const info_r66 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r66.collateralName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r66.collateralType, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r66.categoryName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r66.subcategoryName, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", info_r66.collateralfilemaps.length, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](41, 6, info_r66.updatedAt, "MMM dd, YYYY | HH:mm"), "");
} }
function CollateralComponent_ng_template_197_Template(rf, ctx) { if (rf & 1) {
    const _r69 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](0, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](1, "div", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "h4", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "Collateral Information");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "button", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_ng_template_197_Template_button_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r69); const modal_r64 = restoredCtx.$implicit; return modal_r64.dismiss("Cross click"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](8, "i", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](14, CollateralComponent_ng_template_197_div_14_Template, 45, 9, "div", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n");
} if (rf & 2) {
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r13.collateralInfoList);
} }
const log = new _shared__WEBPACK_IMPORTED_MODULE_0__.Logger('Collateral');
class CollateralComponent {
    constructor(modalService, router, formBuilder, collateralService, productService, filesService, toastr) {
        this.modalService = modalService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.collateralService = collateralService;
        this.productService = productService;
        this.filesService = filesService;
        this.toastr = toastr;
        this.isLoading = false;
        this.popUpCloseResult = '';
        this.searchText = '';
        this.searchOptions = 'Search By Collateral Name';
        this.sortType = 'Last Modified';
        this.productCategory$ = [];
        this.subProductCategory$ = [];
        this.apiEndPoint = '';
        this.collateralList = [];
        this.collateralListLength = 0;
        this.downloadFileList = [];
        this.downloadFileListLength = 0;
        this.collateralInfoList = [];
        this.isFilterOpened = false;
        var credentials = localStorage.getItem('credentials') || '';
        this.userRole = JSON.parse(credentials).userLoggedIn.role;
        this.apiEndPoint = _environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.serverUrl;
        this.pageIndex = 0;
        this.pageSize = 12;
        this.createForm();
    }
    createForm() {
        this.CreateCollateralForm = this.formBuilder.group({
            documentType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
            selectedProductCategory: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
            selectedSubProductCategory: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_8__.Validators.required],
        });
        this.ApplyFilterForm = this.formBuilder.group({
            imageTypeFilter: [''],
            videoTypeFilter: [''],
            documentTypeFilter: [''],
            selectedProductCategoryFilter: [''],
            selectedSubProductCategoryFilter: [''],
        });
    }
    ngOnInit() {
        this.ApplyFilterForm.reset();
        this.getAllCollaterals();
    }
    onProductChange(event) {
        this.getSubProductCategory(event);
    }
    getProductCategory() {
        this.isLoading = true;
        this.error = '';
        this.productService.getProductCategory().subscribe((data) => {
            this.isLoading = false;
            this.productCategory$ = data;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    getSubProductCategory(ProductId) {
        this.isLoading = true;
        this.error = '';
        this.productService.getSubProductCategory({ ProductId: ProductId }).subscribe((data) => {
            this.isLoading = false;
            this.subProductCategory$ = data;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    //Popup start
    openNewCollateralPopUp(content) {
        this.CreateCollateralForm.reset();
        this.error = '';
        this.getProductCategory();
        this.subProductCategory$ = [];
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.popUpCloseResult = `Closed with: ${result}`;
        }, (reason) => {
            this.popUpCloseResult = `Dismissed ${this.getPopUpDismissReason(reason)}`;
        });
    }
    openDownloadPopUp(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.popUpCloseResult = `Closed with: ${result}`;
        }, (reason) => {
            this.popUpCloseResult = `Dismissed ${this.getPopUpDismissReason(reason)}`;
        });
    }
    openInfoPopUp(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.popUpCloseResult = `Closed with: ${result}`;
        }, (reason) => {
            this.popUpCloseResult = `Dismissed ${this.getPopUpDismissReason(reason)}`;
        });
    }
    openFilterPopUp(content) {
        this.isFilterOpened = !this.isFilterOpened;
        if (this.isFilterOpened == true) {
            this.ApplyFilterForm.reset();
            this.getProductCategory();
            this.subProductCategory$ = [];
            this.searchText = '';
        }
    }
    closeFilter() {
        const dropDownId = document.getElementById('dropdownMenuClickable');
        dropDownId === null || dropDownId === void 0 ? void 0 : dropDownId.click();
    }
    getPopUpDismissReason(reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__.ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__.ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return `with: ${reason}`;
        }
    }
    //Popup close
    getAllCollaterals() {
        this.isLoading = true;
        const data = {
            query: {
                statusAsDraft: false,
                isActive: true,
            },
            options: {
                sort: [['id', 'DESC']],
            },
        };
        this.collateralService.getAllCollateralList(data).subscribe((data) => {
            this.isLoading = false;
            this.collateralList = data.data;
            this.collateralListLength = this.collateralList.length;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    proceed() {
        this.isLoading = true;
        if (this.CreateCollateralForm.valid) {
            const data = {
                collateralType: this.CreateCollateralForm.controls.documentType.value,
                categoryId: this.CreateCollateralForm.controls.selectedProductCategory.value.productId,
                categoryName: this.CreateCollateralForm.controls.selectedProductCategory.value.productName,
                subcategoryId: this.CreateCollateralForm.controls.selectedSubProductCategory.value.subproductId,
                subcategoryName: this.CreateCollateralForm.controls.selectedSubProductCategory.value.subproductName,
            };
            const collateral$ = this.collateralService.createCollateral(data);
            collateral$.subscribe((data) => {
                this.isLoading = false;
                this.modalService.dismissAll('Dismissed after saving data');
                this.router.navigate(['/collateral/upload'], { queryParams: { _d: btoa(data.data.id) }, replaceUrl: false });
            }, (error) => {
                this.isLoading = false;
                this.toastr.error('Something went wrong, Please try again');
                this.error = error;
            });
        }
        else {
            this.isLoading = false;
            if (this.CreateCollateralForm.value.documentType == null) {
                this.error = 'Please select document type';
            }
            else if (this.CreateCollateralForm.value.selectedProductCategory == null) {
                this.error = 'Please select fund category';
            }
            else {
                this.error = 'Please select fund sub category';
            }
        }
    }
    getFilesByCollateralId(collateralId) {
        const data = {
            query: {
                id: collateralId,
            },
        };
        this.collateralService.getAllCollateralList(data).subscribe((data) => {
            this.downloadFileList = data.data;
            this.downloadFileListLength = this.downloadFileList.length;
            this.openDownloadPopUp(this.downloadFileModal);
        }, (error) => {
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    createNewCoBrand(collateralId) {
        this.router.navigate(['/co-branding/define'], { queryParams: { _d: btoa(collateralId) }, replaceUrl: false });
    }
    deleteCollateralById(collateralId) {
        this.isLoading = true;
        let imgNames = [];
        let thumbNames = [];
        this.collateralList.forEach((element) => {
            if (element.id == collateralId) {
                for (let i = 0; i < element.collateralfilemaps.length; i++) {
                    const item = element.collateralfilemaps[i];
                    imgNames.push(item.file.imageName);
                    thumbNames.push(item.file.thumnailName);
                }
            }
        });
        this.collateralService.deleteCollateral({ id: collateralId, imgList: imgNames, thumbList: thumbNames }).subscribe((data) => {
            this.isLoading = false;
            imgNames.splice(0, imgNames.length);
            thumbNames.splice(0, thumbNames.length);
            this.toastr.success('Deleted successfully!');
            this.getAllCollaterals();
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    downloadFile(fileName) {
        window.open(this.filesService.downloadFileByName + fileName, '_blank');
    }
    isFilterClick() {
        this.openFilterPopUp(this.filterModal);
    }
    applyFilter() {
        this.isLoading = true;
        const imagetype = this.ApplyFilterForm.controls.imageTypeFilter.value == true ? 'Image' : null;
        const videotype = this.ApplyFilterForm.controls.videoTypeFilter.value == true ? 'Video' : null;
        const documenttype = this.ApplyFilterForm.controls.documentTypeFilter.value == true ? 'Document' : null;
        const categoryid = this.ApplyFilterForm.controls.selectedProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedProductCategoryFilter.value.productId)
            : null;
        const subcategoryid = this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value.subproductId)
            : null;
        const categoryname = this.ApplyFilterForm.controls.selectedProductCategoryFilter.value != null
            ? this.ApplyFilterForm.controls.selectedProductCategoryFilter.value.productName
            : 'All';
        const subcategoryname = this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value != null
            ? this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value.subproductName
            : 'All';
        const collateralType = [];
        const categoryId = [];
        const subcategoryId = [];
        if (imagetype != null) {
            collateralType.push(imagetype);
        }
        if (videotype != null) {
            collateralType.push(videotype);
        }
        if (documenttype != null) {
            collateralType.push(documenttype);
        }
        if (categoryid != null) {
            categoryId.push(categoryid);
        }
        if (subcategoryid != null) {
            subcategoryId.push(subcategoryid);
        }
        const data = {
            filter: [{ collateralType: collateralType }, { categoryId: categoryId }, { subcategoryId: subcategoryId }],
        };
        this.collateralService.getCollateralByFilter(data).subscribe((data) => {
            this.isLoading = false;
            this.collateralList = data.data;
            this.collateralListLength = this.collateralList.length;
            this.searchOptions =
                'type:' +
                    (collateralType.length != 0 ? collateralType : 'All') +
                    '; cat:' +
                    categoryname +
                    '; sub-cat:' +
                    subcategoryname;
            this.closeFilter();
            //this.modalService.dismissAll('Dismissed after fetching data');
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    resetFilter() {
        this.ApplyFilterForm.reset();
        this.searchText = '';
        this.searchOptions = 'Search By Collateral Name';
        //this.modalService.dismissAll('Dismissed after reset data');
        this.getAllCollaterals();
        this.closeFilter();
    }
    dateFilter(type) {
        if (type == 'Last Modified') {
            this.sortType = 'Last Modified';
            this.getAllCollaterals();
        }
    }
    sortFilter(order) {
        this.isLoading = true;
        let data;
        if (order == 'ASC') {
            this.sortType = 'A - Z';
            data = {
                query: {
                    statusAsDraft: false,
                    isActive: true,
                },
                options: {
                    sort: [['collateralName', 'ASC']],
                },
            };
        }
        if (order == 'DESC') {
            this.sortType = 'Z - A';
            data = {
                query: {
                    statusAsDraft: false,
                    isActive: true,
                },
                options: {
                    sort: [['collateralName', 'DESC']],
                },
            };
        }
        this.collateralService.getAllCollateralList(data).subscribe((data) => {
            this.isLoading = false;
            this.collateralList = data.data;
            this.collateralListLength = this.collateralList.length;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    onSearchCollateral() {
        this.isLoading = true;
        const imagetype = this.ApplyFilterForm.controls.imageTypeFilter.value == true ? 'Image' : null;
        const videotype = this.ApplyFilterForm.controls.videoTypeFilter.value == true ? 'Video' : null;
        const documenttype = this.ApplyFilterForm.controls.documentTypeFilter.value == true ? 'Document' : null;
        const categoryid = this.ApplyFilterForm.controls.selectedProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedProductCategoryFilter.value.productId)
            : null;
        const subcategoryid = this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value != null
            ? parseInt(this.ApplyFilterForm.controls.selectedSubProductCategoryFilter.value.subproductId)
            : null;
        let searchedText = this.searchText;
        const collateralType = [];
        const categoryId = [];
        const subcategoryId = [];
        if (imagetype != null) {
            collateralType.push(imagetype);
        }
        if (videotype != null) {
            collateralType.push(videotype);
        }
        if (documenttype != null) {
            collateralType.push(documenttype);
        }
        if (categoryid != null) {
            categoryId.push(categoryid);
        }
        if (subcategoryid != null) {
            subcategoryId.push(subcategoryid);
        }
        const data = {
            filter: [{ collateralType: collateralType }, { categoryId: categoryId }, { subcategoryId: subcategoryId }],
            search: [{ collateralName: searchedText }],
        };
        this.collateralService.getCollateralByFilter(data).subscribe((data) => {
            this.isLoading = false;
            this.collateralList = data.data;
            this.collateralListLength = this.collateralList.length;
            this.modalService.dismissAll('Dismissed after fetching data');
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    infoCollateralClick(collateralId) {
        this.collateralList.forEach((element) => {
            if (element.id == collateralId) {
                this.collateralInfoList = [element];
            }
        });
        this.openInfoPopUp(this.infoFileModal);
    }
}
CollateralComponent.ɵfac = function CollateralComponent_Factory(t) { return new (t || CollateralComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__.NgbModal), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_10__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_2__.CollateralService), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_rest_product_api_service__WEBPACK_IMPORTED_MODULE_3__.ProductService), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_4__.FilesService), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_11__.ToastrService)); };
CollateralComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineComponent"]({ type: CollateralComponent, selectors: [["app-collateral"]], viewQuery: function CollateralComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵviewQuery"](_c0, 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵviewQuery"](_c1, 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵviewQuery"](_c2, 7);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵloadQuery"]()) && (ctx.downloadFileModal = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵloadQuery"]()) && (ctx.filterModal = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵloadQuery"]()) && (ctx.infoFileModal = _t.first);
    } }, decls: 201, vars: 16, consts: [[1, "container-fluid"], [1, "text-center", "jumbotron"], [3, "isLoading"], [1, "mx-5", "mt-5", "page-title"], [1, "text-black", "d-inline"], ["class", "mx-3 btn btn-primary", 3, "click", 4, "ngIf"], [1, "mx-5", "mt-4"], [1, "row"], [1, "col-md-5"], [1, "mb-3", "input-group", "drop-shadow", 2, "z-index", "99"], [1, "input-group-prepend", "text-primary"], [1, "input-group-text"], [1, "fas", "fa-search"], ["type", "text", 1, "form-control", "text-primary", 3, "ngModel", "placeholder", "ngModelChange", "keyup.enter"], ["class", "input-group-text", 4, "ngIf"], [1, "input-group-append", "text-primary"], ["id", "dropdownMenuClickable", "data-bs-toggle", "dropdown", "data-bs-auto-close", "false", "aria-expanded", "false", 1, "fas", "fa-sliders-h", 3, "click"], ["aria-labelledby", "dropdownMenuClickable", 1, "dropdown-menu", "dropdown-menu-lg-end", "filter-box"], [1, "modal-body"], [3, "formGroup", "ngSubmit"], [1, "col-md-10"], [1, "font-weight-bold", "text-black", "d-block"], [1, "col-md-2"], ["type", "button", "aria-label", "Close", 1, "btn", "btn-link", 3, "click"], ["aria-hidden", "true", 1, "fas", "fa-times"], [1, "row", 2, "padding-left", "3%"], [1, "col-md-3", "form-check", "form-check-inline"], ["mdbCheckbox", "", "type", "checkbox", "id", "inlineCheckbox1", "value", "Image", "formControlName", "imageTypeFilter", 1, "form-check-input"], ["for", "inlineCheckbox1", 1, "form-check-label"], ["mdbCheckbox", "", "type", "checkbox", "id", "inlineCheckbox2", "value", "Video", "formControlName", "videoTypeFilter", 1, "form-check-input"], ["for", "inlineCheckbox2", 1, "form-check-label"], ["mdbCheckbox", "", "type", "checkbox", "id", "inlineCheckbox3", "value", "Document", "formControlName", "documentTypeFilter", 1, "form-check-input"], ["for", "inlineCheckbox3", 1, "form-check-label"], [1, "my-4"], [1, "mb-3", "input-group", "input-control"], ["formControlName", "selectedProductCategoryFilter", 1, "form-control", 3, "change"], [3, "value", 4, "ngFor", "ngForOf"], [1, "fas", "fa-caret-down"], ["formControlName", "selectedSubProductCategoryFilter", 1, "form-control"], [1, "modal-footer"], ["type", "button", 1, "btn", "btn-link", 3, "click"], ["type", "button", 1, "btn", "btn-primary", 3, "click"], ["class", "row", 4, "ngIf"], [1, "col-md-6"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange", 4, "ngIf"], ["class", "col-md-6 d-flex align-items-center justify-content-end", "style", "padding-right: 5%", 4, "ngIf"], ["class", "col-md-2 card card-compact box-padding", 4, "ngFor", "ngForOf"], ["data-backdrop", "static", "data-keyboard", "false"], ["addNewModal", ""], ["downloadFileModal", ""], ["infoFileModal", ""], [1, "mx-3", "btn", "btn-primary", 3, "click"], [1, "mx-2", "fas", "fa-plus", "btn-icon"], [1, "fas", "fa-times", 3, "click"], [3, "value"], [2, "text-align", "center"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange"], [1, "col-md-6", "d-flex", "align-items-center", "justify-content-end", 2, "padding-right", "5%"], [1, "btn-group"], [1, "sort-type"], ["type", "button", "data-bs-toggle", "dropdown", "aria-expanded", "false", 1, "fas", "fa-caret-down", "sort-pointer"], [1, "dropdown-menu", "dropdown-menu-end"], ["type", "button", 1, "dropdown-item", "co-menu-items", 3, "click"], [1, "col-md-2", "card", "card-compact", "box-padding"], [1, "card-img"], [3, "src"], [1, "card-info"], [1, "co-name"], ["type", "button", "data-bs-toggle", "dropdown", "aria-expanded", "false", 1, "fas", "fa-ellipsis-v", "co-menu"], [4, "ngIf"], [1, "co-type-name"], [1, "co-date"], [1, "modal-header"], ["id", "modal-basic-title", 1, "modal-title"], [1, "alert", "alert-danger", 3, "hidden"], ["role", "group", "aria-label", "Basic example", 1, "btn-group"], ["role", "group", "aria-label", "Basic radio toggle button group", 1, "btn-group"], ["type", "radio", "formControlName", "documentType", "name", "documentType", "value", "Image", "id", "documentType1", "autocomplete", "off", "checked", "", 1, "btn-check"], ["for", "documentType1", 1, "btn", "btn-outline-primary"], ["type", "radio", "formControlName", "documentType", "name", "documentType", "value", "Video", "id", "documentType2", "autocomplete", "off", 1, "btn-check"], ["for", "documentType2", 1, "btn", "btn-outline-primary"], ["type", "radio", "formControlName", "documentType", "name", "documentType", "value", "Document", "id", "documentType3", "autocomplete", "off", 1, "btn-check"], ["for", "documentType3", 1, "btn", "btn-outline-primary"], ["formControlName", "selectedProductCategory", 1, "form-control", 3, "change"], ["formControlName", "selectedSubProductCategory", 1, "form-control"], [1, "ms-1", "fas", "fa-arrow-right", "btn-icon"], ["class", "my-4", 4, "ngFor", "ngForOf"], [3, "click"], [1, "fas", "fa-download"]], template: function CollateralComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "app-loader", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](9, "h2", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "Marketing collateral");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](12, CollateralComponent_button_12_Template, 4, 0, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](19, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](21, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](23, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](25, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](26, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](29, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("ngModelChange", function CollateralComponent_Template_input_ngModelChange_29_listener($event) { return ctx.searchText = $event; })("keyup.enter", function CollateralComponent_Template_input_keyup_enter_29_listener() { return ctx.onSearchCollateral(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](31, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](33, CollateralComponent_span_33_Template, 4, 0, "span", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](34, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](36, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](41, "i", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_Template_i_click_41_listener() { return ctx.isFilterClick(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](43, "ul", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](45, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](49, "form", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("ngSubmit", function CollateralComponent_Template_form_ngSubmit_49_listener() { return ctx.applyFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](51, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](52, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](53, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](55, "h6", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](56, "Document Type");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](58, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](59, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](60, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](61, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_Template_button_click_61_listener() { return ctx.closeFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](62, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](63, "i", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](64, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](65, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](66, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](67, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](68, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](69, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](70, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](71, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](72, "input", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](74, "label", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](75, "Image");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](77, "\n\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](78, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](79, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](80, "input", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](81, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](82, "label", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](83, "Video");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](85, "\n\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](86, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](87, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](88, "input", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](89, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](90, "label", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](91, "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](92, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](93, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](94, "\n\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](95, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](96, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](97, "h6", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](98, "Fund Category");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](99, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](100, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](101, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](102, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](103, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](104, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](105, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](106, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](107, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](108, "ng-select", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("change", function CollateralComponent_Template_ng_select_change_108_listener($event) { return ctx.onProductChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](109, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](110, CollateralComponent_ng_option_110_Template, 2, 5, "ng-option", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](111, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](112, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](113, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](114, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](115, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](116, "i", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](117, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](118, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](119, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](120, "\n\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](121, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](122, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](123, "h6", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](124, "Fund Sub Category");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](125, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](126, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](127, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](128, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](129, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](130, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](131, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](132, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](133, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](134, "ng-select", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](135, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](136, CollateralComponent_ng_option_136_Template, 2, 5, "ng-option", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](137, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](138, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](139, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](140, "\n                            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](141, "span", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](142, "i", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](143, "\n                          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](144, "\n                        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](145, "\n                      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](146, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](147, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](148, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](149, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](150, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](151, "button", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_Template_button_click_151_listener() { return ctx.resetFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](152, "Reset");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](153, "\n                    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](154, "button", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function CollateralComponent_Template_button_click_154_listener() { return ctx.applyFilter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](155, "Apply");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](156, "\n                  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](157, "\n                ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](158, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](159, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](160, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](161, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](162, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](163, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](164, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](165, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](166, "\n\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](167, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](168, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](169, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](170, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](171, CollateralComponent_div_171_Template, 5, 0, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](172, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](173, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](174, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](175, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](176, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](177, CollateralComponent_pager_177_Template, 2, 3, "pager", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](178, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](179, "\n\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](180, CollateralComponent_div_180_Template, 32, 1, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](181, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](182, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](183, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](184, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](185, CollateralComponent_div_185_Template, 45, 11, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](186, "slice");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](187, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](188, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](189, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](190, "\n\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](191, CollateralComponent_ng_template_191_Template, 111, 5, "ng-template", 47, 48, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](193, "\n\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](194, CollateralComponent_ng_template_194_Template, 17, 1, "ng-template", 47, 49, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](196, "\n\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](197, CollateralComponent_ng_template_197_Template, 17, 1, "ng-template", 47, 50, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](199, "\n\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](200, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("isLoading", ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.userRole == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](17);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngModel", ctx.searchText)("placeholder", ctx.searchOptions);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.searchText != "");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("formGroup", ctx.ApplyFilterForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](61);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx.productCategory$);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](26);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx.subProductCategory$);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](35);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.collateralListLength == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.collateralListLength >= ctx.pageSize);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.collateralListLength > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind3"](186, 12, ctx.collateralList, ctx.pageIndex * ctx.pageSize, (ctx.pageIndex + 1) * ctx.pageSize));
    } }, directives: [_shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_5__.LoaderComponent, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgIf, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.NgModel, _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.CheckboxControlValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormControlName, _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_13__.NgSelectComponent, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgForOf, _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_13__["ɵr"], _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_6__.PagerComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.RadioControlValueAccessor], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_12__.SlicePipe, _angular_common__WEBPACK_IMPORTED_MODULE_12__.DatePipe], styles: [".logo[_ngcontent-%COMP%] {\n  width: 100px;\n}\n\n.filter-box[_ngcontent-%COMP%] {\n  width: 94%;\n  margin-top: 2% !important;\n  margin-right: 3% !important;\n}\n\n.box-padding[_ngcontent-%COMP%] {\n  margin-left: 15px;\n  margin-right: 15px;\n}\n\n.sort-type[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 23px;\n  letter-spacing: 0.02em;\n  color: #717171;\n  padding-right: 5px;\n}\n\n.sort-pointer[_ngcontent-%COMP%] {\n  padding-top: 4px;\n  color: #717171;\n}\n\n.co-name[_ngcontent-%COMP%] {\n  width: 184px;\n  height: 18px;\n  left: 124px;\n  top: 558px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 18px;\n  color: #484848;\n}\n\n.co-type-name[_ngcontent-%COMP%] {\n  width: 184px;\n  height: 18px;\n  left: 446px;\n  top: 576px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 18px;\n  color: #adadad;\n}\n\n.co-date[_ngcontent-%COMP%] {\n  width: 138px;\n  height: 15px;\n  left: 446px;\n  top: 599px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 12px;\n  line-height: 15px;\n  color: #353030;\n}\n\n.co-menu[_ngcontent-%COMP%] {\n  justify-content: flex-end !important;\n}\n\n.co-menu-items[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 18px;\n  letter-spacing: 0.02em;\n  color: #484848;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbGxhdGVyYWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FBQ0Y7O0FBRUE7RUFDRSxVQUFBO0VBQ0EseUJBQUE7RUFDQSwyQkFBQTtBQUNGOztBQUVBO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQUNGOztBQUVBO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFFQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtBQUNGOztBQUVBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFDRjs7QUFFQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBRUE7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUNGOztBQUVBO0VBQ0Usb0NBQUE7QUFDRjs7QUFFQTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtBQUNGIiwiZmlsZSI6ImNvbGxhdGVyYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9nbyB7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG59XHJcblxyXG4uZmlsdGVyLWJveCB7XHJcbiAgd2lkdGg6IDk0JTtcclxuICBtYXJnaW4tdG9wOiAyJSAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1yaWdodDogMyUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmJveC1wYWRkaW5nIHtcclxuICBtYXJnaW4tbGVmdDogMTVweDtcclxuICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbn1cclxuXHJcbi5zb3J0LXR5cGUge1xyXG4gIGZvbnQtZmFtaWx5OiBSYWpkaGFuaTtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyM3B4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjAyZW07XHJcbiAgY29sb3I6ICM3MTcxNzE7XHJcbiAgcGFkZGluZy1yaWdodDogNXB4O1xyXG59XHJcblxyXG4uc29ydC1wb2ludGVyIHtcclxuICBwYWRkaW5nLXRvcDogNHB4O1xyXG4gIGNvbG9yOiAjNzE3MTcxO1xyXG59XHJcblxyXG4uY28tbmFtZSB7XHJcbiAgd2lkdGg6IDE4NHB4O1xyXG4gIGhlaWdodDogMThweDtcclxuICBsZWZ0OiAxMjRweDtcclxuICB0b3A6IDU1OHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBSYWpkaGFuaTtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIGNvbG9yOiAjNDg0ODQ4O1xyXG59XHJcblxyXG4uY28tdHlwZS1uYW1lIHtcclxuICB3aWR0aDogMTg0cHg7XHJcbiAgaGVpZ2h0OiAxOHB4O1xyXG4gIGxlZnQ6IDQ0NnB4O1xyXG4gIHRvcDogNTc2cHg7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBsaW5lLWhlaWdodDogMThweDtcclxuICBjb2xvcjogI2FkYWRhZDtcclxufVxyXG5cclxuLmNvLWRhdGUge1xyXG4gIHdpZHRoOiAxMzhweDtcclxuICBoZWlnaHQ6IDE1cHg7XHJcbiAgbGVmdDogNDQ2cHg7XHJcbiAgdG9wOiA1OTlweDtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxNXB4O1xyXG4gIGNvbG9yOiAjMzUzMDMwO1xyXG59XHJcblxyXG4uY28tbWVudSB7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY28tbWVudS1pdGVtcyB7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBsaW5lLWhlaWdodDogMThweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4wMmVtO1xyXG4gIGNvbG9yOiAjNDg0ODQ4O1xyXG59XHJcbiJdfQ== */"] });


/***/ }),

/***/ 9408:
/*!*************************************************!*\
  !*** ./src/app/collateral/collateral.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CollateralModule": () => (/* binding */ CollateralModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _collateral_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./collateral-routing.module */ 3058);
/* harmony import */ var _collateral_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./collateral.component */ 8099);
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-select/ng-select */ 6640);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _upload_collateral_upload_collateral_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./upload-collateral/upload-collateral.component */ 2662);
/* harmony import */ var _define_collateral_define_collateral_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./define-collateral/define-collateral.component */ 9750);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng2-pdf-viewer */ 3621);
/* harmony import */ var ngx_drag_resize__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-drag-resize */ 9898);
/* harmony import */ var _app_shared_collaterals_files_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/@shared/collaterals.files.directive */ 9284);
/* harmony import */ var _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular-slider/ngx-slider */ 4555);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);













class CollateralModule {
}
CollateralModule.ɵfac = function CollateralModule_Factory(t) { return new (t || CollateralModule)(); };
CollateralModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({ type: CollateralModule });
CollateralModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({ providers: [_app_shared_collaterals_files_directive__WEBPACK_IMPORTED_MODULE_5__.CollateralsDetails], imports: [[
            _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_7__.NgxSliderModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule,
            _shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule,
            _collateral_routing_module__WEBPACK_IMPORTED_MODULE_1__.CollateralRoutingModule,
            _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__.NgSelectModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__.ReactiveFormsModule,
            ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_11__.PdfViewerModule,
            ngx_drag_resize__WEBPACK_IMPORTED_MODULE_12__.NgxDragResizeModule,
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](CollateralModule, { declarations: [_collateral_component__WEBPACK_IMPORTED_MODULE_2__.CollateralComponent, _upload_collateral_upload_collateral_component__WEBPACK_IMPORTED_MODULE_3__.UploadCollateralComponent, _define_collateral_define_collateral_component__WEBPACK_IMPORTED_MODULE_4__.DefineCollateralComponent], imports: [_angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_7__.NgxSliderModule,
        _angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule,
        _shared__WEBPACK_IMPORTED_MODULE_0__.SharedModule,
        _collateral_routing_module__WEBPACK_IMPORTED_MODULE_1__.CollateralRoutingModule,
        _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__.NgSelectModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormsModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_10__.ReactiveFormsModule,
        ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_11__.PdfViewerModule,
        ngx_drag_resize__WEBPACK_IMPORTED_MODULE_12__.NgxDragResizeModule] }); })();


/***/ }),

/***/ 9750:
/*!*****************************************************************************!*\
  !*** ./src/app/collateral/define-collateral/define-collateral.component.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DefineCollateralComponent": () => (/* binding */ DefineCollateralComponent)
/* harmony export */ });
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ 6738);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ngx_drag_resize__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-drag-resize */ 9898);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ 2340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/rest/collateral-api.service */ 2690);
/* harmony import */ var _app_shared_collaterals_files_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/@shared/collaterals.files.directive */ 9284);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ 9699);
/* harmony import */ var _shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../@shared/loader/loader.component */ 9967);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../@shared/pagination/pager.component */ 4454);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ng2-pdf-viewer */ 3621);
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ng-select/ng-select */ 6640);
/* harmony import */ var _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular-slider/ngx-slider */ 4555);


















function DefineCollateralComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_17_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r5.previousPage(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "button", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_17_Template_button_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r7.nextPage(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](12, "i", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("disabled", ctx_r0.page === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate2"]("", ctx_r0.page, " / ", ctx_r0.totalPages, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("disabled", ctx_r0.totalPages === 1 || ctx_r0.page === ctx_r0.totalPages);
} }
function DefineCollateralComponent_pager_19_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "pager", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("pageIndexChange", function DefineCollateralComponent_pager_19_Template_pager_pageIndexChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](); return ctx_r8.pageIndex = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("itemCount", ctx_r1.datalength)("pageIndex", ctx_r1.pageIndex)("pageSize", ctx_r1.pageSize);
} }
const _c0 = function (a0, a1) { return { "disable-div": a0, "enable-div": a1 }; };
function DefineCollateralComponent_div_60_div_2_div_2_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](5, "i", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, " Logo Area ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](12, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](14, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](16, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](18, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](20, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](22, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](24, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](26, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpropertyInterpolate"]("id", "logo-area-" + ctx_r16.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeMinWidth", 50)("ngxResizeMinHeight", 50)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](12, _c0, ctx_r16.isSetLogoAreaDisabled === true, ctx_r16.isSetLogoAreaDisabled === false));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.TopLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.Top);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.TopRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.Right);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.BottomRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.Bottom);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.BottomLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r16.handleType.Left);
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](5, "span", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "Aa");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](12, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](14, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](16, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](18, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](20, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](22, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](24, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](26, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpropertyInterpolate"]("id", "text-area-" + ctx_r17.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeMinWidth", 50)("ngxResizeMinHeight", 50)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](12, _c0, ctx_r17.isSetTextAreaDisabled === true, ctx_r17.isSetTextAreaDisabled === false));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.TopLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.Top);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.TopRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.Right);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.BottomRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.Bottom);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.BottomLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r17.handleType.Left);
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_23_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](2, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "button", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_23_Template_button_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r24); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r23.isSetLogoArea(ctx_r23.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n                + Set Logo Area\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_25_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const pageData_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 4, pageData_r14.logoDetails.x, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 7, pageData_r14.logoDetails.y, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 10, pageData_r14.logoDetails.width, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 13, pageData_r14.logoDetails.height, "1.0-0"));
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_25_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_25_div_17_Template_button_click_44_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r29); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](5); return ctx_r28.cancelLogoArea(ctx_r28.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_25_div_17_Template_button_click_47_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r29); const pageData_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit; const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r30.fixLogoArea(pageData_r14, ctx_r30.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48, "\n                    Set Area\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 4, ctx_r26.logoX, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 7, ctx_r26.logoY, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 10, ctx_r26.logoWidth, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 13, ctx_r26.logoHeight, "1.0-0"));
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_25_Template(rf, ctx) { if (rf & 1) {
    const _r33 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](2, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "Logo Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "i", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_25_Template_i_click_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r33); const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r32.deleteLogoArea(ctx_r32.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](15, DefineCollateralComponent_div_60_div_2_div_2_div_25_div_15_Template, 42, 16, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](17, DefineCollateralComponent_div_60_div_2_div_2_div_25_div_17_Template, 51, 16, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const pageData_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](15);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", pageData_r14.isSetLogoAreaDisabled);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !pageData_r14.isSetLogoAreaDisabled);
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_29_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_29_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r36); const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r35.isSetTextArea(ctx_r35.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                + Set Text Area\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Font Family : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](51, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](52, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](53, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "Font Size : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](56, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](58, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](60, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](61, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](62, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "Bold : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](64, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](65, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](66);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](67, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](69, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](70, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](71, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](72, "Italic : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](74, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](75);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](77, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](78, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](79, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](80, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](81, "Max Length : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](82, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](83, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](85, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const pageData_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 9, pageData_r14.textAreaDetails.x, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 12, pageData_r14.textAreaDetails.y, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 15, pageData_r14.textAreaDetails.width, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 18, pageData_r14.textAreaDetails.height, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](pageData_r14.textAreaDetails.font);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](pageData_r14.textAreaDetails.fontSizeName);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](pageData_r14.textAreaDetails.bold);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](pageData_r14.textAreaDetails.italic);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](pageData_r14.textAreaDetails.maxCharacter);
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_ng_option_52_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const font_r42 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", font_r42);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](font_r42);
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_ng_option_66_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const size_r43 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", size_r43.Id);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](size_r43.name);
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_Template(rf, ctx) { if (rf & 1) {
    const _r45 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Font Family *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](47, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](48, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](50, "ng-select", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](51, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](52, DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_ng_option_52_Template, 2, 2, "ng-option", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](53, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](56, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](58, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "Font Size *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](60, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](61, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](62, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](64, "ng-select", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](65, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](66, DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_ng_option_66_Template, 2, 2, "ng-option", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](67, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](69, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](70, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](71, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](72, "input", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_Template_input_click_72_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r45); const ctx_r44 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](5); return ctx_r44.isBold(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](74, "label", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](75, "Bold");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "\n\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](77, "input", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_Template_input_click_77_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r45); const ctx_r46 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](5); return ctx_r46.isItalic(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](78, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](79, "label", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](80, "Italic");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](81, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](82, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](83, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](85, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "Max Length *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](87, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](88, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](89, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](90, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](91, "input", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](92, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](93, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](94, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](95, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](96, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_Template_button_click_96_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r45); const ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](5); return ctx_r47.cancelTextArea(ctx_r47.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](97, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](98, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](99, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_Template_button_click_99_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r45); const pageData_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit; const ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r48.fixTextArea(pageData_r14, ctx_r48.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](100, "\n                    Set Area\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](101, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](102, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 7, ctx_r38.textX, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 10, ctx_r38.textY, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 13, ctx_r38.textWidth, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 16, ctx_r38.textHeight, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r38.fonts$);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r38.fontsize$);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("placeholder", "Max Length");
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_31_Template(rf, ctx) { if (rf & 1) {
    const _r51 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](8, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "i", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_div_31_Template_i_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r51); const ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r50.deleteTextArea(ctx_r50.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](13, DefineCollateralComponent_div_60_div_2_div_2_div_31_div_13_Template, 87, 21, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](15, DefineCollateralComponent_div_60_div_2_div_2_div_31_div_15_Template, 103, 19, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const pageData_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", pageData_r14.isSetTextAreaDisabled);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !pageData_r14.isSetTextAreaDisabled);
} }
function DefineCollateralComponent_div_60_div_2_div_2_div_37_Template(rf, ctx) { if (rf & 1) {
    const _r54 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "input", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("change", function DefineCollateralComponent_div_60_div_2_div_2_div_37_Template_input_change_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r54); const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r53.isAppliedToAllPages(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                Apply this to all pages\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_60_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r56 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](8, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "pdf-viewer", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("page-rendered", function DefineCollateralComponent_div_60_div_2_div_2_Template_pdf_viewer_page_rendered_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r56); const ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r55.pdfPageRendered($event); })("after-load-complete", function DefineCollateralComponent_div_60_div_2_div_2_Template_pdf_viewer_after_load_complete_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r56); const ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r57.afterLoadComplete($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](13, DefineCollateralComponent_div_60_div_2_div_2_div_13_Template, 28, 15, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](15, DefineCollateralComponent_div_60_div_2_div_2_div_15_Template, 28, 15, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](21, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](23, DefineCollateralComponent_div_60_div_2_div_2_div_23_Template, 7, 0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](25, DefineCollateralComponent_div_60_div_2_div_2_div_25_Template, 19, 2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](27, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](29, DefineCollateralComponent_div_60_div_2_div_2_div_29_Template, 5, 0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](31, DefineCollateralComponent_div_60_div_2_div_2_div_31_Template, 17, 2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](33, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](34, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](35, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](37, DefineCollateralComponent_div_60_div_2_div_2_div_37_Template, 4, 0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](39, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](41, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_60_div_2_div_2_Template_button_click_41_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r56); const ctx_r58 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r58.checkBeforeSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "Save And Submit");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const pageData_r14 = ctx.$implicit;
    const document_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", ctx_r13.apiEndPoint + document_r11.fileData.path)("show-all", false)("page", ctx_r13.page)("original-size", false)("render-text", false)("zoom-scale", "page-width");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", pageData_r14.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", pageData_r14.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !pageData_r14.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", pageData_r14.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !pageData_r14.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", pageData_r14.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx_r13.collateralFileType != "Video");
} }
function DefineCollateralComponent_div_60_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](2, DefineCollateralComponent_div_60_div_2_div_2_Template, 47, 13, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind3"](3, 1, ctx_r10.documentPageData, ctx_r10.pageIndex * ctx_r10.pageSize, (ctx_r10.pageIndex + 1) * ctx_r10.pageSize));
} }
function DefineCollateralComponent_div_60_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](2, DefineCollateralComponent_div_60_div_2_Template, 5, 5, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r2.fileDetailsList);
} }
function DefineCollateralComponent_div_62_div_2_img_10_Template(rf, ctx) { if (rf & 1) {
    const _r72 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "img", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("load", function DefineCollateralComponent_div_62_div_2_img_10_Template_img_load_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r72); const ctx_r71 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r71.calulateRenderedAspectRatio(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    const ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", ctx_r63.apiEndPoint + file_r61.fileData.path, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsanitizeUrl"]);
} }
function DefineCollateralComponent_div_62_div_2_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](5, "i", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](6, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, " Logo Area ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](11, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](13, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](15, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](17, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](19, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](21, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](23, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](25, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r64 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpropertyInterpolate"]("id", "logo-area-" + ctx_r64.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeMinWidth", 50)("ngxResizeMinHeight", 50)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](12, _c0, ctx_r64.isSetLogoAreaDisabled === true, ctx_r64.isSetLogoAreaDisabled === false));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.TopLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.Top);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.TopRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.Right);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.BottomRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.Bottom);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.BottomLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r64.handleType.Left);
} }
function DefineCollateralComponent_div_62_div_2_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](2, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](3, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "span", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "Aa");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](12, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](14, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](16, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](18, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](20, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](22, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](24, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](26, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r65 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpropertyInterpolate"]("id", "text-area-" + ctx_r65.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeMinWidth", 50)("ngxResizeMinHeight", 50)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](12, _c0, ctx_r65.isSetTextAreaDisabled === true, ctx_r65.isSetTextAreaDisabled === false));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.TopLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.Top);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.TopRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.Right);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.BottomRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.Bottom);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.BottomLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r65.handleType.Left);
} }
function DefineCollateralComponent_div_62_div_2_div_22_Template(rf, ctx) { if (rf & 1) {
    const _r75 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](2, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "button", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_22_Template_button_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r75); const ctx_r74 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r74.isSetLogoArea(ctx_r74.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n              + Set Logo Area\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_62_div_2_div_24_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 4, file_r61.logoDetails.x, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 7, file_r61.logoDetails.y, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 10, file_r61.logoDetails.width, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 13, file_r61.logoDetails.height, "1.0-0"));
} }
function DefineCollateralComponent_div_62_div_2_div_24_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r80 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_24_div_17_Template_button_click_44_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r80); const ctx_r79 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r79.cancelLogoArea(ctx_r79.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_24_div_17_Template_button_click_47_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r80); const file_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit; const ctx_r81 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2); return ctx_r81.fixLogoArea(file_r61, ctx_r81.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48, "Set Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r77 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 4, ctx_r77.logoX, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 7, ctx_r77.logoY, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 10, ctx_r77.logoWidth, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 13, ctx_r77.logoHeight, "1.0-0"));
} }
function DefineCollateralComponent_div_62_div_2_div_24_Template(rf, ctx) { if (rf & 1) {
    const _r84 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](2, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "Logo Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "i", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_24_Template_i_click_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r84); const ctx_r83 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r83.deleteLogoArea(ctx_r83.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](15, DefineCollateralComponent_div_62_div_2_div_24_div_15_Template, 42, 16, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](17, DefineCollateralComponent_div_62_div_2_div_24_div_17_Template, 51, 16, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](15);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r61.isSetLogoAreaDisabled);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r61.isSetLogoAreaDisabled);
} }
function DefineCollateralComponent_div_62_div_2_div_28_Template(rf, ctx) { if (rf & 1) {
    const _r87 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_28_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r87); const ctx_r86 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r86.isSetTextArea(ctx_r86.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n              + Set Text Area\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_62_div_2_div_30_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Font Family : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](51, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](52, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](53, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "Font Size : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](56, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](58, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](60, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](61, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](62, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "Bold : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](64, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](65, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](66);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](67, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](69, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](70, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](71, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](72, "Italic : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](74, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](75);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](77, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](78, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](79, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](80, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](81, "Max Length : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](82, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](83, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](85, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 9, file_r61.textAreaDetails.x, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 12, file_r61.textAreaDetails.y, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 15, file_r61.textAreaDetails.width, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 18, file_r61.textAreaDetails.height, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r61.textAreaDetails.font);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r61.textAreaDetails.fontSizeName);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r61.textAreaDetails.bold);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r61.textAreaDetails.italic);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r61.textAreaDetails.maxCharacter);
} }
function DefineCollateralComponent_div_62_div_2_div_30_div_15_ng_option_52_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const font_r93 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", font_r93);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](font_r93);
} }
function DefineCollateralComponent_div_62_div_2_div_30_div_15_ng_option_66_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const size_r94 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", size_r94.Id);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](size_r94.name);
} }
function DefineCollateralComponent_div_62_div_2_div_30_div_15_Template(rf, ctx) { if (rf & 1) {
    const _r96 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Font Family *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](47, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](48, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](50, "ng-select", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](51, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](52, DefineCollateralComponent_div_62_div_2_div_30_div_15_ng_option_52_Template, 2, 2, "ng-option", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](53, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](56, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](58, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "Font Size *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](60, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](61, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](62, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](64, "ng-select", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](65, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](66, DefineCollateralComponent_div_62_div_2_div_30_div_15_ng_option_66_Template, 2, 2, "ng-option", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](67, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](69, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](70, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](71, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](72, "input", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_30_div_15_Template_input_click_72_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r96); const ctx_r95 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r95.isBold(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](74, "label", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](75, "Bold");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "\n\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](77, "input", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_30_div_15_Template_input_click_77_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r96); const ctx_r97 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r97.isItalic(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](78, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](79, "label", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](80, "Italic");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](81, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](82, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](83, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](85, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "Max Length *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](87, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](88, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](89, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](90, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](91, "input", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](92, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](93, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](94, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](95, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](96, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_30_div_15_Template_button_click_96_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r96); const ctx_r98 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r98.cancelTextArea(ctx_r98.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](97, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](98, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](99, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_30_div_15_Template_button_click_99_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r96); const file_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit; const ctx_r99 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2); return ctx_r99.fixTextArea(file_r61, ctx_r99.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](100, "Set Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](101, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](102, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r89 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 7, ctx_r89.textX, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 10, ctx_r89.textY, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 13, ctx_r89.textWidth, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 16, ctx_r89.textHeight, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r89.fonts$);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r89.fontsize$);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("placeholder", "Max Length");
} }
function DefineCollateralComponent_div_62_div_2_div_30_Template(rf, ctx) { if (rf & 1) {
    const _r102 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](8, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "i", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_div_30_Template_i_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r102); const ctx_r101 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r101.deleteTextArea(ctx_r101.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](13, DefineCollateralComponent_div_62_div_2_div_30_div_13_Template, 87, 21, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](15, DefineCollateralComponent_div_62_div_2_div_30_div_15_Template, 103, 19, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r61.isSetTextAreaDisabled);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r61.isSetTextAreaDisabled);
} }
function DefineCollateralComponent_div_62_div_2_div_36_Template(rf, ctx) { if (rf & 1) {
    const _r105 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "input", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("change", function DefineCollateralComponent_div_62_div_2_div_36_Template_input_change_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r105); const ctx_r104 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r104.isAppliedToAllPages(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n              Apply this to all pages\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_62_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r107 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "div", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "div", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](8, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](10, DefineCollateralComponent_div_62_div_2_img_10_Template, 1, 1, "img", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](14, DefineCollateralComponent_div_62_div_2_div_14_Template, 27, 15, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](16, DefineCollateralComponent_div_62_div_2_div_16_Template, 28, 15, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](20, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](22, DefineCollateralComponent_div_62_div_2_div_22_Template, 7, 0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](24, DefineCollateralComponent_div_62_div_2_div_24_Template, 19, 2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](26, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](28, DefineCollateralComponent_div_62_div_2_div_28_Template, 5, 0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](30, DefineCollateralComponent_div_62_div_2_div_30_Template, 17, 2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](32, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](36, DefineCollateralComponent_div_62_div_2_div_36_Template, 4, 0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](40, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_62_div_2_Template_button_click_40_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r107); const ctx_r106 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2); return ctx_r106.checkBeforeSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "Save And Submit");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r61 = ctx.$implicit;
    const ctx_r60 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r61.fileData.mimetype == "image/jpeg" || file_r61.fileData.mimetype == "image/jpg" || file_r61.fileData.mimetype == "image/png");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r61.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r61.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r61.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r61.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r61.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r61.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx_r60.collateralFileType == "Document");
} }
function DefineCollateralComponent_div_62_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](2, DefineCollateralComponent_div_62_div_2_Template, 46, 8, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind3"](3, 1, ctx_r3.fileDetailsList, ctx_r3.pageIndex * ctx_r3.pageSize, (ctx_r3.pageIndex + 1) * ctx_r3.pageSize));
} }
function DefineCollateralComponent_div_64_div_2_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "i", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](5, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, " Logo Area ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](11, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](13, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](15, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](17, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](19, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](21, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](23, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](25, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r111 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpropertyInterpolate"]("id", "logo-area-" + ctx_r111.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeMinWidth", 50)("ngxResizeMinHeight", 50)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](12, _c0, ctx_r111.isSetLogoAreaDisabled === true, ctx_r111.isSetLogoAreaDisabled === false));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.TopLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.Top);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.TopRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.Right);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.BottomRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.Bottom);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.BottomLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r111.handleType.Left);
} }
function DefineCollateralComponent_div_64_div_2_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 63);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](5, "span", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "Aa");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](11, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](13, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](15, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](17, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](19, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](21, "div", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](23, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](25, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r112 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpropertyInterpolate"]("id", "text-area-" + ctx_r112.pageIndex);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeMinWidth", 50)("ngxResizeMinHeight", 50)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpureFunction2"](12, _c0, ctx_r112.isSetTextAreaDisabled === true, ctx_r112.isSetTextAreaDisabled === false));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.TopLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.Top);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.TopRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.Right);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.BottomRight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.Bottom);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.BottomLeft);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngxResizeHandle", ctx_r112.handleType.Left);
} }
function DefineCollateralComponent_div_64_div_2_ngx_slider_17_Template(rf, ctx) { if (rf & 1) {
    const _r120 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ngx-slider", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("valueChange", function DefineCollateralComponent_div_64_div_2_ngx_slider_17_Template_ngx_slider_valueChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r120); const ctx_r119 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r119.value = $event; })("highValueChange", function DefineCollateralComponent_div_64_div_2_ngx_slider_17_Template_ngx_slider_highValueChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r120); const ctx_r121 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r121.highValue = $event; })("userChange", function DefineCollateralComponent_div_64_div_2_ngx_slider_17_Template_ngx_slider_userChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r120); const ctx_r122 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r122.onChangeLogoTimeline($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r113 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", ctx_r113.value)("highValue", ctx_r113.highValue)("options", ctx_r113.options);
} }
function DefineCollateralComponent_div_64_div_2_ngx_slider_19_Template(rf, ctx) { if (rf & 1) {
    const _r124 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ngx-slider", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("valueChange", function DefineCollateralComponent_div_64_div_2_ngx_slider_19_Template_ngx_slider_valueChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r124); const ctx_r123 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r123.value = $event; })("highValueChange", function DefineCollateralComponent_div_64_div_2_ngx_slider_19_Template_ngx_slider_highValueChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r124); const ctx_r125 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r125.highValue = $event; })("userChange", function DefineCollateralComponent_div_64_div_2_ngx_slider_19_Template_ngx_slider_userChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r124); const ctx_r126 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r126.onChangeTextTimeline($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r114 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", ctx_r114.value)("highValue", ctx_r114.highValue)("options", ctx_r114.options);
} }
function DefineCollateralComponent_div_64_div_2_div_26_Template(rf, ctx) { if (rf & 1) {
    const _r128 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](2, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "button", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_26_Template_button_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r128); const ctx_r127 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r127.isSetLogoArea(ctx_r127.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n              + Set Logo Area\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_64_div_2_div_28_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Start Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](51, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](52, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](53, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "End Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](56, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](58, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 6, file_r109.logoDetails.x, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 9, file_r109.logoDetails.y, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 12, file_r109.logoDetails.width, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 15, file_r109.logoDetails.height, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.logoDetails.videoDetails.startTime);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.logoDetails.videoDetails.endTime);
} }
function DefineCollateralComponent_div_64_div_2_div_28_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r133 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](9, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](14, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](17, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](19, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](27, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](29, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](37, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](38);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](39, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_28_div_17_Template_button_click_44_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r133); const ctx_r132 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r132.cancelLogoArea(ctx_r132.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_28_div_17_Template_button_click_47_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r133); const file_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit; const ctx_r134 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2); return ctx_r134.fixLogoArea(file_r109, ctx_r134.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48, "Set Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r130 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](9, 4, ctx_r130.logoX, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](19, 7, ctx_r130.logoY, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](29, 10, ctx_r130.logoWidth, "1.0-0"));
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind2"](39, 13, ctx_r130.logoHeight, "1.0-0"));
} }
function DefineCollateralComponent_div_64_div_2_div_28_Template(rf, ctx) { if (rf & 1) {
    const _r137 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](2, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "Logo Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](12, "i", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_28_Template_i_click_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r137); const ctx_r136 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r136.deleteLogoArea(ctx_r136.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](13, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](15, DefineCollateralComponent_div_64_div_2_div_28_div_15_Template, 60, 18, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](17, DefineCollateralComponent_div_64_div_2_div_28_div_17_Template, 51, 16, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](15);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r109.isSetLogoAreaDisabled);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r109.isSetLogoAreaDisabled);
} }
function DefineCollateralComponent_div_64_div_2_div_32_Template(rf, ctx) { if (rf & 1) {
    const _r140 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "button", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_32_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r140); const ctx_r139 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r139.isSetTextArea(ctx_r139.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n              + Set Text Area\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} }
function DefineCollateralComponent_div_64_div_2_div_34_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](11, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](13, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](16, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](20, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](25, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](29, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](31, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](40, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "Font Family : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](43, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](49, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "Font Size :");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](51, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](52, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](53);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](56, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](58, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "Bold : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](60, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](61, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](62);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](64, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](65, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](66, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](67, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "Italic : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](69, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](70, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](71);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](72, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](73, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](74, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](75, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](76, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](77, "Start Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](78, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](79, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](80);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](81, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](82, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](83, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](85, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "End Time : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](87, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](88, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](89);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](90, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](91, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](92, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](93, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](94, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](95, "Max Length : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](96, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](97, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](98);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](99, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](100, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.x);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.y);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.width);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.height);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.font);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.fontSizeName);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.bold);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.italic);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.videoDetails.startTime);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.videoDetails.endTime);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](file_r109.textAreaDetails.maxCharacter);
} }
function DefineCollateralComponent_div_64_div_2_div_34_div_15_ng_option_48_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const font_r146 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", font_r146);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](font_r146);
} }
function DefineCollateralComponent_div_64_div_2_div_34_div_15_ng_option_62_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "ng-option", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const size_r147 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("value", size_r147.Id);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](size_r147.name);
} }
function DefineCollateralComponent_div_64_div_2_div_34_div_15_Template(rf, ctx) { if (rf & 1) {
    const _r149 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "X : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](11, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](13, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "Y : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](15, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](16, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](19, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](20, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, " Width : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](24, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](25, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](26);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](29, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](31, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](32, "Height : ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "label", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](36, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](40, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "Font Family *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](44, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](46, "ng-select", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](47, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](48, DefineCollateralComponent_div_64_div_2_div_34_div_15_ng_option_48_Template, 2, 2, "ng-option", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](51, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](52, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](53, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](54, "label", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "Font Size *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](56, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](58, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](60, "ng-select", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](61, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](62, DefineCollateralComponent_div_64_div_2_div_34_div_15_ng_option_62_Template, 2, 2, "ng-option", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](64, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](65, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](66, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](67, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](68, "input", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_34_div_15_Template_input_click_68_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r149); const ctx_r148 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r148.isBold(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](69, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](70, "label", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](71, "Bold");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](72, "\n\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](73, "input", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_34_div_15_Template_input_click_73_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r149); const ctx_r150 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r150.isItalic(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](74, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](75, "label", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](76, "Italic");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](77, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](78, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](79, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](80, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](81, "h6", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](82, "Max Length *");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](83, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](84, "\n\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](85, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](86, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](87, "input", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](88, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](89, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](90, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](91, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](92, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_34_div_15_Template_button_click_92_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r149); const ctx_r151 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4); return ctx_r151.cancelTextArea(ctx_r151.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](93, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](94, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](95, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_34_div_15_Template_button_click_95_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r149); const file_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2).$implicit; const ctx_r152 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2); return ctx_r152.fixTextArea(file_r109, ctx_r152.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](96, "Set Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](97, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](98, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r142 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx_r142.textX);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx_r142.textY);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx_r142.textWidth);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx_r142.textHeight);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r142.fonts$);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", ctx_r142.fontsize$);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](25);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("placeholder", "Max Length");
} }
function DefineCollateralComponent_div_64_div_2_div_34_Template(rf, ctx) { if (rf & 1) {
    const _r155 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "label", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "Text Area");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](8, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](10, "i", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_div_34_Template_i_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r155); const ctx_r154 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](3); return ctx_r154.deleteTextArea(ctx_r154.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](11, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](13, DefineCollateralComponent_div_64_div_2_div_34_div_13_Template, 101, 11, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](15, DefineCollateralComponent_div_64_div_2_div_34_div_15_Template, 99, 7, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r109 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r109.isSetTextAreaDisabled);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r109.isSetTextAreaDisabled);
} }
function DefineCollateralComponent_div_64_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r158 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](4, "div", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](6, "video", 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](7, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](8, "source", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](9, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](11, DefineCollateralComponent_div_64_div_2_div_11_Template, 27, 15, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](13, DefineCollateralComponent_div_64_div_2_div_13_Template, 27, 15, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](15, "div", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](17, DefineCollateralComponent_div_64_div_2_ngx_slider_17_Template, 1, 3, "ngx-slider", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](19, DefineCollateralComponent_div_64_div_2_ngx_slider_19_Template, 1, 3, "ngx-slider", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](22, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](26, DefineCollateralComponent_div_64_div_2_div_26_Template, 7, 0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](28, DefineCollateralComponent_div_64_div_2_div_28_Template, 19, 2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](30, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](32, DefineCollateralComponent_div_64_div_2_div_32_Template, 5, 0, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](34, DefineCollateralComponent_div_64_div_2_div_34_Template, 17, 2, "div", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](36, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](40, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](42, "button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_div_64_div_2_Template_button_click_42_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵrestoreView"](_r158); const ctx_r157 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2); return ctx_r157.checkBeforeSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](43, "Save And Submit");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](45, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](47, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r109 = ctx.$implicit;
    const ctx_r108 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("src", ctx_r108.apiEndPoint + file_r109.fileData.path, _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r109.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r109.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r109.isSetLogoAreaDisabled && file_r109.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r109.isSetTextAreaDisabled && file_r109.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r109.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r109.isSetLogo);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", !file_r109.isSetText);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", file_r109.isSetText);
} }
function DefineCollateralComponent_div_64_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](2, DefineCollateralComponent_div_64_div_2_Template, 48, 9, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](4, "\n    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵpipeBind3"](3, 1, ctx_r4.fileDetailsList, ctx_r4.pageIndex * ctx_r4.pageSize, (ctx_r4.pageIndex + 1) * ctx_r4.pageSize));
} }
const log = new _shared__WEBPACK_IMPORTED_MODULE_0__.Logger('Define-Collateral');
class DefineCollateralComponent {
    constructor(router, route, formBuilder, collateralService, collateralsDetails, toastr) {
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.collateralService = collateralService;
        this.collateralsDetails = collateralsDetails;
        this.toastr = toastr;
        // @ViewChild(PdfViewerComponent) private pdfComponent: PdfViewerComponent;
        this.handleType = ngx_drag_resize__WEBPACK_IMPORTED_MODULE_8__.NgxResizeHandleType;
        this.isEditable = false;
        this.isLoading = false;
        this.datalength = 0;
        this.textAreaBold = false;
        this.textAreaItalic = false;
        this.fonts$ = ['Arial', 'Verdana', 'Helvetica', 'Times New Roman', 'Georgia'];
        this.fontsize$ = [
            { Id: 1, name: 'Extra Small', size: 15 },
            { Id: 2, name: 'Small', size: 20 },
            { Id: 3, name: 'Medium', size: 25 },
            { Id: 4, name: 'Large', size: 30 },
            { Id: 5, name: 'Extra Large', size: 35 },
        ];
        this.applyToAllPages = true;
        this.isSetLogo = false;
        this.isSetText = false;
        this.isSetLogoAreaDisabled = false;
        this.isSetTextAreaDisabled = false;
        this.fileDetailsList = [];
        this.apiEndPoint = '';
        this.documentPageData = [];
        this.fileCordinatesList = [];
        this.RenderedAspectRatio = 0;
        this.pdfdragResizeDivHeight = 0;
        this.pdfAspectRatio = 0;
        this.videoAspectRatio = 0;
        this.totalPages = 0;
        this.page = 1;
        this.isLoaded = false;
        this.startTimeLogo = 0;
        this.endTimeLogo = 0;
        this.startTimeText = 0;
        this.endTimeText = 0;
        this.videoDurationTimeLine = 100;
        this.value = 0;
        this.highValue = 0;
        this.options = {
            floor: 0,
            ceil: this.videoDurationTimeLine,
        };
        var credentials = localStorage.getItem('credentials') || '';
        this.loggedUserId = JSON.parse(credentials).userLoggedIn.id;
        this.apiEndPoint = _environments_environment__WEBPACK_IMPORTED_MODULE_2__.environment.serverUrl;
        this.pageIndex = 0;
        this.pageSize = 1;
        this.route.queryParamMap.subscribe((params) => {
            this.queryparams = Object.assign(Object.assign({}, params.keys), params);
            var encodedString = this.queryparams.params._d;
            this.collateralId = atob(encodedString);
        });
        this.createForm();
    }
    createForm() {
        this.CollateralForm = this.formBuilder.group({
            collateralName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
            fontFamily: ['Helvetica', _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
            fontSize: [1, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
            maxCharacter: ['50', _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.required],
            applyToAllPages: [true],
        });
    }
    ngOnInit() {
        this.getCollateralById(this.collateralId);
        this.fileDetailsList = this.filesDetails();
        if (this.fileDetailsList !== undefined) {
            const type = this.fileDetailsList[0].fileData.mimetype;
            if (type == 'image/jpeg' || type == 'image/png' || type == 'image/jpg') {
                this.fileDetailsList.forEach((element) => {
                    const data = {
                        collateralId: parseInt(this.collateralId),
                        isSetLogoAreaDisabled: this.isSetLogoAreaDisabled,
                        isSetLogo: this.isSetLogo,
                        isSetTextAreaDisabled: this.isSetTextAreaDisabled,
                        isSetText: this.isSetText,
                        logoDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                        },
                        textAreaDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                            font: '',
                            fontSize: null,
                            fontSizeName: '',
                            bold: this.textAreaBold,
                            italic: this.textAreaItalic,
                            maxCharacter: null,
                        },
                    };
                    Object.assign(element, data);
                });
                this.datalength = this.fileDetailsList.length;
            }
            else if (type == 'application/pdf') {
                const fixedData = {
                    collateralId: parseInt(this.collateralId),
                };
                Object.assign(this.fileDetailsList[0], fixedData);
                this.fileDetailsList[0].pages.forEach((element) => {
                    const data = {
                        isSetLogoAreaDisabled: this.isSetLogoAreaDisabled,
                        isSetLogo: this.isSetLogo,
                        isSetTextAreaDisabled: this.isSetTextAreaDisabled,
                        isSetText: this.isSetText,
                        logoDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                        },
                        textAreaDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                            font: '',
                            fontSize: null,
                            fontSizeName: '',
                            bold: this.textAreaBold,
                            italic: this.textAreaItalic,
                            maxCharacter: null,
                        },
                    };
                    Object.assign(element, data);
                });
                this.datalength = this.fileDetailsList.length;
                this.documentPageData = this.fileDetailsList[0].pages;
            }
            else if (type == 'video/mp4') {
                this.fileDetailsList.forEach((element) => {
                    const data = {
                        collateralId: parseInt(this.collateralId),
                        isSetLogoAreaDisabled: this.isSetLogoAreaDisabled,
                        isSetLogo: this.isSetLogo,
                        isSetTextAreaDisabled: this.isSetTextAreaDisabled,
                        isSetText: this.isSetText,
                        logoDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                            videoDetails: {
                                height: null,
                                width: null,
                                startTime: null,
                                endTime: null,
                                duration: null,
                            },
                        },
                        textAreaDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                            font: '',
                            fontSize: null,
                            fontSizeName: '',
                            bold: this.textAreaBold,
                            italic: this.textAreaItalic,
                            maxCharacter: null,
                            videoDetails: {
                                height: null,
                                width: null,
                                startTime: null,
                                endTime: null,
                                duration: null,
                            },
                        },
                    };
                    Object.assign(element, data);
                });
                this.datalength = this.fileDetailsList.length;
            }
        }
        else {
            this.router.navigate(['/collateral/upload'], { queryParams: { _d: btoa(this.collateralId) }, replaceUrl: false });
        }
    }
    ngDoCheck() {
        if (this.collateralFileType == 'Image') {
            this.DOMLogoArea();
            this.DOMTextArea();
            const item = this.fileDetailsList[this.pageIndex];
            if (item.isSetLogoAreaDisabled == true && item.isSetLogo == true) {
                this.DOMSetLogoAreaStyle();
            }
            if (item.isSetTextAreaDisabled == true && item.isSetText == true) {
                this.DOMSetTextAreaStyle();
            }
        }
        else if (this.collateralFileType == 'Document') {
            this.DOMLogoArea();
            this.DOMTextArea();
            const item = this.documentPageData[this.pageIndex];
            if (item.isSetLogoAreaDisabled == true && item.isSetLogo == true) {
                this.DOMSetLogoAreaStyle();
            }
            if (item.isSetTextAreaDisabled == true && item.isSetText == true) {
                this.DOMSetTextAreaStyle();
            }
        }
        else if (this.collateralFileType == 'Video') {
            this.DOMLogoArea();
            this.DOMTextArea();
            const item = this.fileDetailsList[this.pageIndex];
            if (item.isSetLogoAreaDisabled == true && item.isSetLogo == true) {
                this.DOMSetLogoAreaStyle();
            }
            if (item.isSetTextAreaDisabled == true && item.isSetText == true) {
                this.DOMSetTextAreaStyle();
            }
        }
    }
    changeMedia() {
        this.isLoading = true;
        let fileList = [];
        this.fileDetailsList.forEach((element) => {
            fileList.push({
                fileId: element.fileData.id,
                imageName: element.fileData.imageName,
                thumnailName: element.fileData.thumnailName,
            });
        });
        this.collateralService.deleteMultipleFilesByPath({ fileList: fileList }).subscribe((data) => {
            this.isLoading = false;
            this.router.navigate(['/collateral/upload'], {
                queryParams: { _d: btoa(this.collateralId) },
                replaceUrl: false,
            });
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    calculatePDFRatio(page) {
        const pageWidth = this.documentPageData[page].width;
        const pageHeight = this.documentPageData[page].height;
        const pageRotation = this.documentPageData[page].rotation;
        const dragResizeDiv = document.getElementById('drag-resize-area');
        const pdfOuterDiv = document.getElementById('pdf-outer-div-id');
        if (dragResizeDiv != undefined) {
            this.dargResizedivWidth = dragResizeDiv.getBoundingClientRect().width;
            if (pageRotation == 0 || pageRotation == 180) {
                let divHeight = (pageHeight * this.dargResizedivWidth) / pageWidth;
                var styleData = 'height: ' + divHeight + 'px;';
                dragResizeDiv.setAttribute('style', styleData);
            }
            else {
                let divHeight = pageWidth / this.pdfAspectRatio;
                var styleData = 'height: ' + divHeight + 'px;';
                dragResizeDiv.setAttribute('style', styleData);
            }
        }
    }
    onChangeLogoTimeline(event) {
        let videoElement = document.getElementById('drag-resize-area');
        if (event.pointerType == 0) {
            videoElement.currentTime = event.value;
        }
        else {
            videoElement.currentTime = event.highValue;
        }
        this.startTimeLogo = event.value;
        this.endTimeLogo = event.highValue;
    }
    onChangeTextTimeline(event) {
        let videoElement = document.getElementById('drag-resize-area');
        if (event.pointerType == 0) {
            videoElement.currentTime = event.value;
        }
        else {
            videoElement.currentTime = event.highValue;
        }
        this.startTimeText = event.value;
        this.endTimeText = event.highValue;
    }
    calculateVideoRatio() {
        var _a, _b, _c;
        let videoWidth = (_a = document.getElementById('drag-resize-area')) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect().width;
        let videoHeight = (_b = document.getElementById('drag-resize-area')) === null || _b === void 0 ? void 0 : _b.getBoundingClientRect().height;
        let videoElement = document.getElementById('drag-resize-area');
        const newOptions = Object.assign({}, this.options);
        newOptions.ceil = videoElement.duration;
        this.options = newOptions;
        this.highValue = videoElement.duration;
        this.startTimeLogo = 0;
        this.endTimeLogo = this.highValue;
        this.startTimeText = 0;
        this.endTimeText = this.highValue;
        let videoFramWidth = videoElement.videoWidth;
        let videoFramHeight = videoElement.videoHeight;
        if (videoWidth != undefined) {
            this.videoAspectRatio = videoFramWidth / videoWidth;
        }
        let sliderPositionTop = videoHeight ? videoHeight - 90 : 0;
        let divId = 'video-slider';
        var styleData = 'position: absolute;width:100%;top: ' + sliderPositionTop + 'px;';
        (_c = document.getElementById(divId)) === null || _c === void 0 ? void 0 : _c.setAttribute('style', styleData);
    }
    filesDetails() {
        return this.collateralsDetails.sharedData;
    }
    getCollateralById(id) {
        this.isLoading = true;
        this.collateralService.getCollateralById({ id: id }).subscribe((data) => {
            this.isLoading = false;
            if (data.collateralName == null) {
                this.collateralName = 'Name the Collateral';
            }
            else {
                this.collateralName = data.collateralName;
            }
            this.collateralCategory = data.categoryName;
            this.collateralSubCategory = data.subcategoryName;
            this.collateralDate = moment__WEBPACK_IMPORTED_MODULE_1__(data.updatedAt).format('MMM DD, YYYY | HH:mm');
            this.collateralFileType = data.collateralType;
            this.applyToAllPages = this.collateralFileType == 'Document' ? true : false;
        }, (error) => {
            this.isLoading = false;
            this.toastr.error('Something went wrong, Please try again');
            console.log(error);
        });
    }
    editCollateralName() {
        this.isEditable = true;
    }
    saveCollateralName() {
        if (this.CollateralForm.controls.collateralName.value != null &&
            this.CollateralForm.controls.collateralName.value != '') {
            this.isLoading = true;
            const data = {
                id: this.collateralId,
                collateralName: this.CollateralForm.controls.collateralName.value,
            };
            this.collateralService.updateCollateralNamedById(data).subscribe((data) => {
                this.isLoading = false;
                this.isEditable = false;
                this.collateralName = data[1][0].collateralName;
                this.collateralDate = moment__WEBPACK_IMPORTED_MODULE_1__(data[1][0].updatedAt).format('MMM DD, YYYY | HH:mm');
            }, (error) => {
                this.isLoading = false;
                this.isEditable = false;
                this.toastr.error('Something went wrong, Please try again');
                console.log(error);
            });
        }
        else {
            this.isEditable = true;
        }
    }
    //PDF Viewer start
    nextPage() {
        this.page += 1;
        this.pageIndex += 1;
    }
    previousPage() {
        this.page -= 1;
        this.pageIndex -= 1;
    }
    afterLoadComplete(pdfData) {
        this.totalPages = pdfData.numPages;
        this.calculatePDFRatio(this.page - 1);
        this.isLoaded = true;
    }
    //PDF Viewer end
    //Image Start
    getPdfCanvasHtWd() {
        var _a;
        const pageWidth = this.documentPageData[this.pageIndex].width;
        const pageHeight = this.documentPageData[this.pageIndex].height;
        const pageRotation = this.documentPageData[this.pageIndex].rotation;
        const pdfViewer = document.getElementsByTagName('pdf-viewer')[0].getElementsByClassName('canvasWrapper')[0];
        this.pdfCanvasHeight = pdfViewer.scrollHeight;
        this.pdfCanvasWidth = pdfViewer.scrollWidth;
        if (pageRotation == 0 || pageRotation == 180) {
            this.pdfAspectRatio = pageWidth / this.pdfCanvasWidth;
        }
        else {
            this.pdfAspectRatio = pageHeight / this.pdfCanvasWidth;
        }
        let divId = 'pdfCanvas';
        var styleData = 'width: ' + this.pdfCanvasWidth + 'px;height: ' + this.pdfCanvasHeight + 'px; margin-left: 0px;';
        (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
        document
            .getElementsByTagName('pdf-viewer')[0]
            .getElementsByClassName('page')[0]
            .setAttribute('style', 'margin-left: 0px;' + styleData);
    }
    pdfPageRendered(e) {
        console.log('(page-rendered)', e);
        this.getPdfCanvasHtWd();
    }
    isSetLogoArea(pageIndex) {
        this.isSetLogo = true;
        this.isSetLogoAreaDisabled = false;
        if (this.collateralFileType == 'Image') {
            this.calulateRenderedAspectRatio();
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetLogoAreaDisabled: false,
                isSetLogo: true,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Document') {
            this.getPdfCanvasHtWd();
            const item = this.documentPageData[pageIndex];
            const data = {
                isSetLogoAreaDisabled: false,
                isSetLogo: true,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Video') {
            this.calculateVideoRatio();
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetLogoAreaDisabled: false,
                isSetLogo: true,
            };
            Object.assign(item, data);
        }
    }
    cancelLogoArea(pageIndex) {
        this.isSetLogo = false;
        this.isSetLogoAreaDisabled = true;
        this.logoX = '';
        this.logoY = '';
        this.logoWidth = '';
        this.logoHeight = '';
        if (this.collateralFileType == 'Image') {
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetLogoAreaDisabled: true,
                isSetLogo: false,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Document') {
            const item = this.documentPageData[pageIndex];
            const data = {
                isSetLogoAreaDisabled: true,
                isSetLogo: false,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Video') {
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetLogoAreaDisabled: true,
                isSetLogo: false,
            };
            Object.assign(item, data);
        }
    }
    fixLogoArea(file, pageIndex) {
        var _a, _b;
        this.isSetLogoAreaDisabled = true;
        if (this.collateralFileType == 'Image') {
            if (this.applyToAllPages == true) {
                this.mappingDataToAllPages();
            }
            else {
                const fileitem = this.fileDetailsList[pageIndex];
                const filedata = {
                    isSetLogoAreaDisabled: true,
                    logoDetails: {
                        x: this.logoX,
                        y: this.logoY,
                        width: this.logoWidth,
                        height: this.logoHeight,
                    },
                };
                Object.assign(fileitem, filedata);
            }
        }
        else if (this.collateralFileType == 'Document') {
            if (this.applyToAllPages == true) {
                this.mappingDataToAllPages();
            }
            else {
                const fileitem = this.documentPageData[pageIndex];
                const filedata = {
                    isSetLogoAreaDisabled: true,
                    logoDetails: {
                        x: this.logoX,
                        y: this.logoY,
                        width: this.logoWidth,
                        height: this.logoHeight,
                    },
                };
                Object.assign(fileitem, filedata);
            }
        }
        else if (this.collateralFileType == 'Video') {
            let videoHeight = (_a = document.getElementById('drag-resize-area')) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect().height;
            let videoWidth = (_b = document.getElementById('drag-resize-area')) === null || _b === void 0 ? void 0 : _b.getBoundingClientRect().width;
            let videoDurationObject = document.getElementById('drag-resize-area');
            let videoDuration = videoDurationObject.duration;
            let currentTimeObject = document.getElementById('drag-resize-area');
            let currentTime = currentTimeObject.currentTime;
            const fileitem = this.fileDetailsList[pageIndex];
            const filedata = {
                isSetLogoAreaDisabled: true,
                logoDetails: {
                    x: this.logoX,
                    y: this.logoY,
                    width: this.logoWidth,
                    height: this.logoHeight,
                    videoDetails: {
                        height: videoHeight,
                        width: videoWidth,
                        startTime: this.startTimeLogo,
                        endTime: this.endTimeLogo,
                        duration: videoDuration,
                    },
                },
            };
            Object.assign(fileitem, filedata);
        }
    }
    calulateRenderedAspectRatio() {
        if (this.collateralFileType == 'Image') {
            var xW = document.getElementById('drag-resize-area');
            let naturalWidth = xW.naturalWidth;
            var yW = document.getElementById('drag-resize-area');
            let convertedWidth = yW.width;
            this.RenderedAspectRatio = naturalWidth / convertedWidth;
        }
    }
    DOMLogoArea() {
        var _a, _b, _c, _e, _f, _g, _h, _j, _k, _l;
        if (this.collateralFileType == 'Image') {
            var DOM = (_a = document.getElementById('logo-area-' + this.pageIndex)) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect();
            var parentDOM = (_c = (_b = document.getElementById('logo-area-' + this.pageIndex)) === null || _b === void 0 ? void 0 : _b.parentElement) === null || _c === void 0 ? void 0 : _c.getBoundingClientRect();
            if (DOM && parentDOM) {
                this.logoX = ((DOM === null || DOM === void 0 ? void 0 : DOM.x) - parentDOM.x) * this.RenderedAspectRatio;
                this.logoY = ((DOM === null || DOM === void 0 ? void 0 : DOM.y) - parentDOM.y) * this.RenderedAspectRatio;
                this.logoWidth = (DOM === null || DOM === void 0 ? void 0 : DOM.width) * this.RenderedAspectRatio;
                this.logoHeight = (DOM === null || DOM === void 0 ? void 0 : DOM.height) * this.RenderedAspectRatio;
            }
        }
        if (this.collateralFileType == 'Video') {
            var DOM = (_e = document.getElementById('logo-area-' + this.pageIndex)) === null || _e === void 0 ? void 0 : _e.getBoundingClientRect();
            var parentDOM = (_g = (_f = document.getElementById('logo-area-' + this.pageIndex)) === null || _f === void 0 ? void 0 : _f.parentElement) === null || _g === void 0 ? void 0 : _g.getBoundingClientRect();
            if (DOM && parentDOM) {
                this.logoX = ((DOM === null || DOM === void 0 ? void 0 : DOM.x) - parentDOM.x) * this.videoAspectRatio;
                this.logoY = ((DOM === null || DOM === void 0 ? void 0 : DOM.y) - parentDOM.y) * this.videoAspectRatio;
                this.logoWidth = (DOM === null || DOM === void 0 ? void 0 : DOM.width) * this.videoAspectRatio;
                this.logoHeight = (DOM === null || DOM === void 0 ? void 0 : DOM.height) * this.videoAspectRatio;
            }
        }
        if (this.collateralFileType == 'Document') {
            var DOM = (_h = document.getElementById('logo-area-' + this.pageIndex)) === null || _h === void 0 ? void 0 : _h.getBoundingClientRect();
            var parentDOM = (_l = (_k = (_j = document
                .getElementById('logo-area-' + this.pageIndex)) === null || _j === void 0 ? void 0 : _j.parentElement) === null || _k === void 0 ? void 0 : _k.parentElement) === null || _l === void 0 ? void 0 : _l.getBoundingClientRect();
            if (DOM && parentDOM) {
                this.logoX = ((DOM === null || DOM === void 0 ? void 0 : DOM.x) - parentDOM.x) * this.pdfAspectRatio;
                this.logoY = ((DOM === null || DOM === void 0 ? void 0 : DOM.y) - parentDOM.y) * this.pdfAspectRatio;
                this.logoWidth = (DOM === null || DOM === void 0 ? void 0 : DOM.width) * this.pdfAspectRatio;
                this.logoHeight = (DOM === null || DOM === void 0 ? void 0 : DOM.height) * this.pdfAspectRatio;
                console.log(' dom x and y', this.logoX, this.logoY);
            }
        }
    }
    DOMSetLogoAreaStyle() {
        var _a, _b, _c;
        if (this.collateralFileType == 'Image') {
            //this.calulateRenderedAspectRatio();
            const item = this.fileDetailsList[this.pageIndex].logoDetails;
            let divId = 'logo-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.RenderedAspectRatio +
                'px;width: ' +
                item.width / this.RenderedAspectRatio +
                'px;top: ' +
                item.y / this.RenderedAspectRatio +
                'px;height: ' +
                item.height / this.RenderedAspectRatio +
                'px;';
            (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
        }
        else if (this.collateralFileType == 'Document') {
            const item = this.documentPageData[this.pageIndex].logoDetails;
            let divId = 'logo-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.pdfAspectRatio +
                'px;width: ' +
                item.width / this.pdfAspectRatio +
                'px;top: ' +
                item.y / this.pdfAspectRatio +
                'px;height: ' +
                item.height / this.pdfAspectRatio +
                'px;';
            (_b = document.getElementById(divId)) === null || _b === void 0 ? void 0 : _b.setAttribute('style', styleData);
        }
        else if (this.collateralFileType == 'Video') {
            const item = this.fileDetailsList[this.pageIndex].logoDetails;
            let divId = 'logo-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.videoAspectRatio +
                'px;width: ' +
                item.width / this.videoAspectRatio +
                'px;top: ' +
                item.y / this.videoAspectRatio +
                'px;height: ' +
                item.height / this.videoAspectRatio +
                'px;';
            (_c = document.getElementById(divId)) === null || _c === void 0 ? void 0 : _c.setAttribute('style', styleData);
        }
    }
    DOMTextArea() {
        var _a, _b, _c, _e, _f, _g, _h, _j, _k;
        if (this.collateralFileType == 'Image') {
            var DOM = (_a = document.getElementById('text-area-' + this.pageIndex)) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect();
            var parentDOM = (_c = (_b = document.getElementById('text-area-' + this.pageIndex)) === null || _b === void 0 ? void 0 : _b.parentElement) === null || _c === void 0 ? void 0 : _c.getBoundingClientRect();
            if (DOM && parentDOM) {
                this.textX = ((DOM === null || DOM === void 0 ? void 0 : DOM.x) - parentDOM.x) * this.RenderedAspectRatio;
                this.textY = ((DOM === null || DOM === void 0 ? void 0 : DOM.y) - parentDOM.y) * this.RenderedAspectRatio;
                this.textWidth = (DOM === null || DOM === void 0 ? void 0 : DOM.width) * this.RenderedAspectRatio;
                this.textHeight = (DOM === null || DOM === void 0 ? void 0 : DOM.height) * this.RenderedAspectRatio;
            }
        }
        if (this.collateralFileType == 'Video') {
            var DOM = (_e = document.getElementById('text-area-' + this.pageIndex)) === null || _e === void 0 ? void 0 : _e.getBoundingClientRect();
            var parentDOM = (_g = (_f = document.getElementById('text-area-' + this.pageIndex)) === null || _f === void 0 ? void 0 : _f.parentElement) === null || _g === void 0 ? void 0 : _g.getBoundingClientRect();
            if (DOM && parentDOM) {
                this.textX = ((DOM === null || DOM === void 0 ? void 0 : DOM.x) - parentDOM.x) * this.videoAspectRatio;
                this.textY = ((DOM === null || DOM === void 0 ? void 0 : DOM.y) - parentDOM.y) * this.videoAspectRatio;
                this.textWidth = (DOM === null || DOM === void 0 ? void 0 : DOM.width) * this.videoAspectRatio;
                this.textHeight = (DOM === null || DOM === void 0 ? void 0 : DOM.height) * this.videoAspectRatio;
            }
        }
        if (this.collateralFileType == 'Document') {
            var DOM = (_h = document.getElementById('text-area-' + this.pageIndex)) === null || _h === void 0 ? void 0 : _h.getBoundingClientRect();
            var parentDOM = (_k = (_j = document.getElementById('text-area-' + this.pageIndex)) === null || _j === void 0 ? void 0 : _j.parentElement) === null || _k === void 0 ? void 0 : _k.getBoundingClientRect();
            if (DOM && parentDOM) {
                this.textX = ((DOM === null || DOM === void 0 ? void 0 : DOM.x) - parentDOM.x) * this.pdfAspectRatio;
                this.textY = ((DOM === null || DOM === void 0 ? void 0 : DOM.y) - parentDOM.y) * this.pdfAspectRatio;
                this.textWidth = (DOM === null || DOM === void 0 ? void 0 : DOM.width) * this.pdfAspectRatio;
                this.textHeight = (DOM === null || DOM === void 0 ? void 0 : DOM.height) * this.pdfAspectRatio;
            }
        }
    }
    DOMSetTextAreaStyle() {
        var _a, _b, _c;
        if (this.collateralFileType == 'Image') {
            //this.calulateRenderedAspectRatio();
            const item = this.fileDetailsList[this.pageIndex].textAreaDetails;
            let divId = 'text-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.RenderedAspectRatio +
                'px;width: ' +
                item.width / this.RenderedAspectRatio +
                'px;top: ' +
                item.y / this.RenderedAspectRatio +
                'px;height: ' +
                item.height / this.RenderedAspectRatio +
                'px;';
            (_a = document.getElementById(divId)) === null || _a === void 0 ? void 0 : _a.setAttribute('style', styleData);
        }
        else if (this.collateralFileType == 'Document') {
            const item = this.documentPageData[this.pageIndex].textAreaDetails;
            let divId = 'text-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.pdfAspectRatio +
                'px;width: ' +
                item.width / this.pdfAspectRatio +
                'px;top: ' +
                item.y / this.pdfAspectRatio +
                'px;height: ' +
                item.height / this.pdfAspectRatio +
                'px;';
            (_b = document.getElementById(divId)) === null || _b === void 0 ? void 0 : _b.setAttribute('style', styleData);
        }
        else if (this.collateralFileType == 'Video') {
            const item = this.fileDetailsList[this.pageIndex].textAreaDetails;
            let divId = 'text-area-' + this.pageIndex;
            var styleData = 'position: absolute;left:' +
                item.x / this.videoAspectRatio +
                'px;width: ' +
                item.width / this.videoAspectRatio +
                'px;top: ' +
                item.y / this.videoAspectRatio +
                'px;height: ' +
                item.height / this.videoAspectRatio +
                'px;';
            (_c = document.getElementById(divId)) === null || _c === void 0 ? void 0 : _c.setAttribute('style', styleData);
        }
    }
    isSetTextArea(pageIndex) {
        this.isSetText = true;
        this.isSetTextAreaDisabled = false;
        this.textAreaBold = false;
        this.textAreaItalic = false;
        if (this.collateralFileType == 'Image') {
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetTextAreaDisabled: false,
                isSetText: true,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Document') {
            const item = this.documentPageData[pageIndex];
            const data = {
                isSetTextAreaDisabled: false,
                isSetText: true,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Video') {
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetTextAreaDisabled: false,
                isSetText: true,
            };
            Object.assign(item, data);
        }
    }
    cancelTextArea(pageIndex) {
        this.isSetText = false;
        this.isSetTextAreaDisabled = true;
        this.textX = '';
        this.textY = '';
        this.textWidth = '';
        this.textHeight = '';
        if (this.collateralFileType == 'Image') {
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetTextAreaDisabled: true,
                isSetText: false,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Document') {
            const item = this.documentPageData[pageIndex];
            const data = {
                isSetTextAreaDisabled: true,
                isSetText: false,
            };
            Object.assign(item, data);
        }
        else if (this.collateralFileType == 'Video') {
            const item = this.fileDetailsList[pageIndex];
            const data = {
                isSetTextAreaDisabled: true,
                isSetText: false,
            };
            Object.assign(item, data);
        }
    }
    fixTextArea(file, pageIndex) {
        var _a, _b;
        this.isSetTextAreaDisabled = true;
        if (this.collateralFileType == 'Image') {
            if (this.applyToAllPages == true) {
                this.mappingDataToAllPages();
            }
            else {
                const fileitem = this.fileDetailsList[pageIndex];
                const filedata = {
                    isSetTextAreaDisabled: true,
                    textAreaDetails: {
                        x: this.textX,
                        y: this.textY,
                        width: this.textWidth,
                        height: this.textHeight,
                        font: this.CollateralForm.controls.fontFamily.value,
                        fontSize: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].size,
                        fontSizeName: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].name,
                        bold: this.textAreaBold,
                        italic: this.textAreaItalic,
                        maxCharacter: this.CollateralForm.controls.maxCharacter.value,
                    },
                };
                Object.assign(fileitem, filedata);
            }
        }
        else if (this.collateralFileType == 'Document') {
            if (this.applyToAllPages == true) {
                this.mappingDataToAllPages();
            }
            else {
                const fileitem = this.documentPageData[pageIndex];
                const filedata = {
                    isSetTextAreaDisabled: true,
                    textAreaDetails: {
                        x: this.textX,
                        y: this.textY,
                        width: this.textWidth,
                        height: this.textHeight,
                        font: this.CollateralForm.controls.fontFamily.value,
                        fontSize: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].size,
                        fontSizeName: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].name,
                        bold: this.textAreaBold,
                        italic: this.textAreaItalic,
                        maxCharacter: this.CollateralForm.controls.maxCharacter.value,
                    },
                };
                Object.assign(fileitem, filedata);
            }
        }
        else if (this.collateralFileType == 'Video') {
            let videoHeight = (_a = document.getElementById('drag-resize-area')) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect().height;
            let videoWidth = (_b = document.getElementById('drag-resize-area')) === null || _b === void 0 ? void 0 : _b.getBoundingClientRect().width;
            let videoDurationObject = document.getElementById('drag-resize-area');
            let videoDuration = videoDurationObject.duration;
            let currentTimeObject = document.getElementById('drag-resize-area');
            let currentTime = currentTimeObject.currentTime;
            const fileitem = this.fileDetailsList[pageIndex];
            const filedata = {
                isSetTextAreaDisabled: true,
                textAreaDetails: {
                    x: this.textX,
                    y: this.textY,
                    width: this.textWidth,
                    height: this.textHeight,
                    font: this.CollateralForm.controls.fontFamily.value,
                    fontSize: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].size,
                    fontSizeName: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].name,
                    bold: this.textAreaBold,
                    italic: this.textAreaItalic,
                    maxCharacter: this.CollateralForm.controls.maxCharacter.value,
                    videoDetails: {
                        height: videoHeight,
                        width: videoWidth,
                        startTime: this.startTimeText,
                        endTime: this.endTimeText,
                        duration: videoDuration,
                    },
                },
            };
            Object.assign(fileitem, filedata);
        }
    }
    //Image End
    isBold() {
        if (this.textAreaBold == false) {
            this.textAreaBold = true;
        }
        else {
            this.textAreaBold = false;
        }
    }
    isItalic() {
        if (this.textAreaItalic == false) {
            this.textAreaItalic = true;
        }
        else {
            this.textAreaItalic = false;
        }
    }
    mappingDataToAllPages() {
        var _a, _b;
        if (this.collateralFileType == 'Image') {
            if (this.applyToAllPages == true) {
                this.fileDetailsList.forEach((element) => {
                    let textSizeName;
                    if (this.CollateralForm.controls.fontSize.value >= 1) {
                        textSizeName = this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].name;
                    }
                    else {
                        textSizeName = '';
                    }
                    const filedata = {
                        isSetLogo: this.isSetLogo,
                        isSetText: this.isSetText,
                        isSetLogoAreaDisabled: this.isSetLogoAreaDisabled,
                        isSetTextAreaDisabled: this.isSetTextAreaDisabled,
                        logoDetails: {
                            x: this.logoX,
                            y: this.logoY,
                            width: this.logoWidth,
                            height: this.logoHeight,
                        },
                        textAreaDetails: {
                            x: this.textX,
                            y: this.textY,
                            width: this.textWidth,
                            height: this.textHeight,
                            font: this.CollateralForm.controls.fontFamily.value,
                            fontSize: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].size,
                            fontSizeName: textSizeName,
                            bold: this.textAreaBold,
                            italic: this.textAreaItalic,
                            maxCharacter: this.CollateralForm.controls.maxCharacter.value,
                        },
                    };
                    Object.assign(element, filedata);
                });
            }
        }
        else if (this.collateralFileType == 'Document') {
            if (this.applyToAllPages == true) {
                this.documentPageData.forEach((element) => {
                    let textSizeName;
                    if (this.CollateralForm.controls.fontSize.value >= 1) {
                        textSizeName = this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].name;
                    }
                    else {
                        textSizeName = '';
                    }
                    const filedata = {
                        isSetLogo: this.isSetLogo,
                        isSetText: this.isSetText,
                        isSetLogoAreaDisabled: this.isSetLogoAreaDisabled,
                        isSetTextAreaDisabled: this.isSetTextAreaDisabled,
                        logoDetails: {
                            x: this.logoX,
                            y: this.logoY,
                            width: this.logoWidth,
                            height: this.logoHeight,
                        },
                        textAreaDetails: {
                            x: this.textX,
                            y: this.textY,
                            width: this.textWidth,
                            height: this.textHeight,
                            font: this.CollateralForm.controls.fontFamily.value,
                            fontSize: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].size,
                            fontSizeName: textSizeName,
                            bold: this.textAreaBold,
                            italic: this.textAreaItalic,
                            maxCharacter: this.CollateralForm.controls.maxCharacter.value,
                        },
                    };
                    Object.assign(element, filedata);
                });
            }
        }
        else if (this.collateralFileType == 'Video') {
            if (this.applyToAllPages == true) {
                let videoHeight = (_a = document.getElementById('drag-resize-area')) === null || _a === void 0 ? void 0 : _a.getBoundingClientRect().height;
                let videoWidth = (_b = document.getElementById('drag-resize-area')) === null || _b === void 0 ? void 0 : _b.getBoundingClientRect().width;
                let videoDurationObject = document.getElementById('drag-resize-area');
                let videoDuration = videoDurationObject.duration;
                let currentTimeObject = document.getElementById('drag-resize-area');
                let currentTime = currentTimeObject.currentTime;
                this.fileDetailsList.forEach((element) => {
                    let textSizeName;
                    if (this.CollateralForm.controls.fontSize.value >= 1) {
                        textSizeName = this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].name;
                    }
                    else {
                        textSizeName = '';
                    }
                    const filedata = {
                        isSetLogo: this.isSetLogo,
                        isSetText: this.isSetText,
                        isSetLogoAreaDisabled: this.isSetLogoAreaDisabled,
                        isSetTextAreaDisabled: this.isSetTextAreaDisabled,
                        logoDetails: {
                            x: this.logoX,
                            y: this.logoY,
                            width: this.logoWidth,
                            height: this.logoHeight,
                            videoDetails: {
                                height: videoHeight,
                                width: videoWidth,
                                startTime: 0,
                                endTime: null,
                                duration: null,
                            },
                        },
                        textAreaDetails: {
                            x: this.textX,
                            y: this.textY,
                            width: this.textWidth,
                            height: this.textHeight,
                            font: this.CollateralForm.controls.fontFamily.value,
                            fontSize: this.fontsize$[this.CollateralForm.controls.fontSize.value - 1].size,
                            fontSizeName: textSizeName,
                            bold: this.textAreaBold,
                            italic: this.textAreaItalic,
                            maxCharacter: this.CollateralForm.controls.maxCharacter.value,
                            videoDetails: {
                                height: videoHeight,
                                width: videoWidth,
                                startTime: 0,
                                endTime: currentTime,
                                duration: videoDuration,
                            },
                        },
                    };
                    Object.assign(element, filedata);
                });
            }
        }
    }
    deleteLogoArea(pageIndex) {
        if (this.collateralFileType == 'Image') {
            if (this.applyToAllPages == true) {
                this.fileDetailsList.forEach((element) => {
                    const data = {
                        isSetLogoAreaDisabled: false,
                        isSetLogo: false,
                        logoDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                        },
                    };
                    Object.assign(element, data);
                });
            }
            else {
                const element = this.fileDetailsList[pageIndex];
                const data = {
                    isSetLogoAreaDisabled: false,
                    isSetLogo: false,
                    logoDetails: {
                        x: null,
                        y: null,
                        width: null,
                        height: null,
                    },
                };
                Object.assign(element, data);
            }
        }
        else if (this.collateralFileType == 'Document') {
            if (this.applyToAllPages == true) {
                this.documentPageData.forEach((element) => {
                    const data = {
                        isSetLogoAreaDisabled: false,
                        isSetLogo: false,
                        logoDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                        },
                    };
                    Object.assign(element, data);
                });
            }
            else {
                const element = this.documentPageData[pageIndex];
                const data = {
                    isSetLogoAreaDisabled: false,
                    isSetLogo: false,
                    logoDetails: {
                        x: null,
                        y: null,
                        width: null,
                        height: null,
                    },
                };
                Object.assign(element, data);
            }
        }
        else if (this.collateralFileType == 'Video') {
            if (this.applyToAllPages == true) {
                this.fileDetailsList.forEach((element) => {
                    const data = {
                        isSetLogoAreaDisabled: false,
                        isSetLogo: false,
                        logoDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                        },
                    };
                    Object.assign(element, data);
                });
            }
            else {
                const element = this.fileDetailsList[pageIndex];
                const data = {
                    isSetLogoAreaDisabled: false,
                    isSetLogo: false,
                    logoDetails: {
                        x: null,
                        y: null,
                        width: null,
                        height: null,
                        videoDetails: {
                            height: null,
                            width: null,
                            startTime: null,
                            endTime: null,
                            duration: null,
                        },
                    },
                };
                Object.assign(element, data);
            }
        }
    }
    deleteTextArea(pageIndex) {
        if (this.collateralFileType == 'Image') {
            if (this.applyToAllPages == true) {
                this.fileDetailsList.forEach((element) => {
                    const data = {
                        isSetTextAreaDisabled: false,
                        isSetText: false,
                        textAreaDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                            font: '',
                            fontSize: null,
                            fontSizeName: '',
                            bold: false,
                            italic: false,
                            maxCharacter: null,
                        },
                    };
                    Object.assign(element, data);
                });
            }
            else {
                const element = this.fileDetailsList[pageIndex];
                const data = {
                    isSetTextAreaDisabled: false,
                    isSetText: false,
                    textAreaDetails: {
                        x: null,
                        y: null,
                        width: null,
                        height: null,
                        font: '',
                        fontSize: null,
                        fontSizeName: '',
                        bold: false,
                        italic: false,
                        maxCharacter: null,
                    },
                };
                Object.assign(element, data);
            }
        }
        else if (this.collateralFileType == 'Document') {
            if (this.applyToAllPages == true) {
                this.documentPageData.forEach((element) => {
                    const data = {
                        isSetTextAreaDisabled: false,
                        isSetText: false,
                        textAreaDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                            font: '',
                            fontSize: null,
                            fontSizeName: '',
                            bold: false,
                            italic: false,
                            maxCharacter: null,
                        },
                    };
                    Object.assign(element, data);
                });
            }
            else {
                const element = this.documentPageData[pageIndex];
                const data = {
                    isSetTextAreaDisabled: false,
                    isSetText: false,
                    textAreaDetails: {
                        x: null,
                        y: null,
                        width: null,
                        height: null,
                        font: '',
                        fontSize: null,
                        fontSizeName: '',
                        bold: false,
                        italic: false,
                        maxCharacter: null,
                    },
                };
                Object.assign(element, data);
            }
        }
        else if (this.collateralFileType == 'Video') {
            if (this.applyToAllPages == true) {
                this.fileDetailsList.forEach((element) => {
                    const data = {
                        isSetTextAreaDisabled: false,
                        isSetText: false,
                        textAreaDetails: {
                            x: null,
                            y: null,
                            width: null,
                            height: null,
                            font: '',
                            fontSize: null,
                            fontSizeName: '',
                            bold: false,
                            italic: false,
                            maxCharacter: null,
                            videoDetails: {
                                height: null,
                                width: null,
                                startTime: null,
                                endTime: null,
                                duration: null,
                            },
                        },
                    };
                    Object.assign(element, data);
                });
            }
            else {
                const element = this.fileDetailsList[pageIndex];
                const data = {
                    isSetTextAreaDisabled: false,
                    isSetText: false,
                    textAreaDetails: {
                        x: null,
                        y: null,
                        width: null,
                        height: null,
                        font: '',
                        fontSize: null,
                        fontSizeName: '',
                        bold: false,
                        italic: false,
                        maxCharacter: null,
                    },
                };
                Object.assign(element, data);
            }
        }
    }
    isAppliedToAllPages() {
        if (this.applyToAllPages == true) {
            this.applyToAllPages = false;
        }
        else {
            this.applyToAllPages = true;
            this.mappingDataToAllPages();
        }
    }
    checkBeforeSubmit() {
        if (this.collateralFileType == 'Image') {
            const fileList = this.fileDetailsList;
            let isCorrect = true;
            fileList.forEach((element) => {
                if (element.isSetLogo == false ||
                    element.isSetText == false ||
                    element.isSetLogoAreaDisabled == false ||
                    element.isSetTextAreaDisabled == false) {
                    isCorrect = false;
                    return isCorrect;
                }
                else {
                    return isCorrect;
                }
            });
            if (isCorrect) {
                this.SubmitCollateral();
            }
            else {
                this.toastr.error('Please Set Logo Area And Text Area');
            }
        }
        else if (this.collateralFileType == 'Document') {
            const fileList = this.documentPageData;
            let isCorrect = true;
            fileList.forEach((element) => {
                if (element.isSetLogo == false ||
                    element.isSetText == false ||
                    element.isSetLogoAreaDisabled == false ||
                    element.isSetTextAreaDisabled == false) {
                    isCorrect = false;
                    return isCorrect;
                }
                else {
                    return isCorrect;
                }
            });
            if (isCorrect) {
                this.SubmitCollateral();
            }
            else {
                this.toastr.error('Please Set Logo Area And Text Area');
            }
        }
        else if (this.collateralFileType == 'Video') {
            const fileList = this.fileDetailsList;
            let isCorrect = true;
            fileList.forEach((element) => {
                if (element.isSetLogo == false ||
                    element.isSetText == false ||
                    element.isSetLogoAreaDisabled == false ||
                    element.isSetTextAreaDisabled == false) {
                    isCorrect = false;
                    return isCorrect;
                }
                else {
                    return isCorrect;
                }
            });
            if (isCorrect) {
                this.SubmitCollateral();
            }
            else {
                this.toastr.error('Please Set Logo Area And Text Area');
            }
        }
    }
    SubmitCollateral() {
        this.isLoading = true;
        if (this.collateralFileType == 'Image') {
            let finalSubmit = [];
            const fileList = this.fileDetailsList;
            fileList.forEach((element) => {
                finalSubmit.push({
                    collateralId: element.collateralId,
                    fileId: element.fileData.fileId,
                    createdBy: this.loggedUserId,
                    updatedBy: this.loggedUserId,
                    watermarkDetails: [
                        {
                            logo: {
                                logoXcordinate: element.logoDetails.x,
                                logoYcordinate: element.logoDetails.y,
                                logoWidthcordinate: element.logoDetails.width,
                                logoHeightcordinate: element.logoDetails.height,
                            },
                            text: {
                                textAreaXcordinate: element.textAreaDetails.x,
                                textAreaYcordinate: element.textAreaDetails.y,
                                textAreaWidthcordinate: element.textAreaDetails.width,
                                textAreaHeightcordinate: element.textAreaDetails.height,
                                textAreaFontText: element.textAreaDetails.font,
                                textAreaFontSize: element.textAreaDetails.fontSize,
                                textAreaBold: element.textAreaDetails.bold,
                                textAreaItalic: element.textAreaDetails.italic,
                                maxCharacter: element.textAreaDetails.maxCharacter,
                            },
                        },
                    ],
                    thumnailName: element.fileData.thumnailName,
                    imageName: element.fileData.imageName,
                });
            });
            this.collateralService.submitCollateral(finalSubmit).subscribe((data) => {
                this.isLoading = false;
                this.router.navigate(['/collateral'], { replaceUrl: false });
            }, (error) => {
                this.isLoading = false;
                this.toastr.error('Something went wrong, Please try again');
                console.log(error);
            });
        }
        else if (this.collateralFileType == 'Document') {
            let finalSubmit = [];
            const fileList = this.fileDetailsList;
            const documentPageDataList = this.documentPageData;
            documentPageDataList.forEach((element) => {
                delete element.isSetLogo;
                delete element.isSetLogoAreaDisabled;
                delete element.isSetText;
                delete element.isSetTextAreaDisabled;
            });
            fileList.forEach((element) => {
                finalSubmit.push({
                    collateralId: element.collateralId,
                    fileId: element.fileData.fileId,
                    createdBy: this.loggedUserId,
                    updatedBy: this.loggedUserId,
                    watermarkDetails: documentPageDataList,
                    thumnailName: element.fileData.thumnailName,
                    imageName: element.fileData.imageName,
                });
            });
            this.collateralService.submitCollateral(finalSubmit).subscribe((data) => {
                this.isLoading = false;
                this.router.navigate(['/collateral'], { replaceUrl: false });
            }, (error) => {
                this.isLoading = false;
                this.toastr.error('Something went wrong, Please try again');
                console.log(error);
            });
        }
        else if (this.collateralFileType == 'Video') {
            let finalSubmit = [];
            const fileList = this.fileDetailsList;
            fileList.forEach((element) => {
                finalSubmit.push({
                    collateralId: element.collateralId,
                    fileId: element.fileData.fileId,
                    createdBy: this.loggedUserId,
                    updatedBy: this.loggedUserId,
                    watermarkDetails: [
                        {
                            logo: {
                                logoXcordinate: element.logoDetails.x,
                                logoYcordinate: element.logoDetails.y,
                                logoWidthcordinate: element.logoDetails.width,
                                logoHeightcordinate: element.logoDetails.height,
                                videoDetails: element.logoDetails.videoDetails,
                            },
                            text: {
                                textAreaXcordinate: element.textAreaDetails.x,
                                textAreaYcordinate: element.textAreaDetails.y,
                                textAreaWidthcordinate: element.textAreaDetails.width,
                                textAreaHeightcordinate: element.textAreaDetails.height,
                                textAreaFontText: element.textAreaDetails.font,
                                textAreaFontSize: element.textAreaDetails.fontSize,
                                textAreaBold: element.textAreaDetails.bold,
                                textAreaItalic: element.textAreaDetails.italic,
                                maxCharacter: element.textAreaDetails.maxCharacter,
                                videoDetails: element.textAreaDetails.videoDetails,
                            },
                        },
                    ],
                    thumnailName: element.fileData.thumnailName,
                    imageName: element.fileData.imageName,
                });
            });
            this.collateralService.submitCollateral(finalSubmit).subscribe((data) => {
                this.isLoading = false;
                this.router.navigate(['/collateral'], { replaceUrl: false });
            }, (error) => {
                this.isLoading = false;
                this.toastr.error('Something went wrong, Please try again');
                console.log(error);
            });
        }
    }
}
DefineCollateralComponent.ɵfac = function DefineCollateralComponent_Factory(t) { return new (t || DefineCollateralComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_10__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_10__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_3__.CollateralService), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](_app_shared_collaterals_files_directive__WEBPACK_IMPORTED_MODULE_4__.CollateralsDetails), _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_11__.ToastrService)); };
DefineCollateralComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineComponent"]({ type: DefineCollateralComponent, selectors: [["app-define-collateral"]], decls: 69, vars: 11, consts: [[1, "container-fluid"], [1, "jumbotron", "text-center"], [3, "isLoading"], [3, "formGroup"], [1, "row"], [1, "col-md-6"], [4, "ngIf"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange", 4, "ngIf"], [1, "col-md-6", "d-flex", "align-items-center", "justify-content-end"], ["type", "button", 1, "btn", "btn-primary", 3, "click"], [1, "fas", "fa-exchange-alt"], [1, "col-md-12"], [1, "collateral-name"], [1, "collateral-catergory"], [1, "collateral-date", "d-flex", "align-items-center", "justify-content-end"], [1, "btn", "paginator-button", 3, "disabled", "click"], [1, "fas", "fa-angle-left", "paginator-icon"], ["disabled", "", 1, "btn", 2, "width", "100px"], [1, "fas", "fa-angle-right", "paginator-icon"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange"], [4, "ngFor", "ngForOf"], ["class", "row", 4, "ngFor", "ngForOf"], ["id", "pdf-outer-div-id", 1, "pdf-outer-div"], ["id", "drag-resize-area", 1, "pdf-position"], ["id", "pdfCanvas"], [3, "src", "show-all", "page", "original-size", "render-text", "zoom-scale", "page-rendered", "after-load-complete"], ["class", "rect offset-small move", "ngxDrag", "", "ngxDragPositionStrategy", "relative", "ngxDragBoundary", "#pdfCanvas", "ngxResize", "", "ngxResizeBoundary", "#pdfCanvas", 3, "id", "ngxResizeMinWidth", "ngxResizeMinHeight", "ngClass", 4, "ngIf"], ["class", "row", 4, "ngIf"], ["class", "col-md-12", 4, "ngIf"], ["ngxDrag", "", "ngxDragPositionStrategy", "relative", "ngxDragBoundary", "#pdfCanvas", "ngxResize", "", "ngxResizeBoundary", "#pdfCanvas", 1, "rect", "offset-small", "move", 3, "id", "ngxResizeMinWidth", "ngxResizeMinHeight", "ngClass"], [1, "drag-box-text"], [1, "far", "fa-file-image", "drag-box-icon"], ["data-resize-handle", "TopLeft", 3, "ngxResizeHandle"], ["data-resize-handle", "Top", 3, "ngxResizeHandle"], ["data-resize-handle", "TopRight", 3, "ngxResizeHandle"], ["data-resize-handle", "Right", 3, "ngxResizeHandle"], ["data-resize-handle", "BottomRight", 3, "ngxResizeHandle"], ["data-resize-handle", "Bottom", 3, "ngxResizeHandle"], ["data-resize-handle", "BottomLeft", 3, "ngxResizeHandle"], ["data-resize-handle", "Left", 3, "ngxResizeHandle"], [1, "drag-box-icon"], ["type", "button", 1, "btn", "btn-primary", "set-button", 3, "click"], [1, "heading"], [1, "far", "fa-trash-alt", 3, "click"], [1, "sub-heading"], [1, "sub-value"], [1, "col-md-3"], [1, "col-md-9"], ["formControlName", "fontFamily"], [3, "value", 4, "ngFor", "ngForOf"], ["formControlName", "fontSize"], ["mdbCheckbox", "", "type", "checkbox", "id", "btn-check-bold", "autocomplete", "off", 1, "btn-check", 3, "click"], ["for", "btn-check-bold", 1, "btn", "btn-secondary"], ["mdbCheckbox", "", "type", "checkbox", "id", "btn-check-italic", "autocomplete", "off", 1, "btn-check", 3, "click"], ["for", "btn-check-italic", 1, "btn", "btn-secondary"], ["type", "number", "accept", "number", "formControlName", "maxCharacter", "autocomplete", "off", 1, "form-control", 2, "width", "30%", 3, "placeholder"], [3, "value"], ["formControlName", "applyToAllPages", "mdbCheckbox", "", "type", "checkbox", "id", "checkboxNoLabel", "value", "", "aria-label", "...", 1, "form-check-input", 3, "change"], [1, "area"], [1, "single-file"], ["id", "drag-resize-area", "width", "100%", 3, "src", "load", 4, "ngIf"], ["class", "rect offset-small move", "ngxDrag", "", "ngxDragPositionStrategy", "relative", "ngxDragBoundary", "#drag-resize-area", "ngxResize", "", "ngxResizeBoundary", "#drag-resize-area", 3, "id", "ngxResizeMinWidth", "ngxResizeMinHeight", "ngClass", 4, "ngIf"], ["id", "drag-resize-area", "width", "100%", 3, "src", "load"], ["ngxDrag", "", "ngxDragPositionStrategy", "relative", "ngxDragBoundary", "#drag-resize-area", "ngxResize", "", "ngxResizeBoundary", "#drag-resize-area", 1, "rect", "offset-small", "move", 3, "id", "ngxResizeMinWidth", "ngxResizeMinHeight", "ngClass"], ["id", "drag-resize-area", "width", "100%", "controls", ""], ["type", "video/mp4", 3, "src"], ["id", "video-slider"], [3, "value", "highValue", "options", "valueChange", "highValueChange", "userChange", 4, "ngIf"], [3, "value", "highValue", "options", "valueChange", "highValueChange", "userChange"], [1, "font-weight-bold", "text-black", "d-block"]], template: function DefineCollateralComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](4, "app-loader", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](5, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](6, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](7, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](8, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](9, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](10, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](12, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](13, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](14, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](15, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](16, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](17, DefineCollateralComponent_div_17_Template, 15, 4, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](18, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](19, DefineCollateralComponent_pager_19_Template, 1, 3, "pager", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](20, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](21, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](22, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](23, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](24, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵlistener"]("click", function DefineCollateralComponent_Template_button_click_24_listener() { return ctx.changeMedia(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](25, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelement"](26, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](27, " Change file\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](28, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](29, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](30, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](31, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](32, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](33, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](34, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](35, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](36, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](37, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](38, "label", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](39);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](40, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](41, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](42, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](43, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](44, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](45, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](46, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](47, "label", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](48);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](49, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](50, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](51, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](52, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementStart"](53, "label", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](54);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](55, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](56, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](57, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](58, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](59, "\n\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](60, DefineCollateralComponent_div_60_Template, 4, 1, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](61, "\n\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](62, DefineCollateralComponent_div_62_Template, 5, 5, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](63, "\n\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtemplate"](64, DefineCollateralComponent_div_64_Template, 5, 5, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](65, "\n\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](66, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](67, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtext"](68, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("isLoading", ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("formGroup", ctx.CollateralForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.datalength >= 1 && ctx.collateralFileType == "Image");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate"](ctx.collateralName);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate2"](" ", ctx.collateralCategory, " | ", ctx.collateralSubCategory, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵtextInterpolate1"](" ", ctx.collateralDate, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Image");
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵproperty"]("ngIf", ctx.collateralFileType == "Video");
    } }, directives: [_shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_5__.LoaderComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormGroupDirective, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgIf, _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_6__.PagerComponent, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgForOf, ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_13__.PdfViewerComponent, ngx_drag_resize__WEBPACK_IMPORTED_MODULE_8__.NgxDragDirective, ngx_drag_resize__WEBPACK_IMPORTED_MODULE_8__.NgxResizeDirective, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgClass, ngx_drag_resize__WEBPACK_IMPORTED_MODULE_8__.NgxResizeHandleDirective, _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_14__.NgSelectComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormControlName, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.NumberValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.DefaultValueAccessor, _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_14__["ɵr"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__.CheckboxControlValueAccessor, _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_15__["ɵa"]], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_12__.SlicePipe, _angular_common__WEBPACK_IMPORTED_MODULE_12__.DecimalPipe], styles: [".logo[_ngcontent-%COMP%] {\n  width: 100px;\n}\n\n.pdf-position[_ngcontent-%COMP%] {\n  position: relative;\n  width: 100%;\n  height: 100%;\n}\n\n.pdf-outer-div[_ngcontent-%COMP%] {\n  height: 720px;\n  width: 100%;\n}\n\n.disable-div[_ngcontent-%COMP%] {\n  pointer-events: none;\n}\n\n.enable-div[_ngcontent-%COMP%] {\n  pointer-events: all;\n}\n\n.app-title[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #ffffff;\n}\n\n.area[_ngcontent-%COMP%] {\n  position: relative;\n  height: 200px;\n  width: 100%;\n  border-radius: 3px;\n  background: rgba(211, 211, 211, 0.25);\n}\n\n.area.large[_ngcontent-%COMP%] {\n  height: 500px;\n}\n\n.rect[_ngcontent-%COMP%] {\n  width: 100px;\n  height: 100px;\n  box-shadow: inset 0 0 1px darkgrey, 0 0 2px rgba(0, 0, 0, 0.8);\n  opacity: 0.5;\n  background: rgba(56, 51, 51, 0.58);\n  border: 3px solid #ffffff;\n  box-sizing: border-box;\n}\n\n.rect.medium[_ngcontent-%COMP%] {\n  height: 200px;\n  width: 200px;\n}\n\n.rect.large[_ngcontent-%COMP%] {\n  height: 250px;\n  width: 250px;\n}\n\n.drag-box-text[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  text-align: center;\n  color: #ffffff;\n}\n\n.drag-box-icon[_ngcontent-%COMP%] {\n  color: #fff !important;\n}\n\n.move[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n\n.handle[_ngcontent-%COMP%] {\n  padding: 8px;\n  display: inline-flex;\n}\n\n.content-inside[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n[data-resize-handle][_ngcontent-%COMP%] {\n  position: absolute;\n  -webkit-user-select: none;\n          user-select: none;\n  z-index: 3;\n}\n\n[data-resize-handle=Top][_ngcontent-%COMP%], [data-resize-handle=Right][_ngcontent-%COMP%] {\n  top: 0;\n  right: 0;\n}\n\n[data-resize-handle=Bottom][_ngcontent-%COMP%], [data-resize-handle=Left][_ngcontent-%COMP%] {\n  bottom: 0;\n  left: 0;\n}\n\n[data-resize-handle=Top][_ngcontent-%COMP%], [data-resize-handle=Bottom][_ngcontent-%COMP%] {\n  width: 100%;\n  height: 10px;\n  cursor: ns-resize;\n}\n\n[data-resize-handle=Right][_ngcontent-%COMP%], [data-resize-handle=Left][_ngcontent-%COMP%] {\n  height: 100%;\n  width: 10px;\n  cursor: ew-resize;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%], [data-resize-handle=TopRight][_ngcontent-%COMP%], [data-resize-handle=BottomLeft][_ngcontent-%COMP%], [data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  width: 10px;\n  height: 10px;\n  z-index: 4;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%], [data-resize-handle=TopRight][_ngcontent-%COMP%] {\n  top: 0;\n}\n\n[data-resize-handle=BottomLeft][_ngcontent-%COMP%], [data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  bottom: 0;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%], [data-resize-handle=BottomLeft][_ngcontent-%COMP%] {\n  left: 0;\n}\n\n[data-resize-handle=TopRight][_ngcontent-%COMP%], [data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  right: 0;\n}\n\n[data-resize-handle=TopLeft][_ngcontent-%COMP%] {\n  cursor: nw-resize;\n}\n\n[data-resize-handle=TopRight][_ngcontent-%COMP%] {\n  cursor: ne-resize;\n}\n\n[data-resize-handle=BottomLeft][_ngcontent-%COMP%] {\n  cursor: sw-resize;\n}\n\n[data-resize-handle=BottomRight][_ngcontent-%COMP%] {\n  cursor: se-resize;\n}\n\n.offset-small[_ngcontent-%COMP%], .offset-large[_ngcontent-%COMP%] {\n  position: absolute;\n}\n\n.offset-small[_ngcontent-%COMP%] {\n  top: 16px;\n  left: 16px;\n}\n\n.offset-large[_ngcontent-%COMP%] {\n  left: 100px;\n  top: 50px;\n}\n\n.offset-center-medium[_ngcontent-%COMP%] {\n  left: calc(50% - 100px);\n  top: calc(50% - 100px);\n}\n\n.set-button[_ngcontent-%COMP%] {\n  width: 50%;\n}\n\n.collateral-name[_ngcontent-%COMP%] {\n  height: 36px;\n  left: 1015px;\n  top: 102px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 28px;\n  line-height: 36px;\n  letter-spacing: 0.02em;\n  color: #484848;\n}\n\n.collateral-catergory[_ngcontent-%COMP%] {\n  width: 152px;\n  height: 18px;\n  left: 1015px;\n  top: 150px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 14px;\n  line-height: 18px;\n  letter-spacing: 0.02em;\n  color: #adadad;\n}\n\n.collateral-date[_ngcontent-%COMP%] {\n  height: 15px;\n  top: 152px;\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 12px;\n  line-height: 15px;\n  color: #353030;\n  padding-right: 40px;\n  padding-top: 10px;\n}\n\n.heading[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 18px;\n  line-height: 23px;\n  color: #000000;\n}\n\n.sub-heading[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 18px;\n  color: #bbbbbb;\n}\n\n.sub-value[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 14px;\n  line-height: 18px;\n  color: #383333;\n}\n\n.paginator-icon[_ngcontent-%COMP%] {\n  color: #484848;\n}\n\n.paginator-button[_ngcontent-%COMP%] {\n  color: #fff;\n  background-color: white;\n  border-color: white;\n}\n\nvideo[_ngcontent-%COMP%]::-webkit-media-controls-timeline {\n  display: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRlZmluZS1jb2xsYXRlcmFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQUNGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtFQUNBLFdBQUE7QUFDRjs7QUFFQTtFQUNFLG9CQUFBO0FBQ0Y7O0FBRUE7RUFDRSxtQkFBQTtBQUNGOztBQUtFO0VBQ0UsY0FBQTtBQUZKOztBQU1BO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EscUNBQUE7QUFIRjs7QUFLRTtFQUNFLGFBQUE7QUFISjs7QUFPQTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBR0EsOERBQUE7RUFFQSxZQUFBO0VBRUEsa0NBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0FBUkY7O0FBVUU7RUFDRSxhQWxDVTtFQW1DVixZQW5DVTtBQTJCZDs7QUFXRTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FBVEo7O0FBYUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUVBLGtCQUFBO0VBQ0EsY0FBQTtBQVhGOztBQWNBO0VBRUUsc0JBQUE7QUFaRjs7QUFlQTtFQUNFLGVBQUE7QUFaRjs7QUFlQTtFQUNFLFlBQUE7RUFDQSxvQkFBQTtBQVpGOztBQWVBO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFaRjs7QUFpQkE7RUFDRSxrQkFBQTtFQUNBLHlCQUFBO1VBQUEsaUJBQUE7RUFDQSxVQUFBO0FBZEY7O0FBaUJBOztFQUVFLE1BQUE7RUFDQSxRQUFBO0FBZEY7O0FBaUJBOztFQUVFLFNBQUE7RUFDQSxPQUFBO0FBZEY7O0FBaUJBOztFQUVFLFdBQUE7RUFDQSxZQXZCaUI7RUF3QmpCLGlCQUFBO0FBZEY7O0FBaUJBOztFQUVFLFlBQUE7RUFDQSxXQTlCaUI7RUErQmpCLGlCQUFBO0FBZEY7O0FBaUJBOzs7O0VBSUUsV0F0Q2lCO0VBdUNqQixZQXZDaUI7RUF3Q2pCLFVBQUE7QUFkRjs7QUFpQkE7O0VBRUUsTUFBQTtBQWRGOztBQWlCQTs7RUFFRSxTQUFBO0FBZEY7O0FBaUJBOztFQUVFLE9BQUE7QUFkRjs7QUFpQkE7O0VBRUUsUUFBQTtBQWRGOztBQWlCQTtFQUNFLGlCQUFBO0FBZEY7O0FBaUJBO0VBQ0UsaUJBQUE7QUFkRjs7QUFpQkE7RUFDRSxpQkFBQTtBQWRGOztBQWlCQTtFQUNFLGlCQUFBO0FBZEY7O0FBaUJBOztFQUVFLGtCQUFBO0FBZEY7O0FBaUJBO0VBQ0UsU0FBQTtFQUNBLFVBQUE7QUFkRjs7QUFpQkE7RUFDRSxXQUFBO0VBQ0EsU0FBQTtBQWRGOztBQWlCQTtFQUNFLHVCQUFBO0VBQ0Esc0JBQUE7QUFkRjs7QUFpQkE7RUFDRSxVQUFBO0FBZEY7O0FBaUJBO0VBRUUsWUFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FBZkY7O0FBa0JBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtBQWZGOztBQWtCQTtFQUVFLFlBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUFoQkY7O0FBbUJBO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQWhCRjs7QUFtQkE7RUFDRSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBaEJGOztBQW1CQTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFoQkY7O0FBbUJBO0VBQ0UsY0FBQTtBQWhCRjs7QUFtQkE7RUFDRSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQWhCRjs7QUE2QkE7RUFDRSx3QkFBQTtBQTFCRiIsImZpbGUiOiJkZWZpbmUtY29sbGF0ZXJhbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dvIHtcclxuICB3aWR0aDogMTAwcHg7XHJcbn1cclxuXHJcbi5wZGYtcG9zaXRpb24ge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5wZGYtb3V0ZXItZGl2IHtcclxuICBoZWlnaHQ6IDcyMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZGlzYWJsZS1kaXYge1xyXG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG59XHJcblxyXG4uZW5hYmxlLWRpdiB7XHJcbiAgcG9pbnRlci1ldmVudHM6IGFsbDtcclxufVxyXG5cclxuJG1lZGl1bS1yZWN0OiAyMDBweDtcclxuXHJcbi5hcHAtdGl0bGUge1xyXG4gIGEge1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgfVxyXG59XHJcblxyXG4uYXJlYSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGhlaWdodDogMjAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMjExLCAyMTEsIDIxMSwgMC4yNSk7XHJcblxyXG4gICYubGFyZ2Uge1xyXG4gICAgaGVpZ2h0OiA1MDBweDtcclxuICB9XHJcbn1cclxuXHJcbi5yZWN0IHtcclxuICB3aWR0aDogMTAwcHg7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICAvLyBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgLy9ib3JkZXI6IGRhc2hlZDtcclxuICBib3gtc2hhZG93OiBpbnNldCAwIDAgMXB4IGRhcmtncmV5LCAwIDAgMnB4IHJnYmEoMCwgMCwgMCwgMC44KTtcclxuICAvL2JhY2tncm91bmQ6ICNmOWY5Zjk7XHJcbiAgb3BhY2l0eTogMC41O1xyXG5cclxuICBiYWNrZ3JvdW5kOiByZ2JhKDU2LCA1MSwgNTEsIDAuNTgpO1xyXG4gIGJvcmRlcjogM3B4IHNvbGlkICNmZmZmZmY7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuXHJcbiAgJi5tZWRpdW0ge1xyXG4gICAgaGVpZ2h0OiAkbWVkaXVtLXJlY3Q7XHJcbiAgICB3aWR0aDogJG1lZGl1bS1yZWN0O1xyXG4gIH1cclxuXHJcbiAgJi5sYXJnZSB7XHJcbiAgICBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgd2lkdGg6IDI1MHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmRyYWctYm94LXRleHQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICAvL2xpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuLmRyYWctYm94LWljb24ge1xyXG4gIC8vZm9udC1zaXplOiA0MHB4O1xyXG4gIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tb3ZlIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5oYW5kbGUge1xyXG4gIHBhZGRpbmc6IDhweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxufVxyXG5cclxuLmNvbnRlbnQtaW5zaWRlIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbiRoYW5kbGUtdGhpY2tuZXNzOiAxMHB4O1xyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZV0ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICB6LWluZGV4OiAzO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiVG9wXCJdLFxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiUmlnaHRcIl0ge1xyXG4gIHRvcDogMDtcclxuICByaWdodDogMDtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkJvdHRvbVwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkxlZnRcIl0ge1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiAwO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiVG9wXCJdLFxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiQm90dG9tXCJdIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6ICRoYW5kbGUtdGhpY2tuZXNzO1xyXG4gIGN1cnNvcjogbnMtcmVzaXplO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiUmlnaHRcIl0sXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJMZWZ0XCJdIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgd2lkdGg6ICRoYW5kbGUtdGhpY2tuZXNzO1xyXG4gIGN1cnNvcjogZXctcmVzaXplO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiVG9wTGVmdFwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcFJpZ2h0XCJdLFxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiQm90dG9tTGVmdFwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkJvdHRvbVJpZ2h0XCJdIHtcclxuICB3aWR0aDogJGhhbmRsZS10aGlja25lc3M7XHJcbiAgaGVpZ2h0OiAkaGFuZGxlLXRoaWNrbmVzcztcclxuICB6LWluZGV4OiA0O1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiVG9wTGVmdFwiXSxcclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcFJpZ2h0XCJdIHtcclxuICB0b3A6IDA7XHJcbn1cclxuXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJCb3R0b21MZWZ0XCJdLFxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiQm90dG9tUmlnaHRcIl0ge1xyXG4gIGJvdHRvbTogMDtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcExlZnRcIl0sXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJCb3R0b21MZWZ0XCJdIHtcclxuICBsZWZ0OiAwO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiVG9wUmlnaHRcIl0sXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJCb3R0b21SaWdodFwiXSB7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuXHJcbltkYXRhLXJlc2l6ZS1oYW5kbGU9XCJUb3BMZWZ0XCJdIHtcclxuICBjdXJzb3I6IG53LXJlc2l6ZTtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIlRvcFJpZ2h0XCJdIHtcclxuICBjdXJzb3I6IG5lLXJlc2l6ZTtcclxufVxyXG5cclxuW2RhdGEtcmVzaXplLWhhbmRsZT1cIkJvdHRvbUxlZnRcIl0ge1xyXG4gIGN1cnNvcjogc3ctcmVzaXplO1xyXG59XHJcblxyXG5bZGF0YS1yZXNpemUtaGFuZGxlPVwiQm90dG9tUmlnaHRcIl0ge1xyXG4gIGN1cnNvcjogc2UtcmVzaXplO1xyXG59XHJcblxyXG4ub2Zmc2V0LXNtYWxsLFxyXG4ub2Zmc2V0LWxhcmdlIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuXHJcbi5vZmZzZXQtc21hbGwge1xyXG4gIHRvcDogMTZweDtcclxuICBsZWZ0OiAxNnB4O1xyXG59XHJcblxyXG4ub2Zmc2V0LWxhcmdlIHtcclxuICBsZWZ0OiAxMDBweDtcclxuICB0b3A6IDUwcHg7XHJcbn1cclxuXHJcbi5vZmZzZXQtY2VudGVyLW1lZGl1bSB7XHJcbiAgbGVmdDogY2FsYyg1MCUgLSAjeyRtZWRpdW0tcmVjdCAvIDJ9KTtcclxuICB0b3A6IGNhbGMoNTAlIC0gI3skbWVkaXVtLXJlY3QgLyAyfSk7XHJcbn1cclxuXHJcbi5zZXQtYnV0dG9uIHtcclxuICB3aWR0aDogNTAlO1xyXG59XHJcblxyXG4uY29sbGF0ZXJhbC1uYW1lIHtcclxuICAvL3dpZHRoOiAyMDRweDtcclxuICBoZWlnaHQ6IDM2cHg7XHJcbiAgbGVmdDogMTAxNXB4O1xyXG4gIHRvcDogMTAycHg7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDI4cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDM2cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDJlbTtcclxuICBjb2xvcjogIzQ4NDg0ODtcclxufVxyXG5cclxuLmNvbGxhdGVyYWwtY2F0ZXJnb3J5IHtcclxuICB3aWR0aDogMTUycHg7XHJcbiAgaGVpZ2h0OiAxOHB4O1xyXG4gIGxlZnQ6IDEwMTVweDtcclxuICB0b3A6IDE1MHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBSYWpkaGFuaTtcclxuICBmb250LXN0eWxlOiBub3JtYWw7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDJlbTtcclxuICBjb2xvcjogI2FkYWRhZDtcclxufVxyXG5cclxuLmNvbGxhdGVyYWwtZGF0ZSB7XHJcbiAgLy93aWR0aDogMTAxcHg7XHJcbiAgaGVpZ2h0OiAxNXB4O1xyXG4gIHRvcDogMTUycHg7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBsaW5lLWhlaWdodDogMTVweDtcclxuICBjb2xvcjogIzM1MzAzMDtcclxuICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uaGVhZGluZyB7XHJcbiAgZm9udC1mYW1pbHk6IFJhamRoYW5pO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogNjAwO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBsaW5lLWhlaWdodDogMjNweDtcclxuICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG5cclxuLnN1Yi1oZWFkaW5nIHtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIGNvbG9yOiAjYmJiYmJiO1xyXG59XHJcblxyXG4uc3ViLXZhbHVlIHtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIGNvbG9yOiAjMzgzMzMzO1xyXG59XHJcblxyXG4ucGFnaW5hdG9yLWljb24ge1xyXG4gIGNvbG9yOiAjNDg0ODQ4O1xyXG59XHJcblxyXG4ucGFnaW5hdG9yLWJ1dHRvbiB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLy8gVmlkZW8gY29udHJvbHMgaGlkZSBzdGFydFxyXG4vLyB2aWRlbzo6LXdlYmtpdC1tZWRpYS1jb250cm9scy1wbGF5LWJ1dHRvbiB7XHJcbi8vICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4vLyB9XHJcbi8vIHZpZGVvOjotd2Via2l0LW1lZGlhLWNvbnRyb2xzLXZvbHVtZS1zbGlkZXIge1xyXG4vLyBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLy8gdmlkZW86Oi13ZWJraXQtbWVkaWEtY29udHJvbHMtbXV0ZS1idXR0b24ge1xyXG4vLyBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxudmlkZW86Oi13ZWJraXQtbWVkaWEtY29udHJvbHMtdGltZWxpbmUge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG4vLyB2aWRlbzo6LXdlYmtpdC1tZWRpYS1jb250cm9scy1jdXJyZW50LXRpbWUtZGlzcGxheXtcclxuLy8gICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuXHJcbi8vIFZpZGVvIGNvbnRyb2xzIGhpZGUgZW5kXHJcbiJdfQ== */"] });


/***/ }),

/***/ 2662:
/*!*****************************************************************************!*\
  !*** ./src/app/collateral/upload-collateral/upload-collateral.component.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadCollateralComponent": () => (/* binding */ UploadCollateralComponent)
/* harmony export */ });
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ 6738);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _app_shared_constant__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @app/@shared/constant */ 3341);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ 2340);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-toastr */ 9699);
/* harmony import */ var _app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @app/rest/collateral-api.service */ 2690);
/* harmony import */ var _app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @app/rest/files-api.service */ 8226);
/* harmony import */ var _shared_collaterals_files_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../@shared/collaterals.files.directive */ 9284);
/* harmony import */ var _shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../@shared/loader/loader.component */ 9967);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ng2-pdf-viewer */ 3621);
/* harmony import */ var _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../@shared/pagination/pager.component */ 4454);

















function UploadCollateralComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "span", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](4, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](5, "i", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function UploadCollateralComponent_div_15_Template_i_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r5.editCollateralName(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"](" ", ctx_r0.collateralName, " ");
} }
function UploadCollateralComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](2, "input", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "i", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function UploadCollateralComponent_div_17_Template_i_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r7.saveCollateralName(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](8, "i", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function UploadCollateralComponent_div_17_Template_i_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r8); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r9.cancelCollateralName(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](10, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("placeholder", "Enter Collateral Name");
} }
function UploadCollateralComponent_div_37_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "Uploading ...");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](4, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](5, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](7, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](10, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵstyleProp"]("width", ctx_r2.progress, "%");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"]("\n                ", ctx_r2.progress, " %\n              ");
} }
function UploadCollateralComponent_label_39_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "label", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("fileDropped", function UploadCollateralComponent_label_39_Template_label_fileDropped_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r11.onFileDropped($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "input", 25, 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("change", function UploadCollateralComponent_label_39_Template_input_change_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r12); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](); return ctx_r13.fileBrowseHandler($event.target); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](4, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](5, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](7, "i", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](9, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](10, "Drag and drop one/multiple images, or ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](11, "label", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "Browse");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](14, "span", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](15, "High resolution images (png,jpg,jpeg) is recommended. Max 10MB each");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](16, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](17, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} }
function UploadCollateralComponent_div_41_div_2_img_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](0, "img", 39);
} if (rf & 2) {
    const file_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r18.apiEndPoint + file_r16.path, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function UploadCollateralComponent_div_41_div_2_img_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](0, "img", 39);
} if (rf & 2) {
    const file_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r19.apiEndPoint + file_r16.path, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function UploadCollateralComponent_div_41_div_2_img_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](0, "img", 39);
} if (rf & 2) {
    const file_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r20.apiEndPoint + file_r16.path, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function UploadCollateralComponent_div_41_div_2_img_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](0, "img", 39);
} if (rf & 2) {
    const file_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r21.apiEndPoint + file_r16.thumnailPath, _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsanitizeUrl"]);
} }
function UploadCollateralComponent_div_41_div_2_div_12_Template(rf, ctx) { if (rf & 1) {
    const _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "pdf-viewer", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("after-load-complete", function UploadCollateralComponent_div_41_div_2_div_12_Template_pdf_viewer_after_load_complete_4_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r29); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r28.afterLoadComplete($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("src", ctx_r22.apiEndPoint + file_r16.path)("show-all", false)("page", ctx_r22.page)("original-size", false)("fit-to-page", false)("render-text", false);
} }
function UploadCollateralComponent_div_41_div_2_div_24_pager_4_Template(rf, ctx) { if (rf & 1) {
    const _r34 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "pager", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("pageIndexChange", function UploadCollateralComponent_div_41_div_2_div_24_pager_4_Template_pager_pageIndexChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r34); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4); return ctx_r33.pageIndex = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("itemCount", ctx_r31.datalength)("pageIndex", ctx_r31.pageIndex)("pageSize", ctx_r31.pageSize);
} }
function UploadCollateralComponent_div_41_div_2_div_24_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "button", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function UploadCollateralComponent_div_41_div_2_div_24_div_6_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r36); const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4); return ctx_r35.previousPage(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](4, "i", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](7, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](10, "button", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function UploadCollateralComponent_div_41_div_2_div_24_div_6_Template_button_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r36); const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4); return ctx_r37.nextPage(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, "\n                      ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](12, "i", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](15, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("disabled", ctx_r32.page === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate2"]("", ctx_r32.page, " / ", ctx_r32.totalPages, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("disabled", ctx_r32.totalPages === 1 || ctx_r32.page === ctx_r32.totalPages);
} }
function UploadCollateralComponent_div_41_div_2_div_24_Template(rf, ctx) { if (rf & 1) {
    const _r39 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](4, UploadCollateralComponent_div_41_div_2_div_24_pager_4_Template, 1, 3, "pager", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](6, UploadCollateralComponent_div_41_div_2_div_24_div_6_Template, 16, 4, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](9, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](10, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](11, "i", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function UploadCollateralComponent_div_41_div_2_div_24_Template_i_click_11_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r39); const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r38.deleteFile(ctx_r38.pageIndex); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]().$implicit;
    const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r23.datalength >= 1 && file_r16.mimetype != "application/pdf");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", file_r16.mimetype == "application/pdf");
} }
function UploadCollateralComponent_div_41_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](4, UploadCollateralComponent_div_41_div_2_img_4_Template, 1, 1, "img", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](6, UploadCollateralComponent_div_41_div_2_img_6_Template, 1, 1, "img", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](8, UploadCollateralComponent_div_41_div_2_img_8_Template, 1, 1, "img", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](9, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](10, UploadCollateralComponent_div_41_div_2_img_10_Template, 1, 1, "img", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](11, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](12, UploadCollateralComponent_div_41_div_2_div_12_Template, 8, 6, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](13, "\n\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](14, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](15, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](16, "h4", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](18, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](19, "p", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](21, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](22, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](23, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](24, UploadCollateralComponent_div_41_div_2_div_24_Template, 14, 2, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](25, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const file_r16 = ctx.$implicit;
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", file_r16.mimetype == "image/jpeg");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", file_r16.mimetype == "image/jpg");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", file_r16.mimetype == "image/png");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", file_r16.mimetype == "video/mp4");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", file_r16.mimetype == "application/pdf");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"]("\n                    ", file_r16 == null ? null : file_r16.originalFileName, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"]("\n                    ", ctx_r14.formatBytes(file_r16 == null ? null : file_r16.size), "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r14.progress == 0);
} }
function UploadCollateralComponent_div_41_div_5_div_18_Template(rf, ctx) { if (rf & 1) {
    const _r44 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "button", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("click", function UploadCollateralComponent_div_41_div_5_div_18_Template_button_click_2_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r44); const ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](3); return ctx_r43.saveCollateralFiles(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                  Define Content Area ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](4, "i", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} }
function UploadCollateralComponent_div_41_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("fileDropped", function UploadCollateralComponent_div_41_div_5_Template_div_fileDropped_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r46); const ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r45.onFileDropped($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](4, "span", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](6, "label", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](7, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](8, "input", 54, 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵlistener"]("change", function UploadCollateralComponent_div_41_div_5_Template_input_change_8_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵrestoreView"](_r46); const ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2); return ctx_r47.fileBrowseHandler($event.target); });
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](10, "\n                    ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](11, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "Upload more file ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](13, "i", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n                  ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](15, "\n                ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](16, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](17, "\n              ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](18, UploadCollateralComponent_div_41_div_5_div_18_Template, 7, 0, "div", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](19, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r15.progress == 0);
} }
function UploadCollateralComponent_div_41_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](2, UploadCollateralComponent_div_41_div_2_Template, 26, 8, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpipe"](3, "slice");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](4, "\n\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](5, UploadCollateralComponent_div_41_div_5_Template, 20, 1, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵpipeBind3"](3, 2, ctx_r4.imageDetailsList, ctx_r4.pageIndex * ctx_r4.pageSize, (ctx_r4.pageIndex + 1) * ctx_r4.pageSize));
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx_r4.files.length > 0 && ctx_r4.progress == 0);
} }
const log = new _shared__WEBPACK_IMPORTED_MODULE_0__.Logger('Upload-Collateral');
class UploadCollateralComponent {
    constructor(router, route, formBuilder, toastr, collateralService, fileService, collateralsDetails) {
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.toastr = toastr;
        this.collateralService = collateralService;
        this.fileService = fileService;
        this.collateralsDetails = collateralsDetails;
        this.isEditable = false;
        this.isLoading = false;
        this.datalength = 0;
        this.apiEndPoint = '';
        this.files = [];
        //imageURLList: any = [];
        this.formData = new FormData();
        this.progress = 0;
        this.imageDetailsList = [];
        this.totalPages = 0;
        this.page = 1;
        this.isLoaded = false;
        this.PDFData = [];
        this.apiEndPoint = _environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.serverUrl;
        this.pageIndex = 0;
        this.pageSize = 1;
        var credentials = localStorage.getItem('credentials') || '';
        this.LoggedInUserId = JSON.parse(credentials).userLoggedIn.id;
        this.route.queryParamMap.subscribe((params) => {
            this.queryparams = Object.assign(Object.assign({}, params.keys), params);
            var encodedString = this.queryparams.params._d;
            this.collateralId = atob(encodedString);
        });
        this.createForm();
    }
    ngOnInit() {
        this.getCollateralById(this.collateralId);
    }
    getCollateralById(id) {
        this.isLoading = true;
        this.collateralService.getCollateralById({ id: id }).subscribe((data) => {
            this.isLoading = false;
            if (data.collateralName == null) {
                this.collateralName = 'Name the Collateral';
            }
            else {
                this.collateralName = data.collateralName;
            }
            this.collateralCategory = data.categoryName;
            this.collateralSubCategory = data.subcategoryName;
            this.collateralDate = moment__WEBPACK_IMPORTED_MODULE_1__(data.updatedAt).format('MMM DD, YYYY | HH:mm');
            this.collateralFileType = data.collateralType;
        }, (error) => {
            this.isLoading = false;
            console.log(error);
        });
    }
    editCollateralName() {
        this.isEditable = true;
    }
    cancelCollateralName() {
        this.isEditable = false;
    }
    saveCollateralName() {
        if (this.CollateralForm.controls.collateralName.value != null &&
            this.CollateralForm.controls.collateralName.value != '') {
            this.collateralName = this.CollateralForm.controls.collateralName.value;
            const data = {
                id: this.collateralId,
                collateralName: this.CollateralForm.controls.collateralName.value,
            };
            this.collateralService.updateCollateralNamedById(data).subscribe((data) => {
                this.isLoading = false;
                this.isEditable = false;
                this.collateralName = data[1][0].collateralName;
                this.collateralDate = moment__WEBPACK_IMPORTED_MODULE_1__(data[1][0].updatedAt).format('MMM DD, YYYY | HH:mm');
            }, (error) => {
                this.isLoading = false;
                this.isEditable = false;
                console.log(error);
            });
        }
        else {
            this.isEditable = true;
            this.toastr.error('Please Enter Collateral Name');
        }
    }
    createForm() {
        this.CollateralForm = this.formBuilder.group({
            collateralName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required],
        });
    }
    //File Upload Starts
    onFileDropped($event) {
        this.prepareFilesList($event);
    }
    fileBrowseHandler(files) {
        this.prepareFilesList(files.files);
    }
    deleteFile(index) {
        const fileId = this.imageDetailsList[index].id;
        const imageName = this.imageDetailsList[index].imageName;
        const thumnailName = this.imageDetailsList[index].thumnailName;
        this.isLoading = true;
        this.fileService.deleteFilesByPath({ fileId: fileId, imageName: imageName, thumnailName: thumnailName }).subscribe((data) => {
            this.isLoading = false;
            this.files.splice(index, 1);
            //this.imageURLList.splice(index, 1);
            this.imageDetailsList.splice(index, 1);
            this.datalength = this.imageDetailsList.length;
        }, (error) => {
            this.isLoading = false;
            console.log(error);
        });
    }
    prepareFilesList(files) {
        this.formData = new FormData();
        var model = {
            userId: this.LoggedInUserId,
        };
        for (const item of files) {
            var extn = item.name.substr(item.name.lastIndexOf('.'), item.name.length).toLowerCase();
            if (this.collateralFileType == 'Image') {
                if (!_app_shared_constant__WEBPACK_IMPORTED_MODULE_2__.ValidImageFileExtensions.includes(extn)) {
                    this.toastr.error('Please select valid image file for upload');
                    break;
                }
                this.files.push(item);
                this.formData.append('files', item);
            }
            else if (this.collateralFileType == 'Video') {
                if (!_app_shared_constant__WEBPACK_IMPORTED_MODULE_2__.ValidVideoFileExtensions.includes(extn)) {
                    this.toastr.error('Please select valid video file for upload');
                    break;
                }
                this.files.push(item);
                this.formData.append('files', item);
            }
            else if (this.collateralFileType == 'Document') {
                if (!_app_shared_constant__WEBPACK_IMPORTED_MODULE_2__.ValidDocumentFileExtensions.includes(extn)) {
                    this.toastr.error('Please select valid document file for upload');
                    break;
                }
                this.files.push(item);
                this.formData.append('files', item);
            }
        }
        for (var key in model) {
            this.formData.append(key, model[key] == null || model[key] == undefined ? '' : model[key]);
        }
        this.fileService.uploadFiles(this.formData).subscribe((event) => {
            switch (event.type) {
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_11__.HttpEventType.UploadProgress:
                    if (event.total != undefined) {
                        this.progress = Math.round((event.loaded / event.total) * 100);
                    }
                    break;
                case _angular_common_http__WEBPACK_IMPORTED_MODULE_11__.HttpEventType.Response:
                    this.isLoading = false;
                    this.progress = 0;
                    this.preview(event.body.data);
            }
        }, (error) => {
            console.log(error);
        });
    }
    formatBytes(bytes, decimals) {
        if (bytes === 0) {
            return '0 Bytes';
        }
        const k = 1024;
        const dm = decimals <= 0 ? 0 : decimals || 2;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
    fileData(value) {
        this.collateralsDetails.sharedData = value;
    }
    preview(files) {
        for (let x of files) {
            //this.imageURLList.push(x.thumnailPath.slice(6));
            this.imageDetailsList.push(x);
        }
        this.datalength = this.imageDetailsList.length;
    }
    //File Upload Ends
    saveCollateralFiles() {
        if (this.collateralName == 'Name the Collateral') {
            this.toastr.error('Please Enter Collateral Name');
        }
        else {
            let sharedData = [];
            if (this.collateralFileType == 'Image' || this.collateralFileType == 'Video') {
                this.imageDetailsList.forEach((element) => {
                    sharedData.push({
                        fileData: element,
                    });
                });
            }
            else if (this.collateralFileType == 'Document') {
                this.imageDetailsList.forEach((element) => {
                    sharedData.push({
                        fileData: element,
                    });
                });
                Object.assign(sharedData[0], { pages: this.PDFData });
            }
            this.fileData(sharedData);
            this.router.navigate(['/collateral/define'], { queryParams: { _d: btoa(this.collateralId) }, replaceUrl: false });
        }
    }
    //PDF Viewer start
    nextPage() {
        this.page += 1;
    }
    previousPage() {
        this.page -= 1;
    }
    afterLoadComplete(pdfData) {
        this.totalPages = pdfData.numPages;
        this.isLoaded = true;
        for (let i = 1; i <= this.totalPages; i++) {
            pdfData.getPage(i).then((result) => {
                this.PDFData.push({
                    pageIndex: i,
                    rotation: result._pageInfo.rotate,
                    width: result._pageInfo.view[2],
                    height: result._pageInfo.view[3],
                });
            });
        }
    }
}
UploadCollateralComponent.ɵfac = function UploadCollateralComponent_Factory(t) { return new (t || UploadCollateralComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_12__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_12__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_13__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_app_rest_collateral_api_service__WEBPACK_IMPORTED_MODULE_4__.CollateralService), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_app_rest_files_api_service__WEBPACK_IMPORTED_MODULE_5__.FilesService), _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdirectiveInject"](_shared_collaterals_files_directive__WEBPACK_IMPORTED_MODULE_6__.CollateralsDetails)); };
UploadCollateralComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdefineComponent"]({ type: UploadCollateralComponent, selectors: [["app-upload-collateral"]], decls: 48, vars: 10, consts: [[1, "container-fluid"], [1, "jumbotron", "text-center"], [3, "isLoading"], ["enctype", "multipart/form-data", 3, "formGroup"], [1, "row", "justify-content-center"], [1, "col-md-8"], [1, "details", "p-4"], [4, "ngIf"], [1, "row"], [1, "col-md-6"], [1, "sub-text"], [1, "col-md-6", "d-flex", "justify-content-end"], [1, "date-text"], ["class", "viewer progress-loader-alignment", 4, "ngIf"], ["class", "drop-area d-flex align-items-center justify-content-center", "for", "fileDropRef", "appDnd", "", 3, "fileDropped", 4, "ngIf"], [1, "main-text"], [1, "fas", "fa-edit", 3, "click"], ["type", "text", "formControlName", "collateralName", "autocomplete", "off", 2, "width", "40%", 3, "placeholder"], [1, "fas", "fa-check-circle", 3, "click"], [1, "fas", "fa-times", 3, "click"], [1, "viewer", "progress-loader-alignment"], [1, "uploading-text"], [1, "progress", "form-group"], ["role", "progressbar", 1, "progress-bar", "progress-bar-striped", "bg-success"], ["for", "fileDropRef", "appDnd", "", 1, "drop-area", "d-flex", "align-items-center", "justify-content-center", 3, "fileDropped"], ["type", "file", "name", "fileData", "id", "fileDropRef", "multiple", "", 3, "change"], ["fileDropRef", ""], [1, "far", "fa-file-image", "icon-color", "image-icon"], ["for", "fileDropRef"], ["class", "single-file pb-2", 4, "ngFor", "ngForOf"], ["class", "row", 4, "ngIf"], [1, "single-file", "pb-2"], [1, "viewer"], ["height", "100%", 3, "src", 4, "ngIf"], ["class", "pdf-outer-div", 4, "ngIf"], [1, "info", 2, "display", "none"], [1, "name"], [1, "size"], ["class", "row p-2", 4, "ngIf"], ["height", "100%", 3, "src"], [1, "pdf-outer-div"], [1, "pdf-position"], [3, "src", "show-all", "page", "original-size", "fit-to-page", "render-text", "after-load-complete"], [1, "row", "p-2"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange", 4, "ngIf"], [1, "col-md-6", "d-flex", "align-items-center", "justify-content-end"], [1, "far", "fa-trash-alt", "icon-color", "btn", "delete-button", 3, "click"], [3, "itemCount", "pageIndex", "pageSize", "pageIndexChange"], [1, "btn", "paginator-button", 3, "disabled", "click"], [1, "fas", "fa-angle-left", "paginator-icon"], ["disabled", "", 1, "btn", 2, "width", "100px"], [1, "fas", "fa-angle-right", "paginator-icon"], ["appDnd", "", 1, "col-md-6", 3, "fileDropped"], [1, "btn", "btn-outline-primary", "btn-dotted"], ["type", "file", "id", "fileDropRef", "multiple", "", "hidden", "", 3, "change"], [1, "fas", "fa-plus"], ["class", "col-md-6 d-flex align-items-center justify-content-end", 4, "ngIf"], ["type", "button", 1, "btn", "btn-primary", 3, "click"], [1, "ms-1", "fas", "fa-arrow-right", "btn-icon"]], template: function UploadCollateralComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelement"](4, "app-loader", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](5, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](6, "\n\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](7, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](8, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](9, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](10, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](12, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](13, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](14, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](15, UploadCollateralComponent_div_15_Template, 7, 1, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](16, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](17, UploadCollateralComponent_div_17_Template, 11, 1, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](18, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](19, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](20, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](21, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](22, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](23, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](24);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](25, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](26, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](27, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](28, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](29, "span", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](30);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](31, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](32, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](33, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](34, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementStart"](35, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](36, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](37, UploadCollateralComponent_div_37_Template, 11, 3, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](38, "\n\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](39, UploadCollateralComponent_label_39_Template, 18, 0, "label", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](40, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtemplate"](41, UploadCollateralComponent_div_41_Template, 7, 6, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](42, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](43, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](44, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](45, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](46, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtext"](47, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("isLoading", ctx.isLoading);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("formGroup", ctx.CollateralForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", !ctx.isEditable);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.isEditable);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate2"](" ", ctx.collateralCategory, " | ", ctx.collateralSubCategory, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵtextInterpolate1"]("\n                ", ctx.collateralDate, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.progress > 0 && ctx.progress < 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.files.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵproperty"]("ngIf", ctx.files.length > 0 && ctx.progress == 0);
    } }, directives: [_shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_7__.LoaderComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormGroupDirective, _angular_common__WEBPACK_IMPORTED_MODULE_14__.NgIf, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormControlName, _angular_common__WEBPACK_IMPORTED_MODULE_14__.NgForOf, ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_15__.PdfViewerComponent, _shared_pagination_pager_component__WEBPACK_IMPORTED_MODULE_8__.PagerComponent], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_14__.SlicePipe], styles: [".logo[_ngcontent-%COMP%] {\n  width: 100px;\n}\n\n.progress-loader-alignment[_ngcontent-%COMP%] {\n  padding-top: 28%;\n  padding-left: 20%;\n  padding-right: 20%;\n}\n\n.uploading-text[_ngcontent-%COMP%] {\n  font-family: Rajdhani;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 23px;\n  letter-spacing: 0.02em;\n  color: #717171;\n}\n\n.delete-button[_ngcontent-%COMP%] {\n  color: #fff;\n  background-color: white;\n  border-color: white;\n}\n\n.pdf-position[_ngcontent-%COMP%] {\n  position: relative;\n  width: 100%;\n  height: 100%;\n}\n\n.pdf-outer-div[_ngcontent-%COMP%] {\n  height: 500px;\n  width: 500px;\n}\n\n.file-input[_ngcontent-%COMP%] {\n  display: none;\n}\n\nlabel[_ngcontent-%COMP%] {\n  color: #a1185a;\n}\n\n.image-icon[_ngcontent-%COMP%] {\n  font-size: 40px;\n}\n\n.drop-area[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 300px;\n  padding: 2rem;\n  text-align: center;\n  border: 4px dashed #adadad;\n  border-radius: 1rem;\n  position: relative;\n  margin: 0 auto;\n  cursor: pointer;\n}\n\n.drop-area[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  opacity: 0;\n  position: absolute;\n  z-index: 2;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  cursor: pointer;\n}\n\n.drop-area[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\n  color: #a1185a;\n}\n\n.drop-area[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n  font-size: 20px;\n  font-weight: 600;\n  color: #38424c;\n}\n\n.fileover[_ngcontent-%COMP%] {\n  animation: shake 1s;\n  animation-iteration-count: infinite;\n}\n\n.viewer[_ngcontent-%COMP%] {\n  min-height: 300px;\n  width: 100%;\n  background: #fff;\n  border-radius: 1rem;\n  overflow: hidden;\n  height: 500px;\n  text-align: center;\n}\n\n.viewer[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  max-height: 100%;\n}\n\n.files-list[_ngcontent-%COMP%] {\n  margin-top: 1.5rem;\n}\n\n.files-list[_ngcontent-%COMP%]   .single-file[_ngcontent-%COMP%] {\n  display: flex;\n  padding: 0.5rem;\n  justify-content: space-between;\n  align-items: center;\n  border: dashed 1px #979797;\n  margin-bottom: 1rem;\n  display: flex;\n  flex-grow: 1;\n}\n\n.files-list[_ngcontent-%COMP%]   .single-file[_ngcontent-%COMP%]   .delete[_ngcontent-%COMP%] {\n  display: flex;\n  margin-left: 0.5rem;\n  cursor: pointer;\n  align-self: flex-end;\n}\n\n.files-list[_ngcontent-%COMP%]   .single-file[_ngcontent-%COMP%]   .name[_ngcontent-%COMP%] {\n  font-size: 14px;\n  font-weight: 500;\n  color: #353f4a;\n  margin: 0;\n}\n\n.files-list[_ngcontent-%COMP%]   .single-file[_ngcontent-%COMP%]   .size[_ngcontent-%COMP%] {\n  font-size: 12px;\n  font-weight: 500;\n  color: #a4a4a4;\n  margin: 0;\n  margin-bottom: 0.25rem;\n}\n\n.files-list[_ngcontent-%COMP%]   .single-file[_ngcontent-%COMP%]   .info[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n\n\n@keyframes shake {\n  0% {\n    transform: translate(1px, 1px) rotate(0deg);\n  }\n  10% {\n    transform: translate(-1px, -2px) rotate(-1deg);\n  }\n  20% {\n    transform: translate(-3px, 0px) rotate(1deg);\n  }\n  30% {\n    transform: translate(3px, 2px) rotate(0deg);\n  }\n  40% {\n    transform: translate(1px, -1px) rotate(1deg);\n  }\n  50% {\n    transform: translate(-1px, 2px) rotate(-1deg);\n  }\n  60% {\n    transform: translate(-3px, 1px) rotate(0deg);\n  }\n  70% {\n    transform: translate(3px, 1px) rotate(-1deg);\n  }\n  80% {\n    transform: translate(-1px, -1px) rotate(1deg);\n  }\n  90% {\n    transform: translate(1px, 2px) rotate(0deg);\n  }\n  100% {\n    transform: translate(1px, -2px) rotate(-1deg);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVwbG9hZC1jb2xsYXRlcmFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQUNGOztBQUVBO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7QUFDRjs7QUFFQTtFQUNFLFdBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBRUE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EsWUFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtBQUNGOztBQUVBO0VBQ0UsY0FBQTtBQUNGOztBQUVBO0VBQ0UsZUFBQTtBQUNGOztBQUVBO0VBQ0UsV0FBQTtFQUNBLGFBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUNGOztBQUFFO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0FBRUo7O0FBQ0U7RUFDRSxjQUFBO0FBQ0o7O0FBRUU7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBQUo7O0FBSUE7RUFDRSxtQkFBQTtFQUNBLG1DQUFBO0FBREY7O0FBSUE7RUFDRSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUFERjs7QUFFRTtFQUNFLGdCQUFBO0FBQUo7O0FBSUE7RUFDRSxrQkFBQTtBQURGOztBQUdFO0VBQ0UsYUFBQTtFQUNBLGVBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsMEJBQUE7RUFDQSxtQkFBQTtFQVNBLGFBQUE7RUFDQSxZQUFBO0FBVEo7O0FBQ0k7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUFDTjs7QUFLSTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FBSE47O0FBTUk7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLHNCQUFBO0FBSk47O0FBT0k7RUFDRSxXQUFBO0FBTE47O0FBVUEsb0JBQUE7O0FBQ0E7RUFDRTtJQUNFLDJDQUFBO0VBUEY7RUFVQTtJQUNFLDhDQUFBO0VBUkY7RUFXQTtJQUNFLDRDQUFBO0VBVEY7RUFZQTtJQUNFLDJDQUFBO0VBVkY7RUFhQTtJQUNFLDRDQUFBO0VBWEY7RUFjQTtJQUNFLDZDQUFBO0VBWkY7RUFlQTtJQUNFLDRDQUFBO0VBYkY7RUFnQkE7SUFDRSw0Q0FBQTtFQWRGO0VBaUJBO0lBQ0UsNkNBQUE7RUFmRjtFQWtCQTtJQUNFLDJDQUFBO0VBaEJGO0VBbUJBO0lBQ0UsNkNBQUE7RUFqQkY7QUFDRiIsImZpbGUiOiJ1cGxvYWQtY29sbGF0ZXJhbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dvIHtcclxuICB3aWR0aDogMTAwcHg7XHJcbn1cclxuXHJcbi5wcm9ncmVzcy1sb2FkZXItYWxpZ25tZW50IHtcclxuICBwYWRkaW5nLXRvcDogMjglO1xyXG4gIHBhZGRpbmctbGVmdDogMjAlO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDIwJTtcclxufVxyXG5cclxuLnVwbG9hZGluZy10ZXh0IHtcclxuICBmb250LWZhbWlseTogUmFqZGhhbmk7XHJcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBsaW5lLWhlaWdodDogMjNweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC4wMmVtO1xyXG4gIGNvbG9yOiAjNzE3MTcxO1xyXG59XHJcblxyXG4uZGVsZXRlLWJ1dHRvbiB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLnBkZi1wb3NpdGlvbiB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLnBkZi1vdXRlci1kaXYge1xyXG4gIGhlaWdodDogNTAwcHg7XHJcbiAgd2lkdGg6IDUwMHB4O1xyXG59XHJcblxyXG4uZmlsZS1pbnB1dCB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxubGFiZWwge1xyXG4gIGNvbG9yOiAjYTExODVhO1xyXG59XHJcblxyXG4uaW1hZ2UtaWNvbiB7XHJcbiAgZm9udC1zaXplOiA0MHB4O1xyXG59XHJcblxyXG4uZHJvcC1hcmVhIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDMwMHB4O1xyXG4gIHBhZGRpbmc6IDJyZW07XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGJvcmRlcjogNHB4IGRhc2hlZCAjYWRhZGFkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBpbnB1dCB7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcblxyXG4gIGxhYmVsIHtcclxuICAgIGNvbG9yOiAjYTExODVhO1xyXG4gIH1cclxuXHJcbiAgaDMge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGNvbG9yOiAjMzg0MjRjO1xyXG4gIH1cclxufVxyXG5cclxuLmZpbGVvdmVyIHtcclxuICBhbmltYXRpb246IHNoYWtlIDFzO1xyXG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xyXG59XHJcblxyXG4udmlld2VyIHtcclxuICBtaW4taGVpZ2h0OiAzMDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlci1yYWRpdXM6IDFyZW07XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBoZWlnaHQ6IDUwMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBpbWcge1xyXG4gICAgbWF4LWhlaWdodDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcbi5maWxlcy1saXN0IHtcclxuICBtYXJnaW4tdG9wOiAxLjVyZW07XHJcblxyXG4gIC5zaW5nbGUtZmlsZSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgcGFkZGluZzogMC41cmVtO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJvcmRlcjogZGFzaGVkIDFweCAjOTc5Nzk3O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuXHJcbiAgICAuZGVsZXRlIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICBhbGlnbi1zZWxmOiBmbGV4LWVuZDtcclxuICAgIH1cclxuXHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG5cclxuICAgIC5uYW1lIHtcclxuICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBjb2xvcjogIzM1M2Y0YTtcclxuICAgICAgbWFyZ2luOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIC5zaXplIHtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBjb2xvcjogI2E0YTRhNDtcclxuICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAwLjI1cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIC5pbmZvIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vKiBTaGFrZSBhbmltYXRpb24gKi9cclxuQGtleWZyYW1lcyBzaGFrZSB7XHJcbiAgMCUge1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMXB4LCAxcHgpIHJvdGF0ZSgwZGVnKTtcclxuICB9XHJcblxyXG4gIDEwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMXB4LCAtMnB4KSByb3RhdGUoLTFkZWcpO1xyXG4gIH1cclxuXHJcbiAgMjAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC0zcHgsIDBweCkgcm90YXRlKDFkZWcpO1xyXG4gIH1cclxuXHJcbiAgMzAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDNweCwgMnB4KSByb3RhdGUoMGRlZyk7XHJcbiAgfVxyXG5cclxuICA0MCUge1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMXB4LCAtMXB4KSByb3RhdGUoMWRlZyk7XHJcbiAgfVxyXG5cclxuICA1MCUge1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTFweCwgMnB4KSByb3RhdGUoLTFkZWcpO1xyXG4gIH1cclxuXHJcbiAgNjAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC0zcHgsIDFweCkgcm90YXRlKDBkZWcpO1xyXG4gIH1cclxuXHJcbiAgNzAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDNweCwgMXB4KSByb3RhdGUoLTFkZWcpO1xyXG4gIH1cclxuXHJcbiAgODAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC0xcHgsIC0xcHgpIHJvdGF0ZSgxZGVnKTtcclxuICB9XHJcblxyXG4gIDkwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxcHgsIDJweCkgcm90YXRlKDBkZWcpO1xyXG4gIH1cclxuXHJcbiAgMTAwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxcHgsIC0ycHgpIHJvdGF0ZSgtMWRlZyk7XHJcbiAgfVxyXG59XHJcbiJdfQ== */"] });


/***/ }),

/***/ 8721:
/*!*********************************************************!*\
  !*** ./src/app/my-profile/my-profile-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MyProfileRoutingModule": () => (/* binding */ MyProfileRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/shell/shell.service */ 6042);
/* harmony import */ var _my_profile_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./my-profile.component */ 5908);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);





const routes = [
    _app_shell_shell_service__WEBPACK_IMPORTED_MODULE_0__.Shell.childRoutes([
        { path: '', redirectTo: '/my-profile', pathMatch: 'full' },
        { path: 'my-profile', component: _my_profile_component__WEBPACK_IMPORTED_MODULE_1__.MyProfileComponent, data: { title: 'My-Profile' } },
    ]),
];
class MyProfileRoutingModule {
}
MyProfileRoutingModule.ɵfac = function MyProfileRoutingModule_Factory(t) { return new (t || MyProfileRoutingModule)(); };
MyProfileRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: MyProfileRoutingModule });
MyProfileRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ providers: [], imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](MyProfileRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule] }); })();


/***/ }),

/***/ 5908:
/*!****************************************************!*\
  !*** ./src/app/my-profile/my-profile.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MyProfileComponent": () => (/* binding */ MyProfileComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../@shared/loader/loader.component */ 9967);


class MyProfileComponent {
    constructor() {
        this.isLoading = false;
    }
    ngOnInit() { }
}
MyProfileComponent.ɵfac = function MyProfileComponent_Factory(t) { return new (t || MyProfileComponent)(); };
MyProfileComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: MyProfileComponent, selectors: [["app-my-profile"]], decls: 14, vars: 1, consts: [[1, "container-fluid"], [1, "jumbotron", "text-center"], [3, "isLoading"]], template: function MyProfileComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "My Profile Page Works");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "app-loader", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("isLoading", ctx.isLoading);
    } }, directives: [_shared_loader_loader_component__WEBPACK_IMPORTED_MODULE_0__.LoaderComponent], styles: [".logo[_ngcontent-%COMP%] {\n  width: 100px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm15LXByb2ZpbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FBQ0YiLCJmaWxlIjoibXktcHJvZmlsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dvIHtcclxuICB3aWR0aDogMTAwcHg7XHJcbn1cclxuIl19 */"] });


/***/ }),

/***/ 4907:
/*!*************************************************!*\
  !*** ./src/app/my-profile/my-profile.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MyProfileModule": () => (/* binding */ MyProfileModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./my-profile-routing.module */ 8721);
/* harmony import */ var _my_profile_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./my-profile.component */ 5908);
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @shared */ 2842);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);





class MyProfileModule {
}
MyProfileModule.ɵfac = function MyProfileModule_Factory(t) { return new (t || MyProfileModule)(); };
MyProfileModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: MyProfileModule });
MyProfileModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _shared__WEBPACK_IMPORTED_MODULE_2__.SharedModule, _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_0__.MyProfileRoutingModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](MyProfileModule, { declarations: [_my_profile_component__WEBPACK_IMPORTED_MODULE_1__.MyProfileComponent], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _shared__WEBPACK_IMPORTED_MODULE_2__.SharedModule, _my_profile_routing_module__WEBPACK_IMPORTED_MODULE_0__.MyProfileRoutingModule] }); })();


/***/ }),

/***/ 1436:
/*!*********************************************!*\
  !*** ./src/app/rest/cobrand-api.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CobrandService": () => (/* binding */ CobrandService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 8002);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 1841);





const routes = {
    createCobrandFiles: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/cobrand/create`,
    getAllCoBrandingList: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/cobrand/list`,
    getCoBrandsByFilter: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/cobrand/filter`,
    deleteCobrand: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/cobrand/files/multiple/deletecobrand`,
};
class CobrandService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    createCobrandFiles(context) {
        return this.httpClient.post(routes.createCobrandFiles, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getAllCoBrandingList(context) {
        return this.httpClient.post(routes.getAllCoBrandingList, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getCoBrandsByFilter(context) {
        return this.httpClient.post(routes.getCoBrandsByFilter, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    deleteCobrand(context) {
        return this.httpClient.post(routes.deleteCobrand, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
}
CobrandService.ɵfac = function CobrandService_Factory(t) { return new (t || CobrandService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient)); };
CobrandService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: CobrandService, factory: CobrandService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 2690:
/*!************************************************!*\
  !*** ./src/app/rest/collateral-api.service.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CollateralService": () => (/* binding */ CollateralService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 8002);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 1841);





const routes = {
    createCollateral: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/create`,
    getCollateralById: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/`,
    updateCollateralNamedById: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/update`,
    submitCollateral: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/submit`,
    getAllCollateralList: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/list`,
    getAllFilesByCollateralId: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/filelist/`,
    getCollateralByFilter: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/filter`,
    deleteMultipleFilesByPath: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/files/multiple/delete`,
    deleteCollateral: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/files/multiple/deletecollateral`,
};
class CollateralService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    createCollateral(context) {
        return this.httpClient.post(routes.createCollateral, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getCollateralById(context) {
        return this.httpClient.get(routes.getCollateralById + context.id).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    updateCollateralNamedById(context) {
        return this.httpClient.post(routes.updateCollateralNamedById, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    submitCollateral(context) {
        return this.httpClient.post(routes.submitCollateral, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getAllCollateralList(context) {
        return this.httpClient.post(routes.getAllCollateralList, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getCollateralByFilter(context) {
        return this.httpClient.post(routes.getCollateralByFilter, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getAllFilesByCollateralId(context) {
        return this.httpClient.get(routes.getAllFilesByCollateralId + context.collateralId).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    deleteMultipleFilesByPath(context) {
        return this.httpClient.post(routes.deleteMultipleFilesByPath, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    deleteCollateral(context) {
        return this.httpClient.post(routes.deleteCollateral, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
}
CollateralService.ɵfac = function CollateralService_Factory(t) { return new (t || CollateralService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient)); };
CollateralService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: CollateralService, factory: CollateralService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 8226:
/*!*******************************************!*\
  !*** ./src/app/rest/files-api.service.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FilesService": () => (/* binding */ FilesService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 8002);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 1841);





const routes = {
    uploadFiles: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/files/upload`,
    uploadCobrandFiles: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/cobrand/files/upload`,
    getFilesById: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/files/`,
    //getFilesByuserId: environment.serverUrl + `client/api/v1/collateral/files/all/:userId`,
    deleteFilesById: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/files/delete`,
};
class FilesService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.downloadFileByName = _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/collateral/files/download/`;
        this.downloadFileByNameCobrand = _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/cobrand/files/download/`;
    }
    uploadFiles(context) {
        return this.httpClient
            .post(routes.uploadFiles, context, {
            reportProgress: true,
            observe: 'events',
        })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((event) => event), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    uploadCobrandFiles(context) {
        return this.httpClient.post(routes.uploadCobrandFiles, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getFilesById(context) {
        return this.httpClient.get(routes.getFilesById + context.id).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    // getFilesByUserId(context: FilesByUserId) {
    //   return this.httpClient.get(routes.getFilesByuserId + context.userId).pipe(
    //     map((body: any) => body.data),
    //     catchError(() => of('Error, could not load joke :-('))
    //   );
    // }
    deleteFilesByPath(context) {
        return this.httpClient.post(routes.deleteFilesById, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    deleteFilesByCollateralId(context) {
        return this.httpClient.post(routes.deleteFilesById, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
}
FilesService.ɵfac = function FilesService_Factory(t) { return new (t || FilesService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient)); };
FilesService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: FilesService, factory: FilesService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 3173:
/*!*********************************************!*\
  !*** ./src/app/rest/product-api.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductService": () => (/* binding */ ProductService)
/* harmony export */ });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 8002);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 1841);





const routes = {
    productCategory: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/products/category/list`,
    subProductCategory: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/api/v1/products/sub-category/list`,
};
class ProductService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getProductCategory() {
        return this.httpClient.get(routes.productCategory).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    getSubProductCategory(context) {
        return this.httpClient.post(routes.subProductCategory, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body.data), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
}
ProductService.ɵfac = function ProductService_Factory(t) { return new (t || ProductService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient)); };
ProductService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: ProductService, factory: ProductService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 4962:
/*!******************************************!*\
  !*** ./src/app/rest/user-api.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserApiService": () => (/* binding */ UserApiService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 8002);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @env/environment */ 2340);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 1841);





const routes = {
    adminLogin: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/auth/login`,
    userLogin: _env_environment__WEBPACK_IMPORTED_MODULE_0__.environment.serverUrl + `client/auth/login`,
};
class UserApiService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    loginAdmin(context) {
        return this.httpClient.post(routes.adminLogin, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
    login(context) {
        return this.httpClient.post(routes.userLogin, context).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)((body) => body), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)(() => (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)('Error, could not load joke :-(')));
    }
}
UserApiService.ɵfac = function UserApiService_Factory(t) { return new (t || UserApiService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient)); };
UserApiService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: UserApiService, factory: UserApiService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 5213:
/*!**************************************************!*\
  !*** ./src/app/shell/header/header.component.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderComponent": () => (/* binding */ HeaderComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _app_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/auth */ 6282);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ 2664);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);





function HeaderComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "a", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Collateral");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "a", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "\n            ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Co-Branding");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "\n          ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "\n        ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
class HeaderComponent {
    constructor(router, authenticationService, credentialsService) {
        this.router = router;
        this.authenticationService = authenticationService;
        this.credentialsService = credentialsService;
        this.menuHidden = true;
        var credentials = localStorage.getItem('credentials') || '';
        this.userRole = JSON.parse(credentials).userLoggedIn.role;
    }
    ngOnInit() { }
    toggleMenu() {
        this.menuHidden = !this.menuHidden;
    }
    logout() {
        this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
    }
    get username() {
        const credentials = this.credentialsService.credentials;
        return credentials ? credentials.username : null;
    }
}
HeaderComponent.ɵfac = function HeaderComponent_Factory(t) { return new (t || HeaderComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_auth__WEBPACK_IMPORTED_MODULE_0__.AuthenticationService), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_auth__WEBPACK_IMPORTED_MODULE_0__.CredentialsService)); };
HeaderComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: HeaderComponent, selectors: [["app-header"]], decls: 53, vars: 4, consts: [[1, "navbar", "navbar-expand-lg", "navbar-light"], [1, "container-fluid"], ["routerLink", "/collateral", 1, "navbar-brand"], [1, "fab", "fa-artstation"], ["type", "button", "aria-controls", "navbar-menu", "aria-label", "Toggle navigation", 1, "navbar-toggler", 3, "click"], [1, "navbar-toggler-icon"], ["id", "navbar-menu", 1, "collapse", "navbar-collapse", "justify-content-end", "float-xs-none", 3, "ngbCollapse"], ["class", "mb-2 navbar-nav m-auto mb-lg-0", 4, "ngIf"], [1, "ml-auto", "navbar-nav"], ["ngbDropdown", "", 1, "nav-item", "mx-3"], ["id", "user-dropdown", "ngbDropdownToggle", "", 1, "nav-link"], [1, "fas", "fa-user-circle"], ["ngbDropdownMenu", "", "aria-labelledby", "user-dropdown", 1, "dropdown-menu", "dropdown-menu-right"], ["routerLink", "/my-profile", 1, "dropdown-item"], [1, "dropdown-divider"], [1, "dropdown-item", 3, "click"], [1, "mb-2", "navbar-nav", "m-auto", "mb-lg-0"], ["routerLink", "/collateral", "routerLinkActive", "active", 1, "nav-item", "nav-link", "top-nav", "mx-3", "text-uppercase"], ["routerLink", "/co-branding", "routerLinkActive", "active", 1, "nav-item", "nav-link", "top-nav", "mx-3", "text-uppercase"]], template: function HeaderComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "header");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "nav", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](8, "i", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "CoBrandz");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_14_listener() { return ctx.toggleMenu(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "span", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, HeaderComponent_div_21_Template, 14, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](29, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](32);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](34, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](36, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](39, "My Profile");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](40, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](41, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](42, "\n              ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](43, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function HeaderComponent_Template_button_click_43_listener() { return ctx.logout(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](44, "Logout");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](45, "\n            ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](46, "\n          ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](47, "\n        ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](48, "\n      ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](49, "\n    ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](50, "\n  ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](51, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](52, "\n");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵattribute"]("aria-expanded", !ctx.menuHidden);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngbCollapse", ctx.menuHidden);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.userRole == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.username);
    } }, directives: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbNavbar, _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterLinkWithHref, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbCollapse, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbDropdown, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbDropdownToggle, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbDropdownMenu, _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterLink, _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterLinkActive], styles: [".navbar[_ngcontent-%COMP%] {\n  margin-bottom: 1rem;\n}\n.nav-link.dropdown-toggle[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n.logo[_ngcontent-%COMP%] {\n  width: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRoZW1lXFx0aGVtZS12YXJpYWJsZXMuc2NzcyIsImhlYWRlci5jb21wb25lbnQuc2NzcyIsIi4uXFwuLlxcLi5cXC4uXFxub2RlX21vZHVsZXNcXGJvb3RzdHJhcFxcc2Nzc1xcX3ZhcmlhYmxlcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUFBO0FDRUE7RUFDRSxtQkNtWE87QURqWFQ7QUFDQTtFQUNFLGVBQUE7QUFFRjtBQUNBO0VBQ0UsV0FBQTtBQUVGIiwiZmlsZSI6ImhlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiAqIEFwcGxpY2F0aW9uIGdsb2JhbCB2YXJpYWJsZXMuXHJcbiAqL1xyXG5cclxuLy8gU2V0IEZvbnQgQXdlc29tZSBmb250IHBhdGhcclxuJGZhLWZvbnQtcGF0aDogXCJ+QGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLWZyZWUvd2ViZm9udHNcIjtcclxuXHJcbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4vLyBCb290c3RyYXAgdmFyaWFibGVzXHJcbi8vXHJcbi8vIE92ZXJyaWRlIEJvb3RzdHJhcCB2YXJpYWJsZXMgaGVyZSB0byBzdWl0ZSB5b3VyIHRoZW1lLlxyXG4vLyBDb3B5IHZhcmlhYmxlcyB5b3Ugd2FudCB0byBjdXN0b21pemUgZnJvbSBub2RlX21vZHVsZXMvYm9vdHN0cmFwL3Njc3MvX3ZhcmlhYmxlcy5zY3NzXHJcblxyXG4vL1xyXG4vLyBDb2xvciBzeXN0ZW1cclxuLy9cclxuXHJcbiR3aGl0ZTogI2ZmZjtcclxuJGdyYXktMTAwOiAjZjhmOWZhO1xyXG4kZ3JheS0yMDA6ICNlOWVjZWY7XHJcbiRncmF5LTMwMDogI2RlZTJlNjtcclxuJGdyYXktNDAwOiAjY2VkNGRhO1xyXG4kZ3JheS01MDA6ICNhZGI1YmQ7XHJcbiRncmF5LTYwMDogIzg2OGU5NjtcclxuJGdyYXktNzAwOiAjNDk1MDU3O1xyXG4kZ3JheS04MDA6ICMzNDNhNDA7XHJcbiRncmF5LTkwMDogIzIxMjUyOTtcclxuJGdyYXktYmc6ICNlNWU1ZTU7XHJcbiRibGFjazogIzQ4NDg0ODtcclxuXHJcbiRwcmltYXJ5OiAjYTExODVhO1xyXG4kc2Vjb25kYXJ5OiAjNzE3MTcxO1xyXG4kYmx1ZTogIzAwNzNkZDtcclxuJGluZGlnbzogIzY2MTBmMjtcclxuJHB1cnBsZTogIzZmNDJjMTtcclxuJHBpbms6ICNlODNlOGM7XHJcbiRyZWQ6ICNkYzM1NDU7XHJcbiRvcmFuZ2U6ICNmZDdlMTQ7XHJcbiR5ZWxsb3c6ICNmZmMxMDc7XHJcbiRncmVlbjogIzI4YTc0NTtcclxuJHRlYWw6ICMyMGM5OTc7XHJcbiRjeWFuOiAjMTdhMmI4O1xyXG5cclxuJHRoZW1lLWNvbG9yczogKFxyXG4gIHByaW1hcnk6ICRwcmltYXJ5LFxyXG4gIHNlY29uZGFyeTogJGdyYXktNjAwLFxyXG4gIHN1Y2Nlc3M6ICRncmVlbixcclxuICBpbmZvOiAkY3lhbixcclxuICB3YXJuaW5nOiAkeWVsbG93LFxyXG4gIGRhbmdlcjogJHJlZCxcclxuICBsaWdodDogJGdyYXktMTAwLFxyXG4gIGRhcms6ICRncmF5LTgwMCxcclxuICBibGFjazogJGJsYWNrLFxyXG4pO1xyXG5cclxuLy8gVXNlIEJvb3RzdHJhcCBkZWZhdWx0cyBmb3Igb3RoZXIgdmFyaWFibGVzLCBpbXBvcnRlZCBoZXJlIHNvIHdlIGNhbiBhY2Nlc3MgYWxsIGFwcCB2YXJpYWJsZXMgaW4gb25lIHBsYWNlIHdoZW4gdXNlZFxyXG4vLyBpbiBjb21wb25lbnRzLlxyXG5AaW1wb3J0IFwifmJvb3RzdHJhcC9zY3NzL19mdW5jdGlvbnNcIjtcclxuQGltcG9ydCBcIn5ib290c3RyYXAvc2Nzcy9fdmFyaWFibGVzXCI7XHJcbiIsIkBpbXBvcnQgXCJzcmMvdGhlbWUvdGhlbWUtdmFyaWFibGVzXCI7XHJcblxyXG4ubmF2YmFyIHtcclxuICBtYXJnaW4tYm90dG9tOiAkc3BhY2VyO1xyXG59XHJcblxyXG4ubmF2LWxpbmsuZHJvcGRvd24tdG9nZ2xlIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5sb2dvIHtcclxuICB3aWR0aDogMzBweDtcclxufVxyXG4iLCIvLyBWYXJpYWJsZXNcbi8vXG4vLyBWYXJpYWJsZXMgc2hvdWxkIGZvbGxvdyB0aGUgYCRjb21wb25lbnQtc3RhdGUtcHJvcGVydHktc2l6ZWAgZm9ybXVsYSBmb3Jcbi8vIGNvbnNpc3RlbnQgbmFtaW5nLiBFeDogJG5hdi1saW5rLWRpc2FibGVkLWNvbG9yIGFuZCAkbW9kYWwtY29udGVudC1ib3gtc2hhZG93LXhzLlxuXG4vLyBDb2xvciBzeXN0ZW1cblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGdyYXktY29sb3ItdmFyaWFibGVzXG4kd2hpdGU6ICAgICNmZmYgIWRlZmF1bHQ7XG4kZ3JheS0xMDA6ICNmOGY5ZmEgIWRlZmF1bHQ7XG4kZ3JheS0yMDA6ICNlOWVjZWYgIWRlZmF1bHQ7XG4kZ3JheS0zMDA6ICNkZWUyZTYgIWRlZmF1bHQ7XG4kZ3JheS00MDA6ICNjZWQ0ZGEgIWRlZmF1bHQ7XG4kZ3JheS01MDA6ICNhZGI1YmQgIWRlZmF1bHQ7XG4kZ3JheS02MDA6ICM2Yzc1N2QgIWRlZmF1bHQ7XG4kZ3JheS03MDA6ICM0OTUwNTcgIWRlZmF1bHQ7XG4kZ3JheS04MDA6ICMzNDNhNDAgIWRlZmF1bHQ7XG4kZ3JheS05MDA6ICMyMTI1MjkgIWRlZmF1bHQ7XG4kYmxhY2s6ICAgICMwMDAgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGdyYXktY29sb3ItdmFyaWFibGVzXG5cbi8vIGZ1c3YtZGlzYWJsZVxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGdyYXktY29sb3JzLW1hcFxuJGdyYXlzOiAoXG4gIFwiMTAwXCI6ICRncmF5LTEwMCxcbiAgXCIyMDBcIjogJGdyYXktMjAwLFxuICBcIjMwMFwiOiAkZ3JheS0zMDAsXG4gIFwiNDAwXCI6ICRncmF5LTQwMCxcbiAgXCI1MDBcIjogJGdyYXktNTAwLFxuICBcIjYwMFwiOiAkZ3JheS02MDAsXG4gIFwiNzAwXCI6ICRncmF5LTcwMCxcbiAgXCI4MDBcIjogJGdyYXktODAwLFxuICBcIjkwMFwiOiAkZ3JheS05MDBcbikgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGdyYXktY29sb3JzLW1hcFxuLy8gZnVzdi1lbmFibGVcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGNvbG9yLXZhcmlhYmxlc1xuJGJsdWU6ICAgICMwZDZlZmQgIWRlZmF1bHQ7XG4kaW5kaWdvOiAgIzY2MTBmMiAhZGVmYXVsdDtcbiRwdXJwbGU6ICAjNmY0MmMxICFkZWZhdWx0O1xuJHBpbms6ICAgICNkNjMzODQgIWRlZmF1bHQ7XG4kcmVkOiAgICAgI2RjMzU0NSAhZGVmYXVsdDtcbiRvcmFuZ2U6ICAjZmQ3ZTE0ICFkZWZhdWx0O1xuJHllbGxvdzogICNmZmMxMDcgIWRlZmF1bHQ7XG4kZ3JlZW46ICAgIzE5ODc1NCAhZGVmYXVsdDtcbiR0ZWFsOiAgICAjMjBjOTk3ICFkZWZhdWx0O1xuJGN5YW46ICAgICMwZGNhZjAgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGNvbG9yLXZhcmlhYmxlc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgY29sb3JzLW1hcFxuJGNvbG9yczogKFxuICBcImJsdWVcIjogICAgICAgJGJsdWUsXG4gIFwiaW5kaWdvXCI6ICAgICAkaW5kaWdvLFxuICBcInB1cnBsZVwiOiAgICAgJHB1cnBsZSxcbiAgXCJwaW5rXCI6ICAgICAgICRwaW5rLFxuICBcInJlZFwiOiAgICAgICAgJHJlZCxcbiAgXCJvcmFuZ2VcIjogICAgICRvcmFuZ2UsXG4gIFwieWVsbG93XCI6ICAgICAkeWVsbG93LFxuICBcImdyZWVuXCI6ICAgICAgJGdyZWVuLFxuICBcInRlYWxcIjogICAgICAgJHRlYWwsXG4gIFwiY3lhblwiOiAgICAgICAkY3lhbixcbiAgXCJ3aGl0ZVwiOiAgICAgICR3aGl0ZSxcbiAgXCJncmF5XCI6ICAgICAgICRncmF5LTYwMCxcbiAgXCJncmF5LWRhcmtcIjogICRncmF5LTgwMFxuKSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgY29sb3JzLW1hcFxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgdGhlbWUtY29sb3ItdmFyaWFibGVzXG4kcHJpbWFyeTogICAgICAgJGJsdWUgIWRlZmF1bHQ7XG4kc2Vjb25kYXJ5OiAgICAgJGdyYXktNjAwICFkZWZhdWx0O1xuJHN1Y2Nlc3M6ICAgICAgICRncmVlbiAhZGVmYXVsdDtcbiRpbmZvOiAgICAgICAgICAkY3lhbiAhZGVmYXVsdDtcbiR3YXJuaW5nOiAgICAgICAkeWVsbG93ICFkZWZhdWx0O1xuJGRhbmdlcjogICAgICAgICRyZWQgIWRlZmF1bHQ7XG4kbGlnaHQ6ICAgICAgICAgJGdyYXktMTAwICFkZWZhdWx0O1xuJGRhcms6ICAgICAgICAgICRncmF5LTkwMCAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgdGhlbWUtY29sb3ItdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCB0aGVtZS1jb2xvcnMtbWFwXG4kdGhlbWUtY29sb3JzOiAoXG4gIFwicHJpbWFyeVwiOiAgICAkcHJpbWFyeSxcbiAgXCJzZWNvbmRhcnlcIjogICRzZWNvbmRhcnksXG4gIFwic3VjY2Vzc1wiOiAgICAkc3VjY2VzcyxcbiAgXCJpbmZvXCI6ICAgICAgICRpbmZvLFxuICBcIndhcm5pbmdcIjogICAgJHdhcm5pbmcsXG4gIFwiZGFuZ2VyXCI6ICAgICAkZGFuZ2VyLFxuICBcImxpZ2h0XCI6ICAgICAgJGxpZ2h0LFxuICBcImRhcmtcIjogICAgICAgJGRhcmtcbikgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHRoZW1lLWNvbG9ycy1tYXBcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHRoZW1lLWNvbG9ycy1yZ2JcbiR0aGVtZS1jb2xvcnMtcmdiOiBtYXAtbG9vcCgkdGhlbWUtY29sb3JzLCB0by1yZ2IsIFwiJHZhbHVlXCIpICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCB0aGVtZS1jb2xvcnMtcmdiXG5cbi8vIFRoZSBjb250cmFzdCByYXRpbyB0byByZWFjaCBhZ2FpbnN0IHdoaXRlLCB0byBkZXRlcm1pbmUgaWYgY29sb3IgY2hhbmdlcyBmcm9tIFwibGlnaHRcIiB0byBcImRhcmtcIi4gQWNjZXB0YWJsZSB2YWx1ZXMgZm9yIFdDQUcgMi4wIGFyZSAzLCA0LjUgYW5kIDcuXG4vLyBTZWUgaHR0cHM6Ly93d3cudzMub3JnL1RSL1dDQUcyMC8jdmlzdWFsLWF1ZGlvLWNvbnRyYXN0LWNvbnRyYXN0XG4kbWluLWNvbnRyYXN0LXJhdGlvOiAgIDQuNSAhZGVmYXVsdDtcblxuLy8gQ3VzdG9taXplIHRoZSBsaWdodCBhbmQgZGFyayB0ZXh0IGNvbG9ycyBmb3IgdXNlIGluIG91ciBjb2xvciBjb250cmFzdCBmdW5jdGlvbi5cbiRjb2xvci1jb250cmFzdC1kYXJrOiAgICAgICRibGFjayAhZGVmYXVsdDtcbiRjb2xvci1jb250cmFzdC1saWdodDogICAgICR3aGl0ZSAhZGVmYXVsdDtcblxuLy8gZnVzdi1kaXNhYmxlXG4kYmx1ZS0xMDA6IHRpbnQtY29sb3IoJGJsdWUsIDgwJSkgIWRlZmF1bHQ7XG4kYmx1ZS0yMDA6IHRpbnQtY29sb3IoJGJsdWUsIDYwJSkgIWRlZmF1bHQ7XG4kYmx1ZS0zMDA6IHRpbnQtY29sb3IoJGJsdWUsIDQwJSkgIWRlZmF1bHQ7XG4kYmx1ZS00MDA6IHRpbnQtY29sb3IoJGJsdWUsIDIwJSkgIWRlZmF1bHQ7XG4kYmx1ZS01MDA6ICRibHVlICFkZWZhdWx0O1xuJGJsdWUtNjAwOiBzaGFkZS1jb2xvcigkYmx1ZSwgMjAlKSAhZGVmYXVsdDtcbiRibHVlLTcwMDogc2hhZGUtY29sb3IoJGJsdWUsIDQwJSkgIWRlZmF1bHQ7XG4kYmx1ZS04MDA6IHNoYWRlLWNvbG9yKCRibHVlLCA2MCUpICFkZWZhdWx0O1xuJGJsdWUtOTAwOiBzaGFkZS1jb2xvcigkYmx1ZSwgODAlKSAhZGVmYXVsdDtcblxuJGluZGlnby0xMDA6IHRpbnQtY29sb3IoJGluZGlnbywgODAlKSAhZGVmYXVsdDtcbiRpbmRpZ28tMjAwOiB0aW50LWNvbG9yKCRpbmRpZ28sIDYwJSkgIWRlZmF1bHQ7XG4kaW5kaWdvLTMwMDogdGludC1jb2xvcigkaW5kaWdvLCA0MCUpICFkZWZhdWx0O1xuJGluZGlnby00MDA6IHRpbnQtY29sb3IoJGluZGlnbywgMjAlKSAhZGVmYXVsdDtcbiRpbmRpZ28tNTAwOiAkaW5kaWdvICFkZWZhdWx0O1xuJGluZGlnby02MDA6IHNoYWRlLWNvbG9yKCRpbmRpZ28sIDIwJSkgIWRlZmF1bHQ7XG4kaW5kaWdvLTcwMDogc2hhZGUtY29sb3IoJGluZGlnbywgNDAlKSAhZGVmYXVsdDtcbiRpbmRpZ28tODAwOiBzaGFkZS1jb2xvcigkaW5kaWdvLCA2MCUpICFkZWZhdWx0O1xuJGluZGlnby05MDA6IHNoYWRlLWNvbG9yKCRpbmRpZ28sIDgwJSkgIWRlZmF1bHQ7XG5cbiRwdXJwbGUtMTAwOiB0aW50LWNvbG9yKCRwdXJwbGUsIDgwJSkgIWRlZmF1bHQ7XG4kcHVycGxlLTIwMDogdGludC1jb2xvcigkcHVycGxlLCA2MCUpICFkZWZhdWx0O1xuJHB1cnBsZS0zMDA6IHRpbnQtY29sb3IoJHB1cnBsZSwgNDAlKSAhZGVmYXVsdDtcbiRwdXJwbGUtNDAwOiB0aW50LWNvbG9yKCRwdXJwbGUsIDIwJSkgIWRlZmF1bHQ7XG4kcHVycGxlLTUwMDogJHB1cnBsZSAhZGVmYXVsdDtcbiRwdXJwbGUtNjAwOiBzaGFkZS1jb2xvcigkcHVycGxlLCAyMCUpICFkZWZhdWx0O1xuJHB1cnBsZS03MDA6IHNoYWRlLWNvbG9yKCRwdXJwbGUsIDQwJSkgIWRlZmF1bHQ7XG4kcHVycGxlLTgwMDogc2hhZGUtY29sb3IoJHB1cnBsZSwgNjAlKSAhZGVmYXVsdDtcbiRwdXJwbGUtOTAwOiBzaGFkZS1jb2xvcigkcHVycGxlLCA4MCUpICFkZWZhdWx0O1xuXG4kcGluay0xMDA6IHRpbnQtY29sb3IoJHBpbmssIDgwJSkgIWRlZmF1bHQ7XG4kcGluay0yMDA6IHRpbnQtY29sb3IoJHBpbmssIDYwJSkgIWRlZmF1bHQ7XG4kcGluay0zMDA6IHRpbnQtY29sb3IoJHBpbmssIDQwJSkgIWRlZmF1bHQ7XG4kcGluay00MDA6IHRpbnQtY29sb3IoJHBpbmssIDIwJSkgIWRlZmF1bHQ7XG4kcGluay01MDA6ICRwaW5rICFkZWZhdWx0O1xuJHBpbmstNjAwOiBzaGFkZS1jb2xvcigkcGluaywgMjAlKSAhZGVmYXVsdDtcbiRwaW5rLTcwMDogc2hhZGUtY29sb3IoJHBpbmssIDQwJSkgIWRlZmF1bHQ7XG4kcGluay04MDA6IHNoYWRlLWNvbG9yKCRwaW5rLCA2MCUpICFkZWZhdWx0O1xuJHBpbmstOTAwOiBzaGFkZS1jb2xvcigkcGluaywgODAlKSAhZGVmYXVsdDtcblxuJHJlZC0xMDA6IHRpbnQtY29sb3IoJHJlZCwgODAlKSAhZGVmYXVsdDtcbiRyZWQtMjAwOiB0aW50LWNvbG9yKCRyZWQsIDYwJSkgIWRlZmF1bHQ7XG4kcmVkLTMwMDogdGludC1jb2xvcigkcmVkLCA0MCUpICFkZWZhdWx0O1xuJHJlZC00MDA6IHRpbnQtY29sb3IoJHJlZCwgMjAlKSAhZGVmYXVsdDtcbiRyZWQtNTAwOiAkcmVkICFkZWZhdWx0O1xuJHJlZC02MDA6IHNoYWRlLWNvbG9yKCRyZWQsIDIwJSkgIWRlZmF1bHQ7XG4kcmVkLTcwMDogc2hhZGUtY29sb3IoJHJlZCwgNDAlKSAhZGVmYXVsdDtcbiRyZWQtODAwOiBzaGFkZS1jb2xvcigkcmVkLCA2MCUpICFkZWZhdWx0O1xuJHJlZC05MDA6IHNoYWRlLWNvbG9yKCRyZWQsIDgwJSkgIWRlZmF1bHQ7XG5cbiRvcmFuZ2UtMTAwOiB0aW50LWNvbG9yKCRvcmFuZ2UsIDgwJSkgIWRlZmF1bHQ7XG4kb3JhbmdlLTIwMDogdGludC1jb2xvcigkb3JhbmdlLCA2MCUpICFkZWZhdWx0O1xuJG9yYW5nZS0zMDA6IHRpbnQtY29sb3IoJG9yYW5nZSwgNDAlKSAhZGVmYXVsdDtcbiRvcmFuZ2UtNDAwOiB0aW50LWNvbG9yKCRvcmFuZ2UsIDIwJSkgIWRlZmF1bHQ7XG4kb3JhbmdlLTUwMDogJG9yYW5nZSAhZGVmYXVsdDtcbiRvcmFuZ2UtNjAwOiBzaGFkZS1jb2xvcigkb3JhbmdlLCAyMCUpICFkZWZhdWx0O1xuJG9yYW5nZS03MDA6IHNoYWRlLWNvbG9yKCRvcmFuZ2UsIDQwJSkgIWRlZmF1bHQ7XG4kb3JhbmdlLTgwMDogc2hhZGUtY29sb3IoJG9yYW5nZSwgNjAlKSAhZGVmYXVsdDtcbiRvcmFuZ2UtOTAwOiBzaGFkZS1jb2xvcigkb3JhbmdlLCA4MCUpICFkZWZhdWx0O1xuXG4keWVsbG93LTEwMDogdGludC1jb2xvcigkeWVsbG93LCA4MCUpICFkZWZhdWx0O1xuJHllbGxvdy0yMDA6IHRpbnQtY29sb3IoJHllbGxvdywgNjAlKSAhZGVmYXVsdDtcbiR5ZWxsb3ctMzAwOiB0aW50LWNvbG9yKCR5ZWxsb3csIDQwJSkgIWRlZmF1bHQ7XG4keWVsbG93LTQwMDogdGludC1jb2xvcigkeWVsbG93LCAyMCUpICFkZWZhdWx0O1xuJHllbGxvdy01MDA6ICR5ZWxsb3cgIWRlZmF1bHQ7XG4keWVsbG93LTYwMDogc2hhZGUtY29sb3IoJHllbGxvdywgMjAlKSAhZGVmYXVsdDtcbiR5ZWxsb3ctNzAwOiBzaGFkZS1jb2xvcigkeWVsbG93LCA0MCUpICFkZWZhdWx0O1xuJHllbGxvdy04MDA6IHNoYWRlLWNvbG9yKCR5ZWxsb3csIDYwJSkgIWRlZmF1bHQ7XG4keWVsbG93LTkwMDogc2hhZGUtY29sb3IoJHllbGxvdywgODAlKSAhZGVmYXVsdDtcblxuJGdyZWVuLTEwMDogdGludC1jb2xvcigkZ3JlZW4sIDgwJSkgIWRlZmF1bHQ7XG4kZ3JlZW4tMjAwOiB0aW50LWNvbG9yKCRncmVlbiwgNjAlKSAhZGVmYXVsdDtcbiRncmVlbi0zMDA6IHRpbnQtY29sb3IoJGdyZWVuLCA0MCUpICFkZWZhdWx0O1xuJGdyZWVuLTQwMDogdGludC1jb2xvcigkZ3JlZW4sIDIwJSkgIWRlZmF1bHQ7XG4kZ3JlZW4tNTAwOiAkZ3JlZW4gIWRlZmF1bHQ7XG4kZ3JlZW4tNjAwOiBzaGFkZS1jb2xvcigkZ3JlZW4sIDIwJSkgIWRlZmF1bHQ7XG4kZ3JlZW4tNzAwOiBzaGFkZS1jb2xvcigkZ3JlZW4sIDQwJSkgIWRlZmF1bHQ7XG4kZ3JlZW4tODAwOiBzaGFkZS1jb2xvcigkZ3JlZW4sIDYwJSkgIWRlZmF1bHQ7XG4kZ3JlZW4tOTAwOiBzaGFkZS1jb2xvcigkZ3JlZW4sIDgwJSkgIWRlZmF1bHQ7XG5cbiR0ZWFsLTEwMDogdGludC1jb2xvcigkdGVhbCwgODAlKSAhZGVmYXVsdDtcbiR0ZWFsLTIwMDogdGludC1jb2xvcigkdGVhbCwgNjAlKSAhZGVmYXVsdDtcbiR0ZWFsLTMwMDogdGludC1jb2xvcigkdGVhbCwgNDAlKSAhZGVmYXVsdDtcbiR0ZWFsLTQwMDogdGludC1jb2xvcigkdGVhbCwgMjAlKSAhZGVmYXVsdDtcbiR0ZWFsLTUwMDogJHRlYWwgIWRlZmF1bHQ7XG4kdGVhbC02MDA6IHNoYWRlLWNvbG9yKCR0ZWFsLCAyMCUpICFkZWZhdWx0O1xuJHRlYWwtNzAwOiBzaGFkZS1jb2xvcigkdGVhbCwgNDAlKSAhZGVmYXVsdDtcbiR0ZWFsLTgwMDogc2hhZGUtY29sb3IoJHRlYWwsIDYwJSkgIWRlZmF1bHQ7XG4kdGVhbC05MDA6IHNoYWRlLWNvbG9yKCR0ZWFsLCA4MCUpICFkZWZhdWx0O1xuXG4kY3lhbi0xMDA6IHRpbnQtY29sb3IoJGN5YW4sIDgwJSkgIWRlZmF1bHQ7XG4kY3lhbi0yMDA6IHRpbnQtY29sb3IoJGN5YW4sIDYwJSkgIWRlZmF1bHQ7XG4kY3lhbi0zMDA6IHRpbnQtY29sb3IoJGN5YW4sIDQwJSkgIWRlZmF1bHQ7XG4kY3lhbi00MDA6IHRpbnQtY29sb3IoJGN5YW4sIDIwJSkgIWRlZmF1bHQ7XG4kY3lhbi01MDA6ICRjeWFuICFkZWZhdWx0O1xuJGN5YW4tNjAwOiBzaGFkZS1jb2xvcigkY3lhbiwgMjAlKSAhZGVmYXVsdDtcbiRjeWFuLTcwMDogc2hhZGUtY29sb3IoJGN5YW4sIDQwJSkgIWRlZmF1bHQ7XG4kY3lhbi04MDA6IHNoYWRlLWNvbG9yKCRjeWFuLCA2MCUpICFkZWZhdWx0O1xuJGN5YW4tOTAwOiBzaGFkZS1jb2xvcigkY3lhbiwgODAlKSAhZGVmYXVsdDtcblxuJGJsdWVzOiAoXG4gIFwiYmx1ZS0xMDBcIjogJGJsdWUtMTAwLFxuICBcImJsdWUtMjAwXCI6ICRibHVlLTIwMCxcbiAgXCJibHVlLTMwMFwiOiAkYmx1ZS0zMDAsXG4gIFwiYmx1ZS00MDBcIjogJGJsdWUtNDAwLFxuICBcImJsdWUtNTAwXCI6ICRibHVlLTUwMCxcbiAgXCJibHVlLTYwMFwiOiAkYmx1ZS02MDAsXG4gIFwiYmx1ZS03MDBcIjogJGJsdWUtNzAwLFxuICBcImJsdWUtODAwXCI6ICRibHVlLTgwMCxcbiAgXCJibHVlLTkwMFwiOiAkYmx1ZS05MDBcbikgIWRlZmF1bHQ7XG5cbiRpbmRpZ29zOiAoXG4gIFwiaW5kaWdvLTEwMFwiOiAkaW5kaWdvLTEwMCxcbiAgXCJpbmRpZ28tMjAwXCI6ICRpbmRpZ28tMjAwLFxuICBcImluZGlnby0zMDBcIjogJGluZGlnby0zMDAsXG4gIFwiaW5kaWdvLTQwMFwiOiAkaW5kaWdvLTQwMCxcbiAgXCJpbmRpZ28tNTAwXCI6ICRpbmRpZ28tNTAwLFxuICBcImluZGlnby02MDBcIjogJGluZGlnby02MDAsXG4gIFwiaW5kaWdvLTcwMFwiOiAkaW5kaWdvLTcwMCxcbiAgXCJpbmRpZ28tODAwXCI6ICRpbmRpZ28tODAwLFxuICBcImluZGlnby05MDBcIjogJGluZGlnby05MDBcbikgIWRlZmF1bHQ7XG5cbiRwdXJwbGVzOiAoXG4gIFwicHVycGxlLTEwMFwiOiAkcHVycGxlLTIwMCxcbiAgXCJwdXJwbGUtMjAwXCI6ICRwdXJwbGUtMTAwLFxuICBcInB1cnBsZS0zMDBcIjogJHB1cnBsZS0zMDAsXG4gIFwicHVycGxlLTQwMFwiOiAkcHVycGxlLTQwMCxcbiAgXCJwdXJwbGUtNTAwXCI6ICRwdXJwbGUtNTAwLFxuICBcInB1cnBsZS02MDBcIjogJHB1cnBsZS02MDAsXG4gIFwicHVycGxlLTcwMFwiOiAkcHVycGxlLTcwMCxcbiAgXCJwdXJwbGUtODAwXCI6ICRwdXJwbGUtODAwLFxuICBcInB1cnBsZS05MDBcIjogJHB1cnBsZS05MDBcbikgIWRlZmF1bHQ7XG5cbiRwaW5rczogKFxuICBcInBpbmstMTAwXCI6ICRwaW5rLTEwMCxcbiAgXCJwaW5rLTIwMFwiOiAkcGluay0yMDAsXG4gIFwicGluay0zMDBcIjogJHBpbmstMzAwLFxuICBcInBpbmstNDAwXCI6ICRwaW5rLTQwMCxcbiAgXCJwaW5rLTUwMFwiOiAkcGluay01MDAsXG4gIFwicGluay02MDBcIjogJHBpbmstNjAwLFxuICBcInBpbmstNzAwXCI6ICRwaW5rLTcwMCxcbiAgXCJwaW5rLTgwMFwiOiAkcGluay04MDAsXG4gIFwicGluay05MDBcIjogJHBpbmstOTAwXG4pICFkZWZhdWx0O1xuXG4kcmVkczogKFxuICBcInJlZC0xMDBcIjogJHJlZC0xMDAsXG4gIFwicmVkLTIwMFwiOiAkcmVkLTIwMCxcbiAgXCJyZWQtMzAwXCI6ICRyZWQtMzAwLFxuICBcInJlZC00MDBcIjogJHJlZC00MDAsXG4gIFwicmVkLTUwMFwiOiAkcmVkLTUwMCxcbiAgXCJyZWQtNjAwXCI6ICRyZWQtNjAwLFxuICBcInJlZC03MDBcIjogJHJlZC03MDAsXG4gIFwicmVkLTgwMFwiOiAkcmVkLTgwMCxcbiAgXCJyZWQtOTAwXCI6ICRyZWQtOTAwXG4pICFkZWZhdWx0O1xuXG4kb3JhbmdlczogKFxuICBcIm9yYW5nZS0xMDBcIjogJG9yYW5nZS0xMDAsXG4gIFwib3JhbmdlLTIwMFwiOiAkb3JhbmdlLTIwMCxcbiAgXCJvcmFuZ2UtMzAwXCI6ICRvcmFuZ2UtMzAwLFxuICBcIm9yYW5nZS00MDBcIjogJG9yYW5nZS00MDAsXG4gIFwib3JhbmdlLTUwMFwiOiAkb3JhbmdlLTUwMCxcbiAgXCJvcmFuZ2UtNjAwXCI6ICRvcmFuZ2UtNjAwLFxuICBcIm9yYW5nZS03MDBcIjogJG9yYW5nZS03MDAsXG4gIFwib3JhbmdlLTgwMFwiOiAkb3JhbmdlLTgwMCxcbiAgXCJvcmFuZ2UtOTAwXCI6ICRvcmFuZ2UtOTAwXG4pICFkZWZhdWx0O1xuXG4keWVsbG93czogKFxuICBcInllbGxvdy0xMDBcIjogJHllbGxvdy0xMDAsXG4gIFwieWVsbG93LTIwMFwiOiAkeWVsbG93LTIwMCxcbiAgXCJ5ZWxsb3ctMzAwXCI6ICR5ZWxsb3ctMzAwLFxuICBcInllbGxvdy00MDBcIjogJHllbGxvdy00MDAsXG4gIFwieWVsbG93LTUwMFwiOiAkeWVsbG93LTUwMCxcbiAgXCJ5ZWxsb3ctNjAwXCI6ICR5ZWxsb3ctNjAwLFxuICBcInllbGxvdy03MDBcIjogJHllbGxvdy03MDAsXG4gIFwieWVsbG93LTgwMFwiOiAkeWVsbG93LTgwMCxcbiAgXCJ5ZWxsb3ctOTAwXCI6ICR5ZWxsb3ctOTAwXG4pICFkZWZhdWx0O1xuXG4kZ3JlZW5zOiAoXG4gIFwiZ3JlZW4tMTAwXCI6ICRncmVlbi0xMDAsXG4gIFwiZ3JlZW4tMjAwXCI6ICRncmVlbi0yMDAsXG4gIFwiZ3JlZW4tMzAwXCI6ICRncmVlbi0zMDAsXG4gIFwiZ3JlZW4tNDAwXCI6ICRncmVlbi00MDAsXG4gIFwiZ3JlZW4tNTAwXCI6ICRncmVlbi01MDAsXG4gIFwiZ3JlZW4tNjAwXCI6ICRncmVlbi02MDAsXG4gIFwiZ3JlZW4tNzAwXCI6ICRncmVlbi03MDAsXG4gIFwiZ3JlZW4tODAwXCI6ICRncmVlbi04MDAsXG4gIFwiZ3JlZW4tOTAwXCI6ICRncmVlbi05MDBcbikgIWRlZmF1bHQ7XG5cbiR0ZWFsczogKFxuICBcInRlYWwtMTAwXCI6ICR0ZWFsLTEwMCxcbiAgXCJ0ZWFsLTIwMFwiOiAkdGVhbC0yMDAsXG4gIFwidGVhbC0zMDBcIjogJHRlYWwtMzAwLFxuICBcInRlYWwtNDAwXCI6ICR0ZWFsLTQwMCxcbiAgXCJ0ZWFsLTUwMFwiOiAkdGVhbC01MDAsXG4gIFwidGVhbC02MDBcIjogJHRlYWwtNjAwLFxuICBcInRlYWwtNzAwXCI6ICR0ZWFsLTcwMCxcbiAgXCJ0ZWFsLTgwMFwiOiAkdGVhbC04MDAsXG4gIFwidGVhbC05MDBcIjogJHRlYWwtOTAwXG4pICFkZWZhdWx0O1xuXG4kY3lhbnM6IChcbiAgXCJjeWFuLTEwMFwiOiAkY3lhbi0xMDAsXG4gIFwiY3lhbi0yMDBcIjogJGN5YW4tMjAwLFxuICBcImN5YW4tMzAwXCI6ICRjeWFuLTMwMCxcbiAgXCJjeWFuLTQwMFwiOiAkY3lhbi00MDAsXG4gIFwiY3lhbi01MDBcIjogJGN5YW4tNTAwLFxuICBcImN5YW4tNjAwXCI6ICRjeWFuLTYwMCxcbiAgXCJjeWFuLTcwMFwiOiAkY3lhbi03MDAsXG4gIFwiY3lhbi04MDBcIjogJGN5YW4tODAwLFxuICBcImN5YW4tOTAwXCI6ICRjeWFuLTkwMFxuKSAhZGVmYXVsdDtcbi8vIGZ1c3YtZW5hYmxlXG5cbi8vIENoYXJhY3RlcnMgd2hpY2ggYXJlIGVzY2FwZWQgYnkgdGhlIGVzY2FwZS1zdmcgZnVuY3Rpb25cbiRlc2NhcGVkLWNoYXJhY3RlcnM6IChcbiAgKFwiPFwiLCBcIiUzY1wiKSxcbiAgKFwiPlwiLCBcIiUzZVwiKSxcbiAgKFwiI1wiLCBcIiUyM1wiKSxcbiAgKFwiKFwiLCBcIiUyOFwiKSxcbiAgKFwiKVwiLCBcIiUyOVwiKSxcbikgIWRlZmF1bHQ7XG5cbi8vIE9wdGlvbnNcbi8vXG4vLyBRdWlja2x5IG1vZGlmeSBnbG9iYWwgc3R5bGluZyBieSBlbmFibGluZyBvciBkaXNhYmxpbmcgb3B0aW9uYWwgZmVhdHVyZXMuXG5cbiRlbmFibGUtY2FyZXQ6ICAgICAgICAgICAgICAgIHRydWUgIWRlZmF1bHQ7XG4kZW5hYmxlLXJvdW5kZWQ6ICAgICAgICAgICAgICB0cnVlICFkZWZhdWx0O1xuJGVuYWJsZS1zaGFkb3dzOiAgICAgICAgICAgICAgZmFsc2UgIWRlZmF1bHQ7XG4kZW5hYmxlLWdyYWRpZW50czogICAgICAgICAgICBmYWxzZSAhZGVmYXVsdDtcbiRlbmFibGUtdHJhbnNpdGlvbnM6ICAgICAgICAgIHRydWUgIWRlZmF1bHQ7XG4kZW5hYmxlLXJlZHVjZWQtbW90aW9uOiAgICAgICB0cnVlICFkZWZhdWx0O1xuJGVuYWJsZS1zbW9vdGgtc2Nyb2xsOiAgICAgICAgdHJ1ZSAhZGVmYXVsdDtcbiRlbmFibGUtZ3JpZC1jbGFzc2VzOiAgICAgICAgIHRydWUgIWRlZmF1bHQ7XG4kZW5hYmxlLWNzc2dyaWQ6ICAgICAgICAgICAgICBmYWxzZSAhZGVmYXVsdDtcbiRlbmFibGUtYnV0dG9uLXBvaW50ZXJzOiAgICAgIHRydWUgIWRlZmF1bHQ7XG4kZW5hYmxlLXJmczogICAgICAgICAgICAgICAgICB0cnVlICFkZWZhdWx0O1xuJGVuYWJsZS12YWxpZGF0aW9uLWljb25zOiAgICAgdHJ1ZSAhZGVmYXVsdDtcbiRlbmFibGUtbmVnYXRpdmUtbWFyZ2luczogICAgIGZhbHNlICFkZWZhdWx0O1xuJGVuYWJsZS1kZXByZWNhdGlvbi1tZXNzYWdlczogdHJ1ZSAhZGVmYXVsdDtcbiRlbmFibGUtaW1wb3J0YW50LXV0aWxpdGllczogIHRydWUgIWRlZmF1bHQ7XG5cbi8vIFByZWZpeCBmb3IgOnJvb3QgQ1NTIHZhcmlhYmxlc1xuXG4kdmFyaWFibGUtcHJlZml4OiAgICAgICAgICAgICBicy0gIWRlZmF1bHQ7XG5cbi8vIEdyYWRpZW50XG4vL1xuLy8gVGhlIGdyYWRpZW50IHdoaWNoIGlzIGFkZGVkIHRvIGNvbXBvbmVudHMgaWYgYCRlbmFibGUtZ3JhZGllbnRzYCBpcyBgdHJ1ZWBcbi8vIFRoaXMgZ3JhZGllbnQgaXMgYWxzbyBhZGRlZCB0byBlbGVtZW50cyB3aXRoIGAuYmctZ3JhZGllbnRgXG4vLyBzY3NzLWRvY3Mtc3RhcnQgdmFyaWFibGUtZ3JhZGllbnRcbiRncmFkaWVudDogbGluZWFyLWdyYWRpZW50KDE4MGRlZywgcmdiYSgkd2hpdGUsIC4xNSksIHJnYmEoJHdoaXRlLCAwKSkgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHZhcmlhYmxlLWdyYWRpZW50XG5cbi8vIFNwYWNpbmdcbi8vXG4vLyBDb250cm9sIHRoZSBkZWZhdWx0IHN0eWxpbmcgb2YgbW9zdCBCb290c3RyYXAgZWxlbWVudHMgYnkgbW9kaWZ5aW5nIHRoZXNlXG4vLyB2YXJpYWJsZXMuIE1vc3RseSBmb2N1c2VkIG9uIHNwYWNpbmcuXG4vLyBZb3UgY2FuIGFkZCBtb3JlIGVudHJpZXMgdG8gdGhlICRzcGFjZXJzIG1hcCwgc2hvdWxkIHlvdSBuZWVkIG1vcmUgdmFyaWF0aW9uLlxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgc3BhY2VyLXZhcmlhYmxlcy1tYXBzXG4kc3BhY2VyOiAxcmVtICFkZWZhdWx0O1xuJHNwYWNlcnM6IChcbiAgMDogMCxcbiAgMTogJHNwYWNlciAqIC4yNSxcbiAgMjogJHNwYWNlciAqIC41LFxuICAzOiAkc3BhY2VyLFxuICA0OiAkc3BhY2VyICogMS41LFxuICA1OiAkc3BhY2VyICogMyxcbikgIWRlZmF1bHQ7XG5cbiRuZWdhdGl2ZS1zcGFjZXJzOiBpZigkZW5hYmxlLW5lZ2F0aXZlLW1hcmdpbnMsIG5lZ2F0aXZpZnktbWFwKCRzcGFjZXJzKSwgbnVsbCkgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHNwYWNlci12YXJpYWJsZXMtbWFwc1xuXG4vLyBQb3NpdGlvblxuLy9cbi8vIERlZmluZSB0aGUgZWRnZSBwb3NpdGlvbmluZyBhbmNob3JzIG9mIHRoZSBwb3NpdGlvbiB1dGlsaXRpZXMuXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBwb3NpdGlvbi1tYXBcbiRwb3NpdGlvbi12YWx1ZXM6IChcbiAgMDogMCxcbiAgNTA6IDUwJSxcbiAgMTAwOiAxMDAlXG4pICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBwb3NpdGlvbi1tYXBcblxuLy8gQm9keVxuLy9cbi8vIFNldHRpbmdzIGZvciB0aGUgYDxib2R5PmAgZWxlbWVudC5cblxuJGJvZHktYmc6ICAgICAgICAgICAgICAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRib2R5LWNvbG9yOiAgICAgICAgICAgICAgICAkZ3JheS05MDAgIWRlZmF1bHQ7XG4kYm9keS10ZXh0LWFsaWduOiAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcblxuLy8gVXRpbGl0aWVzIG1hcHNcbi8vXG4vLyBFeHRlbmRzIHRoZSBkZWZhdWx0IGAkdGhlbWUtY29sb3JzYCBtYXBzIHRvIGhlbHAgY3JlYXRlIG91ciB1dGlsaXRpZXMuXG5cbi8vIENvbWUgdjYsIHdlJ2xsIGRlLWR1cGUgdGhlc2UgdmFyaWFibGVzLiBVbnRpbCB0aGVuLCBmb3IgYmFja3dhcmQgY29tcGF0aWJpbGl0eSwgd2Uga2VlcCB0aGVtIHRvIHJlYXNzaWduLlxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHV0aWxpdGllcy1jb2xvcnNcbiR1dGlsaXRpZXMtY29sb3JzOiAkdGhlbWUtY29sb3JzLXJnYiAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgdXRpbGl0aWVzLWNvbG9yc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgdXRpbGl0aWVzLXRleHQtY29sb3JzXG4kdXRpbGl0aWVzLXRleHQ6IG1hcC1tZXJnZShcbiAgJHV0aWxpdGllcy1jb2xvcnMsXG4gIChcbiAgICBcImJsYWNrXCI6IHRvLXJnYigkYmxhY2spLFxuICAgIFwid2hpdGVcIjogdG8tcmdiKCR3aGl0ZSksXG4gICAgXCJib2R5XCI6IHRvLXJnYigkYm9keS1jb2xvcilcbiAgKVxuKSAhZGVmYXVsdDtcbiR1dGlsaXRpZXMtdGV4dC1jb2xvcnM6IG1hcC1sb29wKCR1dGlsaXRpZXMtdGV4dCwgcmdiYS1jc3MtdmFyLCBcIiRrZXlcIiwgXCJ0ZXh0XCIpICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCB1dGlsaXRpZXMtdGV4dC1jb2xvcnNcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHV0aWxpdGllcy1iZy1jb2xvcnNcbiR1dGlsaXRpZXMtYmc6IG1hcC1tZXJnZShcbiAgJHV0aWxpdGllcy1jb2xvcnMsXG4gIChcbiAgICBcImJsYWNrXCI6IHRvLXJnYigkYmxhY2spLFxuICAgIFwid2hpdGVcIjogdG8tcmdiKCR3aGl0ZSksXG4gICAgXCJib2R5XCI6IHRvLXJnYigkYm9keS1iZylcbiAgKVxuKSAhZGVmYXVsdDtcbiR1dGlsaXRpZXMtYmctY29sb3JzOiBtYXAtbG9vcCgkdXRpbGl0aWVzLWJnLCByZ2JhLWNzcy12YXIsIFwiJGtleVwiLCBcImJnXCIpICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCB1dGlsaXRpZXMtYmctY29sb3JzXG5cbi8vIExpbmtzXG4vL1xuLy8gU3R5bGUgYW5jaG9yIGVsZW1lbnRzLlxuXG4kbGluay1jb2xvcjogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkcHJpbWFyeSAhZGVmYXVsdDtcbiRsaW5rLWRlY29yYXRpb246ICAgICAgICAgICAgICAgICAgICAgICAgIHVuZGVybGluZSAhZGVmYXVsdDtcbiRsaW5rLXNoYWRlLXBlcmNlbnRhZ2U6ICAgICAgICAgICAgICAgICAgIDIwJSAhZGVmYXVsdDtcbiRsaW5rLWhvdmVyLWNvbG9yOiAgICAgICAgICAgICAgICAgICAgICAgIHNoaWZ0LWNvbG9yKCRsaW5rLWNvbG9yLCAkbGluay1zaGFkZS1wZXJjZW50YWdlKSAhZGVmYXVsdDtcbiRsaW5rLWhvdmVyLWRlY29yYXRpb246ICAgICAgICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG5cbiRzdHJldGNoZWQtbGluay1wc2V1ZG8tZWxlbWVudDogICAgICAgICAgIGFmdGVyICFkZWZhdWx0O1xuJHN0cmV0Y2hlZC1saW5rLXotaW5kZXg6ICAgICAgICAgICAgICAgICAgMSAhZGVmYXVsdDtcblxuLy8gUGFyYWdyYXBoc1xuLy9cbi8vIFN0eWxlIHAgZWxlbWVudC5cblxuJHBhcmFncmFwaC1tYXJnaW4tYm90dG9tOiAgIDFyZW0gIWRlZmF1bHQ7XG5cblxuLy8gR3JpZCBicmVha3BvaW50c1xuLy9cbi8vIERlZmluZSB0aGUgbWluaW11bSBkaW1lbnNpb25zIGF0IHdoaWNoIHlvdXIgbGF5b3V0IHdpbGwgY2hhbmdlLFxuLy8gYWRhcHRpbmcgdG8gZGlmZmVyZW50IHNjcmVlbiBzaXplcywgZm9yIHVzZSBpbiBtZWRpYSBxdWVyaWVzLlxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZ3JpZC1icmVha3BvaW50c1xuJGdyaWQtYnJlYWtwb2ludHM6IChcbiAgeHM6IDAsXG4gIHNtOiA1NzZweCxcbiAgbWQ6IDc2OHB4LFxuICBsZzogOTkycHgsXG4gIHhsOiAxMjAwcHgsXG4gIHh4bDogMTQwMHB4XG4pICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBncmlkLWJyZWFrcG9pbnRzXG5cbkBpbmNsdWRlIF9hc3NlcnQtYXNjZW5kaW5nKCRncmlkLWJyZWFrcG9pbnRzLCBcIiRncmlkLWJyZWFrcG9pbnRzXCIpO1xuQGluY2x1ZGUgX2Fzc2VydC1zdGFydHMtYXQtemVybygkZ3JpZC1icmVha3BvaW50cywgXCIkZ3JpZC1icmVha3BvaW50c1wiKTtcblxuXG4vLyBHcmlkIGNvbnRhaW5lcnNcbi8vXG4vLyBEZWZpbmUgdGhlIG1heGltdW0gd2lkdGggb2YgYC5jb250YWluZXJgIGZvciBkaWZmZXJlbnQgc2NyZWVuIHNpemVzLlxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgY29udGFpbmVyLW1heC13aWR0aHNcbiRjb250YWluZXItbWF4LXdpZHRoczogKFxuICBzbTogNTQwcHgsXG4gIG1kOiA3MjBweCxcbiAgbGc6IDk2MHB4LFxuICB4bDogMTE0MHB4LFxuICB4eGw6IDEzMjBweFxuKSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgY29udGFpbmVyLW1heC13aWR0aHNcblxuQGluY2x1ZGUgX2Fzc2VydC1hc2NlbmRpbmcoJGNvbnRhaW5lci1tYXgtd2lkdGhzLCBcIiRjb250YWluZXItbWF4LXdpZHRoc1wiKTtcblxuXG4vLyBHcmlkIGNvbHVtbnNcbi8vXG4vLyBTZXQgdGhlIG51bWJlciBvZiBjb2x1bW5zIGFuZCBzcGVjaWZ5IHRoZSB3aWR0aCBvZiB0aGUgZ3V0dGVycy5cblxuJGdyaWQtY29sdW1uczogICAgICAgICAgICAgICAgMTIgIWRlZmF1bHQ7XG4kZ3JpZC1ndXR0ZXItd2lkdGg6ICAgICAgICAgICAxLjVyZW0gIWRlZmF1bHQ7XG4kZ3JpZC1yb3ctY29sdW1uczogICAgICAgICAgICA2ICFkZWZhdWx0O1xuXG4kZ3V0dGVyczogJHNwYWNlcnMgIWRlZmF1bHQ7XG5cbi8vIENvbnRhaW5lciBwYWRkaW5nXG5cbiRjb250YWluZXItcGFkZGluZy14OiAkZ3JpZC1ndXR0ZXItd2lkdGggKiAuNSAhZGVmYXVsdDtcblxuXG4vLyBDb21wb25lbnRzXG4vL1xuLy8gRGVmaW5lIGNvbW1vbiBwYWRkaW5nIGFuZCBib3JkZXIgcmFkaXVzIHNpemVzIGFuZCBtb3JlLlxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgYm9yZGVyLXZhcmlhYmxlc1xuJGJvcmRlci13aWR0aDogICAgICAgICAgICAgICAgMXB4ICFkZWZhdWx0O1xuJGJvcmRlci13aWR0aHM6IChcbiAgMTogMXB4LFxuICAyOiAycHgsXG4gIDM6IDNweCxcbiAgNDogNHB4LFxuICA1OiA1cHhcbikgIWRlZmF1bHQ7XG5cbiRib3JkZXItY29sb3I6ICAgICAgICAgICAgICAgICRncmF5LTMwMCAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgYm9yZGVyLXZhcmlhYmxlc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgYm9yZGVyLXJhZGl1cy12YXJpYWJsZXNcbiRib3JkZXItcmFkaXVzOiAgICAgICAgICAgICAgIC4yNXJlbSAhZGVmYXVsdDtcbiRib3JkZXItcmFkaXVzLXNtOiAgICAgICAgICAgIC4ycmVtICFkZWZhdWx0O1xuJGJvcmRlci1yYWRpdXMtbGc6ICAgICAgICAgICAgLjNyZW0gIWRlZmF1bHQ7XG4kYm9yZGVyLXJhZGl1cy1waWxsOiAgICAgICAgICA1MHJlbSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgYm9yZGVyLXJhZGl1cy12YXJpYWJsZXNcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGJveC1zaGFkb3ctdmFyaWFibGVzXG4kYm94LXNoYWRvdzogICAgICAgICAgICAgICAgICAwIC41cmVtIDFyZW0gcmdiYSgkYmxhY2ssIC4xNSkgIWRlZmF1bHQ7XG4kYm94LXNoYWRvdy1zbTogICAgICAgICAgICAgICAwIC4xMjVyZW0gLjI1cmVtIHJnYmEoJGJsYWNrLCAuMDc1KSAhZGVmYXVsdDtcbiRib3gtc2hhZG93LWxnOiAgICAgICAgICAgICAgIDAgMXJlbSAzcmVtIHJnYmEoJGJsYWNrLCAuMTc1KSAhZGVmYXVsdDtcbiRib3gtc2hhZG93LWluc2V0OiAgICAgICAgICAgIGluc2V0IDAgMXB4IDJweCByZ2JhKCRibGFjaywgLjA3NSkgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGJveC1zaGFkb3ctdmFyaWFibGVzXG5cbiRjb21wb25lbnQtYWN0aXZlLWNvbG9yOiAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRjb21wb25lbnQtYWN0aXZlLWJnOiAgICAgICAgICRwcmltYXJ5ICFkZWZhdWx0O1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgY2FyZXQtdmFyaWFibGVzXG4kY2FyZXQtd2lkdGg6ICAgICAgICAgICAgICAgICAuM2VtICFkZWZhdWx0O1xuJGNhcmV0LXZlcnRpY2FsLWFsaWduOiAgICAgICAgJGNhcmV0LXdpZHRoICogLjg1ICFkZWZhdWx0O1xuJGNhcmV0LXNwYWNpbmc6ICAgICAgICAgICAgICAgJGNhcmV0LXdpZHRoICogLjg1ICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBjYXJldC12YXJpYWJsZXNcblxuJHRyYW5zaXRpb24tYmFzZTogICAgICAgICAgICAgYWxsIC4ycyBlYXNlLWluLW91dCAhZGVmYXVsdDtcbiR0cmFuc2l0aW9uLWZhZGU6ICAgICAgICAgICAgIG9wYWNpdHkgLjE1cyBsaW5lYXIgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3Mtc3RhcnQgY29sbGFwc2UtdHJhbnNpdGlvblxuJHRyYW5zaXRpb24tY29sbGFwc2U6ICAgICAgICAgaGVpZ2h0IC4zNXMgZWFzZSAhZGVmYXVsdDtcbiR0cmFuc2l0aW9uLWNvbGxhcHNlLXdpZHRoOiAgIHdpZHRoIC4zNXMgZWFzZSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgY29sbGFwc2UtdHJhbnNpdGlvblxuXG4vLyBzdHlsZWxpbnQtZGlzYWJsZSBmdW5jdGlvbi1kaXNhbGxvd2VkLWxpc3Rcbi8vIHNjc3MtZG9jcy1zdGFydCBhc3BlY3QtcmF0aW9zXG4kYXNwZWN0LXJhdGlvczogKFxuICBcIjF4MVwiOiAxMDAlLFxuICBcIjR4M1wiOiBjYWxjKDMgLyA0ICogMTAwJSksXG4gIFwiMTZ4OVwiOiBjYWxjKDkgLyAxNiAqIDEwMCUpLFxuICBcIjIxeDlcIjogY2FsYyg5IC8gMjEgKiAxMDAlKVxuKSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgYXNwZWN0LXJhdGlvc1xuLy8gc3R5bGVsaW50LWVuYWJsZSBmdW5jdGlvbi1kaXNhbGxvd2VkLWxpc3RcblxuLy8gVHlwb2dyYXBoeVxuLy9cbi8vIEZvbnQsIGxpbmUtaGVpZ2h0LCBhbmQgY29sb3IgZm9yIGJvZHkgdGV4dCwgaGVhZGluZ3MsIGFuZCBtb3JlLlxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZm9udC12YXJpYWJsZXNcbi8vIHN0eWxlbGludC1kaXNhYmxlIHZhbHVlLWtleXdvcmQtY2FzZVxuJGZvbnQtZmFtaWx5LXNhbnMtc2VyaWY6ICAgICAgc3lzdGVtLXVpLCAtYXBwbGUtc3lzdGVtLCBcIlNlZ29lIFVJXCIsIFJvYm90bywgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgXCJOb3RvIFNhbnNcIiwgXCJMaWJlcmF0aW9uIFNhbnNcIiwgc2Fucy1zZXJpZiwgXCJBcHBsZSBDb2xvciBFbW9qaVwiLCBcIlNlZ29lIFVJIEVtb2ppXCIsIFwiU2Vnb2UgVUkgU3ltYm9sXCIsIFwiTm90byBDb2xvciBFbW9qaVwiICFkZWZhdWx0O1xuJGZvbnQtZmFtaWx5LW1vbm9zcGFjZTogICAgICAgU0ZNb25vLVJlZ3VsYXIsIE1lbmxvLCBNb25hY28sIENvbnNvbGFzLCBcIkxpYmVyYXRpb24gTW9ub1wiLCBcIkNvdXJpZXIgTmV3XCIsIG1vbm9zcGFjZSAhZGVmYXVsdDtcbi8vIHN0eWxlbGludC1lbmFibGUgdmFsdWUta2V5d29yZC1jYXNlXG4kZm9udC1mYW1pbHktYmFzZTogICAgICAgICAgICB2YXIoLS0jeyR2YXJpYWJsZS1wcmVmaXh9Zm9udC1zYW5zLXNlcmlmKSAhZGVmYXVsdDtcbiRmb250LWZhbWlseS1jb2RlOiAgICAgICAgICAgIHZhcigtLSN7JHZhcmlhYmxlLXByZWZpeH1mb250LW1vbm9zcGFjZSkgIWRlZmF1bHQ7XG5cbi8vICRmb250LXNpemUtcm9vdCBhZmZlY3RzIHRoZSB2YWx1ZSBvZiBgcmVtYCwgd2hpY2ggaXMgdXNlZCBmb3IgYXMgd2VsbCBmb250IHNpemVzLCBwYWRkaW5ncywgYW5kIG1hcmdpbnNcbi8vICRmb250LXNpemUtYmFzZSBhZmZlY3RzIHRoZSBmb250IHNpemUgb2YgdGhlIGJvZHkgdGV4dFxuJGZvbnQtc2l6ZS1yb290OiAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRmb250LXNpemUtYmFzZTogICAgICAgICAgICAgIDFyZW0gIWRlZmF1bHQ7IC8vIEFzc3VtZXMgdGhlIGJyb3dzZXIgZGVmYXVsdCwgdHlwaWNhbGx5IGAxNnB4YFxuJGZvbnQtc2l6ZS1zbTogICAgICAgICAgICAgICAgJGZvbnQtc2l6ZS1iYXNlICogLjg3NSAhZGVmYXVsdDtcbiRmb250LXNpemUtbGc6ICAgICAgICAgICAgICAgICRmb250LXNpemUtYmFzZSAqIDEuMjUgIWRlZmF1bHQ7XG5cbiRmb250LXdlaWdodC1saWdodGVyOiAgICAgICAgIGxpZ2h0ZXIgIWRlZmF1bHQ7XG4kZm9udC13ZWlnaHQtbGlnaHQ6ICAgICAgICAgICAzMDAgIWRlZmF1bHQ7XG4kZm9udC13ZWlnaHQtbm9ybWFsOiAgICAgICAgICA0MDAgIWRlZmF1bHQ7XG4kZm9udC13ZWlnaHQtYm9sZDogICAgICAgICAgICA3MDAgIWRlZmF1bHQ7XG4kZm9udC13ZWlnaHQtYm9sZGVyOiAgICAgICAgICBib2xkZXIgIWRlZmF1bHQ7XG5cbiRmb250LXdlaWdodC1iYXNlOiAgICAgICAgICAgICRmb250LXdlaWdodC1ub3JtYWwgIWRlZmF1bHQ7XG5cbiRsaW5lLWhlaWdodC1iYXNlOiAgICAgICAgICAgIDEuNSAhZGVmYXVsdDtcbiRsaW5lLWhlaWdodC1zbTogICAgICAgICAgICAgIDEuMjUgIWRlZmF1bHQ7XG4kbGluZS1oZWlnaHQtbGc6ICAgICAgICAgICAgICAyICFkZWZhdWx0O1xuXG4kaDEtZm9udC1zaXplOiAgICAgICAgICAgICAgICAkZm9udC1zaXplLWJhc2UgKiAyLjUgIWRlZmF1bHQ7XG4kaDItZm9udC1zaXplOiAgICAgICAgICAgICAgICAkZm9udC1zaXplLWJhc2UgKiAyICFkZWZhdWx0O1xuJGgzLWZvbnQtc2l6ZTogICAgICAgICAgICAgICAgJGZvbnQtc2l6ZS1iYXNlICogMS43NSAhZGVmYXVsdDtcbiRoNC1mb250LXNpemU6ICAgICAgICAgICAgICAgICRmb250LXNpemUtYmFzZSAqIDEuNSAhZGVmYXVsdDtcbiRoNS1mb250LXNpemU6ICAgICAgICAgICAgICAgICRmb250LXNpemUtYmFzZSAqIDEuMjUgIWRlZmF1bHQ7XG4kaDYtZm9udC1zaXplOiAgICAgICAgICAgICAgICAkZm9udC1zaXplLWJhc2UgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGZvbnQtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBmb250LXNpemVzXG4kZm9udC1zaXplczogKFxuICAxOiAkaDEtZm9udC1zaXplLFxuICAyOiAkaDItZm9udC1zaXplLFxuICAzOiAkaDMtZm9udC1zaXplLFxuICA0OiAkaDQtZm9udC1zaXplLFxuICA1OiAkaDUtZm9udC1zaXplLFxuICA2OiAkaDYtZm9udC1zaXplXG4pICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBmb250LXNpemVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBoZWFkaW5ncy12YXJpYWJsZXNcbiRoZWFkaW5ncy1tYXJnaW4tYm90dG9tOiAgICAgICRzcGFjZXIgKiAuNSAhZGVmYXVsdDtcbiRoZWFkaW5ncy1mb250LWZhbWlseTogICAgICAgIG51bGwgIWRlZmF1bHQ7XG4kaGVhZGluZ3MtZm9udC1zdHlsZTogICAgICAgICBudWxsICFkZWZhdWx0O1xuJGhlYWRpbmdzLWZvbnQtd2VpZ2h0OiAgICAgICAgNTAwICFkZWZhdWx0O1xuJGhlYWRpbmdzLWxpbmUtaGVpZ2h0OiAgICAgICAgMS4yICFkZWZhdWx0O1xuJGhlYWRpbmdzLWNvbG9yOiAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgaGVhZGluZ3MtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBkaXNwbGF5LWhlYWRpbmdzXG4kZGlzcGxheS1mb250LXNpemVzOiAoXG4gIDE6IDVyZW0sXG4gIDI6IDQuNXJlbSxcbiAgMzogNHJlbSxcbiAgNDogMy41cmVtLFxuICA1OiAzcmVtLFxuICA2OiAyLjVyZW1cbikgIWRlZmF1bHQ7XG5cbiRkaXNwbGF5LWZvbnQtd2VpZ2h0OiAzMDAgIWRlZmF1bHQ7XG4kZGlzcGxheS1saW5lLWhlaWdodDogJGhlYWRpbmdzLWxpbmUtaGVpZ2h0ICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBkaXNwbGF5LWhlYWRpbmdzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCB0eXBlLXZhcmlhYmxlc1xuJGxlYWQtZm9udC1zaXplOiAgICAgICAgICAgICAgJGZvbnQtc2l6ZS1iYXNlICogMS4yNSAhZGVmYXVsdDtcbiRsZWFkLWZvbnQtd2VpZ2h0OiAgICAgICAgICAgIDMwMCAhZGVmYXVsdDtcblxuJHNtYWxsLWZvbnQtc2l6ZTogICAgICAgICAgICAgLjg3NWVtICFkZWZhdWx0O1xuXG4kc3ViLXN1cC1mb250LXNpemU6ICAgICAgICAgICAuNzVlbSAhZGVmYXVsdDtcblxuJHRleHQtbXV0ZWQ6ICAgICAgICAgICAgICAgICAgJGdyYXktNjAwICFkZWZhdWx0O1xuXG4kaW5pdGlhbGlzbS1mb250LXNpemU6ICAgICAgICAkc21hbGwtZm9udC1zaXplICFkZWZhdWx0O1xuXG4kYmxvY2txdW90ZS1tYXJnaW4teTogICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuJGJsb2NrcXVvdGUtZm9udC1zaXplOiAgICAgICAgJGZvbnQtc2l6ZS1iYXNlICogMS4yNSAhZGVmYXVsdDtcbiRibG9ja3F1b3RlLWZvb3Rlci1jb2xvcjogICAgICRncmF5LTYwMCAhZGVmYXVsdDtcbiRibG9ja3F1b3RlLWZvb3Rlci1mb250LXNpemU6ICRzbWFsbC1mb250LXNpemUgIWRlZmF1bHQ7XG5cbiRoci1tYXJnaW4teTogICAgICAgICAgICAgICAgICRzcGFjZXIgIWRlZmF1bHQ7XG4kaHItY29sb3I6ICAgICAgICAgICAgICAgICAgICBpbmhlcml0ICFkZWZhdWx0O1xuJGhyLWhlaWdodDogICAgICAgICAgICAgICAgICAgJGJvcmRlci13aWR0aCAhZGVmYXVsdDtcbiRoci1vcGFjaXR5OiAgICAgICAgICAgICAgICAgIC4yNSAhZGVmYXVsdDtcblxuJGxlZ2VuZC1tYXJnaW4tYm90dG9tOiAgICAgICAgLjVyZW0gIWRlZmF1bHQ7XG4kbGVnZW5kLWZvbnQtc2l6ZTogICAgICAgICAgICAxLjVyZW0gIWRlZmF1bHQ7XG4kbGVnZW5kLWZvbnQtd2VpZ2h0OiAgICAgICAgICBudWxsICFkZWZhdWx0O1xuXG4kbWFyay1wYWRkaW5nOiAgICAgICAgICAgICAgICAuMmVtICFkZWZhdWx0O1xuXG4kZHQtZm9udC13ZWlnaHQ6ICAgICAgICAgICAgICAkZm9udC13ZWlnaHQtYm9sZCAhZGVmYXVsdDtcblxuJG5lc3RlZC1rYmQtZm9udC13ZWlnaHQ6ICAgICAgJGZvbnQtd2VpZ2h0LWJvbGQgIWRlZmF1bHQ7XG5cbiRsaXN0LWlubGluZS1wYWRkaW5nOiAgICAgICAgIC41cmVtICFkZWZhdWx0O1xuXG4kbWFyay1iZzogICAgICAgICAgICAgICAgICAgICAjZmNmOGUzICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCB0eXBlLXZhcmlhYmxlc1xuXG5cbi8vIFRhYmxlc1xuLy9cbi8vIEN1c3RvbWl6ZXMgdGhlIGAudGFibGVgIGNvbXBvbmVudCB3aXRoIGJhc2ljIHZhbHVlcywgZWFjaCB1c2VkIGFjcm9zcyBhbGwgdGFibGUgdmFyaWF0aW9ucy5cblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHRhYmxlLXZhcmlhYmxlc1xuJHRhYmxlLWNlbGwtcGFkZGluZy15OiAgICAgICAgLjVyZW0gIWRlZmF1bHQ7XG4kdGFibGUtY2VsbC1wYWRkaW5nLXg6ICAgICAgICAuNXJlbSAhZGVmYXVsdDtcbiR0YWJsZS1jZWxsLXBhZGRpbmcteS1zbTogICAgIC4yNXJlbSAhZGVmYXVsdDtcbiR0YWJsZS1jZWxsLXBhZGRpbmcteC1zbTogICAgIC4yNXJlbSAhZGVmYXVsdDtcblxuJHRhYmxlLWNlbGwtdmVydGljYWwtYWxpZ246ICAgdG9wICFkZWZhdWx0O1xuXG4kdGFibGUtY29sb3I6ICAgICAgICAgICAgICAgICAkYm9keS1jb2xvciAhZGVmYXVsdDtcbiR0YWJsZS1iZzogICAgICAgICAgICAgICAgICAgIHRyYW5zcGFyZW50ICFkZWZhdWx0O1xuJHRhYmxlLWFjY2VudC1iZzogICAgICAgICAgICAgdHJhbnNwYXJlbnQgIWRlZmF1bHQ7XG5cbiR0YWJsZS10aC1mb250LXdlaWdodDogICAgICAgIG51bGwgIWRlZmF1bHQ7XG5cbiR0YWJsZS1zdHJpcGVkLWNvbG9yOiAgICAgICAgICR0YWJsZS1jb2xvciAhZGVmYXVsdDtcbiR0YWJsZS1zdHJpcGVkLWJnLWZhY3RvcjogICAgIC4wNSAhZGVmYXVsdDtcbiR0YWJsZS1zdHJpcGVkLWJnOiAgICAgICAgICAgIHJnYmEoJGJsYWNrLCAkdGFibGUtc3RyaXBlZC1iZy1mYWN0b3IpICFkZWZhdWx0O1xuXG4kdGFibGUtYWN0aXZlLWNvbG9yOiAgICAgICAgICAkdGFibGUtY29sb3IgIWRlZmF1bHQ7XG4kdGFibGUtYWN0aXZlLWJnLWZhY3RvcjogICAgICAuMSAhZGVmYXVsdDtcbiR0YWJsZS1hY3RpdmUtYmc6ICAgICAgICAgICAgIHJnYmEoJGJsYWNrLCAkdGFibGUtYWN0aXZlLWJnLWZhY3RvcikgIWRlZmF1bHQ7XG5cbiR0YWJsZS1ob3Zlci1jb2xvcjogICAgICAgICAgICR0YWJsZS1jb2xvciAhZGVmYXVsdDtcbiR0YWJsZS1ob3Zlci1iZy1mYWN0b3I6ICAgICAgIC4wNzUgIWRlZmF1bHQ7XG4kdGFibGUtaG92ZXItYmc6ICAgICAgICAgICAgICByZ2JhKCRibGFjaywgJHRhYmxlLWhvdmVyLWJnLWZhY3RvcikgIWRlZmF1bHQ7XG5cbiR0YWJsZS1ib3JkZXItZmFjdG9yOiAgICAgICAgIC4xICFkZWZhdWx0O1xuJHRhYmxlLWJvcmRlci13aWR0aDogICAgICAgICAgJGJvcmRlci13aWR0aCAhZGVmYXVsdDtcbiR0YWJsZS1ib3JkZXItY29sb3I6ICAgICAgICAgICRib3JkZXItY29sb3IgIWRlZmF1bHQ7XG5cbiR0YWJsZS1zdHJpcGVkLW9yZGVyOiAgICAgICAgIG9kZCAhZGVmYXVsdDtcblxuJHRhYmxlLWdyb3VwLXNlcGFyYXRvci1jb2xvcjogY3VycmVudENvbG9yICFkZWZhdWx0O1xuXG4kdGFibGUtY2FwdGlvbi1jb2xvcjogICAgICAgICAkdGV4dC1tdXRlZCAhZGVmYXVsdDtcblxuJHRhYmxlLWJnLXNjYWxlOiAgICAgICAgICAgICAgLTgwJSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgdGFibGUtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCB0YWJsZS1sb29wXG4kdGFibGUtdmFyaWFudHM6IChcbiAgXCJwcmltYXJ5XCI6ICAgIHNoaWZ0LWNvbG9yKCRwcmltYXJ5LCAkdGFibGUtYmctc2NhbGUpLFxuICBcInNlY29uZGFyeVwiOiAgc2hpZnQtY29sb3IoJHNlY29uZGFyeSwgJHRhYmxlLWJnLXNjYWxlKSxcbiAgXCJzdWNjZXNzXCI6ICAgIHNoaWZ0LWNvbG9yKCRzdWNjZXNzLCAkdGFibGUtYmctc2NhbGUpLFxuICBcImluZm9cIjogICAgICAgc2hpZnQtY29sb3IoJGluZm8sICR0YWJsZS1iZy1zY2FsZSksXG4gIFwid2FybmluZ1wiOiAgICBzaGlmdC1jb2xvcigkd2FybmluZywgJHRhYmxlLWJnLXNjYWxlKSxcbiAgXCJkYW5nZXJcIjogICAgIHNoaWZ0LWNvbG9yKCRkYW5nZXIsICR0YWJsZS1iZy1zY2FsZSksXG4gIFwibGlnaHRcIjogICAgICAkbGlnaHQsXG4gIFwiZGFya1wiOiAgICAgICAkZGFyayxcbikgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHRhYmxlLWxvb3BcblxuXG4vLyBCdXR0b25zICsgRm9ybXNcbi8vXG4vLyBTaGFyZWQgdmFyaWFibGVzIHRoYXQgYXJlIHJlYXNzaWduZWQgdG8gYCRpbnB1dC1gIGFuZCBgJGJ0bi1gIHNwZWNpZmljIHZhcmlhYmxlcy5cblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGlucHV0LWJ0bi12YXJpYWJsZXNcbiRpbnB1dC1idG4tcGFkZGluZy15OiAgICAgICAgIC4zNzVyZW0gIWRlZmF1bHQ7XG4kaW5wdXQtYnRuLXBhZGRpbmcteDogICAgICAgICAuNzVyZW0gIWRlZmF1bHQ7XG4kaW5wdXQtYnRuLWZvbnQtZmFtaWx5OiAgICAgICBudWxsICFkZWZhdWx0O1xuJGlucHV0LWJ0bi1mb250LXNpemU6ICAgICAgICAgJGZvbnQtc2l6ZS1iYXNlICFkZWZhdWx0O1xuJGlucHV0LWJ0bi1saW5lLWhlaWdodDogICAgICAgJGxpbmUtaGVpZ2h0LWJhc2UgIWRlZmF1bHQ7XG5cbiRpbnB1dC1idG4tZm9jdXMtd2lkdGg6ICAgICAgICAgLjI1cmVtICFkZWZhdWx0O1xuJGlucHV0LWJ0bi1mb2N1cy1jb2xvci1vcGFjaXR5OiAuMjUgIWRlZmF1bHQ7XG4kaW5wdXQtYnRuLWZvY3VzLWNvbG9yOiAgICAgICAgIHJnYmEoJGNvbXBvbmVudC1hY3RpdmUtYmcsICRpbnB1dC1idG4tZm9jdXMtY29sb3Itb3BhY2l0eSkgIWRlZmF1bHQ7XG4kaW5wdXQtYnRuLWZvY3VzLWJsdXI6ICAgICAgICAgIDAgIWRlZmF1bHQ7XG4kaW5wdXQtYnRuLWZvY3VzLWJveC1zaGFkb3c6ICAgIDAgMCAkaW5wdXQtYnRuLWZvY3VzLWJsdXIgJGlucHV0LWJ0bi1mb2N1cy13aWR0aCAkaW5wdXQtYnRuLWZvY3VzLWNvbG9yICFkZWZhdWx0O1xuXG4kaW5wdXQtYnRuLXBhZGRpbmcteS1zbTogICAgICAuMjVyZW0gIWRlZmF1bHQ7XG4kaW5wdXQtYnRuLXBhZGRpbmcteC1zbTogICAgICAuNXJlbSAhZGVmYXVsdDtcbiRpbnB1dC1idG4tZm9udC1zaXplLXNtOiAgICAgICRmb250LXNpemUtc20gIWRlZmF1bHQ7XG5cbiRpbnB1dC1idG4tcGFkZGluZy15LWxnOiAgICAgIC41cmVtICFkZWZhdWx0O1xuJGlucHV0LWJ0bi1wYWRkaW5nLXgtbGc6ICAgICAgMXJlbSAhZGVmYXVsdDtcbiRpbnB1dC1idG4tZm9udC1zaXplLWxnOiAgICAgICRmb250LXNpemUtbGcgIWRlZmF1bHQ7XG5cbiRpbnB1dC1idG4tYm9yZGVyLXdpZHRoOiAgICAgICRib3JkZXItd2lkdGggIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGlucHV0LWJ0bi12YXJpYWJsZXNcblxuXG4vLyBCdXR0b25zXG4vL1xuLy8gRm9yIGVhY2ggb2YgQm9vdHN0cmFwJ3MgYnV0dG9ucywgZGVmaW5lIHRleHQsIGJhY2tncm91bmQsIGFuZCBib3JkZXIgY29sb3IuXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBidG4tdmFyaWFibGVzXG4kYnRuLXBhZGRpbmcteTogICAgICAgICAgICAgICAkaW5wdXQtYnRuLXBhZGRpbmcteSAhZGVmYXVsdDtcbiRidG4tcGFkZGluZy14OiAgICAgICAgICAgICAgICRpbnB1dC1idG4tcGFkZGluZy14ICFkZWZhdWx0O1xuJGJ0bi1mb250LWZhbWlseTogICAgICAgICAgICAgJGlucHV0LWJ0bi1mb250LWZhbWlseSAhZGVmYXVsdDtcbiRidG4tZm9udC1zaXplOiAgICAgICAgICAgICAgICRpbnB1dC1idG4tZm9udC1zaXplICFkZWZhdWx0O1xuJGJ0bi1saW5lLWhlaWdodDogICAgICAgICAgICAgJGlucHV0LWJ0bi1saW5lLWhlaWdodCAhZGVmYXVsdDtcbiRidG4td2hpdGUtc3BhY2U6ICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7IC8vIFNldCB0byBgbm93cmFwYCB0byBwcmV2ZW50IHRleHQgd3JhcHBpbmdcblxuJGJ0bi1wYWRkaW5nLXktc206ICAgICAgICAgICAgJGlucHV0LWJ0bi1wYWRkaW5nLXktc20gIWRlZmF1bHQ7XG4kYnRuLXBhZGRpbmcteC1zbTogICAgICAgICAgICAkaW5wdXQtYnRuLXBhZGRpbmcteC1zbSAhZGVmYXVsdDtcbiRidG4tZm9udC1zaXplLXNtOiAgICAgICAgICAgICRpbnB1dC1idG4tZm9udC1zaXplLXNtICFkZWZhdWx0O1xuXG4kYnRuLXBhZGRpbmcteS1sZzogICAgICAgICAgICAkaW5wdXQtYnRuLXBhZGRpbmcteS1sZyAhZGVmYXVsdDtcbiRidG4tcGFkZGluZy14LWxnOiAgICAgICAgICAgICRpbnB1dC1idG4tcGFkZGluZy14LWxnICFkZWZhdWx0O1xuJGJ0bi1mb250LXNpemUtbGc6ICAgICAgICAgICAgJGlucHV0LWJ0bi1mb250LXNpemUtbGcgIWRlZmF1bHQ7XG5cbiRidG4tYm9yZGVyLXdpZHRoOiAgICAgICAgICAgICRpbnB1dC1idG4tYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuXG4kYnRuLWZvbnQtd2VpZ2h0OiAgICAgICAgICAgICAkZm9udC13ZWlnaHQtbm9ybWFsICFkZWZhdWx0O1xuJGJ0bi1ib3gtc2hhZG93OiAgICAgICAgICAgICAgaW5zZXQgMCAxcHggMCByZ2JhKCR3aGl0ZSwgLjE1KSwgMCAxcHggMXB4IHJnYmEoJGJsYWNrLCAuMDc1KSAhZGVmYXVsdDtcbiRidG4tZm9jdXMtd2lkdGg6ICAgICAgICAgICAgICRpbnB1dC1idG4tZm9jdXMtd2lkdGggIWRlZmF1bHQ7XG4kYnRuLWZvY3VzLWJveC1zaGFkb3c6ICAgICAgICAkaW5wdXQtYnRuLWZvY3VzLWJveC1zaGFkb3cgIWRlZmF1bHQ7XG4kYnRuLWRpc2FibGVkLW9wYWNpdHk6ICAgICAgICAuNjUgIWRlZmF1bHQ7XG4kYnRuLWFjdGl2ZS1ib3gtc2hhZG93OiAgICAgICBpbnNldCAwIDNweCA1cHggcmdiYSgkYmxhY2ssIC4xMjUpICFkZWZhdWx0O1xuXG4kYnRuLWxpbmstY29sb3I6ICAgICAgICAgICAgICAkbGluay1jb2xvciAhZGVmYXVsdDtcbiRidG4tbGluay1ob3Zlci1jb2xvcjogICAgICAgICRsaW5rLWhvdmVyLWNvbG9yICFkZWZhdWx0O1xuJGJ0bi1saW5rLWRpc2FibGVkLWNvbG9yOiAgICAgJGdyYXktNjAwICFkZWZhdWx0O1xuXG4vLyBBbGxvd3MgZm9yIGN1c3RvbWl6aW5nIGJ1dHRvbiByYWRpdXMgaW5kZXBlbmRlbnRseSBmcm9tIGdsb2JhbCBib3JkZXIgcmFkaXVzXG4kYnRuLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAkYm9yZGVyLXJhZGl1cyAhZGVmYXVsdDtcbiRidG4tYm9yZGVyLXJhZGl1cy1zbTogICAgICAgICRib3JkZXItcmFkaXVzLXNtICFkZWZhdWx0O1xuJGJ0bi1ib3JkZXItcmFkaXVzLWxnOiAgICAgICAgJGJvcmRlci1yYWRpdXMtbGcgIWRlZmF1bHQ7XG5cbiRidG4tdHJhbnNpdGlvbjogICAgICAgICAgICAgIGNvbG9yIC4xNXMgZWFzZS1pbi1vdXQsIGJhY2tncm91bmQtY29sb3IgLjE1cyBlYXNlLWluLW91dCwgYm9yZGVyLWNvbG9yIC4xNXMgZWFzZS1pbi1vdXQsIGJveC1zaGFkb3cgLjE1cyBlYXNlLWluLW91dCAhZGVmYXVsdDtcblxuJGJ0bi1ob3Zlci1iZy1zaGFkZS1hbW91bnQ6ICAgICAgIDE1JSAhZGVmYXVsdDtcbiRidG4taG92ZXItYmctdGludC1hbW91bnQ6ICAgICAgICAxNSUgIWRlZmF1bHQ7XG4kYnRuLWhvdmVyLWJvcmRlci1zaGFkZS1hbW91bnQ6ICAgMjAlICFkZWZhdWx0O1xuJGJ0bi1ob3Zlci1ib3JkZXItdGludC1hbW91bnQ6ICAgIDEwJSAhZGVmYXVsdDtcbiRidG4tYWN0aXZlLWJnLXNoYWRlLWFtb3VudDogICAgICAyMCUgIWRlZmF1bHQ7XG4kYnRuLWFjdGl2ZS1iZy10aW50LWFtb3VudDogICAgICAgMjAlICFkZWZhdWx0O1xuJGJ0bi1hY3RpdmUtYm9yZGVyLXNoYWRlLWFtb3VudDogIDI1JSAhZGVmYXVsdDtcbiRidG4tYWN0aXZlLWJvcmRlci10aW50LWFtb3VudDogICAxMCUgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGJ0bi12YXJpYWJsZXNcblxuXG4vLyBGb3Jtc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZm9ybS10ZXh0LXZhcmlhYmxlc1xuJGZvcm0tdGV4dC1tYXJnaW4tdG9wOiAgICAgICAgICAgICAgICAgIC4yNXJlbSAhZGVmYXVsdDtcbiRmb3JtLXRleHQtZm9udC1zaXplOiAgICAgICAgICAgICAgICAgICAkc21hbGwtZm9udC1zaXplICFkZWZhdWx0O1xuJGZvcm0tdGV4dC1mb250LXN0eWxlOiAgICAgICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG4kZm9ybS10ZXh0LWZvbnQtd2VpZ2h0OiAgICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRmb3JtLXRleHQtY29sb3I6ICAgICAgICAgICAgICAgICAgICAgICAkdGV4dC1tdXRlZCAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgZm9ybS10ZXh0LXZhcmlhYmxlc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZm9ybS1sYWJlbC12YXJpYWJsZXNcbiRmb3JtLWxhYmVsLW1hcmdpbi1ib3R0b206ICAgICAgICAgICAgICAuNXJlbSAhZGVmYXVsdDtcbiRmb3JtLWxhYmVsLWZvbnQtc2l6ZTogICAgICAgICAgICAgICAgICBudWxsICFkZWZhdWx0O1xuJGZvcm0tbGFiZWwtZm9udC1zdHlsZTogICAgICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG4kZm9ybS1sYWJlbC1mb250LXdlaWdodDogICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRmb3JtLWxhYmVsLWNvbG9yOiAgICAgICAgICAgICAgICAgICAgICBudWxsICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBmb3JtLWxhYmVsLXZhcmlhYmxlc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZm9ybS1pbnB1dC12YXJpYWJsZXNcbiRpbnB1dC1wYWRkaW5nLXk6ICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYnRuLXBhZGRpbmcteSAhZGVmYXVsdDtcbiRpbnB1dC1wYWRkaW5nLXg6ICAgICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYnRuLXBhZGRpbmcteCAhZGVmYXVsdDtcbiRpbnB1dC1mb250LWZhbWlseTogICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYnRuLWZvbnQtZmFtaWx5ICFkZWZhdWx0O1xuJGlucHV0LWZvbnQtc2l6ZTogICAgICAgICAgICAgICAgICAgICAgICRpbnB1dC1idG4tZm9udC1zaXplICFkZWZhdWx0O1xuJGlucHV0LWZvbnQtd2VpZ2h0OiAgICAgICAgICAgICAgICAgICAgICRmb250LXdlaWdodC1iYXNlICFkZWZhdWx0O1xuJGlucHV0LWxpbmUtaGVpZ2h0OiAgICAgICAgICAgICAgICAgICAgICRpbnB1dC1idG4tbGluZS1oZWlnaHQgIWRlZmF1bHQ7XG5cbiRpbnB1dC1wYWRkaW5nLXktc206ICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYnRuLXBhZGRpbmcteS1zbSAhZGVmYXVsdDtcbiRpbnB1dC1wYWRkaW5nLXgtc206ICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYnRuLXBhZGRpbmcteC1zbSAhZGVmYXVsdDtcbiRpbnB1dC1mb250LXNpemUtc206ICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYnRuLWZvbnQtc2l6ZS1zbSAhZGVmYXVsdDtcblxuJGlucHV0LXBhZGRpbmcteS1sZzogICAgICAgICAgICAgICAgICAgICRpbnB1dC1idG4tcGFkZGluZy15LWxnICFkZWZhdWx0O1xuJGlucHV0LXBhZGRpbmcteC1sZzogICAgICAgICAgICAgICAgICAgICRpbnB1dC1idG4tcGFkZGluZy14LWxnICFkZWZhdWx0O1xuJGlucHV0LWZvbnQtc2l6ZS1sZzogICAgICAgICAgICAgICAgICAgICRpbnB1dC1idG4tZm9udC1zaXplLWxnICFkZWZhdWx0O1xuXG4kaW5wdXQtYmc6ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGJvZHktYmcgIWRlZmF1bHQ7XG4kaW5wdXQtZGlzYWJsZWQtYmc6ICAgICAgICAgICAgICAgICAgICAgJGdyYXktMjAwICFkZWZhdWx0O1xuJGlucHV0LWRpc2FibGVkLWJvcmRlci1jb2xvcjogICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG5cbiRpbnB1dC1jb2xvcjogICAgICAgICAgICAgICAgICAgICAgICAgICAkYm9keS1jb2xvciAhZGVmYXVsdDtcbiRpbnB1dC1ib3JkZXItY29sb3I6ICAgICAgICAgICAgICAgICAgICAkZ3JheS00MDAgIWRlZmF1bHQ7XG4kaW5wdXQtYm9yZGVyLXdpZHRoOiAgICAgICAgICAgICAgICAgICAgJGlucHV0LWJ0bi1ib3JkZXItd2lkdGggIWRlZmF1bHQ7XG4kaW5wdXQtYm94LXNoYWRvdzogICAgICAgICAgICAgICAgICAgICAgJGJveC1zaGFkb3ctaW5zZXQgIWRlZmF1bHQ7XG5cbiRpbnB1dC1ib3JkZXItcmFkaXVzOiAgICAgICAgICAgICAgICAgICAkYm9yZGVyLXJhZGl1cyAhZGVmYXVsdDtcbiRpbnB1dC1ib3JkZXItcmFkaXVzLXNtOiAgICAgICAgICAgICAgICAkYm9yZGVyLXJhZGl1cy1zbSAhZGVmYXVsdDtcbiRpbnB1dC1ib3JkZXItcmFkaXVzLWxnOiAgICAgICAgICAgICAgICAkYm9yZGVyLXJhZGl1cy1sZyAhZGVmYXVsdDtcblxuJGlucHV0LWZvY3VzLWJnOiAgICAgICAgICAgICAgICAgICAgICAgICRpbnB1dC1iZyAhZGVmYXVsdDtcbiRpbnB1dC1mb2N1cy1ib3JkZXItY29sb3I6ICAgICAgICAgICAgICB0aW50LWNvbG9yKCRjb21wb25lbnQtYWN0aXZlLWJnLCA1MCUpICFkZWZhdWx0O1xuJGlucHV0LWZvY3VzLWNvbG9yOiAgICAgICAgICAgICAgICAgICAgICRpbnB1dC1jb2xvciAhZGVmYXVsdDtcbiRpbnB1dC1mb2N1cy13aWR0aDogICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYnRuLWZvY3VzLXdpZHRoICFkZWZhdWx0O1xuJGlucHV0LWZvY3VzLWJveC1zaGFkb3c6ICAgICAgICAgICAgICAgICRpbnB1dC1idG4tZm9jdXMtYm94LXNoYWRvdyAhZGVmYXVsdDtcblxuJGlucHV0LXBsYWNlaG9sZGVyLWNvbG9yOiAgICAgICAgICAgICAgICRncmF5LTYwMCAhZGVmYXVsdDtcbiRpbnB1dC1wbGFpbnRleHQtY29sb3I6ICAgICAgICAgICAgICAgICAkYm9keS1jb2xvciAhZGVmYXVsdDtcblxuJGlucHV0LWhlaWdodC1ib3JkZXI6ICAgICAgICAgICAgICAgICAgICRpbnB1dC1ib3JkZXItd2lkdGggKiAyICFkZWZhdWx0O1xuXG4kaW5wdXQtaGVpZ2h0LWlubmVyOiAgICAgICAgICAgICAgICAgICAgYWRkKCRpbnB1dC1saW5lLWhlaWdodCAqIDFlbSwgJGlucHV0LXBhZGRpbmcteSAqIDIpICFkZWZhdWx0O1xuJGlucHV0LWhlaWdodC1pbm5lci1oYWxmOiAgICAgICAgICAgICAgIGFkZCgkaW5wdXQtbGluZS1oZWlnaHQgKiAuNWVtLCAkaW5wdXQtcGFkZGluZy15KSAhZGVmYXVsdDtcbiRpbnB1dC1oZWlnaHQtaW5uZXItcXVhcnRlcjogICAgICAgICAgICBhZGQoJGlucHV0LWxpbmUtaGVpZ2h0ICogLjI1ZW0sICRpbnB1dC1wYWRkaW5nLXkgKiAuNSkgIWRlZmF1bHQ7XG5cbiRpbnB1dC1oZWlnaHQ6ICAgICAgICAgICAgICAgICAgICAgICAgICBhZGQoJGlucHV0LWxpbmUtaGVpZ2h0ICogMWVtLCBhZGQoJGlucHV0LXBhZGRpbmcteSAqIDIsICRpbnB1dC1oZWlnaHQtYm9yZGVyLCBmYWxzZSkpICFkZWZhdWx0O1xuJGlucHV0LWhlaWdodC1zbTogICAgICAgICAgICAgICAgICAgICAgIGFkZCgkaW5wdXQtbGluZS1oZWlnaHQgKiAxZW0sIGFkZCgkaW5wdXQtcGFkZGluZy15LXNtICogMiwgJGlucHV0LWhlaWdodC1ib3JkZXIsIGZhbHNlKSkgIWRlZmF1bHQ7XG4kaW5wdXQtaGVpZ2h0LWxnOiAgICAgICAgICAgICAgICAgICAgICAgYWRkKCRpbnB1dC1saW5lLWhlaWdodCAqIDFlbSwgYWRkKCRpbnB1dC1wYWRkaW5nLXktbGcgKiAyLCAkaW5wdXQtaGVpZ2h0LWJvcmRlciwgZmFsc2UpKSAhZGVmYXVsdDtcblxuJGlucHV0LXRyYW5zaXRpb246ICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1jb2xvciAuMTVzIGVhc2UtaW4tb3V0LCBib3gtc2hhZG93IC4xNXMgZWFzZS1pbi1vdXQgIWRlZmF1bHQ7XG5cbiRmb3JtLWNvbG9yLXdpZHRoOiAgICAgICAgICAgICAgICAgICAgICAzcmVtICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBmb3JtLWlucHV0LXZhcmlhYmxlc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZm9ybS1jaGVjay12YXJpYWJsZXNcbiRmb3JtLWNoZWNrLWlucHV0LXdpZHRoOiAgICAgICAgICAgICAgICAgIDFlbSAhZGVmYXVsdDtcbiRmb3JtLWNoZWNrLW1pbi1oZWlnaHQ6ICAgICAgICAgICAgICAgICAgICRmb250LXNpemUtYmFzZSAqICRsaW5lLWhlaWdodC1iYXNlICFkZWZhdWx0O1xuJGZvcm0tY2hlY2stcGFkZGluZy1zdGFydDogICAgICAgICAgICAgICAgJGZvcm0tY2hlY2staW5wdXQtd2lkdGggKyAuNWVtICFkZWZhdWx0O1xuJGZvcm0tY2hlY2stbWFyZ2luLWJvdHRvbTogICAgICAgICAgICAgICAgLjEyNXJlbSAhZGVmYXVsdDtcbiRmb3JtLWNoZWNrLWxhYmVsLWNvbG9yOiAgICAgICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG4kZm9ybS1jaGVjay1sYWJlbC1jdXJzb3I6ICAgICAgICAgICAgICAgICBudWxsICFkZWZhdWx0O1xuJGZvcm0tY2hlY2stdHJhbnNpdGlvbjogICAgICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcblxuJGZvcm0tY2hlY2staW5wdXQtYWN0aXZlLWZpbHRlcjogICAgICAgICAgYnJpZ2h0bmVzcyg5MCUpICFkZWZhdWx0O1xuXG4kZm9ybS1jaGVjay1pbnB1dC1iZzogICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYmcgIWRlZmF1bHQ7XG4kZm9ybS1jaGVjay1pbnB1dC1ib3JkZXI6ICAgICAgICAgICAgICAgICAxcHggc29saWQgcmdiYSgkYmxhY2ssIC4yNSkgIWRlZmF1bHQ7XG4kZm9ybS1jaGVjay1pbnB1dC1ib3JkZXItcmFkaXVzOiAgICAgICAgICAuMjVlbSAhZGVmYXVsdDtcbiRmb3JtLWNoZWNrLXJhZGlvLWJvcmRlci1yYWRpdXM6ICAgICAgICAgIDUwJSAhZGVmYXVsdDtcbiRmb3JtLWNoZWNrLWlucHV0LWZvY3VzLWJvcmRlcjogICAgICAgICAgICRpbnB1dC1mb2N1cy1ib3JkZXItY29sb3IgIWRlZmF1bHQ7XG4kZm9ybS1jaGVjay1pbnB1dC1mb2N1cy1ib3gtc2hhZG93OiAgICAgICAkaW5wdXQtYnRuLWZvY3VzLWJveC1zaGFkb3cgIWRlZmF1bHQ7XG5cbiRmb3JtLWNoZWNrLWlucHV0LWNoZWNrZWQtY29sb3I6ICAgICAgICAgICRjb21wb25lbnQtYWN0aXZlLWNvbG9yICFkZWZhdWx0O1xuJGZvcm0tY2hlY2staW5wdXQtY2hlY2tlZC1iZy1jb2xvcjogICAgICAgJGNvbXBvbmVudC1hY3RpdmUtYmcgIWRlZmF1bHQ7XG4kZm9ybS1jaGVjay1pbnB1dC1jaGVja2VkLWJvcmRlci1jb2xvcjogICAkZm9ybS1jaGVjay1pbnB1dC1jaGVja2VkLWJnLWNvbG9yICFkZWZhdWx0O1xuJGZvcm0tY2hlY2staW5wdXQtY2hlY2tlZC1iZy1pbWFnZTogICAgICAgdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgMjAgMjAnPjxwYXRoIGZpbGw9J25vbmUnIHN0cm9rZT0nI3skZm9ybS1jaGVjay1pbnB1dC1jaGVja2VkLWNvbG9yfScgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJyBzdHJva2UtbGluZWpvaW49J3JvdW5kJyBzdHJva2Utd2lkdGg9JzMnIGQ9J002IDEwbDMgM2w2LTYnLz48L3N2Zz5cIikgIWRlZmF1bHQ7XG4kZm9ybS1jaGVjay1yYWRpby1jaGVja2VkLWJnLWltYWdlOiAgICAgICB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsPHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9Jy00IC00IDggOCc+PGNpcmNsZSByPScyJyBmaWxsPScjeyRmb3JtLWNoZWNrLWlucHV0LWNoZWNrZWQtY29sb3J9Jy8+PC9zdmc+XCIpICFkZWZhdWx0O1xuXG4kZm9ybS1jaGVjay1pbnB1dC1pbmRldGVybWluYXRlLWNvbG9yOiAgICAgICAgICAkY29tcG9uZW50LWFjdGl2ZS1jb2xvciAhZGVmYXVsdDtcbiRmb3JtLWNoZWNrLWlucHV0LWluZGV0ZXJtaW5hdGUtYmctY29sb3I6ICAgICAgICRjb21wb25lbnQtYWN0aXZlLWJnICFkZWZhdWx0O1xuJGZvcm0tY2hlY2staW5wdXQtaW5kZXRlcm1pbmF0ZS1ib3JkZXItY29sb3I6ICAgJGZvcm0tY2hlY2staW5wdXQtaW5kZXRlcm1pbmF0ZS1iZy1jb2xvciAhZGVmYXVsdDtcbiRmb3JtLWNoZWNrLWlucHV0LWluZGV0ZXJtaW5hdGUtYmctaW1hZ2U6ICAgICAgIHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCw8c3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nMCAwIDIwIDIwJz48cGF0aCBmaWxsPSdub25lJyBzdHJva2U9JyN7JGZvcm0tY2hlY2staW5wdXQtaW5kZXRlcm1pbmF0ZS1jb2xvcn0nIHN0cm9rZS1saW5lY2FwPSdyb3VuZCcgc3Ryb2tlLWxpbmVqb2luPSdyb3VuZCcgc3Ryb2tlLXdpZHRoPSczJyBkPSdNNiAxMGg4Jy8+PC9zdmc+XCIpICFkZWZhdWx0O1xuXG4kZm9ybS1jaGVjay1pbnB1dC1kaXNhYmxlZC1vcGFjaXR5OiAgICAgICAgLjUgIWRlZmF1bHQ7XG4kZm9ybS1jaGVjay1sYWJlbC1kaXNhYmxlZC1vcGFjaXR5OiAgICAgICAgJGZvcm0tY2hlY2staW5wdXQtZGlzYWJsZWQtb3BhY2l0eSAhZGVmYXVsdDtcbiRmb3JtLWNoZWNrLWJ0bi1jaGVjay1kaXNhYmxlZC1vcGFjaXR5OiAgICAkYnRuLWRpc2FibGVkLW9wYWNpdHkgIWRlZmF1bHQ7XG5cbiRmb3JtLWNoZWNrLWlubGluZS1tYXJnaW4tZW5kOiAgICAxcmVtICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBmb3JtLWNoZWNrLXZhcmlhYmxlc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZm9ybS1zd2l0Y2gtdmFyaWFibGVzXG4kZm9ybS1zd2l0Y2gtY29sb3I6ICAgICAgICAgICAgICAgcmdiYSgkYmxhY2ssIC4yNSkgIWRlZmF1bHQ7XG4kZm9ybS1zd2l0Y2gtd2lkdGg6ICAgICAgICAgICAgICAgMmVtICFkZWZhdWx0O1xuJGZvcm0tc3dpdGNoLXBhZGRpbmctc3RhcnQ6ICAgICAgICRmb3JtLXN3aXRjaC13aWR0aCArIC41ZW0gIWRlZmF1bHQ7XG4kZm9ybS1zd2l0Y2gtYmctaW1hZ2U6ICAgICAgICAgICAgdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PSctNCAtNCA4IDgnPjxjaXJjbGUgcj0nMycgZmlsbD0nI3skZm9ybS1zd2l0Y2gtY29sb3J9Jy8+PC9zdmc+XCIpICFkZWZhdWx0O1xuJGZvcm0tc3dpdGNoLWJvcmRlci1yYWRpdXM6ICAgICAgICRmb3JtLXN3aXRjaC13aWR0aCAhZGVmYXVsdDtcbiRmb3JtLXN3aXRjaC10cmFuc2l0aW9uOiAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uIC4xNXMgZWFzZS1pbi1vdXQgIWRlZmF1bHQ7XG5cbiRmb3JtLXN3aXRjaC1mb2N1cy1jb2xvcjogICAgICAgICAkaW5wdXQtZm9jdXMtYm9yZGVyLWNvbG9yICFkZWZhdWx0O1xuJGZvcm0tc3dpdGNoLWZvY3VzLWJnLWltYWdlOiAgICAgIHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCw8c3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nLTQgLTQgOCA4Jz48Y2lyY2xlIHI9JzMnIGZpbGw9JyN7JGZvcm0tc3dpdGNoLWZvY3VzLWNvbG9yfScvPjwvc3ZnPlwiKSAhZGVmYXVsdDtcblxuJGZvcm0tc3dpdGNoLWNoZWNrZWQtY29sb3I6ICAgICAgICRjb21wb25lbnQtYWN0aXZlLWNvbG9yICFkZWZhdWx0O1xuJGZvcm0tc3dpdGNoLWNoZWNrZWQtYmctaW1hZ2U6ICAgIHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCw8c3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nLTQgLTQgOCA4Jz48Y2lyY2xlIHI9JzMnIGZpbGw9JyN7JGZvcm0tc3dpdGNoLWNoZWNrZWQtY29sb3J9Jy8+PC9zdmc+XCIpICFkZWZhdWx0O1xuJGZvcm0tc3dpdGNoLWNoZWNrZWQtYmctcG9zaXRpb246IHJpZ2h0IGNlbnRlciAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgZm9ybS1zd2l0Y2gtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBpbnB1dC1ncm91cC12YXJpYWJsZXNcbiRpbnB1dC1ncm91cC1hZGRvbi1wYWRkaW5nLXk6ICAgICAgICAgICAkaW5wdXQtcGFkZGluZy15ICFkZWZhdWx0O1xuJGlucHV0LWdyb3VwLWFkZG9uLXBhZGRpbmcteDogICAgICAgICAgICRpbnB1dC1wYWRkaW5nLXggIWRlZmF1bHQ7XG4kaW5wdXQtZ3JvdXAtYWRkb24tZm9udC13ZWlnaHQ6ICAgICAgICAgJGlucHV0LWZvbnQtd2VpZ2h0ICFkZWZhdWx0O1xuJGlucHV0LWdyb3VwLWFkZG9uLWNvbG9yOiAgICAgICAgICAgICAgICRpbnB1dC1jb2xvciAhZGVmYXVsdDtcbiRpbnB1dC1ncm91cC1hZGRvbi1iZzogICAgICAgICAgICAgICAgICAkZ3JheS0yMDAgIWRlZmF1bHQ7XG4kaW5wdXQtZ3JvdXAtYWRkb24tYm9yZGVyLWNvbG9yOiAgICAgICAgJGlucHV0LWJvcmRlci1jb2xvciAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgaW5wdXQtZ3JvdXAtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBmb3JtLXNlbGVjdC12YXJpYWJsZXNcbiRmb3JtLXNlbGVjdC1wYWRkaW5nLXk6ICAgICAgICAgICAgICRpbnB1dC1wYWRkaW5nLXkgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtcGFkZGluZy14OiAgICAgICAgICAgICAkaW5wdXQtcGFkZGluZy14ICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWZvbnQtZmFtaWx5OiAgICAgICAgICAgJGlucHV0LWZvbnQtZmFtaWx5ICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWZvbnQtc2l6ZTogICAgICAgICAgICAgJGlucHV0LWZvbnQtc2l6ZSAhZGVmYXVsdDtcbiRmb3JtLXNlbGVjdC1pbmRpY2F0b3ItcGFkZGluZzogICAgICRmb3JtLXNlbGVjdC1wYWRkaW5nLXggKiAzICFkZWZhdWx0OyAvLyBFeHRyYSBwYWRkaW5nIGZvciBiYWNrZ3JvdW5kLWltYWdlXG4kZm9ybS1zZWxlY3QtZm9udC13ZWlnaHQ6ICAgICAgICAgICAkaW5wdXQtZm9udC13ZWlnaHQgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtbGluZS1oZWlnaHQ6ICAgICAgICAgICAkaW5wdXQtbGluZS1oZWlnaHQgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtY29sb3I6ICAgICAgICAgICAgICAgICAkaW5wdXQtY29sb3IgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtYmc6ICAgICAgICAgICAgICAgICAgICAkaW5wdXQtYmcgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtZGlzYWJsZWQtY29sb3I6ICAgICAgICBudWxsICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWRpc2FibGVkLWJnOiAgICAgICAgICAgJGdyYXktMjAwICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWRpc2FibGVkLWJvcmRlci1jb2xvcjogJGlucHV0LWRpc2FibGVkLWJvcmRlci1jb2xvciAhZGVmYXVsdDtcbiRmb3JtLXNlbGVjdC1iZy1wb3NpdGlvbjogICAgICAgICAgIHJpZ2h0ICRmb3JtLXNlbGVjdC1wYWRkaW5nLXggY2VudGVyICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWJnLXNpemU6ICAgICAgICAgICAgICAgMTZweCAxMnB4ICFkZWZhdWx0OyAvLyBJbiBwaXhlbHMgYmVjYXVzZSBpbWFnZSBkaW1lbnNpb25zXG4kZm9ybS1zZWxlY3QtaW5kaWNhdG9yLWNvbG9yOiAgICAgICAkZ3JheS04MDAgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtaW5kaWNhdG9yOiAgICAgICAgICAgICB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsPHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCAxNiAxNic+PHBhdGggZmlsbD0nbm9uZScgc3Ryb2tlPScjeyRmb3JtLXNlbGVjdC1pbmRpY2F0b3ItY29sb3J9JyBzdHJva2UtbGluZWNhcD0ncm91bmQnIHN0cm9rZS1saW5lam9pbj0ncm91bmQnIHN0cm9rZS13aWR0aD0nMicgZD0nTTIgNWw2IDYgNi02Jy8+PC9zdmc+XCIpICFkZWZhdWx0O1xuXG4kZm9ybS1zZWxlY3QtZmVlZGJhY2staWNvbi1wYWRkaW5nLWVuZDogJGZvcm0tc2VsZWN0LXBhZGRpbmcteCAqIDIuNSArICRmb3JtLXNlbGVjdC1pbmRpY2F0b3ItcGFkZGluZyAhZGVmYXVsdDtcbiRmb3JtLXNlbGVjdC1mZWVkYmFjay1pY29uLXBvc2l0aW9uOiAgICBjZW50ZXIgcmlnaHQgJGZvcm0tc2VsZWN0LWluZGljYXRvci1wYWRkaW5nICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWZlZWRiYWNrLWljb24tc2l6ZTogICAgICAgICRpbnB1dC1oZWlnaHQtaW5uZXItaGFsZiAkaW5wdXQtaGVpZ2h0LWlubmVyLWhhbGYgIWRlZmF1bHQ7XG5cbiRmb3JtLXNlbGVjdC1ib3JkZXItd2lkdGg6ICAgICAgICAkaW5wdXQtYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWJvcmRlci1jb2xvcjogICAgICAgICRpbnB1dC1ib3JkZXItY29sb3IgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtYm9yZGVyLXJhZGl1czogICAgICAgJGlucHV0LWJvcmRlci1yYWRpdXMgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtYm94LXNoYWRvdzogICAgICAgICAgJGJveC1zaGFkb3ctaW5zZXQgIWRlZmF1bHQ7XG5cbiRmb3JtLXNlbGVjdC1mb2N1cy1ib3JkZXItY29sb3I6ICAkaW5wdXQtZm9jdXMtYm9yZGVyLWNvbG9yICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWZvY3VzLXdpZHRoOiAgICAgICAgICRpbnB1dC1mb2N1cy13aWR0aCAhZGVmYXVsdDtcbiRmb3JtLXNlbGVjdC1mb2N1cy1ib3gtc2hhZG93OiAgICAwIDAgMCAkZm9ybS1zZWxlY3QtZm9jdXMtd2lkdGggJGlucHV0LWJ0bi1mb2N1cy1jb2xvciAhZGVmYXVsdDtcblxuJGZvcm0tc2VsZWN0LXBhZGRpbmcteS1zbTogICAgICAgICRpbnB1dC1wYWRkaW5nLXktc20gIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtcGFkZGluZy14LXNtOiAgICAgICAgJGlucHV0LXBhZGRpbmcteC1zbSAhZGVmYXVsdDtcbiRmb3JtLXNlbGVjdC1mb250LXNpemUtc206ICAgICAgICAkaW5wdXQtZm9udC1zaXplLXNtICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWJvcmRlci1yYWRpdXMtc206ICAgICRpbnB1dC1ib3JkZXItcmFkaXVzLXNtICFkZWZhdWx0O1xuXG4kZm9ybS1zZWxlY3QtcGFkZGluZy15LWxnOiAgICAgICAgJGlucHV0LXBhZGRpbmcteS1sZyAhZGVmYXVsdDtcbiRmb3JtLXNlbGVjdC1wYWRkaW5nLXgtbGc6ICAgICAgICAkaW5wdXQtcGFkZGluZy14LWxnICFkZWZhdWx0O1xuJGZvcm0tc2VsZWN0LWZvbnQtc2l6ZS1sZzogICAgICAgICRpbnB1dC1mb250LXNpemUtbGcgIWRlZmF1bHQ7XG4kZm9ybS1zZWxlY3QtYm9yZGVyLXJhZGl1cy1sZzogICAgJGlucHV0LWJvcmRlci1yYWRpdXMtbGcgIWRlZmF1bHQ7XG5cbiRmb3JtLXNlbGVjdC10cmFuc2l0aW9uOiAgICAgICAgICAkaW5wdXQtdHJhbnNpdGlvbiAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgZm9ybS1zZWxlY3QtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBmb3JtLXJhbmdlLXZhcmlhYmxlc1xuJGZvcm0tcmFuZ2UtdHJhY2std2lkdGg6ICAgICAgICAgIDEwMCUgIWRlZmF1bHQ7XG4kZm9ybS1yYW5nZS10cmFjay1oZWlnaHQ6ICAgICAgICAgLjVyZW0gIWRlZmF1bHQ7XG4kZm9ybS1yYW5nZS10cmFjay1jdXJzb3I6ICAgICAgICAgcG9pbnRlciAhZGVmYXVsdDtcbiRmb3JtLXJhbmdlLXRyYWNrLWJnOiAgICAgICAgICAgICAkZ3JheS0zMDAgIWRlZmF1bHQ7XG4kZm9ybS1yYW5nZS10cmFjay1ib3JkZXItcmFkaXVzOiAgMXJlbSAhZGVmYXVsdDtcbiRmb3JtLXJhbmdlLXRyYWNrLWJveC1zaGFkb3c6ICAgICAkYm94LXNoYWRvdy1pbnNldCAhZGVmYXVsdDtcblxuJGZvcm0tcmFuZ2UtdGh1bWItd2lkdGg6ICAgICAgICAgICAgICAgICAgIDFyZW0gIWRlZmF1bHQ7XG4kZm9ybS1yYW5nZS10aHVtYi1oZWlnaHQ6ICAgICAgICAgICAgICAgICAgJGZvcm0tcmFuZ2UtdGh1bWItd2lkdGggIWRlZmF1bHQ7XG4kZm9ybS1yYW5nZS10aHVtYi1iZzogICAgICAgICAgICAgICAgICAgICAgJGNvbXBvbmVudC1hY3RpdmUtYmcgIWRlZmF1bHQ7XG4kZm9ybS1yYW5nZS10aHVtYi1ib3JkZXI6ICAgICAgICAgICAgICAgICAgMCAhZGVmYXVsdDtcbiRmb3JtLXJhbmdlLXRodW1iLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAxcmVtICFkZWZhdWx0O1xuJGZvcm0tcmFuZ2UtdGh1bWItYm94LXNoYWRvdzogICAgICAgICAgICAgIDAgLjFyZW0gLjI1cmVtIHJnYmEoJGJsYWNrLCAuMSkgIWRlZmF1bHQ7XG4kZm9ybS1yYW5nZS10aHVtYi1mb2N1cy1ib3gtc2hhZG93OiAgICAgICAgMCAwIDAgMXB4ICRib2R5LWJnLCAkaW5wdXQtZm9jdXMtYm94LXNoYWRvdyAhZGVmYXVsdDtcbiRmb3JtLXJhbmdlLXRodW1iLWZvY3VzLWJveC1zaGFkb3ctd2lkdGg6ICAkaW5wdXQtZm9jdXMtd2lkdGggIWRlZmF1bHQ7IC8vIEZvciBmb2N1cyBib3ggc2hhZG93IGlzc3VlIGluIEVkZ2VcbiRmb3JtLXJhbmdlLXRodW1iLWFjdGl2ZS1iZzogICAgICAgICAgICAgICB0aW50LWNvbG9yKCRjb21wb25lbnQtYWN0aXZlLWJnLCA3MCUpICFkZWZhdWx0O1xuJGZvcm0tcmFuZ2UtdGh1bWItZGlzYWJsZWQtYmc6ICAgICAgICAgICAgICRncmF5LTUwMCAhZGVmYXVsdDtcbiRmb3JtLXJhbmdlLXRodW1iLXRyYW5zaXRpb246ICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yIC4xNXMgZWFzZS1pbi1vdXQsIGJvcmRlci1jb2xvciAuMTVzIGVhc2UtaW4tb3V0LCBib3gtc2hhZG93IC4xNXMgZWFzZS1pbi1vdXQgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGZvcm0tcmFuZ2UtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBmb3JtLWZpbGUtdmFyaWFibGVzXG4kZm9ybS1maWxlLWJ1dHRvbi1jb2xvcjogICAgICAgICAgJGlucHV0LWNvbG9yICFkZWZhdWx0O1xuJGZvcm0tZmlsZS1idXR0b24tYmc6ICAgICAgICAgICAgICRpbnB1dC1ncm91cC1hZGRvbi1iZyAhZGVmYXVsdDtcbiRmb3JtLWZpbGUtYnV0dG9uLWhvdmVyLWJnOiAgICAgICBzaGFkZS1jb2xvcigkZm9ybS1maWxlLWJ1dHRvbi1iZywgNSUpICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBmb3JtLWZpbGUtdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBmb3JtLWZsb2F0aW5nLXZhcmlhYmxlc1xuJGZvcm0tZmxvYXRpbmctaGVpZ2h0OiAgICAgICAgICAgIGFkZCgzLjVyZW0sICRpbnB1dC1oZWlnaHQtYm9yZGVyKSAhZGVmYXVsdDtcbiRmb3JtLWZsb2F0aW5nLWxpbmUtaGVpZ2h0OiAgICAgICAxLjI1ICFkZWZhdWx0O1xuJGZvcm0tZmxvYXRpbmctcGFkZGluZy14OiAgICAgICAgICRpbnB1dC1wYWRkaW5nLXggIWRlZmF1bHQ7XG4kZm9ybS1mbG9hdGluZy1wYWRkaW5nLXk6ICAgICAgICAgMXJlbSAhZGVmYXVsdDtcbiRmb3JtLWZsb2F0aW5nLWlucHV0LXBhZGRpbmctdDogICAxLjYyNXJlbSAhZGVmYXVsdDtcbiRmb3JtLWZsb2F0aW5nLWlucHV0LXBhZGRpbmctYjogICAuNjI1cmVtICFkZWZhdWx0O1xuJGZvcm0tZmxvYXRpbmctbGFiZWwtb3BhY2l0eTogICAgIC42NSAhZGVmYXVsdDtcbiRmb3JtLWZsb2F0aW5nLWxhYmVsLXRyYW5zZm9ybTogICBzY2FsZSguODUpIHRyYW5zbGF0ZVkoLS41cmVtKSB0cmFuc2xhdGVYKC4xNXJlbSkgIWRlZmF1bHQ7XG4kZm9ybS1mbG9hdGluZy10cmFuc2l0aW9uOiAgICAgICAgb3BhY2l0eSAuMXMgZWFzZS1pbi1vdXQsIHRyYW5zZm9ybSAuMXMgZWFzZS1pbi1vdXQgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGZvcm0tZmxvYXRpbmctdmFyaWFibGVzXG5cbi8vIEZvcm0gdmFsaWRhdGlvblxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgZm9ybS1mZWVkYmFjay12YXJpYWJsZXNcbiRmb3JtLWZlZWRiYWNrLW1hcmdpbi10b3A6ICAgICAgICAgICRmb3JtLXRleHQtbWFyZ2luLXRvcCAhZGVmYXVsdDtcbiRmb3JtLWZlZWRiYWNrLWZvbnQtc2l6ZTogICAgICAgICAgICRmb3JtLXRleHQtZm9udC1zaXplICFkZWZhdWx0O1xuJGZvcm0tZmVlZGJhY2stZm9udC1zdHlsZTogICAgICAgICAgJGZvcm0tdGV4dC1mb250LXN0eWxlICFkZWZhdWx0O1xuJGZvcm0tZmVlZGJhY2stdmFsaWQtY29sb3I6ICAgICAgICAgJHN1Y2Nlc3MgIWRlZmF1bHQ7XG4kZm9ybS1mZWVkYmFjay1pbnZhbGlkLWNvbG9yOiAgICAgICAkZGFuZ2VyICFkZWZhdWx0O1xuXG4kZm9ybS1mZWVkYmFjay1pY29uLXZhbGlkLWNvbG9yOiAgICAkZm9ybS1mZWVkYmFjay12YWxpZC1jb2xvciAhZGVmYXVsdDtcbiRmb3JtLWZlZWRiYWNrLWljb24tdmFsaWQ6ICAgICAgICAgIHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCw8c3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nMCAwIDggOCc+PHBhdGggZmlsbD0nI3skZm9ybS1mZWVkYmFjay1pY29uLXZhbGlkLWNvbG9yfScgZD0nTTIuMyA2LjczTC42IDQuNTNjLS40LTEuMDQuNDYtMS40IDEuMS0uOGwxLjEgMS40IDMuNC0zLjhjLjYtLjYzIDEuNi0uMjcgMS4yLjdsLTQgNC42Yy0uNDMuNS0uOC40LTEuMS4xeicvPjwvc3ZnPlwiKSAhZGVmYXVsdDtcbiRmb3JtLWZlZWRiYWNrLWljb24taW52YWxpZC1jb2xvcjogICRmb3JtLWZlZWRiYWNrLWludmFsaWQtY29sb3IgIWRlZmF1bHQ7XG4kZm9ybS1mZWVkYmFjay1pY29uLWludmFsaWQ6ICAgICAgICB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsPHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCAxMiAxMicgd2lkdGg9JzEyJyBoZWlnaHQ9JzEyJyBmaWxsPSdub25lJyBzdHJva2U9JyN7JGZvcm0tZmVlZGJhY2staWNvbi1pbnZhbGlkLWNvbG9yfSc+PGNpcmNsZSBjeD0nNicgY3k9JzYnIHI9JzQuNScvPjxwYXRoIHN0cm9rZS1saW5lam9pbj0ncm91bmQnIGQ9J001LjggMy42aC40TDYgNi41eicvPjxjaXJjbGUgY3g9JzYnIGN5PSc4LjInIHI9Jy42JyBmaWxsPScjeyRmb3JtLWZlZWRiYWNrLWljb24taW52YWxpZC1jb2xvcn0nIHN0cm9rZT0nbm9uZScvPjwvc3ZnPlwiKSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgZm9ybS1mZWVkYmFjay12YXJpYWJsZXNcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGZvcm0tdmFsaWRhdGlvbi1zdGF0ZXNcbiRmb3JtLXZhbGlkYXRpb24tc3RhdGVzOiAoXG4gIFwidmFsaWRcIjogKFxuICAgIFwiY29sb3JcIjogJGZvcm0tZmVlZGJhY2stdmFsaWQtY29sb3IsXG4gICAgXCJpY29uXCI6ICRmb3JtLWZlZWRiYWNrLWljb24tdmFsaWRcbiAgKSxcbiAgXCJpbnZhbGlkXCI6IChcbiAgICBcImNvbG9yXCI6ICRmb3JtLWZlZWRiYWNrLWludmFsaWQtY29sb3IsXG4gICAgXCJpY29uXCI6ICRmb3JtLWZlZWRiYWNrLWljb24taW52YWxpZFxuICApXG4pICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBmb3JtLXZhbGlkYXRpb24tc3RhdGVzXG5cbi8vIFotaW5kZXggbWFzdGVyIGxpc3Rcbi8vXG4vLyBXYXJuaW5nOiBBdm9pZCBjdXN0b21pemluZyB0aGVzZSB2YWx1ZXMuIFRoZXkncmUgdXNlZCBmb3IgYSBiaXJkJ3MgZXllIHZpZXdcbi8vIG9mIGNvbXBvbmVudHMgZGVwZW5kZW50IG9uIHRoZSB6LWF4aXMgYW5kIGFyZSBkZXNpZ25lZCB0byBhbGwgd29yayB0b2dldGhlci5cblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHppbmRleC1zdGFja1xuJHppbmRleC1kcm9wZG93bjogICAgICAgICAgICAgICAgICAgMTAwMCAhZGVmYXVsdDtcbiR6aW5kZXgtc3RpY2t5OiAgICAgICAgICAgICAgICAgICAgIDEwMjAgIWRlZmF1bHQ7XG4kemluZGV4LWZpeGVkOiAgICAgICAgICAgICAgICAgICAgICAxMDMwICFkZWZhdWx0O1xuJHppbmRleC1vZmZjYW52YXMtYmFja2Ryb3A6ICAgICAgICAgMTA0MCAhZGVmYXVsdDtcbiR6aW5kZXgtb2ZmY2FudmFzOiAgICAgICAgICAgICAgICAgIDEwNDUgIWRlZmF1bHQ7XG4kemluZGV4LW1vZGFsLWJhY2tkcm9wOiAgICAgICAgICAgICAxMDUwICFkZWZhdWx0O1xuJHppbmRleC1tb2RhbDogICAgICAgICAgICAgICAgICAgICAgMTA1NSAhZGVmYXVsdDtcbiR6aW5kZXgtcG9wb3ZlcjogICAgICAgICAgICAgICAgICAgIDEwNzAgIWRlZmF1bHQ7XG4kemluZGV4LXRvb2x0aXA6ICAgICAgICAgICAgICAgICAgICAxMDgwICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCB6aW5kZXgtc3RhY2tcblxuXG4vLyBOYXZzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBuYXYtdmFyaWFibGVzXG4kbmF2LWxpbmstcGFkZGluZy15OiAgICAgICAgICAgICAgICAuNXJlbSAhZGVmYXVsdDtcbiRuYXYtbGluay1wYWRkaW5nLXg6ICAgICAgICAgICAgICAgIDFyZW0gIWRlZmF1bHQ7XG4kbmF2LWxpbmstZm9udC1zaXplOiAgICAgICAgICAgICAgICBudWxsICFkZWZhdWx0O1xuJG5hdi1saW5rLWZvbnQtd2VpZ2h0OiAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRuYXYtbGluay1jb2xvcjogICAgICAgICAgICAgICAgICAgICRsaW5rLWNvbG9yICFkZWZhdWx0O1xuJG5hdi1saW5rLWhvdmVyLWNvbG9yOiAgICAgICAgICAgICAgJGxpbmstaG92ZXItY29sb3IgIWRlZmF1bHQ7XG4kbmF2LWxpbmstdHJhbnNpdGlvbjogICAgICAgICAgICAgICBjb2xvciAuMTVzIGVhc2UtaW4tb3V0LCBiYWNrZ3JvdW5kLWNvbG9yIC4xNXMgZWFzZS1pbi1vdXQsIGJvcmRlci1jb2xvciAuMTVzIGVhc2UtaW4tb3V0ICFkZWZhdWx0O1xuJG5hdi1saW5rLWRpc2FibGVkLWNvbG9yOiAgICAgICAgICAgJGdyYXktNjAwICFkZWZhdWx0O1xuXG4kbmF2LXRhYnMtYm9yZGVyLWNvbG9yOiAgICAgICAgICAgICAkZ3JheS0zMDAgIWRlZmF1bHQ7XG4kbmF2LXRhYnMtYm9yZGVyLXdpZHRoOiAgICAgICAgICAgICAkYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJG5hdi10YWJzLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAgJGJvcmRlci1yYWRpdXMgIWRlZmF1bHQ7XG4kbmF2LXRhYnMtbGluay1ob3Zlci1ib3JkZXItY29sb3I6ICAkZ3JheS0yMDAgJGdyYXktMjAwICRuYXYtdGFicy1ib3JkZXItY29sb3IgIWRlZmF1bHQ7XG4kbmF2LXRhYnMtbGluay1hY3RpdmUtY29sb3I6ICAgICAgICAkZ3JheS03MDAgIWRlZmF1bHQ7XG4kbmF2LXRhYnMtbGluay1hY3RpdmUtYmc6ICAgICAgICAgICAkYm9keS1iZyAhZGVmYXVsdDtcbiRuYXYtdGFicy1saW5rLWFjdGl2ZS1ib3JkZXItY29sb3I6ICRncmF5LTMwMCAkZ3JheS0zMDAgJG5hdi10YWJzLWxpbmstYWN0aXZlLWJnICFkZWZhdWx0O1xuXG4kbmF2LXBpbGxzLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAkYm9yZGVyLXJhZGl1cyAhZGVmYXVsdDtcbiRuYXYtcGlsbHMtbGluay1hY3RpdmUtY29sb3I6ICAgICAgICRjb21wb25lbnQtYWN0aXZlLWNvbG9yICFkZWZhdWx0O1xuJG5hdi1waWxscy1saW5rLWFjdGl2ZS1iZzogICAgICAgICAgJGNvbXBvbmVudC1hY3RpdmUtYmcgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIG5hdi12YXJpYWJsZXNcblxuXG4vLyBOYXZiYXJcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IG5hdmJhci12YXJpYWJsZXNcbiRuYXZiYXItcGFkZGluZy15OiAgICAgICAgICAgICAgICAgICRzcGFjZXIgKiAuNSAhZGVmYXVsdDtcbiRuYXZiYXItcGFkZGluZy14OiAgICAgICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG5cbiRuYXZiYXItbmF2LWxpbmstcGFkZGluZy14OiAgICAgICAgIC41cmVtICFkZWZhdWx0O1xuXG4kbmF2YmFyLWJyYW5kLWZvbnQtc2l6ZTogICAgICAgICAgICAkZm9udC1zaXplLWxnICFkZWZhdWx0O1xuLy8gQ29tcHV0ZSB0aGUgbmF2YmFyLWJyYW5kIHBhZGRpbmcteSBzbyB0aGUgbmF2YmFyLWJyYW5kIHdpbGwgaGF2ZSB0aGUgc2FtZSBoZWlnaHQgYXMgbmF2YmFyLXRleHQgYW5kIG5hdi1saW5rXG4kbmF2LWxpbmstaGVpZ2h0OiAgICAgICAgICAgICAgICAgICAkZm9udC1zaXplLWJhc2UgKiAkbGluZS1oZWlnaHQtYmFzZSArICRuYXYtbGluay1wYWRkaW5nLXkgKiAyICFkZWZhdWx0O1xuJG5hdmJhci1icmFuZC1oZWlnaHQ6ICAgICAgICAgICAgICAgJG5hdmJhci1icmFuZC1mb250LXNpemUgKiAkbGluZS1oZWlnaHQtYmFzZSAhZGVmYXVsdDtcbiRuYXZiYXItYnJhbmQtcGFkZGluZy15OiAgICAgICAgICAgICgkbmF2LWxpbmstaGVpZ2h0IC0gJG5hdmJhci1icmFuZC1oZWlnaHQpICogLjUgIWRlZmF1bHQ7XG4kbmF2YmFyLWJyYW5kLW1hcmdpbi1lbmQ6ICAgICAgICAgICAxcmVtICFkZWZhdWx0O1xuXG4kbmF2YmFyLXRvZ2dsZXItcGFkZGluZy15OiAgICAgICAgICAuMjVyZW0gIWRlZmF1bHQ7XG4kbmF2YmFyLXRvZ2dsZXItcGFkZGluZy14OiAgICAgICAgICAuNzVyZW0gIWRlZmF1bHQ7XG4kbmF2YmFyLXRvZ2dsZXItZm9udC1zaXplOiAgICAgICAgICAkZm9udC1zaXplLWxnICFkZWZhdWx0O1xuJG5hdmJhci10b2dnbGVyLWJvcmRlci1yYWRpdXM6ICAgICAgJGJ0bi1ib3JkZXItcmFkaXVzICFkZWZhdWx0O1xuJG5hdmJhci10b2dnbGVyLWZvY3VzLXdpZHRoOiAgICAgICAgJGJ0bi1mb2N1cy13aWR0aCAhZGVmYXVsdDtcbiRuYXZiYXItdG9nZ2xlci10cmFuc2l0aW9uOiAgICAgICAgIGJveC1zaGFkb3cgLjE1cyBlYXNlLWluLW91dCAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgbmF2YmFyLXZhcmlhYmxlc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgbmF2YmFyLXRoZW1lLXZhcmlhYmxlc1xuJG5hdmJhci1kYXJrLWNvbG9yOiAgICAgICAgICAgICAgICAgcmdiYSgkd2hpdGUsIC41NSkgIWRlZmF1bHQ7XG4kbmF2YmFyLWRhcmstaG92ZXItY29sb3I6ICAgICAgICAgICByZ2JhKCR3aGl0ZSwgLjc1KSAhZGVmYXVsdDtcbiRuYXZiYXItZGFyay1hY3RpdmUtY29sb3I6ICAgICAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRuYXZiYXItZGFyay1kaXNhYmxlZC1jb2xvcjogICAgICAgIHJnYmEoJHdoaXRlLCAuMjUpICFkZWZhdWx0O1xuJG5hdmJhci1kYXJrLXRvZ2dsZXItaWNvbi1iZzogICAgICAgdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgMzAgMzAnPjxwYXRoIHN0cm9rZT0nI3skbmF2YmFyLWRhcmstY29sb3J9JyBzdHJva2UtbGluZWNhcD0ncm91bmQnIHN0cm9rZS1taXRlcmxpbWl0PScxMCcgc3Ryb2tlLXdpZHRoPScyJyBkPSdNNCA3aDIyTTQgMTVoMjJNNCAyM2gyMicvPjwvc3ZnPlwiKSAhZGVmYXVsdDtcbiRuYXZiYXItZGFyay10b2dnbGVyLWJvcmRlci1jb2xvcjogIHJnYmEoJHdoaXRlLCAuMSkgIWRlZmF1bHQ7XG5cbiRuYXZiYXItbGlnaHQtY29sb3I6ICAgICAgICAgICAgICAgIHJnYmEoJGJsYWNrLCAuNTUpICFkZWZhdWx0O1xuJG5hdmJhci1saWdodC1ob3Zlci1jb2xvcjogICAgICAgICAgcmdiYSgkYmxhY2ssIC43KSAhZGVmYXVsdDtcbiRuYXZiYXItbGlnaHQtYWN0aXZlLWNvbG9yOiAgICAgICAgIHJnYmEoJGJsYWNrLCAuOSkgIWRlZmF1bHQ7XG4kbmF2YmFyLWxpZ2h0LWRpc2FibGVkLWNvbG9yOiAgICAgICByZ2JhKCRibGFjaywgLjMpICFkZWZhdWx0O1xuJG5hdmJhci1saWdodC10b2dnbGVyLWljb24tYmc6ICAgICAgdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgMzAgMzAnPjxwYXRoIHN0cm9rZT0nI3skbmF2YmFyLWxpZ2h0LWNvbG9yfScgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJyBzdHJva2UtbWl0ZXJsaW1pdD0nMTAnIHN0cm9rZS13aWR0aD0nMicgZD0nTTQgN2gyMk00IDE1aDIyTTQgMjNoMjInLz48L3N2Zz5cIikgIWRlZmF1bHQ7XG4kbmF2YmFyLWxpZ2h0LXRvZ2dsZXItYm9yZGVyLWNvbG9yOiByZ2JhKCRibGFjaywgLjEpICFkZWZhdWx0O1xuXG4kbmF2YmFyLWxpZ2h0LWJyYW5kLWNvbG9yOiAgICAgICAgICAgICAgICAkbmF2YmFyLWxpZ2h0LWFjdGl2ZS1jb2xvciAhZGVmYXVsdDtcbiRuYXZiYXItbGlnaHQtYnJhbmQtaG92ZXItY29sb3I6ICAgICAgICAgICRuYXZiYXItbGlnaHQtYWN0aXZlLWNvbG9yICFkZWZhdWx0O1xuJG5hdmJhci1kYXJrLWJyYW5kLWNvbG9yOiAgICAgICAgICAgICAgICAgJG5hdmJhci1kYXJrLWFjdGl2ZS1jb2xvciAhZGVmYXVsdDtcbiRuYXZiYXItZGFyay1icmFuZC1ob3Zlci1jb2xvcjogICAgICAgICAgICRuYXZiYXItZGFyay1hY3RpdmUtY29sb3IgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIG5hdmJhci10aGVtZS12YXJpYWJsZXNcblxuXG4vLyBEcm9wZG93bnNcbi8vXG4vLyBEcm9wZG93biBtZW51IGNvbnRhaW5lciBhbmQgY29udGVudHMuXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBkcm9wZG93bi12YXJpYWJsZXNcbiRkcm9wZG93bi1taW4td2lkdGg6ICAgICAgICAgICAgICAgIDEwcmVtICFkZWZhdWx0O1xuJGRyb3Bkb3duLXBhZGRpbmcteDogICAgICAgICAgICAgICAgMCAhZGVmYXVsdDtcbiRkcm9wZG93bi1wYWRkaW5nLXk6ICAgICAgICAgICAgICAgIC41cmVtICFkZWZhdWx0O1xuJGRyb3Bkb3duLXNwYWNlcjogICAgICAgICAgICAgICAgICAgLjEyNXJlbSAhZGVmYXVsdDtcbiRkcm9wZG93bi1mb250LXNpemU6ICAgICAgICAgICAgICAgICRmb250LXNpemUtYmFzZSAhZGVmYXVsdDtcbiRkcm9wZG93bi1jb2xvcjogICAgICAgICAgICAgICAgICAgICRib2R5LWNvbG9yICFkZWZhdWx0O1xuJGRyb3Bkb3duLWJnOiAgICAgICAgICAgICAgICAgICAgICAgJHdoaXRlICFkZWZhdWx0O1xuJGRyb3Bkb3duLWJvcmRlci1jb2xvcjogICAgICAgICAgICAgcmdiYSgkYmxhY2ssIC4xNSkgIWRlZmF1bHQ7XG4kZHJvcGRvd24tYm9yZGVyLXJhZGl1czogICAgICAgICAgICAkYm9yZGVyLXJhZGl1cyAhZGVmYXVsdDtcbiRkcm9wZG93bi1ib3JkZXItd2lkdGg6ICAgICAgICAgICAgICRib3JkZXItd2lkdGggIWRlZmF1bHQ7XG4kZHJvcGRvd24taW5uZXItYm9yZGVyLXJhZGl1czogICAgICBzdWJ0cmFjdCgkZHJvcGRvd24tYm9yZGVyLXJhZGl1cywgJGRyb3Bkb3duLWJvcmRlci13aWR0aCkgIWRlZmF1bHQ7XG4kZHJvcGRvd24tZGl2aWRlci1iZzogICAgICAgICAgICAgICAkZHJvcGRvd24tYm9yZGVyLWNvbG9yICFkZWZhdWx0O1xuJGRyb3Bkb3duLWRpdmlkZXItbWFyZ2luLXk6ICAgICAgICAgJHNwYWNlciAqIC41ICFkZWZhdWx0O1xuJGRyb3Bkb3duLWJveC1zaGFkb3c6ICAgICAgICAgICAgICAgJGJveC1zaGFkb3cgIWRlZmF1bHQ7XG5cbiRkcm9wZG93bi1saW5rLWNvbG9yOiAgICAgICAgICAgICAgICRncmF5LTkwMCAhZGVmYXVsdDtcbiRkcm9wZG93bi1saW5rLWhvdmVyLWNvbG9yOiAgICAgICAgIHNoYWRlLWNvbG9yKCRkcm9wZG93bi1saW5rLWNvbG9yLCAxMCUpICFkZWZhdWx0O1xuJGRyb3Bkb3duLWxpbmstaG92ZXItYmc6ICAgICAgICAgICAgJGdyYXktMjAwICFkZWZhdWx0O1xuXG4kZHJvcGRvd24tbGluay1hY3RpdmUtY29sb3I6ICAgICAgICAkY29tcG9uZW50LWFjdGl2ZS1jb2xvciAhZGVmYXVsdDtcbiRkcm9wZG93bi1saW5rLWFjdGl2ZS1iZzogICAgICAgICAgICRjb21wb25lbnQtYWN0aXZlLWJnICFkZWZhdWx0O1xuXG4kZHJvcGRvd24tbGluay1kaXNhYmxlZC1jb2xvcjogICAgICAkZ3JheS01MDAgIWRlZmF1bHQ7XG5cbiRkcm9wZG93bi1pdGVtLXBhZGRpbmcteTogICAgICAgICAgICRzcGFjZXIgKiAuMjUgIWRlZmF1bHQ7XG4kZHJvcGRvd24taXRlbS1wYWRkaW5nLXg6ICAgICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuXG4kZHJvcGRvd24taGVhZGVyLWNvbG9yOiAgICAgICAgICAgICAkZ3JheS02MDAgIWRlZmF1bHQ7XG4kZHJvcGRvd24taGVhZGVyLXBhZGRpbmc6ICAgICAgICAgICAkZHJvcGRvd24tcGFkZGluZy15ICRkcm9wZG93bi1pdGVtLXBhZGRpbmcteCAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgZHJvcGRvd24tdmFyaWFibGVzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBkcm9wZG93bi1kYXJrLXZhcmlhYmxlc1xuJGRyb3Bkb3duLWRhcmstY29sb3I6ICAgICAgICAgICAgICAgJGdyYXktMzAwICFkZWZhdWx0O1xuJGRyb3Bkb3duLWRhcmstYmc6ICAgICAgICAgICAgICAgICAgJGdyYXktODAwICFkZWZhdWx0O1xuJGRyb3Bkb3duLWRhcmstYm9yZGVyLWNvbG9yOiAgICAgICAgJGRyb3Bkb3duLWJvcmRlci1jb2xvciAhZGVmYXVsdDtcbiRkcm9wZG93bi1kYXJrLWRpdmlkZXItYmc6ICAgICAgICAgICRkcm9wZG93bi1kaXZpZGVyLWJnICFkZWZhdWx0O1xuJGRyb3Bkb3duLWRhcmstYm94LXNoYWRvdzogICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRkcm9wZG93bi1kYXJrLWxpbmstY29sb3I6ICAgICAgICAgICRkcm9wZG93bi1kYXJrLWNvbG9yICFkZWZhdWx0O1xuJGRyb3Bkb3duLWRhcmstbGluay1ob3Zlci1jb2xvcjogICAgJHdoaXRlICFkZWZhdWx0O1xuJGRyb3Bkb3duLWRhcmstbGluay1ob3Zlci1iZzogICAgICAgcmdiYSgkd2hpdGUsIC4xNSkgIWRlZmF1bHQ7XG4kZHJvcGRvd24tZGFyay1saW5rLWFjdGl2ZS1jb2xvcjogICAkZHJvcGRvd24tbGluay1hY3RpdmUtY29sb3IgIWRlZmF1bHQ7XG4kZHJvcGRvd24tZGFyay1saW5rLWFjdGl2ZS1iZzogICAgICAkZHJvcGRvd24tbGluay1hY3RpdmUtYmcgIWRlZmF1bHQ7XG4kZHJvcGRvd24tZGFyay1saW5rLWRpc2FibGVkLWNvbG9yOiAkZ3JheS01MDAgIWRlZmF1bHQ7XG4kZHJvcGRvd24tZGFyay1oZWFkZXItY29sb3I6ICAgICAgICAkZ3JheS01MDAgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGRyb3Bkb3duLWRhcmstdmFyaWFibGVzXG5cblxuLy8gUGFnaW5hdGlvblxuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgcGFnaW5hdGlvbi12YXJpYWJsZXNcbiRwYWdpbmF0aW9uLXBhZGRpbmcteTogICAgICAgICAgICAgIC4zNzVyZW0gIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1wYWRkaW5nLXg6ICAgICAgICAgICAgICAuNzVyZW0gIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1wYWRkaW5nLXktc206ICAgICAgICAgICAuMjVyZW0gIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1wYWRkaW5nLXgtc206ICAgICAgICAgICAuNXJlbSAhZGVmYXVsdDtcbiRwYWdpbmF0aW9uLXBhZGRpbmcteS1sZzogICAgICAgICAgIC43NXJlbSAhZGVmYXVsdDtcbiRwYWdpbmF0aW9uLXBhZGRpbmcteC1sZzogICAgICAgICAgIDEuNXJlbSAhZGVmYXVsdDtcblxuJHBhZ2luYXRpb24tY29sb3I6ICAgICAgICAgICAgICAgICAgJGxpbmstY29sb3IgIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1iZzogICAgICAgICAgICAgICAgICAgICAkd2hpdGUgIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1ib3JkZXItd2lkdGg6ICAgICAgICAgICAkYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJHBhZ2luYXRpb24tYm9yZGVyLXJhZGl1czogICAgICAgICAgJGJvcmRlci1yYWRpdXMgIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1tYXJnaW4tc3RhcnQ6ICAgICAgICAgICAtJHBhZ2luYXRpb24tYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJHBhZ2luYXRpb24tYm9yZGVyLWNvbG9yOiAgICAgICAgICAgJGdyYXktMzAwICFkZWZhdWx0O1xuXG4kcGFnaW5hdGlvbi1mb2N1cy1jb2xvcjogICAgICAgICAgICAkbGluay1ob3Zlci1jb2xvciAhZGVmYXVsdDtcbiRwYWdpbmF0aW9uLWZvY3VzLWJnOiAgICAgICAgICAgICAgICRncmF5LTIwMCAhZGVmYXVsdDtcbiRwYWdpbmF0aW9uLWZvY3VzLWJveC1zaGFkb3c6ICAgICAgICRpbnB1dC1idG4tZm9jdXMtYm94LXNoYWRvdyAhZGVmYXVsdDtcbiRwYWdpbmF0aW9uLWZvY3VzLW91dGxpbmU6ICAgICAgICAgIDAgIWRlZmF1bHQ7XG5cbiRwYWdpbmF0aW9uLWhvdmVyLWNvbG9yOiAgICAgICAgICAgICRsaW5rLWhvdmVyLWNvbG9yICFkZWZhdWx0O1xuJHBhZ2luYXRpb24taG92ZXItYmc6ICAgICAgICAgICAgICAgJGdyYXktMjAwICFkZWZhdWx0O1xuJHBhZ2luYXRpb24taG92ZXItYm9yZGVyLWNvbG9yOiAgICAgJGdyYXktMzAwICFkZWZhdWx0O1xuXG4kcGFnaW5hdGlvbi1hY3RpdmUtY29sb3I6ICAgICAgICAgICAkY29tcG9uZW50LWFjdGl2ZS1jb2xvciAhZGVmYXVsdDtcbiRwYWdpbmF0aW9uLWFjdGl2ZS1iZzogICAgICAgICAgICAgICRjb21wb25lbnQtYWN0aXZlLWJnICFkZWZhdWx0O1xuJHBhZ2luYXRpb24tYWN0aXZlLWJvcmRlci1jb2xvcjogICAgJHBhZ2luYXRpb24tYWN0aXZlLWJnICFkZWZhdWx0O1xuXG4kcGFnaW5hdGlvbi1kaXNhYmxlZC1jb2xvcjogICAgICAgICAkZ3JheS02MDAgIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1kaXNhYmxlZC1iZzogICAgICAgICAgICAkd2hpdGUgIWRlZmF1bHQ7XG4kcGFnaW5hdGlvbi1kaXNhYmxlZC1ib3JkZXItY29sb3I6ICAkZ3JheS0zMDAgIWRlZmF1bHQ7XG5cbiRwYWdpbmF0aW9uLXRyYW5zaXRpb246ICAgICAgICAgICAgICBjb2xvciAuMTVzIGVhc2UtaW4tb3V0LCBiYWNrZ3JvdW5kLWNvbG9yIC4xNXMgZWFzZS1pbi1vdXQsIGJvcmRlci1jb2xvciAuMTVzIGVhc2UtaW4tb3V0LCBib3gtc2hhZG93IC4xNXMgZWFzZS1pbi1vdXQgIWRlZmF1bHQ7XG5cbiRwYWdpbmF0aW9uLWJvcmRlci1yYWRpdXMtc206ICAgICAgICRib3JkZXItcmFkaXVzLXNtICFkZWZhdWx0O1xuJHBhZ2luYXRpb24tYm9yZGVyLXJhZGl1cy1sZzogICAgICAgJGJvcmRlci1yYWRpdXMtbGcgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHBhZ2luYXRpb24tdmFyaWFibGVzXG5cblxuLy8gUGxhY2Vob2xkZXJzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBwbGFjZWhvbGRlcnNcbiRwbGFjZWhvbGRlci1vcGFjaXR5LW1heDogICAgICAgICAgIC41ICFkZWZhdWx0O1xuJHBsYWNlaG9sZGVyLW9wYWNpdHktbWluOiAgICAgICAgICAgLjIgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHBsYWNlaG9sZGVyc1xuXG4vLyBDYXJkc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgY2FyZC12YXJpYWJsZXNcbiRjYXJkLXNwYWNlci15OiAgICAgICAgICAgICAgICAgICAgICRzcGFjZXIgIWRlZmF1bHQ7XG4kY2FyZC1zcGFjZXIteDogICAgICAgICAgICAgICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuJGNhcmQtdGl0bGUtc3BhY2VyLXk6ICAgICAgICAgICAgICAgJHNwYWNlciAqIC41ICFkZWZhdWx0O1xuJGNhcmQtYm9yZGVyLXdpZHRoOiAgICAgICAgICAgICAgICAgJGJvcmRlci13aWR0aCAhZGVmYXVsdDtcbiRjYXJkLWJvcmRlci1jb2xvcjogICAgICAgICAgICAgICAgIHJnYmEoJGJsYWNrLCAuMTI1KSAhZGVmYXVsdDtcbiRjYXJkLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAgICAgICRib3JkZXItcmFkaXVzICFkZWZhdWx0O1xuJGNhcmQtYm94LXNoYWRvdzogICAgICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRjYXJkLWlubmVyLWJvcmRlci1yYWRpdXM6ICAgICAgICAgIHN1YnRyYWN0KCRjYXJkLWJvcmRlci1yYWRpdXMsICRjYXJkLWJvcmRlci13aWR0aCkgIWRlZmF1bHQ7XG4kY2FyZC1jYXAtcGFkZGluZy15OiAgICAgICAgICAgICAgICAkY2FyZC1zcGFjZXIteSAqIC41ICFkZWZhdWx0O1xuJGNhcmQtY2FwLXBhZGRpbmcteDogICAgICAgICAgICAgICAgJGNhcmQtc3BhY2VyLXggIWRlZmF1bHQ7XG4kY2FyZC1jYXAtYmc6ICAgICAgICAgICAgICAgICAgICAgICByZ2JhKCRibGFjaywgLjAzKSAhZGVmYXVsdDtcbiRjYXJkLWNhcC1jb2xvcjogICAgICAgICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG4kY2FyZC1oZWlnaHQ6ICAgICAgICAgICAgICAgICAgICAgICBudWxsICFkZWZhdWx0O1xuJGNhcmQtY29sb3I6ICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRjYXJkLWJnOiAgICAgICAgICAgICAgICAgICAgICAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRjYXJkLWltZy1vdmVybGF5LXBhZGRpbmc6ICAgICAgICAgICRzcGFjZXIgIWRlZmF1bHQ7XG4kY2FyZC1ncm91cC1tYXJnaW46ICAgICAgICAgICAgICAgICAkZ3JpZC1ndXR0ZXItd2lkdGggKiAuNSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgY2FyZC12YXJpYWJsZXNcblxuLy8gQWNjb3JkaW9uXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBhY2NvcmRpb24tdmFyaWFibGVzXG4kYWNjb3JkaW9uLXBhZGRpbmcteTogICAgICAgICAgICAgICAgICAgICAxcmVtICFkZWZhdWx0O1xuJGFjY29yZGlvbi1wYWRkaW5nLXg6ICAgICAgICAgICAgICAgICAgICAgMS4yNXJlbSAhZGVmYXVsdDtcbiRhY2NvcmRpb24tY29sb3I6ICAgICAgICAgICAgICAgICAgICAgICAgICRib2R5LWNvbG9yICFkZWZhdWx0O1xuJGFjY29yZGlvbi1iZzogICAgICAgICAgICAgICAgICAgICAgICAgICAgJGJvZHktYmcgIWRlZmF1bHQ7XG4kYWNjb3JkaW9uLWJvcmRlci13aWR0aDogICAgICAgICAgICAgICAgICAkYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJGFjY29yZGlvbi1ib3JkZXItY29sb3I6ICAgICAgICAgICAgICAgICAgcmdiYSgkYmxhY2ssIC4xMjUpICFkZWZhdWx0O1xuJGFjY29yZGlvbi1ib3JkZXItcmFkaXVzOiAgICAgICAgICAgICAgICAgJGJvcmRlci1yYWRpdXMgIWRlZmF1bHQ7XG4kYWNjb3JkaW9uLWlubmVyLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICBzdWJ0cmFjdCgkYWNjb3JkaW9uLWJvcmRlci1yYWRpdXMsICRhY2NvcmRpb24tYm9yZGVyLXdpZHRoKSAhZGVmYXVsdDtcblxuJGFjY29yZGlvbi1ib2R5LXBhZGRpbmcteTogICAgICAgICAgICAgICAgJGFjY29yZGlvbi1wYWRkaW5nLXkgIWRlZmF1bHQ7XG4kYWNjb3JkaW9uLWJvZHktcGFkZGluZy14OiAgICAgICAgICAgICAgICAkYWNjb3JkaW9uLXBhZGRpbmcteCAhZGVmYXVsdDtcblxuJGFjY29yZGlvbi1idXR0b24tcGFkZGluZy15OiAgICAgICAgICAgICAgJGFjY29yZGlvbi1wYWRkaW5nLXkgIWRlZmF1bHQ7XG4kYWNjb3JkaW9uLWJ1dHRvbi1wYWRkaW5nLXg6ICAgICAgICAgICAgICAkYWNjb3JkaW9uLXBhZGRpbmcteCAhZGVmYXVsdDtcbiRhY2NvcmRpb24tYnV0dG9uLWNvbG9yOiAgICAgICAgICAgICAgICAgICRhY2NvcmRpb24tY29sb3IgIWRlZmF1bHQ7XG4kYWNjb3JkaW9uLWJ1dHRvbi1iZzogICAgICAgICAgICAgICAgICAgICAkYWNjb3JkaW9uLWJnICFkZWZhdWx0O1xuJGFjY29yZGlvbi10cmFuc2l0aW9uOiAgICAgICAgICAgICAgICAgICAgJGJ0bi10cmFuc2l0aW9uLCBib3JkZXItcmFkaXVzIC4xNXMgZWFzZSAhZGVmYXVsdDtcbiRhY2NvcmRpb24tYnV0dG9uLWFjdGl2ZS1iZzogICAgICAgICAgICAgIHRpbnQtY29sb3IoJGNvbXBvbmVudC1hY3RpdmUtYmcsIDkwJSkgIWRlZmF1bHQ7XG4kYWNjb3JkaW9uLWJ1dHRvbi1hY3RpdmUtY29sb3I6ICAgICAgICAgICBzaGFkZS1jb2xvcigkcHJpbWFyeSwgMTAlKSAhZGVmYXVsdDtcblxuJGFjY29yZGlvbi1idXR0b24tZm9jdXMtYm9yZGVyLWNvbG9yOiAgICAgJGlucHV0LWZvY3VzLWJvcmRlci1jb2xvciAhZGVmYXVsdDtcbiRhY2NvcmRpb24tYnV0dG9uLWZvY3VzLWJveC1zaGFkb3c6ICAgICAgICRidG4tZm9jdXMtYm94LXNoYWRvdyAhZGVmYXVsdDtcblxuJGFjY29yZGlvbi1pY29uLXdpZHRoOiAgICAgICAgICAgICAgICAgICAgMS4yNXJlbSAhZGVmYXVsdDtcbiRhY2NvcmRpb24taWNvbi1jb2xvcjogICAgICAgICAgICAgICAgICAgICRhY2NvcmRpb24tYnV0dG9uLWNvbG9yICFkZWZhdWx0O1xuJGFjY29yZGlvbi1pY29uLWFjdGl2ZS1jb2xvcjogICAgICAgICAgICAgJGFjY29yZGlvbi1idXR0b24tYWN0aXZlLWNvbG9yICFkZWZhdWx0O1xuJGFjY29yZGlvbi1pY29uLXRyYW5zaXRpb246ICAgICAgICAgICAgICAgdHJhbnNmb3JtIC4ycyBlYXNlLWluLW91dCAhZGVmYXVsdDtcbiRhY2NvcmRpb24taWNvbi10cmFuc2Zvcm06ICAgICAgICAgICAgICAgIHJvdGF0ZSgtMTgwZGVnKSAhZGVmYXVsdDtcblxuJGFjY29yZGlvbi1idXR0b24taWNvbjogICAgICAgICB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsPHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9JzAgMCAxNiAxNicgZmlsbD0nI3skYWNjb3JkaW9uLWljb24tY29sb3J9Jz48cGF0aCBmaWxsLXJ1bGU9J2V2ZW5vZGQnIGQ9J00xLjY0NiA0LjY0NmEuNS41IDAgMCAxIC43MDggMEw4IDEwLjI5M2w1LjY0Ni01LjY0N2EuNS41IDAgMCAxIC43MDguNzA4bC02IDZhLjUuNSAwIDAgMS0uNzA4IDBsLTYtNmEuNS41IDAgMCAxIDAtLjcwOHonLz48L3N2Zz5cIikgIWRlZmF1bHQ7XG4kYWNjb3JkaW9uLWJ1dHRvbi1hY3RpdmUtaWNvbjogIHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCw8c3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nMCAwIDE2IDE2JyBmaWxsPScjeyRhY2NvcmRpb24taWNvbi1hY3RpdmUtY29sb3J9Jz48cGF0aCBmaWxsLXJ1bGU9J2V2ZW5vZGQnIGQ9J00xLjY0NiA0LjY0NmEuNS41IDAgMCAxIC43MDggMEw4IDEwLjI5M2w1LjY0Ni01LjY0N2EuNS41IDAgMCAxIC43MDguNzA4bC02IDZhLjUuNSAwIDAgMS0uNzA4IDBsLTYtNmEuNS41IDAgMCAxIDAtLjcwOHonLz48L3N2Zz5cIikgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGFjY29yZGlvbi12YXJpYWJsZXNcblxuLy8gVG9vbHRpcHNcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHRvb2x0aXAtdmFyaWFibGVzXG4kdG9vbHRpcC1mb250LXNpemU6ICAgICAgICAgICAgICAgICAkZm9udC1zaXplLXNtICFkZWZhdWx0O1xuJHRvb2x0aXAtbWF4LXdpZHRoOiAgICAgICAgICAgICAgICAgMjAwcHggIWRlZmF1bHQ7XG4kdG9vbHRpcC1jb2xvcjogICAgICAgICAgICAgICAgICAgICAkd2hpdGUgIWRlZmF1bHQ7XG4kdG9vbHRpcC1iZzogICAgICAgICAgICAgICAgICAgICAgICAkYmxhY2sgIWRlZmF1bHQ7XG4kdG9vbHRpcC1ib3JkZXItcmFkaXVzOiAgICAgICAgICAgICAkYm9yZGVyLXJhZGl1cyAhZGVmYXVsdDtcbiR0b29sdGlwLW9wYWNpdHk6ICAgICAgICAgICAgICAgICAgIC45ICFkZWZhdWx0O1xuJHRvb2x0aXAtcGFkZGluZy15OiAgICAgICAgICAgICAgICAgJHNwYWNlciAqIC4yNSAhZGVmYXVsdDtcbiR0b29sdGlwLXBhZGRpbmcteDogICAgICAgICAgICAgICAgICRzcGFjZXIgKiAuNSAhZGVmYXVsdDtcbiR0b29sdGlwLW1hcmdpbjogICAgICAgICAgICAgICAgICAgIDAgIWRlZmF1bHQ7XG5cbiR0b29sdGlwLWFycm93LXdpZHRoOiAgICAgICAgICAgICAgIC44cmVtICFkZWZhdWx0O1xuJHRvb2x0aXAtYXJyb3ctaGVpZ2h0OiAgICAgICAgICAgICAgLjRyZW0gIWRlZmF1bHQ7XG4kdG9vbHRpcC1hcnJvdy1jb2xvcjogICAgICAgICAgICAgICAkdG9vbHRpcC1iZyAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgdG9vbHRpcC12YXJpYWJsZXNcblxuLy8gRm9ybSB0b29sdGlwcyBtdXN0IGNvbWUgYWZ0ZXIgcmVndWxhciB0b29sdGlwc1xuLy8gc2Nzcy1kb2NzLXN0YXJ0IHRvb2x0aXAtZmVlZGJhY2stdmFyaWFibGVzXG4kZm9ybS1mZWVkYmFjay10b29sdGlwLXBhZGRpbmcteTogICAgICR0b29sdGlwLXBhZGRpbmcteSAhZGVmYXVsdDtcbiRmb3JtLWZlZWRiYWNrLXRvb2x0aXAtcGFkZGluZy14OiAgICAgJHRvb2x0aXAtcGFkZGluZy14ICFkZWZhdWx0O1xuJGZvcm0tZmVlZGJhY2stdG9vbHRpcC1mb250LXNpemU6ICAgICAkdG9vbHRpcC1mb250LXNpemUgIWRlZmF1bHQ7XG4kZm9ybS1mZWVkYmFjay10b29sdGlwLWxpbmUtaGVpZ2h0OiAgIG51bGwgIWRlZmF1bHQ7XG4kZm9ybS1mZWVkYmFjay10b29sdGlwLW9wYWNpdHk6ICAgICAgICR0b29sdGlwLW9wYWNpdHkgIWRlZmF1bHQ7XG4kZm9ybS1mZWVkYmFjay10b29sdGlwLWJvcmRlci1yYWRpdXM6ICR0b29sdGlwLWJvcmRlci1yYWRpdXMgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHRvb2x0aXAtZmVlZGJhY2stdmFyaWFibGVzXG5cblxuLy8gUG9wb3ZlcnNcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHBvcG92ZXItdmFyaWFibGVzXG4kcG9wb3Zlci1mb250LXNpemU6ICAgICAgICAgICAgICAgICAkZm9udC1zaXplLXNtICFkZWZhdWx0O1xuJHBvcG92ZXItYmc6ICAgICAgICAgICAgICAgICAgICAgICAgJHdoaXRlICFkZWZhdWx0O1xuJHBvcG92ZXItbWF4LXdpZHRoOiAgICAgICAgICAgICAgICAgMjc2cHggIWRlZmF1bHQ7XG4kcG9wb3Zlci1ib3JkZXItd2lkdGg6ICAgICAgICAgICAgICAkYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJHBvcG92ZXItYm9yZGVyLWNvbG9yOiAgICAgICAgICAgICAgcmdiYSgkYmxhY2ssIC4yKSAhZGVmYXVsdDtcbiRwb3BvdmVyLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAgICRib3JkZXItcmFkaXVzLWxnICFkZWZhdWx0O1xuJHBvcG92ZXItaW5uZXItYm9yZGVyLXJhZGl1czogICAgICAgc3VidHJhY3QoJHBvcG92ZXItYm9yZGVyLXJhZGl1cywgJHBvcG92ZXItYm9yZGVyLXdpZHRoKSAhZGVmYXVsdDtcbiRwb3BvdmVyLWJveC1zaGFkb3c6ICAgICAgICAgICAgICAgICRib3gtc2hhZG93ICFkZWZhdWx0O1xuXG4kcG9wb3Zlci1oZWFkZXItYmc6ICAgICAgICAgICAgICAgICBzaGFkZS1jb2xvcigkcG9wb3Zlci1iZywgNiUpICFkZWZhdWx0O1xuJHBvcG92ZXItaGVhZGVyLWNvbG9yOiAgICAgICAgICAgICAgJGhlYWRpbmdzLWNvbG9yICFkZWZhdWx0O1xuJHBvcG92ZXItaGVhZGVyLXBhZGRpbmcteTogICAgICAgICAgLjVyZW0gIWRlZmF1bHQ7XG4kcG9wb3Zlci1oZWFkZXItcGFkZGluZy14OiAgICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuXG4kcG9wb3Zlci1ib2R5LWNvbG9yOiAgICAgICAgICAgICAgICAkYm9keS1jb2xvciAhZGVmYXVsdDtcbiRwb3BvdmVyLWJvZHktcGFkZGluZy15OiAgICAgICAgICAgICRzcGFjZXIgIWRlZmF1bHQ7XG4kcG9wb3Zlci1ib2R5LXBhZGRpbmcteDogICAgICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuXG4kcG9wb3Zlci1hcnJvdy13aWR0aDogICAgICAgICAgICAgICAxcmVtICFkZWZhdWx0O1xuJHBvcG92ZXItYXJyb3ctaGVpZ2h0OiAgICAgICAgICAgICAgLjVyZW0gIWRlZmF1bHQ7XG4kcG9wb3Zlci1hcnJvdy1jb2xvcjogICAgICAgICAgICAgICAkcG9wb3Zlci1iZyAhZGVmYXVsdDtcblxuJHBvcG92ZXItYXJyb3ctb3V0ZXItY29sb3I6ICAgICAgICAgZmFkZS1pbigkcG9wb3Zlci1ib3JkZXItY29sb3IsIC4wNSkgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIHBvcG92ZXItdmFyaWFibGVzXG5cblxuLy8gVG9hc3RzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCB0b2FzdC12YXJpYWJsZXNcbiR0b2FzdC1tYXgtd2lkdGg6ICAgICAgICAgICAgICAgICAgIDM1MHB4ICFkZWZhdWx0O1xuJHRvYXN0LXBhZGRpbmcteDogICAgICAgICAgICAgICAgICAgLjc1cmVtICFkZWZhdWx0O1xuJHRvYXN0LXBhZGRpbmcteTogICAgICAgICAgICAgICAgICAgLjVyZW0gIWRlZmF1bHQ7XG4kdG9hc3QtZm9udC1zaXplOiAgICAgICAgICAgICAgICAgICAuODc1cmVtICFkZWZhdWx0O1xuJHRvYXN0LWNvbG9yOiAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiR0b2FzdC1iYWNrZ3JvdW5kLWNvbG9yOiAgICAgICAgICAgIHJnYmEoJHdoaXRlLCAuODUpICFkZWZhdWx0O1xuJHRvYXN0LWJvcmRlci13aWR0aDogICAgICAgICAgICAgICAgMXB4ICFkZWZhdWx0O1xuJHRvYXN0LWJvcmRlci1jb2xvcjogICAgICAgICAgICAgICAgcmdiYSgkYmxhY2ssIC4xKSAhZGVmYXVsdDtcbiR0b2FzdC1ib3JkZXItcmFkaXVzOiAgICAgICAgICAgICAgICRib3JkZXItcmFkaXVzICFkZWZhdWx0O1xuJHRvYXN0LWJveC1zaGFkb3c6ICAgICAgICAgICAgICAgICAgJGJveC1zaGFkb3cgIWRlZmF1bHQ7XG4kdG9hc3Qtc3BhY2luZzogICAgICAgICAgICAgICAgICAgICAkY29udGFpbmVyLXBhZGRpbmcteCAhZGVmYXVsdDtcblxuJHRvYXN0LWhlYWRlci1jb2xvcjogICAgICAgICAgICAgICAgJGdyYXktNjAwICFkZWZhdWx0O1xuJHRvYXN0LWhlYWRlci1iYWNrZ3JvdW5kLWNvbG9yOiAgICAgcmdiYSgkd2hpdGUsIC44NSkgIWRlZmF1bHQ7XG4kdG9hc3QtaGVhZGVyLWJvcmRlci1jb2xvcjogICAgICAgICByZ2JhKCRibGFjaywgLjA1KSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgdG9hc3QtdmFyaWFibGVzXG5cblxuLy8gQmFkZ2VzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBiYWRnZS12YXJpYWJsZXNcbiRiYWRnZS1mb250LXNpemU6ICAgICAgICAgICAgICAgICAgIC43NWVtICFkZWZhdWx0O1xuJGJhZGdlLWZvbnQtd2VpZ2h0OiAgICAgICAgICAgICAgICAgJGZvbnQtd2VpZ2h0LWJvbGQgIWRlZmF1bHQ7XG4kYmFkZ2UtY29sb3I6ICAgICAgICAgICAgICAgICAgICAgICAkd2hpdGUgIWRlZmF1bHQ7XG4kYmFkZ2UtcGFkZGluZy15OiAgICAgICAgICAgICAgICAgICAuMzVlbSAhZGVmYXVsdDtcbiRiYWRnZS1wYWRkaW5nLXg6ICAgICAgICAgICAgICAgICAgIC42NWVtICFkZWZhdWx0O1xuJGJhZGdlLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAgICAgJGJvcmRlci1yYWRpdXMgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGJhZGdlLXZhcmlhYmxlc1xuXG5cbi8vIE1vZGFsc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgbW9kYWwtdmFyaWFibGVzXG4kbW9kYWwtaW5uZXItcGFkZGluZzogICAgICAgICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuXG4kbW9kYWwtZm9vdGVyLW1hcmdpbi1iZXR3ZWVuOiAgICAgICAuNXJlbSAhZGVmYXVsdDtcblxuJG1vZGFsLWRpYWxvZy1tYXJnaW46ICAgICAgICAgICAgICAgLjVyZW0gIWRlZmF1bHQ7XG4kbW9kYWwtZGlhbG9nLW1hcmdpbi15LXNtLXVwOiAgICAgICAxLjc1cmVtICFkZWZhdWx0O1xuXG4kbW9kYWwtdGl0bGUtbGluZS1oZWlnaHQ6ICAgICAgICAgICAkbGluZS1oZWlnaHQtYmFzZSAhZGVmYXVsdDtcblxuJG1vZGFsLWNvbnRlbnQtY29sb3I6ICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiRtb2RhbC1jb250ZW50LWJnOiAgICAgICAgICAgICAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRtb2RhbC1jb250ZW50LWJvcmRlci1jb2xvcjogICAgICAgIHJnYmEoJGJsYWNrLCAuMikgIWRlZmF1bHQ7XG4kbW9kYWwtY29udGVudC1ib3JkZXItd2lkdGg6ICAgICAgICAkYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJG1vZGFsLWNvbnRlbnQtYm9yZGVyLXJhZGl1czogICAgICAgJGJvcmRlci1yYWRpdXMtbGcgIWRlZmF1bHQ7XG4kbW9kYWwtY29udGVudC1pbm5lci1ib3JkZXItcmFkaXVzOiBzdWJ0cmFjdCgkbW9kYWwtY29udGVudC1ib3JkZXItcmFkaXVzLCAkbW9kYWwtY29udGVudC1ib3JkZXItd2lkdGgpICFkZWZhdWx0O1xuJG1vZGFsLWNvbnRlbnQtYm94LXNoYWRvdy14czogICAgICAgJGJveC1zaGFkb3ctc20gIWRlZmF1bHQ7XG4kbW9kYWwtY29udGVudC1ib3gtc2hhZG93LXNtLXVwOiAgICAkYm94LXNoYWRvdyAhZGVmYXVsdDtcblxuJG1vZGFsLWJhY2tkcm9wLWJnOiAgICAgICAgICAgICAgICAgJGJsYWNrICFkZWZhdWx0O1xuJG1vZGFsLWJhY2tkcm9wLW9wYWNpdHk6ICAgICAgICAgICAgLjUgIWRlZmF1bHQ7XG4kbW9kYWwtaGVhZGVyLWJvcmRlci1jb2xvcjogICAgICAgICAkYm9yZGVyLWNvbG9yICFkZWZhdWx0O1xuJG1vZGFsLWZvb3Rlci1ib3JkZXItY29sb3I6ICAgICAgICAgJG1vZGFsLWhlYWRlci1ib3JkZXItY29sb3IgIWRlZmF1bHQ7XG4kbW9kYWwtaGVhZGVyLWJvcmRlci13aWR0aDogICAgICAgICAkbW9kYWwtY29udGVudC1ib3JkZXItd2lkdGggIWRlZmF1bHQ7XG4kbW9kYWwtZm9vdGVyLWJvcmRlci13aWR0aDogICAgICAgICAkbW9kYWwtaGVhZGVyLWJvcmRlci13aWR0aCAhZGVmYXVsdDtcbiRtb2RhbC1oZWFkZXItcGFkZGluZy15OiAgICAgICAgICAgICRtb2RhbC1pbm5lci1wYWRkaW5nICFkZWZhdWx0O1xuJG1vZGFsLWhlYWRlci1wYWRkaW5nLXg6ICAgICAgICAgICAgJG1vZGFsLWlubmVyLXBhZGRpbmcgIWRlZmF1bHQ7XG4kbW9kYWwtaGVhZGVyLXBhZGRpbmc6ICAgICAgICAgICAgICAkbW9kYWwtaGVhZGVyLXBhZGRpbmcteSAkbW9kYWwtaGVhZGVyLXBhZGRpbmcteCAhZGVmYXVsdDsgLy8gS2VlcCB0aGlzIGZvciBiYWNrd2FyZHMgY29tcGF0aWJpbGl0eVxuXG4kbW9kYWwtc206ICAgICAgICAgICAgICAgICAgICAgICAgICAzMDBweCAhZGVmYXVsdDtcbiRtb2RhbC1tZDogICAgICAgICAgICAgICAgICAgICAgICAgIDUwMHB4ICFkZWZhdWx0O1xuJG1vZGFsLWxnOiAgICAgICAgICAgICAgICAgICAgICAgICAgODAwcHggIWRlZmF1bHQ7XG4kbW9kYWwteGw6ICAgICAgICAgICAgICAgICAgICAgICAgICAxMTQwcHggIWRlZmF1bHQ7XG5cbiRtb2RhbC1mYWRlLXRyYW5zZm9ybTogICAgICAgICAgICAgIHRyYW5zbGF0ZSgwLCAtNTBweCkgIWRlZmF1bHQ7XG4kbW9kYWwtc2hvdy10cmFuc2Zvcm06ICAgICAgICAgICAgICBub25lICFkZWZhdWx0O1xuJG1vZGFsLXRyYW5zaXRpb246ICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtIC4zcyBlYXNlLW91dCAhZGVmYXVsdDtcbiRtb2RhbC1zY2FsZS10cmFuc2Zvcm06ICAgICAgICAgICAgIHNjYWxlKDEuMDIpICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBtb2RhbC12YXJpYWJsZXNcblxuXG4vLyBBbGVydHNcbi8vXG4vLyBEZWZpbmUgYWxlcnQgY29sb3JzLCBib3JkZXIgcmFkaXVzLCBhbmQgcGFkZGluZy5cblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGFsZXJ0LXZhcmlhYmxlc1xuJGFsZXJ0LXBhZGRpbmcteTogICAgICAgICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuJGFsZXJ0LXBhZGRpbmcteDogICAgICAgICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuJGFsZXJ0LW1hcmdpbi1ib3R0b206ICAgICAgICAgICAxcmVtICFkZWZhdWx0O1xuJGFsZXJ0LWJvcmRlci1yYWRpdXM6ICAgICAgICAgICAkYm9yZGVyLXJhZGl1cyAhZGVmYXVsdDtcbiRhbGVydC1saW5rLWZvbnQtd2VpZ2h0OiAgICAgICAgJGZvbnQtd2VpZ2h0LWJvbGQgIWRlZmF1bHQ7XG4kYWxlcnQtYm9yZGVyLXdpZHRoOiAgICAgICAgICAgICRib3JkZXItd2lkdGggIWRlZmF1bHQ7XG4kYWxlcnQtYmctc2NhbGU6ICAgICAgICAgICAgICAgIC04MCUgIWRlZmF1bHQ7XG4kYWxlcnQtYm9yZGVyLXNjYWxlOiAgICAgICAgICAgIC03MCUgIWRlZmF1bHQ7XG4kYWxlcnQtY29sb3Itc2NhbGU6ICAgICAgICAgICAgIDQwJSAhZGVmYXVsdDtcbiRhbGVydC1kaXNtaXNzaWJsZS1wYWRkaW5nLXI6ICAgJGFsZXJ0LXBhZGRpbmcteCAqIDMgIWRlZmF1bHQ7IC8vIDN4IGNvdmVycyB3aWR0aCBvZiB4IHBsdXMgZGVmYXVsdCBwYWRkaW5nIG9uIGVpdGhlciBzaWRlXG4vLyBzY3NzLWRvY3MtZW5kIGFsZXJ0LXZhcmlhYmxlc1xuXG5cbi8vIFByb2dyZXNzIGJhcnNcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IHByb2dyZXNzLXZhcmlhYmxlc1xuJHByb2dyZXNzLWhlaWdodDogICAgICAgICAgICAgICAgICAgMXJlbSAhZGVmYXVsdDtcbiRwcm9ncmVzcy1mb250LXNpemU6ICAgICAgICAgICAgICAgICRmb250LXNpemUtYmFzZSAqIC43NSAhZGVmYXVsdDtcbiRwcm9ncmVzcy1iZzogICAgICAgICAgICAgICAgICAgICAgICRncmF5LTIwMCAhZGVmYXVsdDtcbiRwcm9ncmVzcy1ib3JkZXItcmFkaXVzOiAgICAgICAgICAgICRib3JkZXItcmFkaXVzICFkZWZhdWx0O1xuJHByb2dyZXNzLWJveC1zaGFkb3c6ICAgICAgICAgICAgICAgJGJveC1zaGFkb3ctaW5zZXQgIWRlZmF1bHQ7XG4kcHJvZ3Jlc3MtYmFyLWNvbG9yOiAgICAgICAgICAgICAgICAkd2hpdGUgIWRlZmF1bHQ7XG4kcHJvZ3Jlc3MtYmFyLWJnOiAgICAgICAgICAgICAgICAgICAkcHJpbWFyeSAhZGVmYXVsdDtcbiRwcm9ncmVzcy1iYXItYW5pbWF0aW9uLXRpbWluZzogICAgIDFzIGxpbmVhciBpbmZpbml0ZSAhZGVmYXVsdDtcbiRwcm9ncmVzcy1iYXItdHJhbnNpdGlvbjogICAgICAgICAgIHdpZHRoIC42cyBlYXNlICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBwcm9ncmVzcy12YXJpYWJsZXNcblxuXG4vLyBMaXN0IGdyb3VwXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBsaXN0LWdyb3VwLXZhcmlhYmxlc1xuJGxpc3QtZ3JvdXAtY29sb3I6ICAgICAgICAgICAgICAgICAgJGdyYXktOTAwICFkZWZhdWx0O1xuJGxpc3QtZ3JvdXAtYmc6ICAgICAgICAgICAgICAgICAgICAgJHdoaXRlICFkZWZhdWx0O1xuJGxpc3QtZ3JvdXAtYm9yZGVyLWNvbG9yOiAgICAgICAgICAgcmdiYSgkYmxhY2ssIC4xMjUpICFkZWZhdWx0O1xuJGxpc3QtZ3JvdXAtYm9yZGVyLXdpZHRoOiAgICAgICAgICAgJGJvcmRlci13aWR0aCAhZGVmYXVsdDtcbiRsaXN0LWdyb3VwLWJvcmRlci1yYWRpdXM6ICAgICAgICAgICRib3JkZXItcmFkaXVzICFkZWZhdWx0O1xuXG4kbGlzdC1ncm91cC1pdGVtLXBhZGRpbmcteTogICAgICAgICAkc3BhY2VyICogLjUgIWRlZmF1bHQ7XG4kbGlzdC1ncm91cC1pdGVtLXBhZGRpbmcteDogICAgICAgICAkc3BhY2VyICFkZWZhdWx0O1xuJGxpc3QtZ3JvdXAtaXRlbS1iZy1zY2FsZTogICAgICAgICAgLTgwJSAhZGVmYXVsdDtcbiRsaXN0LWdyb3VwLWl0ZW0tY29sb3Itc2NhbGU6ICAgICAgIDQwJSAhZGVmYXVsdDtcblxuJGxpc3QtZ3JvdXAtaG92ZXItYmc6ICAgICAgICAgICAgICAgJGdyYXktMTAwICFkZWZhdWx0O1xuJGxpc3QtZ3JvdXAtYWN0aXZlLWNvbG9yOiAgICAgICAgICAgJGNvbXBvbmVudC1hY3RpdmUtY29sb3IgIWRlZmF1bHQ7XG4kbGlzdC1ncm91cC1hY3RpdmUtYmc6ICAgICAgICAgICAgICAkY29tcG9uZW50LWFjdGl2ZS1iZyAhZGVmYXVsdDtcbiRsaXN0LWdyb3VwLWFjdGl2ZS1ib3JkZXItY29sb3I6ICAgICRsaXN0LWdyb3VwLWFjdGl2ZS1iZyAhZGVmYXVsdDtcblxuJGxpc3QtZ3JvdXAtZGlzYWJsZWQtY29sb3I6ICAgICAgICAgJGdyYXktNjAwICFkZWZhdWx0O1xuJGxpc3QtZ3JvdXAtZGlzYWJsZWQtYmc6ICAgICAgICAgICAgJGxpc3QtZ3JvdXAtYmcgIWRlZmF1bHQ7XG5cbiRsaXN0LWdyb3VwLWFjdGlvbi1jb2xvcjogICAgICAgICAgICRncmF5LTcwMCAhZGVmYXVsdDtcbiRsaXN0LWdyb3VwLWFjdGlvbi1ob3Zlci1jb2xvcjogICAgICRsaXN0LWdyb3VwLWFjdGlvbi1jb2xvciAhZGVmYXVsdDtcblxuJGxpc3QtZ3JvdXAtYWN0aW9uLWFjdGl2ZS1jb2xvcjogICAgJGJvZHktY29sb3IgIWRlZmF1bHQ7XG4kbGlzdC1ncm91cC1hY3Rpb24tYWN0aXZlLWJnOiAgICAgICAkZ3JheS0yMDAgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGxpc3QtZ3JvdXAtdmFyaWFibGVzXG5cblxuLy8gSW1hZ2UgdGh1bWJuYWlsc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgdGh1bWJuYWlsLXZhcmlhYmxlc1xuJHRodW1ibmFpbC1wYWRkaW5nOiAgICAgICAgICAgICAgICAgLjI1cmVtICFkZWZhdWx0O1xuJHRodW1ibmFpbC1iZzogICAgICAgICAgICAgICAgICAgICAgJGJvZHktYmcgIWRlZmF1bHQ7XG4kdGh1bWJuYWlsLWJvcmRlci13aWR0aDogICAgICAgICAgICAkYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJHRodW1ibmFpbC1ib3JkZXItY29sb3I6ICAgICAgICAgICAgJGdyYXktMzAwICFkZWZhdWx0O1xuJHRodW1ibmFpbC1ib3JkZXItcmFkaXVzOiAgICAgICAgICAgJGJvcmRlci1yYWRpdXMgIWRlZmF1bHQ7XG4kdGh1bWJuYWlsLWJveC1zaGFkb3c6ICAgICAgICAgICAgICAkYm94LXNoYWRvdy1zbSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgdGh1bWJuYWlsLXZhcmlhYmxlc1xuXG5cbi8vIEZpZ3VyZXNcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGZpZ3VyZS12YXJpYWJsZXNcbiRmaWd1cmUtY2FwdGlvbi1mb250LXNpemU6ICAgICAgICAgICRzbWFsbC1mb250LXNpemUgIWRlZmF1bHQ7XG4kZmlndXJlLWNhcHRpb24tY29sb3I6ICAgICAgICAgICAgICAkZ3JheS02MDAgIWRlZmF1bHQ7XG4vLyBzY3NzLWRvY3MtZW5kIGZpZ3VyZS12YXJpYWJsZXNcblxuXG4vLyBCcmVhZGNydW1ic1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgYnJlYWRjcnVtYi12YXJpYWJsZXNcbiRicmVhZGNydW1iLWZvbnQtc2l6ZTogICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG4kYnJlYWRjcnVtYi1wYWRkaW5nLXk6ICAgICAgICAgICAgICAwICFkZWZhdWx0O1xuJGJyZWFkY3J1bWItcGFkZGluZy14OiAgICAgICAgICAgICAgMCAhZGVmYXVsdDtcbiRicmVhZGNydW1iLWl0ZW0tcGFkZGluZy14OiAgICAgICAgIC41cmVtICFkZWZhdWx0O1xuJGJyZWFkY3J1bWItbWFyZ2luLWJvdHRvbTogICAgICAgICAgMXJlbSAhZGVmYXVsdDtcbiRicmVhZGNydW1iLWJnOiAgICAgICAgICAgICAgICAgICAgIG51bGwgIWRlZmF1bHQ7XG4kYnJlYWRjcnVtYi1kaXZpZGVyLWNvbG9yOiAgICAgICAgICAkZ3JheS02MDAgIWRlZmF1bHQ7XG4kYnJlYWRjcnVtYi1hY3RpdmUtY29sb3I6ICAgICAgICAgICAkZ3JheS02MDAgIWRlZmF1bHQ7XG4kYnJlYWRjcnVtYi1kaXZpZGVyOiAgICAgICAgICAgICAgICBxdW90ZShcIi9cIikgIWRlZmF1bHQ7XG4kYnJlYWRjcnVtYi1kaXZpZGVyLWZsaXBwZWQ6ICAgICAgICAkYnJlYWRjcnVtYi1kaXZpZGVyICFkZWZhdWx0O1xuJGJyZWFkY3J1bWItYm9yZGVyLXJhZGl1czogICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgYnJlYWRjcnVtYi12YXJpYWJsZXNcblxuLy8gQ2Fyb3VzZWxcblxuLy8gc2Nzcy1kb2NzLXN0YXJ0IGNhcm91c2VsLXZhcmlhYmxlc1xuJGNhcm91c2VsLWNvbnRyb2wtY29sb3I6ICAgICAgICAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRjYXJvdXNlbC1jb250cm9sLXdpZHRoOiAgICAgICAgICAgICAxNSUgIWRlZmF1bHQ7XG4kY2Fyb3VzZWwtY29udHJvbC1vcGFjaXR5OiAgICAgICAgICAgLjUgIWRlZmF1bHQ7XG4kY2Fyb3VzZWwtY29udHJvbC1ob3Zlci1vcGFjaXR5OiAgICAgLjkgIWRlZmF1bHQ7XG4kY2Fyb3VzZWwtY29udHJvbC10cmFuc2l0aW9uOiAgICAgICAgb3BhY2l0eSAuMTVzIGVhc2UgIWRlZmF1bHQ7XG5cbiRjYXJvdXNlbC1pbmRpY2F0b3Itd2lkdGg6ICAgICAgICAgICAzMHB4ICFkZWZhdWx0O1xuJGNhcm91c2VsLWluZGljYXRvci1oZWlnaHQ6ICAgICAgICAgIDNweCAhZGVmYXVsdDtcbiRjYXJvdXNlbC1pbmRpY2F0b3ItaGl0LWFyZWEtaGVpZ2h0OiAxMHB4ICFkZWZhdWx0O1xuJGNhcm91c2VsLWluZGljYXRvci1zcGFjZXI6ICAgICAgICAgIDNweCAhZGVmYXVsdDtcbiRjYXJvdXNlbC1pbmRpY2F0b3Itb3BhY2l0eTogICAgICAgICAuNSAhZGVmYXVsdDtcbiRjYXJvdXNlbC1pbmRpY2F0b3ItYWN0aXZlLWJnOiAgICAgICAkd2hpdGUgIWRlZmF1bHQ7XG4kY2Fyb3VzZWwtaW5kaWNhdG9yLWFjdGl2ZS1vcGFjaXR5OiAgMSAhZGVmYXVsdDtcbiRjYXJvdXNlbC1pbmRpY2F0b3ItdHJhbnNpdGlvbjogICAgICBvcGFjaXR5IC42cyBlYXNlICFkZWZhdWx0O1xuXG4kY2Fyb3VzZWwtY2FwdGlvbi13aWR0aDogICAgICAgICAgICAgNzAlICFkZWZhdWx0O1xuJGNhcm91c2VsLWNhcHRpb24tY29sb3I6ICAgICAgICAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRjYXJvdXNlbC1jYXB0aW9uLXBhZGRpbmcteTogICAgICAgICAxLjI1cmVtICFkZWZhdWx0O1xuJGNhcm91c2VsLWNhcHRpb24tc3BhY2VyOiAgICAgICAgICAgIDEuMjVyZW0gIWRlZmF1bHQ7XG5cbiRjYXJvdXNlbC1jb250cm9sLWljb24td2lkdGg6ICAgICAgICAycmVtICFkZWZhdWx0O1xuXG4kY2Fyb3VzZWwtY29udHJvbC1wcmV2LWljb24tYmc6ICAgICAgdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgMTYgMTYnIGZpbGw9JyN7JGNhcm91c2VsLWNvbnRyb2wtY29sb3J9Jz48cGF0aCBkPSdNMTEuMzU0IDEuNjQ2YS41LjUgMCAwIDEgMCAuNzA4TDUuNzA3IDhsNS42NDcgNS42NDZhLjUuNSAwIDAgMS0uNzA4LjcwOGwtNi02YS41LjUgMCAwIDEgMC0uNzA4bDYtNmEuNS41IDAgMCAxIC43MDggMHonLz48L3N2Zz5cIikgIWRlZmF1bHQ7XG4kY2Fyb3VzZWwtY29udHJvbC1uZXh0LWljb24tYmc6ICAgICAgdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgMTYgMTYnIGZpbGw9JyN7JGNhcm91c2VsLWNvbnRyb2wtY29sb3J9Jz48cGF0aCBkPSdNNC42NDYgMS42NDZhLjUuNSAwIDAgMSAuNzA4IDBsNiA2YS41LjUgMCAwIDEgMCAuNzA4bC02IDZhLjUuNSAwIDAgMS0uNzA4LS43MDhMMTAuMjkzIDggNC42NDYgMi4zNTRhLjUuNSAwIDAgMSAwLS43MDh6Jy8+PC9zdmc+XCIpICFkZWZhdWx0O1xuXG4kY2Fyb3VzZWwtdHJhbnNpdGlvbi1kdXJhdGlvbjogICAgICAgLjZzICFkZWZhdWx0O1xuJGNhcm91c2VsLXRyYW5zaXRpb246ICAgICAgICAgICAgICAgIHRyYW5zZm9ybSAkY2Fyb3VzZWwtdHJhbnNpdGlvbi1kdXJhdGlvbiBlYXNlLWluLW91dCAhZGVmYXVsdDsgLy8gRGVmaW5lIHRyYW5zZm9ybSB0cmFuc2l0aW9uIGZpcnN0IGlmIHVzaW5nIG11bHRpcGxlIHRyYW5zaXRpb25zIChlLmcuLCBgdHJhbnNmb3JtIDJzIGVhc2UsIG9wYWNpdHkgLjVzIGVhc2Utb3V0YClcblxuJGNhcm91c2VsLWRhcmstaW5kaWNhdG9yLWFjdGl2ZS1iZzogICRibGFjayAhZGVmYXVsdDtcbiRjYXJvdXNlbC1kYXJrLWNhcHRpb24tY29sb3I6ICAgICAgICAkYmxhY2sgIWRlZmF1bHQ7XG4kY2Fyb3VzZWwtZGFyay1jb250cm9sLWljb24tZmlsdGVyOiAgaW52ZXJ0KDEpIGdyYXlzY2FsZSgxMDApICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBjYXJvdXNlbC12YXJpYWJsZXNcblxuXG4vLyBTcGlubmVyc1xuXG4vLyBzY3NzLWRvY3Mtc3RhcnQgc3Bpbm5lci12YXJpYWJsZXNcbiRzcGlubmVyLXdpZHRoOiAgICAgICAgICAgMnJlbSAhZGVmYXVsdDtcbiRzcGlubmVyLWhlaWdodDogICAgICAgICAgJHNwaW5uZXItd2lkdGggIWRlZmF1bHQ7XG4kc3Bpbm5lci12ZXJ0aWNhbC1hbGlnbjogIC0uMTI1ZW0gIWRlZmF1bHQ7XG4kc3Bpbm5lci1ib3JkZXItd2lkdGg6ICAgIC4yNWVtICFkZWZhdWx0O1xuJHNwaW5uZXItYW5pbWF0aW9uLXNwZWVkOiAuNzVzICFkZWZhdWx0O1xuXG4kc3Bpbm5lci13aWR0aC1zbTogICAgICAgIDFyZW0gIWRlZmF1bHQ7XG4kc3Bpbm5lci1oZWlnaHQtc206ICAgICAgICRzcGlubmVyLXdpZHRoLXNtICFkZWZhdWx0O1xuJHNwaW5uZXItYm9yZGVyLXdpZHRoLXNtOiAuMmVtICFkZWZhdWx0O1xuLy8gc2Nzcy1kb2NzLWVuZCBzcGlubmVyLXZhcmlhYmxlc1xuXG5cbi8vIENsb3NlXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBjbG9zZS12YXJpYWJsZXNcbiRidG4tY2xvc2Utd2lkdGg6ICAgICAgICAgICAgMWVtICFkZWZhdWx0O1xuJGJ0bi1jbG9zZS1oZWlnaHQ6ICAgICAgICAgICAkYnRuLWNsb3NlLXdpZHRoICFkZWZhdWx0O1xuJGJ0bi1jbG9zZS1wYWRkaW5nLXg6ICAgICAgICAuMjVlbSAhZGVmYXVsdDtcbiRidG4tY2xvc2UtcGFkZGluZy15OiAgICAgICAgJGJ0bi1jbG9zZS1wYWRkaW5nLXggIWRlZmF1bHQ7XG4kYnRuLWNsb3NlLWNvbG9yOiAgICAgICAgICAgICRibGFjayAhZGVmYXVsdDtcbiRidG4tY2xvc2UtYmc6ICAgICAgICAgICAgICAgdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLDxzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB2aWV3Qm94PScwIDAgMTYgMTYnIGZpbGw9JyN7JGJ0bi1jbG9zZS1jb2xvcn0nPjxwYXRoIGQ9J00uMjkzLjI5M2ExIDEgMCAwMTEuNDE0IDBMOCA2LjU4NiAxNC4yOTMuMjkzYTEgMSAwIDExMS40MTQgMS40MTRMOS40MTQgOGw2LjI5MyA2LjI5M2ExIDEgMCAwMS0xLjQxNCAxLjQxNEw4IDkuNDE0bC02LjI5MyA2LjI5M2ExIDEgMCAwMS0xLjQxNC0xLjQxNEw2LjU4NiA4IC4yOTMgMS43MDdhMSAxIDAgMDEwLTEuNDE0eicvPjwvc3ZnPlwiKSAhZGVmYXVsdDtcbiRidG4tY2xvc2UtZm9jdXMtc2hhZG93OiAgICAgJGlucHV0LWJ0bi1mb2N1cy1ib3gtc2hhZG93ICFkZWZhdWx0O1xuJGJ0bi1jbG9zZS1vcGFjaXR5OiAgICAgICAgICAuNSAhZGVmYXVsdDtcbiRidG4tY2xvc2UtaG92ZXItb3BhY2l0eTogICAgLjc1ICFkZWZhdWx0O1xuJGJ0bi1jbG9zZS1mb2N1cy1vcGFjaXR5OiAgICAxICFkZWZhdWx0O1xuJGJ0bi1jbG9zZS1kaXNhYmxlZC1vcGFjaXR5OiAuMjUgIWRlZmF1bHQ7XG4kYnRuLWNsb3NlLXdoaXRlLWZpbHRlcjogICAgIGludmVydCgxKSBncmF5c2NhbGUoMTAwJSkgYnJpZ2h0bmVzcygyMDAlKSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgY2xvc2UtdmFyaWFibGVzXG5cblxuLy8gT2ZmY2FudmFzXG5cbi8vIHNjc3MtZG9jcy1zdGFydCBvZmZjYW52YXMtdmFyaWFibGVzXG4kb2ZmY2FudmFzLXBhZGRpbmcteTogICAgICAgICAgICAgICAkbW9kYWwtaW5uZXItcGFkZGluZyAhZGVmYXVsdDtcbiRvZmZjYW52YXMtcGFkZGluZy14OiAgICAgICAgICAgICAgICRtb2RhbC1pbm5lci1wYWRkaW5nICFkZWZhdWx0O1xuJG9mZmNhbnZhcy1ob3Jpem9udGFsLXdpZHRoOiAgICAgICAgNDAwcHggIWRlZmF1bHQ7XG4kb2ZmY2FudmFzLXZlcnRpY2FsLWhlaWdodDogICAgICAgICAzMHZoICFkZWZhdWx0O1xuJG9mZmNhbnZhcy10cmFuc2l0aW9uLWR1cmF0aW9uOiAgICAgLjNzICFkZWZhdWx0O1xuJG9mZmNhbnZhcy1ib3JkZXItY29sb3I6ICAgICAgICAgICAgJG1vZGFsLWNvbnRlbnQtYm9yZGVyLWNvbG9yICFkZWZhdWx0O1xuJG9mZmNhbnZhcy1ib3JkZXItd2lkdGg6ICAgICAgICAgICAgJG1vZGFsLWNvbnRlbnQtYm9yZGVyLXdpZHRoICFkZWZhdWx0O1xuJG9mZmNhbnZhcy10aXRsZS1saW5lLWhlaWdodDogICAgICAgJG1vZGFsLXRpdGxlLWxpbmUtaGVpZ2h0ICFkZWZhdWx0O1xuJG9mZmNhbnZhcy1iZy1jb2xvcjogICAgICAgICAgICAgICAgJG1vZGFsLWNvbnRlbnQtYmcgIWRlZmF1bHQ7XG4kb2ZmY2FudmFzLWNvbG9yOiAgICAgICAgICAgICAgICAgICAkbW9kYWwtY29udGVudC1jb2xvciAhZGVmYXVsdDtcbiRvZmZjYW52YXMtYm94LXNoYWRvdzogICAgICAgICAgICAgICRtb2RhbC1jb250ZW50LWJveC1zaGFkb3cteHMgIWRlZmF1bHQ7XG4kb2ZmY2FudmFzLWJhY2tkcm9wLWJnOiAgICAgICAgICAgICAkbW9kYWwtYmFja2Ryb3AtYmcgIWRlZmF1bHQ7XG4kb2ZmY2FudmFzLWJhY2tkcm9wLW9wYWNpdHk6ICAgICAgICAkbW9kYWwtYmFja2Ryb3Atb3BhY2l0eSAhZGVmYXVsdDtcbi8vIHNjc3MtZG9jcy1lbmQgb2ZmY2FudmFzLXZhcmlhYmxlc1xuXG4vLyBDb2RlXG5cbiRjb2RlLWZvbnQtc2l6ZTogICAgICAgICAgICAgICAgICAgICRzbWFsbC1mb250LXNpemUgIWRlZmF1bHQ7XG4kY29kZS1jb2xvcjogICAgICAgICAgICAgICAgICAgICAgICAkcGluayAhZGVmYXVsdDtcblxuJGtiZC1wYWRkaW5nLXk6ICAgICAgICAgICAgICAgICAgICAgLjJyZW0gIWRlZmF1bHQ7XG4ka2JkLXBhZGRpbmcteDogICAgICAgICAgICAgICAgICAgICAuNHJlbSAhZGVmYXVsdDtcbiRrYmQtZm9udC1zaXplOiAgICAgICAgICAgICAgICAgICAgICRjb2RlLWZvbnQtc2l6ZSAhZGVmYXVsdDtcbiRrYmQtY29sb3I6ICAgICAgICAgICAgICAgICAgICAgICAgICR3aGl0ZSAhZGVmYXVsdDtcbiRrYmQtYmc6ICAgICAgICAgICAgICAgICAgICAgICAgICAgICRncmF5LTkwMCAhZGVmYXVsdDtcblxuJHByZS1jb2xvcjogICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCAhZGVmYXVsdDtcbiJdfQ== */"] });


/***/ }),

/***/ 1066:
/*!******************************************!*\
  !*** ./src/app/shell/shell.component.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ShellComponent": () => (/* binding */ ShellComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header/header.component */ 5213);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);



class ShellComponent {
    constructor() { }
    ngOnInit() { }
}
ShellComponent.ɵfac = function ShellComponent_Factory(t) { return new (t || ShellComponent)(); };
ShellComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ShellComponent, selectors: [["app-shell"]], decls: 4, vars: 0, template: function ShellComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "app-header");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "\n");
    } }, directives: [_header_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent, _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterOutlet], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzaGVsbC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ 5506:
/*!***************************************!*\
  !*** ./src/app/shell/shell.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ShellModule": () => (/* binding */ ShellModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ 2664);
/* harmony import */ var _app_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/auth */ 6282);
/* harmony import */ var _shell_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shell.component */ 1066);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./header/header.component */ 5213);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);







class ShellModule {
}
ShellModule.ɵfac = function ShellModule_Factory(t) { return new (t || ShellModule)(); };
ShellModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: ShellModule });
ShellModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__.NgbModule, _app_auth__WEBPACK_IMPORTED_MODULE_0__.AuthModule, _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](ShellModule, { declarations: [_header_header_component__WEBPACK_IMPORTED_MODULE_2__.HeaderComponent, _shell_component__WEBPACK_IMPORTED_MODULE_1__.ShellComponent], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__.NgbModule, _app_auth__WEBPACK_IMPORTED_MODULE_0__.AuthModule, _angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule] }); })();


/***/ }),

/***/ 6042:
/*!****************************************!*\
  !*** ./src/app/shell/shell.service.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Shell": () => (/* binding */ Shell)
/* harmony export */ });
/* harmony import */ var _app_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/auth */ 6282);
/* harmony import */ var _shell_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shell.component */ 1066);


/**
 * Provides helper methods to create routes.
 */
class Shell {
    /**
     * Creates routes using the shell component and authentication.
     * @param routes The routes to add.
     * @return The new route using shell as the base.
     */
    static childRoutes(routes) {
        return {
            path: '',
            component: _shell_component__WEBPACK_IMPORTED_MODULE_1__.ShellComponent,
            children: routes,
            canActivate: [_app_auth__WEBPACK_IMPORTED_MODULE_0__.AuthenticationGuard],
        };
    }
}


/***/ }),

/***/ 2340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// `.env.ts` is generated by the `npm run env` command
// `npm run env` exposes environment variables as JSON for any usage you might
// want, like displaying the version or getting extra config from your CI bot, etc.
// This is useful for granularity you might need beyond just the environment.
// Note that as usual, any environment variables you expose through it will end up in your
// bundle, and you should not use it for any sensitive information like passwords or keys.
//import { env } from './.env';
const environment = {
    production: false,
    //version: env.npm_package_version + '-dev',
    version: '1.0',
    serverUrl: 'http://13.233.92.1:1339/',
    defaultLanguage: 'en-US',
    supportedLanguages: ['en-US'],
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 4431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ 9075);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @app/app.module */ 6747);
/* harmony import */ var _env_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @env/environment */ 2340);
/*
 * Entry point of the application.
 * Only platform bootstrapping code should be here.
 * For app-specific initialization, use `app/app.component.ts`.
 */




if (_env_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.platformBrowser()
    .bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch((err) => console.error(err));


/***/ }),

/***/ 6700:
/*!***************************************************!*\
  !*** ./node_modules/moment/locale/ sync ^\.\/.*$ ***!
  \***************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./af": 6431,
	"./af.js": 6431,
	"./ar": 1286,
	"./ar-dz": 1616,
	"./ar-dz.js": 1616,
	"./ar-kw": 9759,
	"./ar-kw.js": 9759,
	"./ar-ly": 3160,
	"./ar-ly.js": 3160,
	"./ar-ma": 2551,
	"./ar-ma.js": 2551,
	"./ar-sa": 30,
	"./ar-sa.js": 30,
	"./ar-tn": 6962,
	"./ar-tn.js": 6962,
	"./ar.js": 1286,
	"./az": 5887,
	"./az.js": 5887,
	"./be": 4572,
	"./be.js": 4572,
	"./bg": 3276,
	"./bg.js": 3276,
	"./bm": 3344,
	"./bm.js": 3344,
	"./bn": 8985,
	"./bn-bd": 3990,
	"./bn-bd.js": 3990,
	"./bn.js": 8985,
	"./bo": 4391,
	"./bo.js": 4391,
	"./br": 6728,
	"./br.js": 6728,
	"./bs": 5536,
	"./bs.js": 5536,
	"./ca": 1043,
	"./ca.js": 1043,
	"./cs": 420,
	"./cs.js": 420,
	"./cv": 3513,
	"./cv.js": 3513,
	"./cy": 6771,
	"./cy.js": 6771,
	"./da": 7978,
	"./da.js": 7978,
	"./de": 6061,
	"./de-at": 5204,
	"./de-at.js": 5204,
	"./de-ch": 2653,
	"./de-ch.js": 2653,
	"./de.js": 6061,
	"./dv": 85,
	"./dv.js": 85,
	"./el": 8579,
	"./el.js": 8579,
	"./en-au": 5724,
	"./en-au.js": 5724,
	"./en-ca": 525,
	"./en-ca.js": 525,
	"./en-gb": 2847,
	"./en-gb.js": 2847,
	"./en-ie": 7216,
	"./en-ie.js": 7216,
	"./en-il": 9305,
	"./en-il.js": 9305,
	"./en-in": 3364,
	"./en-in.js": 3364,
	"./en-nz": 9130,
	"./en-nz.js": 9130,
	"./en-sg": 1161,
	"./en-sg.js": 1161,
	"./eo": 802,
	"./eo.js": 802,
	"./es": 328,
	"./es-do": 5551,
	"./es-do.js": 5551,
	"./es-mx": 5615,
	"./es-mx.js": 5615,
	"./es-us": 4790,
	"./es-us.js": 4790,
	"./es.js": 328,
	"./et": 6389,
	"./et.js": 6389,
	"./eu": 2961,
	"./eu.js": 2961,
	"./fa": 6151,
	"./fa.js": 6151,
	"./fi": 7997,
	"./fi.js": 7997,
	"./fil": 8898,
	"./fil.js": 8898,
	"./fo": 7779,
	"./fo.js": 7779,
	"./fr": 8174,
	"./fr-ca": 3287,
	"./fr-ca.js": 3287,
	"./fr-ch": 8867,
	"./fr-ch.js": 8867,
	"./fr.js": 8174,
	"./fy": 452,
	"./fy.js": 452,
	"./ga": 5014,
	"./ga.js": 5014,
	"./gd": 4127,
	"./gd.js": 4127,
	"./gl": 2124,
	"./gl.js": 2124,
	"./gom-deva": 6444,
	"./gom-deva.js": 6444,
	"./gom-latn": 7953,
	"./gom-latn.js": 7953,
	"./gu": 6604,
	"./gu.js": 6604,
	"./he": 1222,
	"./he.js": 1222,
	"./hi": 4235,
	"./hi.js": 4235,
	"./hr": 622,
	"./hr.js": 622,
	"./hu": 7735,
	"./hu.js": 7735,
	"./hy-am": 402,
	"./hy-am.js": 402,
	"./id": 9187,
	"./id.js": 9187,
	"./is": 536,
	"./is.js": 536,
	"./it": 5007,
	"./it-ch": 4667,
	"./it-ch.js": 4667,
	"./it.js": 5007,
	"./ja": 2093,
	"./ja.js": 2093,
	"./jv": 59,
	"./jv.js": 59,
	"./ka": 6870,
	"./ka.js": 6870,
	"./kk": 880,
	"./kk.js": 880,
	"./km": 1083,
	"./km.js": 1083,
	"./kn": 8785,
	"./kn.js": 8785,
	"./ko": 1721,
	"./ko.js": 1721,
	"./ku": 7851,
	"./ku.js": 7851,
	"./ky": 1727,
	"./ky.js": 1727,
	"./lb": 346,
	"./lb.js": 346,
	"./lo": 3002,
	"./lo.js": 3002,
	"./lt": 4035,
	"./lt.js": 4035,
	"./lv": 6927,
	"./lv.js": 6927,
	"./me": 5634,
	"./me.js": 5634,
	"./mi": 4173,
	"./mi.js": 4173,
	"./mk": 6320,
	"./mk.js": 6320,
	"./ml": 1705,
	"./ml.js": 1705,
	"./mn": 1062,
	"./mn.js": 1062,
	"./mr": 2805,
	"./mr.js": 2805,
	"./ms": 1341,
	"./ms-my": 9900,
	"./ms-my.js": 9900,
	"./ms.js": 1341,
	"./mt": 7734,
	"./mt.js": 7734,
	"./my": 9034,
	"./my.js": 9034,
	"./nb": 9324,
	"./nb.js": 9324,
	"./ne": 6495,
	"./ne.js": 6495,
	"./nl": 673,
	"./nl-be": 6272,
	"./nl-be.js": 6272,
	"./nl.js": 673,
	"./nn": 2486,
	"./nn.js": 2486,
	"./oc-lnc": 6219,
	"./oc-lnc.js": 6219,
	"./pa-in": 2829,
	"./pa-in.js": 2829,
	"./pl": 8444,
	"./pl.js": 8444,
	"./pt": 3170,
	"./pt-br": 6117,
	"./pt-br.js": 6117,
	"./pt.js": 3170,
	"./ro": 6587,
	"./ro.js": 6587,
	"./ru": 9264,
	"./ru.js": 9264,
	"./sd": 2135,
	"./sd.js": 2135,
	"./se": 5366,
	"./se.js": 5366,
	"./si": 3379,
	"./si.js": 3379,
	"./sk": 6143,
	"./sk.js": 6143,
	"./sl": 196,
	"./sl.js": 196,
	"./sq": 1082,
	"./sq.js": 1082,
	"./sr": 1621,
	"./sr-cyrl": 8963,
	"./sr-cyrl.js": 8963,
	"./sr.js": 1621,
	"./ss": 1404,
	"./ss.js": 1404,
	"./sv": 5685,
	"./sv.js": 5685,
	"./sw": 3872,
	"./sw.js": 3872,
	"./ta": 4106,
	"./ta.js": 4106,
	"./te": 9204,
	"./te.js": 9204,
	"./tet": 3692,
	"./tet.js": 3692,
	"./tg": 6361,
	"./tg.js": 6361,
	"./th": 1735,
	"./th.js": 1735,
	"./tk": 1568,
	"./tk.js": 1568,
	"./tl-ph": 6129,
	"./tl-ph.js": 6129,
	"./tlh": 3759,
	"./tlh.js": 3759,
	"./tr": 1644,
	"./tr.js": 1644,
	"./tzl": 875,
	"./tzl.js": 875,
	"./tzm": 6878,
	"./tzm-latn": 1041,
	"./tzm-latn.js": 1041,
	"./tzm.js": 6878,
	"./ug-cn": 4357,
	"./ug-cn.js": 4357,
	"./uk": 4810,
	"./uk.js": 4810,
	"./ur": 6794,
	"./ur.js": 6794,
	"./uz": 8966,
	"./uz-latn": 7959,
	"./uz-latn.js": 7959,
	"./uz.js": 8966,
	"./vi": 5386,
	"./vi.js": 5386,
	"./x-pseudo": 3156,
	"./x-pseudo.js": 3156,
	"./yo": 8028,
	"./yo.js": 8028,
	"./zh-cn": 9330,
	"./zh-cn.js": 9330,
	"./zh-hk": 9380,
	"./zh-hk.js": 9380,
	"./zh-mo": 874,
	"./zh-mo.js": 874,
	"./zh-tw": 6508,
	"./zh-tw.js": 6508
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 6700;

/***/ }),

/***/ 4601:
/*!************************!*\
  !*** canvas (ignored) ***!
  \************************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 2767:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 8251:
/*!**********************!*\
  !*** http (ignored) ***!
  \**********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 7677:
/*!***********************!*\
  !*** https (ignored) ***!
  \***********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 1543:
/*!*********************!*\
  !*** url (ignored) ***!
  \*********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 7324:
/*!**********************!*\
  !*** zlib (ignored) ***!
  \**********************/
/***/ (() => {

/* (ignored) */

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(4431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map