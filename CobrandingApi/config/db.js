module.exports = {
  HOST: process.env.HOST,
  USER: process.env.DATABASE_USERNAME,
  PASSWORD: process.env.DATABASE_PASSWORD,
  DB: process.env.DB_URL,
  dialect: 'postgres',
  dialectOptions : process.env.DB_SSL_ENABLED == 'true' ? {
    ssl : {
      require: true,
      rejectUnauthorized: false 
    }
  } : undefined  ,
  port: process.env.DB_PORT
};