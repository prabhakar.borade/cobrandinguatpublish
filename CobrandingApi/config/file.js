module.exports = {
    APP_HOST_URL: process.env.APP_HOST_URL,
    UPLOADS_FOLDER: process.env.UPLOADS_FOLDER,
    TEMP_FOLDER: process.env.TEMP_FOLDER,
    COLLATERAL_FOLDER: process.env.COLLATERAL_FOLDER,
    COBRAND_FOLDER: process.env.COBRAND_FOLDER,   
};