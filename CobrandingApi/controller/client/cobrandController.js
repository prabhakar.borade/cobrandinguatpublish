const utils = require('../../utils/messages');
const dbService = require('../../utils/dbService');
const { v4 } = require("uuid");
const Cobrands = require('../../model/cobrands');
const CobrandFilesMap = require('../../model/cobrandFilesMap');
const Files = require('../../model/files');
const { watermarkImage, waterMarkPDF, watermarkVideo } = require("../../services/files/watermarkService");
const path = require('path');
const fs =  require("fs");
const { Op } = require("sequelize");
const mime = require("mime-types");
const {UPLOADS_FOLDER, TEMP_FOLDER, COBRAND_FOLDER , COLLATERAL_FOLDER} = require("../../config/file")

  async function createCobrand(req, cobrandId) {
    let finalResponse;
    let coBrandFilesMapDBData = [];
    let filesData = [];
    let data = req.body;
    let collateraldetails = req.body[0].collateralDetails;
    let docType = collateraldetails.collateralType;
    try{
      let textImages = [];
      const CobrandLogoName = req.body[0].cobrandDetails.logoImage.split('/').pop();
        await writeTextImages();
        await watermark()
        await filesDbentry(filesData)
       .then(async() =>{
        await dbentry(coBrandFilesMapDBData);
        return finalResponse
       })
       .catch((error) =>{
         console.log(error);
         //return utils.failureResponse(error.message, res);
         return error
       });   
       
      

     
      
      // let textImagePath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${newFileName}`);
        
        function writeTextImages() {
        return new Promise((resolve, reject) =>{
          try {
            if(collateraldetails.collateralType == 'Document'){
              for (let j = 0; j < data[0].cobrandDetails.textImage.length; j++) {
                const element = data[0].cobrandDetails.textImage[j];
                let CobrandTextImagepath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${'CobrandText'+ v4() + '.png'}`);
                let base64Image = element.split(';base64,').pop();
                textImages.push({
                   base64 :base64Image,
                   path : CobrandTextImagepath
                   });
                fs.writeFileSync(CobrandTextImagepath, base64Image, {encoding: 'base64'})
                  if (j == (data.length - 1)){
                    resolve()
                   };
                }
            }
            else{
              for (let j = 0; j < data.length; j++) {
                const element = data[j];
                let CobrandTextImagepath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${'CobrandText'+ v4() + '.png'}`);
                let base64Image = element.cobrandDetails.textImage.split(';base64,').pop();
                textImages.push({
                   base64 :base64Image,
                   path : CobrandTextImagepath
                   });
                fs.writeFileSync(CobrandTextImagepath, base64Image, {encoding: 'base64'})
                  if (j == (data.length - 1)){
                    resolve()
                   };
                }
            }
               
          } catch (error) {
            reject(error);
            console.log(error);
            //return utils.failureResponse(error.message, res);
            return error
          }
        })
      }
          function watermark() {
          return new Promise ( async(resolve, reject) => {
          for(let i = 0; i < data.length; i++) {
          const element = data[i];
          if(docType == 'Image') {
            
          const fileid = v4();
          const mergedImagename = 'Cobrand'+ fileid + '.png';
          const thumnailImageName = 'thumbnail'+ mergedImagename;
          let outputImagePath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER + `/${mergedImagename}`);
            const thumbnailpath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + `/${thumnailImageName}`);
            await watermarkImage(element.cobrandDetails.mainImage,
            element.cobrandDetails.logoImage,
            textImages[i].path,
            outputImagePath,
            element.cobrandDetails.Ximg,
            element.cobrandDetails.Yimg,
            element.cobrandDetails.logoWidth,
            element.cobrandDetails.logoHeight,
            element.cobrandDetails.Xtext,
            element.cobrandDetails.Ytext,
            element.cobrandDetails.textWidth,
            element.cobrandDetails.textHeight,thumbnailpath).then((result) => {
            }).then(async(result)=> {
              let fileSize = await fs.statSync(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + "/" + mergedImagename).size
              let filepath = __dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + "/" + mergedImagename ;
              let fileMimeType = await mime.contentType(path.extname(filepath));
              filesData.push({
                fileId : fileid,
                size:fileSize,
                path: COBRAND_FOLDER + "/" + mergedImagename ,
                originalFileName: mergedImagename,
                imageName: mergedImagename,
                mimetype: fileMimeType,
                thumnailName: thumnailImageName,
                thumnailPath: COBRAND_FOLDER  + "/" + thumnailImageName,
                isActive: false,
                createdBy:req.user.id,
                updatedBy:req.user.id
              })
              }).catch(error => {
                console.log(error);
                reject(error);
              });
            coBrandFilesMapDBData.push({
              fileId: fileid,
              cobrandPath : COBRAND_FOLDER + '/' + mergedImagename,
              thumbnailPath : COBRAND_FOLDER + '/' + thumnailImageName
            })
            
          }
          else if (docType == 'Document') {
            const fileid = v4();
            const mergedPDFname = 'Cobrand'+  fileid  + '.pdf';
            const thumnailPDFName = 'thumbnail'+ mergedPDFname.split(".")[0] + ".png";
            const thumbnailPDFpath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + `/${thumnailPDFName}`);
            let watermarkDetails = element.cobrandDetails.watermarkDetails
              let ximg = [];
              let yimg = [];
              let xtext = [];
              let ytext = [];
              let logoWidth = watermarkDetails[0].logoDetails.width;
              let logoHeight = watermarkDetails[0].logoDetails.height;
              let textWidth = watermarkDetails[0].textAreaDetails.width;
              let textHeight = watermarkDetails[0].textAreaDetails.height;
              watermarkDetails.forEach(element => {
              let pageWidth = element.width;
              let pageHeight = element.height;
              const logoDetails = element.logoDetails;
              const textDetails = element.textAreaDetails;
              ximg.push(logoDetails.x);
              yimg.push(pageHeight- logoHeight - logoDetails.y);
              xtext.push(textDetails.x);
              ytext.push(pageHeight - textHeight - textDetails.y);
            });
          let inputPDFName = element.cobrandDetails.mainImage.split("/").pop();
          let logoImageName = element.cobrandDetails.logoImage.split("/").pop();
            
           let inputPdfPath =  path.join(__dirname + UPLOADS_FOLDER + COLLATERAL_FOLDER + `/${inputPDFName}`);
           let logoimagePath = path.join(__dirname + UPLOADS_FOLDER+ TEMP_FOLDER + `/${logoImageName}`); 
           let outputPDFPath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER + `/${mergedPDFname}`); 
           await waterMarkPDF(
             inputPdfPath,
              logoimagePath,
              textImages,
              outputPDFPath,
              ximg,
              yimg,
              xtext,
              ytext,
              logoWidth,
              logoHeight,
              textWidth,
              textHeight,
              thumbnailPDFpath
            ).then(async(result)=> {
              let fileSize = await fs.statSync(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + "/" + mergedPDFname).size
              let filepath = __dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + "/" + mergedPDFname ;
              let fileMimeType = await mime.contentType(path.extname(filepath));
              filesData.push({
                fileId : fileid,
                size: fileSize,
                path: COBRAND_FOLDER + "/" + mergedPDFname ,
                originalFileName: mergedPDFname,
                imageName: mergedPDFname,
                mimetype: fileMimeType,
                thumnailName: thumnailPDFName,
                thumnailPath: COBRAND_FOLDER + "/" + thumnailPDFName,
                isActive: false,
                createdBy:req.user.id,
                updatedBy:req.user.id
              })
            }).catch(error => {
                console.log(error);
                reject(error);
              });;
              coBrandFilesMapDBData.push({
              fileId: fileid, 
              cobrandPath : COBRAND_FOLDER + '/' + mergedPDFname,
              thumbnailPath : COBRAND_FOLDER + '/' + thumnailPDFName
            })
          }
          else if (docType == 'Video') {
            const fileid = v4();
            const mergedVideoname = 'Cobrand'+ fileid + '.mp4';
            const thumnailVideoName = 'thumbnail'+ mergedVideoname.split(".")[0] + ".png";
            let inputVideoName = element.cobrandDetails.mainImage.split("/").pop();
            let logoImageName = element.cobrandDetails.logoImage.split("/").pop();
            let inputVideoPath =  path.join(__dirname +UPLOADS_FOLDER + COLLATERAL_FOLDER + `/${inputVideoName}`);
            let logoImagePath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${logoImageName}`); 
            let outputVideoPath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER + `/${mergedVideoname}`);
            let mergedVideoThumbFolderPath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER);
            let logoElement = element.cobrandDetails.watermarkDetails[0].logo;
            let textElement = element.cobrandDetails.watermarkDetails[0].text;
            let Ximg = logoElement.logoXcordinate;
            let Yimg = logoElement.logoYcordinate;
            let LogoWidth = logoElement.logoWidthcordinate;
            let LogoHeight = logoElement.logoHeightcordinate;
            let Xtext = textElement.textAreaXcordinate;
            let Ytext = textElement.textAreaYcordinate;
            let duration = logoElement.videoDetails.duration;
            let videoLogoStartTime = logoElement.videoDetails.startTime;
            let videoLogoEndTime = logoElement.videoDetails.endTime;
            let videoTextStartTime = textElement.videoDetails.startTime;
            let videoTextEndTime = textElement.videoDetails.endTime;
            let TextWidth = textElement.textAreaWidthcordinate;
            let TextHeight = textElement.textAreaHeightcordinate;
             await watermarkVideo(inputVideoPath, logoImagePath, textImages[i].path, outputVideoPath, duration, Ximg, Yimg, Xtext, Ytext,mergedVideoThumbFolderPath, thumnailVideoName , LogoWidth, LogoHeight, videoLogoStartTime,  videoLogoEndTime, videoTextStartTime, videoTextEndTime, TextWidth, TextHeight).then(async() =>{
              let fileSize = await fs.statSync(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + "/" + mergedVideoname).size
              let filepath = __dirname + UPLOADS_FOLDER + COBRAND_FOLDER  + "/" + mergedVideoname ;
              let fileMimeType = await mime.contentType(path.extname(filepath)); 
              filesData.push({

                fileId : fileid,
                size:fileSize,
                //userId : "2",
                path: COBRAND_FOLDER + "/" + mergedVideoname ,
                originalFileName: mergedVideoname,
                imageName: mergedVideoname,
                mimetype: fileMimeType,
                thumnailName: thumnailVideoName,
                thumnailPath: COBRAND_FOLDER + "/" + thumnailVideoName,
                isActive: false,
                createdBy:req.user.id,
                updatedBy:req.user.id
              })
              }).catch(error => {
                  console.log(error);
                reject(error);
              });;
               coBrandFilesMapDBData.push({
              fileId : fileid,
            })
          };
          if (i == (data.length -1)) {
           resolve()
          }
          }     
        })
      };

      

        function filesDbentry(filesdata) {
          return new Promise ((resolve, reject) => {
            dbService.createMany(Files, filesdata).then((result) => {
              if(!result) {
                reject();
              }
              resolve();
          })
          })
        }

         async function dbentry(coBrandFilesMapDBData) {
           coBrandFilesMapDBData.forEach((element)=> Object.assign(element, {
             cobrandId : cobrandId,
             createdBy:req.user.id,
             updatedBy:req.user.id
           }))
           await dbService.createMany(CobrandFilesMap, coBrandFilesMapDBData)
            .then( async (result) => {
              let CobrandLogoPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${CobrandLogoName}`);
              fs.unlinkSync(CobrandLogoPath);
                    coBrandFilesMapDBData.splice(0, coBrandFilesMapDBData.length)
                    filesData.splice(0, filesData.length);
              textImages.forEach(element => {
                fs.unlinkSync(element.path);
              });
              await dbService.updateByPk(Cobrands, cobrandId, {
                isProcessed : true
              }).then(() => {
                finalResponse = getCoBrandsById(cobrandId);
              })
              
              //console.log("Cobrand Details",cobrand)


              //return utils.successResponse(cobrand, res);
              //return cobrand
            })
            .catch((error) => {
              console.log(error);
              //return utils.failureResponse(error.message, res);
              return error
            });
            
        }
  }
  catch (error){
      console.log(error);
      //return utils.failureResponse(error.message,res);
      return error
    };
  }    
  
  process.on('message',async(message) =>{
    const jsonResponse = await createCobrand(message.req, message.cobrandId.id)
    process.send({"Sucesss":true});
    process.exit();
    //process.send(jsonResponse); 
    //process.exit();
    // .then((result)=>{
    //   //console.log(result);
    //   process.send(result); 
    //   process.exit();
    // });
  })

  const getAllCoBrands = async (req,res) =>{
    try {
      let options = {};
      let query = {};
      let result;
      if (req.body.isCountOnly){
        if (req.body.query !== undefined) {
          query = { ...req.body.query };
        }
        result = await dbService.count(Cobrands, query);
        if (result) {
          result = { totalRecords: result };
          return utils.successResponse(result, res);
        } 
        return utils.recordNotFound([], res);
      }
      else {
        if (req.body.options !== undefined) {
          options = { ...req.body.options };
        }
        if (options && options.select && options.select.length){
          options.attributes = options.select;
        }
  
        if (options && options.sort && options.sort.length){
          options.order = options.sort;
        }

        options.include = [{ all: true ,nested: true }]
              
        if (req.body.query !== undefined){
          query = { ...req.body.query };
        }
        if (req.collateral){
          query = {
            ...query,
            ...{ 'id': { [Op.ne]: req.collateral.id } } 
          };
          if (req.body.query.id) {
            Object.assign(query.id, { [Op.in]: [req.body.query.id] });
          }
        }
        result = await dbService.findMany(Cobrands,query,options);
              
        if (!result){
          return utils.recordNotFound([],res);
        }
        return utils.successResponse(result, res);   
      }
    }
    catch (error){
      console.log(error);
      return utils.failureResponse(error.message,res);
    }
  };

  const downloadFileByPath = async (req, res) => {
    try {
        let fileName = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER + '/' + req.params.name);
        res.download(fileName);
    }
    catch (error){
      console.log(error);
      return utils.failureResponse(error.message,res);
    }
  };

  const getCoBrandsByFilter = async (req,res) =>{
      try {
        let options = {};
        let query = {};
        let result;
        if (req.body.isCountOnly){
          if (req.body.query !== undefined) {
            query = { ...req.body.query };
          }
          result = await dbService.count(Cobrands, query);
          if (result) {
            result = { totalRecords: result };
            return utils.successResponse(result, res);
          }
          return utils.recordNotFound([], res);
        }
        else {
          if (req.body.options !== undefined) {
            options = { ...req.body.options };
          }
          options.include = [{ all: true ,nested: true }]
        
            if (req.body.filter !== undefined){

              if(req.body.filter[0].collateralType.length == 0 && req.body.filter[1].categoryId.length != 0 && req.body.filter[2].subcategoryId.length != 0 ){
                query = {
                  [Op.and]: [
                        {[Op.or]: [
                            { categoryId: req.body.filter[1].categoryId }
                          ]},
                        {[Op.or]: [
                            { subcategoryId: req.body.filter[2].subcategoryId }
                          ]},
                        {[Op.or]:[
                          {createdBy:req.user.id}
                        ]}
                  ]
                };
              }
              else if(req.body.filter[1].categoryId.length == 0 && req.body.filter[0].collateralType.length != 0 && req.body.filter[2].subcategoryId.length != 0 ){
                query = {
                  [Op.and]: [
                        {[Op.or]: [
                            { collateralType:req.body.filter[0].collateralType }
                          ]},
                        {[Op.or]: [
                            { subcategoryId: req.body.filter[2].subcategoryId }
                          ]},
                          {[Op.or]:[
                            {createdBy:req.user.id}
                          ]}
                  ]
                };
              }
              else if(req.body.filter[2].subcategoryId.length == 0 && req.body.filter[0].collateralType.length != 0 && req.body.filter[1].categoryId.length != 0){
                query = {
                  [Op.and]: [
                        {[Op.or]: [
                            { collateralType:req.body.filter[0].collateralType }
                          ]},
                        {[Op.or]: [
                            { categoryId: req.body.filter[1].categoryId }
                          ]},
                          {[Op.or]:[
                            {createdBy:req.user.id}
                          ]}
                  ]
                };
              }

              else if(req.body.filter[0].collateralType.length == 0 && req.body.filter[1].categoryId.length == 0 && req.body.filter[2].subcategoryId.length != 0){
                query = {
                  [Op.and]: [
                    { subcategoryId: req.body.filter[2].subcategoryId },
                    {createdBy:req.user.id}
                  ]
                };
              }
              else if(req.body.filter[0].collateralType.length == 0 && req.body.filter[2].subcategoryId.length == 0 && req.body.filter[1].categoryId.length != 0){
                query = {
                  [Op.and]: [
                    { categoryId: req.body.filter[1].categoryId },
                    {createdBy:req.user.id}
                  ]
                };
              }
              else if(req.body.filter[1].categoryId.length == 0 && req.body.filter[2].subcategoryId.length == 0 && req.body.filter[0].collateralType.length != 0){
                query = {
                  [Op.and]: [
                            { collateralType:req.body.filter[0].collateralType },
                            {createdBy:req.user.id}
                          ]
                };
              }

              else if(req.body.filter[0].collateralType.length == 0 && req.body.filter[1].categoryId.length == 0 && req.body.filter[2].subcategoryId.length == 0 ){
                query = {
                  createdBy:req.user.id
                };
              }

              else{
                query = {
                  [Op.and]: [
                        {[Op.or]: [
                            { collateralType:req.body.filter[0].collateralType }
                          ]},
                        {[Op.or]: [
                            { categoryId: req.body.filter[1].categoryId }
                          ]},
                        {[Op.or]: [
                            { subcategoryId: req.body.filter[2].subcategoryId }
                          ]},
                          {[Op.or]:[
                            {createdBy:req.user.id}
                          ]}
                  ]
                };
              }
            }

            if(req.body.search !== undefined){
            
              query= {
                ...query,
                [Op.or]:[
                  {collateralName: { [Op.and] : { [Op.like]: req.body.search[0].collateralName + '%' }   },
                  createdBy:req.user.id
                }
                ] 
              }   
            }

          result = await dbService.findMany(Cobrands,query,options);
              
          if (!result){
            return utils.recordNotFound([],res);
          }
          return utils.successResponse(result, res);  
        }
      }
      catch (error){
        console.log(error)
        return utils.failureResponse(error.message,res);
      }
    };

   async function getCoBrandsById(cobrandId){
    try {
      let options = {};
      let query = { cobrandId : cobrandId};
      options.include = [{ all: true ,nested: true }];
      let result = await dbService.findMany(CobrandFilesMap,query,options);
      if (result){
        return  result           
      }
    }
    catch (error){
      console.log(error);
      return error
    }
  };

module.exports = {
    createCobrand,
    getAllCoBrands,
    downloadFileByPath,
    getCoBrandsByFilter
};
