const productService =  require('../../services/files/productService');
const utils = require('../../utils/messages');

  const getProduct = async (req,res)=>{
    try {
        let result = await productService.getproductCategory(); 
        if (result.flag){
            const data = [{id:'1',name:'Product 1'},{id:'2',name:'Product 2'},
                            {id:'3',name:'Product 3'},{id:'4',name:'Product 4'}]
            return utils.successResponse(data,res); 
        }
    } catch (error) {
      return utils.failureResponse(error.message, res);
    }
  };

  const getSubProduct = async (req,res)=>{
    try {
        let result = await productService.getproductSubCategory(req.body.ProductId); 
        if (result.flag){
            const data = [{id:'1',name:'Product Sub 1'},{id:'2',name:'Product Sub 2'},
                            {id:'3',name:'Product Sub 3'},{id:'4',name:'Product Sub 4'}]
            return utils.successResponse(data,res); 
        }
    } catch (error) {
      return utils.failureResponse(error.message, res);
    }
  };

  const getProductByID = async (req,res)=>{
    try {
        let ProductID = req.params.Id;
            const data = [{id:ProductID,name:'Product '+ProductID}]
            return utils.successResponse(data,res); 
    } catch (error) {
      return utils.failureResponse(error.message, res);
    }
  };

  const getSubProductByID = async (req,res)=>{
    try {
        let SubProductID = req.params.Id;
            const data = [{id:SubProductID,name:'Sub Product '+SubProductID}]
            return utils.successResponse(data,res); 
    } catch (error) {
      return utils.failureResponse(error.message, res);
    }
  };

module.exports = {
    getProduct,
    getSubProduct,
    getProductByID,
    getSubProductByID
};