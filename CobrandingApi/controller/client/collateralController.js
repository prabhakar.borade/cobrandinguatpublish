const utils = require('../../utils/messages');
const dbService = require('../../utils/dbService');
const moment = require('moment');
const collateralSchemaKey = require('../../utils/validation/collateralValidation');
const validation = require('../../utils/validateRequest');
const Collateral = require('../../model/collaterals');
const Cobrands = require('../../model/cobrands');
const CollateralFilesMap = require('../../model/collateralFilesMap');
const { moveToPermanentFile , moveToPermanentThumbnail } = require("../../services/files/filehandler");
const path = require("path");
const { Op } = require("sequelize");
const sequelize = require("../../config/dbConnection");
const { UPLOADS_FOLDER, TEMP_FOLDER, COBRAND_FOLDER, COLLATERAL_FOLDER } = require('../../config/file');


  const addCollateral = async (req, res) => {
    try {
  
      let isValid = validation.validateParamsWithJoi(
        req.body,
        collateralSchemaKey.schemaKeys);
      if (isValid.error) {
        console.log(isValid.error);
        return utils.inValidParam(isValid.error, res);
      }
      //console.log(moment(new Date()).format('MMMM DD YYYY, h:mm')) 
      const data = ({ 
                        collateralName: 'Collateral - '+ moment(new Date()).format('MMMM DD YYYY, h:mm') ,
                        collateralType:req.body.collateralType,
                        categoryId:req.body.categoryId,
                        categoryName:req.body.categoryName,
                        subcategoryId:req.body.subcategoryId,
                        subcategoryName:req.body.subcategoryName,
                        numberOfCollateral:0,
                        statusAsDraft:true,
                        isActive:true,
                        createdBy:req.user.id,
                        updatedBy:req.user.id
                    });
      let result = await dbService.createOne(Collateral,data);

      
      return  utils.successResponse(result, res);
    } catch (error) {
      console.log(error)
      return utils.failureResponse(error.message,res); 
    }
  };
  
  const findAllCollateral = async (req, res) => {
    try {
      let options = {};
      let query = {};
      let result;
      if (req.body.isCountOnly){
        if (req.body.query !== undefined) {
          query = { ...req.body.query };
        }
        if (req.user){
          query = {
            ...query,
            ...{ 'id': { [Op.ne]: req.user.id } } 
          };
          if (req.body.query.id) {
            Object.assign(query.id, { [Op.in]: [req.body.query.id] });
          }
        }
        result = await dbService.count(Collateral, query);
        if (result) {
          result = { totalRecords: result };
          return utils.successResponse(result, res);
        } 
        return utils.recordNotFound([], res);
      }
      else {
        if (req.body.options !== undefined) {
          options = { ...req.body.options };
        }
        if (options && options.select && options.select.length){
          options.attributes = options.select;
        }
              
        if (req.body.query !== undefined){
          query = { ...req.body.query };
        }
        if (req.user){
          query = {
            ...query,
            ...{ 'id': { [Op.ne]: req.user.id } } 
          };
          if (req.body.query.id) {
            Object.assign(query.id, { [Op.in]: [req.body.query.id] });
          }
        }
        result = await dbService.findMany( Collateral,query,options);
              
        if (!result){
          return utils.recordNotFound([],res);
        }
        return utils.successResponse(result, res);   
      }
    }
    catch (error){
      console.log(error);
      return utils.failureResponse(error.message,res);
    }
  };
  
  const getCollateralById = async (req, res) => {
    try {
      let query = {};
  
      let id = req.params.id;
          
      let result = await dbService.findByPk(Collateral,id,query);
      if (result){
        return  utils.successResponse(result, res);
              
      }
      return utils.recordNotFound([],res);
    }
    catch (error){
      return utils.failureResponse(error.message,res);
    }
  };
   
  const updateCollateralName = async (req, res) => {
    try {
      const data = ({ 
        collateralName:req.body.collateralName,
        //createdBy:req.user.id,
        updatedBy:req.user.id
       });
      let query = ({id:req.body.id});
      let result = await dbService.updateMany(Collateral,query,data);
      if (!result){
        return utils.failureResponse('something is wrong',res);
      }
      return  utils.successResponse(result, res);
    }
    catch (error){
      console.log(error);
      return utils.failureResponse(error.message,res);
    }
  };

  const submitCollateral = async (req, res) => {
    try {
      // let isValid = validation.validateParamsWithJoi(
      //   req.body,
      //   collateralFileMapSchemaKey.submitSchemaKeys);
      // if (isValid.error) {
      //   return utils.inValidParam(isValid.error, res);
      // } 
      const data = req.body
      let result = await dbService.createMany(CollateralFilesMap,data);
      await movefile(data).then(() =>{
         dbentry(data);
      }).catch(error => {
        console.log(error);
        return utils.failureResponse(error.message,res);
      });
      if (!result){
        return utils.failureResponse('Something went wrong, please try again',res);
      }


      function movefile(data) {
        return new Promise(async (resolve, reject) => {
          try {
            for(let i = 0; i < data.length; i++) {
           const element = data[i];
           //console.log("element", element);
        let oldPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + '/' + element.imageName );
        let newPath = path.join(__dirname + UPLOADS_FOLDER + COLLATERAL_FOLDER + '/' + element.imageName );

        let oldThumbnailPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER  + '/' + element.thumnailName );
        let newThumbnailPath = path.join(__dirname + UPLOADS_FOLDER + COLLATERAL_FOLDER + '/' + element.thumnailName );
        
        await moveToPermanentFile(oldPath,newPath, element.thumnailName)
          .then(result => {
            // let data = {statusAsDraft:false};
            // let id = element.collateralId
            // dbService.updateByPk(Collateral,id,data)
            //console.log("new file path", result);
        });

        await moveToPermanentThumbnail(oldThumbnailPath,newThumbnailPath)
        .then(result => {
          // let dataPK = {statusAsDraft:false};
          // let id = element.collateralId
          // dbService.updateByPk(Collateral,id,dataPK)
           //console.log("new thumb path",result);
          // return  utils.successResponse(result, res);
        });

        if(i == (data.length-1)) {
          resolve();
        }
        }
          } catch (error) {
            console.log(error);
            reject(error);
          }    
      });     
      }
      

      function dbentry(data) {
        let dataPK = {
          statusAsDraft:false,
          //createdBy:req.user.id,
          //updatedBy:req.user.id
        };
          let id = data[0].collateralId;
          dbService.updateByPk(Collateral,id,dataPK)
          //console.log("new thumb path",result);
          return  utils.successResponse(result, res);
      }
          

      
    }
    catch (error){
      console.log(error)
      return utils.failureResponse(error.message,res);
    }
  };

  const getAllCollateral = async (req,res) =>{
    try {
      let options = {};
      let query = {};
      let result;
      if (req.body.isCountOnly){
        if (req.body.query !== undefined) {
          query = { ...req.body.query };
        }
        result = await dbService.count(Collateral, query);
        if (result) {
          result = { totalRecords: result };
          return utils.successResponse(result, res);
        } 
        return utils.recordNotFound([], res);
      }
      else {
        if (req.body.options !== undefined) {
          options = { ...req.body.options };
        }
        if (options && options.select && options.select.length){
          options.attributes = options.select;
        }
  
        if (options && options.sort && options.sort.length){
          options.order = options.sort;
        }

        options.include = [{ all: true ,nested: true }]
              
        if (req.body.query !== undefined){
          query = { ...req.body.query };
        }
        if (req.collateral){
          query = {
            ...query,
            ...{ 'id': { [Op.ne]: req.collateral.id } } 
          };
          if (req.body.query.id) {
            Object.assign(query.id, { [Op.in]: [req.body.query.id] });
          }
        }
        result = await dbService.findMany(Collateral,query,options);
              
        if (!result){
          return utils.recordNotFound([],res);
        }
        return utils.successResponse(result, res);   
      }
    }
    catch (error){
      console.log(error);
      return utils.failureResponse(error.message,res);
    }
  };
  

const getCollateralByFilter = async (req,res) =>{
    try {
      let options = {};
      let query = {};
      let result;
      if (req.body.isCountOnly){
        if (req.body.query !== undefined) {
          query = { ...req.body.query };
        }
        result = await dbService.count(Collateral, query);
        if (result) {
          result = { totalRecords: result };
          return utils.successResponse(result, res);
        }
        return utils.recordNotFound([], res);
      }
      else {
        if (req.body.options !== undefined) {
          options = { ...req.body.options };
        }
        options.include = [{ all: true ,nested: true }]
       
          if (req.body.filter !== undefined){

            if(req.body.filter[0].collateralType.length == 0 && req.body.filter[1].categoryId.length != 0 && req.body.filter[2].subcategoryId.length != 0 ){
              query = {
                [Op.and]: [
                      {[Op.or]: [
                          { categoryId: req.body.filter[1].categoryId }
                        ]},
                      {[Op.or]: [
                          { subcategoryId: req.body.filter[2].subcategoryId }
                        ]},
                        {[Op.or]:[
                          {statusAsDraft:false,isActive:true}
                        ]}
                ]
              };
            }
            else if(req.body.filter[1].categoryId.length == 0 && req.body.filter[0].collateralType.length != 0 && req.body.filter[2].subcategoryId.length != 0 ){
              query = {
                [Op.and]: [
                      {[Op.or]: [
                          { collateralType:req.body.filter[0].collateralType }
                        ]},
                      {[Op.or]: [
                          { subcategoryId: req.body.filter[2].subcategoryId }
                        ]},
                        {[Op.or]:[
                          {statusAsDraft:false,isActive:true}
                        ]}
                ]
              };
            }
            else if(req.body.filter[2].subcategoryId.length == 0 && req.body.filter[0].collateralType.length != 0 && req.body.filter[1].categoryId.length != 0){
              query = {
                [Op.and]: [
                      {[Op.or]: [
                          { collateralType:req.body.filter[0].collateralType }
                        ]},
                      {[Op.or]: [
                          { categoryId: req.body.filter[1].categoryId }
                        ]},
                        {[Op.or]:[
                          {statusAsDraft:false,isActive:true}
                        ]}
                ]
              };
            }

            else if(req.body.filter[0].collateralType.length == 0 && req.body.filter[1].categoryId.length == 0 && req.body.filter[2].subcategoryId.length != 0){
              query = {
                [Op.and]: [
                  { subcategoryId: req.body.filter[2].subcategoryId },
                  {statusAsDraft:false,isActive:true}
                ]
              };
            }
            else if(req.body.filter[0].collateralType.length == 0 && req.body.filter[2].subcategoryId.length == 0 && req.body.filter[1].categoryId.length != 0){
              query = {
                [Op.and]: [
                  { categoryId: req.body.filter[1].categoryId },
                  {statusAsDraft:false,isActive:true}
                ]
              };
            }
            else if(req.body.filter[1].categoryId.length == 0 && req.body.filter[2].subcategoryId.length == 0 && req.body.filter[0].collateralType.length != 0){
              query = {
                [Op.and]: [
                          { collateralType:req.body.filter[0].collateralType },
                          {statusAsDraft:false,isActive:true}
                        ]
              };
            }

            else if(req.body.filter[0].collateralType.length == 0 && req.body.filter[1].categoryId.length == 0 && req.body.filter[2].subcategoryId.length == 0 ){
              query = {
                statusAsDraft:false,isActive:true
              };
            }

            else{
              query = {
                [Op.and]: [
                      {[Op.or]: [
                          { collateralType:req.body.filter[0].collateralType }
                        ]},
                      {[Op.or]: [
                          { categoryId: req.body.filter[1].categoryId }
                        ]},
                      {[Op.or]: [
                          { subcategoryId: req.body.filter[2].subcategoryId }
                        ]},
                        {[Op.or]:[
                          {statusAsDraft:false,isActive:true}
                        ]}
                ]
              };
            }
          }

          if(req.body.search !== undefined){
           
            query= {
              ...query,
              [Op.or]:[
                {collateralName: { [Op.and] : { [Op.like]: req.body.search[0].collateralName + '%' }   },
                statusAsDraft:false,isActive:true
              }
              ] 
            }  
          }

        result = await dbService.findMany(Collateral,query,options);
             
        if (!result){
          return utils.recordNotFound([],res);
        }
        return utils.successResponse(result, res);  
      }
    }
    catch (error){
      console.log(error)
      return utils.failureResponse(error.message,res);
    }
  };

module.exports = {
    addCollateral,
    findAllCollateral,
    getCollateralById,
    updateCollateralName,
    submitCollateral,
    getAllCollateral,
    getCollateralByFilter
};