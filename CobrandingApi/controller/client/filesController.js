const utils = require('../../utils/messages');
const dbService = require('../../utils/dbService');
const Collateral = require('../../model/collaterals');
const Cobrand = require('../../model/cobrands');
const Files = require('../../model/files');
const dotenv = require("dotenv");
dotenv.config();
const { v4 } = require("uuid");
const ImgData = require('../../model/files');
const path = require('path');
const fse = require("fs-extra");
const fs = require("fs");
const { addManyImages,findAllImages } = require('../../utils/dbService');
const { imageThumbnail,imageThumbnailCobrand,videoThumbnail,pdfThumbnail } = require('../../services/files/thumbnailService');
const { UPLOADS_FOLDER,TEMP_FOLDER,COLLATERAL_FOLDER,COBRAND_FOLDER ,APP_HOST_URL} = require('../../config/file')

  // const getFileByUserId = async (req,res) =>{
  //   try{
  //     const userId = req.params.userId;
  //     findAllImages(ImgData, userId)
  //     .then((result) => {
  //       return  utils.successResponse(result, res);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  //   }
  //   catch (error){
  //     return utils.failureResponse(error.message,res);
  //   }
  // };
  
  
  const getFileById = async (req, res) => {
    try {
      let query = {};
  
      let id = req.params.Id;
          
      let result = await dbService.findByPk(Collateral,id,query);
      if (result){
        return  utils.successResponse(result, res);
              
      }
      return utils.recordNotFound([],res);
    }
    catch (error){
      return utils.failureResponse(error.message,res);
    }
  };
  
  const uploadFiles =  async (req, res) => {
    try{
      const images = [];
      const files = req.files;
      if (!files) {
        const error = new Error("Please upload a file");
        error.httpStatusCode = 400;
        return next(error);
      }


      let pr = files.map(async (file) => {
        const newFileName = v4() + path.extname(file.originalname);
        console.log(file.originalname);
        await fse.writeFile(
          path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${newFileName}`),
          file.buffer,
          (err) => console.log(err)
        );
        if(file.mimetype == 'image/jpeg' ||  file.mimetype == 'image/jpg' ||file.mimetype == 'image/png'){
          const thumnailName = 'thumbnail'+newFileName;
          const thumnailPath = (path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${thumnailName}` ));

          await imageThumbnail(path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${newFileName}`),thumnailPath)
          .then(async() =>{
            //const thumnailName = 'thumbnail'+newFileName;
            await images.push({
              //collateralId : req.body.collateralId,
              //userId: req.body.userId,
              fileId: newFileName.split(".")[0],
              path: TEMP_FOLDER + '/' + newFileName,
              originalFileName: file.originalname,
              imageName: newFileName,
              isActive: true,
              mimetype: file.mimetype,
              //encoding: file.encoding,
              size: file.size,
              thumnailName:thumnailName,
              thumnailPath: TEMP_FOLDER + '/' + thumnailName,
              createdBy:req.user.id,
              updatedBy:req.user.id
            });
            console.log("Image Data",images)
          }) 
        }
        else if(file.mimetype == 'video/mp4'){
          let videoInputPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + '/'+ newFileName);
          let videoOutputPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER);
          let videoThumbnailName = 'thumbnail'+newFileName.split('.')[0]+'.png';
          
          
          await videoThumbnail(videoInputPath,videoOutputPath,videoThumbnailName)
          .then(async() =>{
            const thumnailName = 'thumbnail'+newFileName.split('.')[0]+'.png';
            await images.push({
              //userId: req.body.userId,
              fileId: newFileName.split(".")[0],
              path: TEMP_FOLDER + '/' + newFileName,
              originalFileName: file.originalname,
              imageName: newFileName,
              isActive: true,
              mimetype: file.mimetype,
              //encoding: file.encoding,
              size: file.size,
              thumnailName:thumnailName,
              thumnailPath: TEMP_FOLDER +'/'+thumnailName,
              createdBy:req.user.id,
              updatedBy:req.user.id
            });

          }) 
        }  
        else if(file.mimetype == 'application/pdf'){
          const fileName = 'thumbnail'+newFileName.split('.')[0]+'.png';

          let pdfInputPath = APP_HOST_URL + '/'  + TEMP_FOLDER + '/' + newFileName;
          let pdfOutputPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + '/'+ fileName);
          console.log("pdfInputPath",pdfInputPath);
          console.log("pdfOutputPath",pdfOutputPath);
          await pdfThumbnail(pdfInputPath,pdfOutputPath)
          .then(async() =>{
            const thumnailName = 'thumbnail'+newFileName.split('.')[0]+'.png';
            await images.push({
              //userId: req.body.userId,
              fileId: newFileName.split(".")[0],
              path: TEMP_FOLDER + '/' + newFileName,
              originalFileName: file.originalname,
              imageName: newFileName,
              isActive: true,
              mimetype: file.mimetype,
              //encoding: file.encoding,
              size: file.size,
              thumnailName:thumnailName,
              thumnailPath: TEMP_FOLDER +'/'+thumnailName,
              createdBy:req.user.id,
              updatedBy:req.user.id
            });
          })
        }  
      }); 
      await Promise.all(pr);
         addManyImages(ImgData, images)
            .then((result) => {
              console.log(result);
              return utils.successResponse(result, res);
            })
            .catch((error) => {
              console.log(error);
            });
    }
    catch (error){
      console.log(error);
      return utils.failureResponse(error.message,res);
    }
  };

  const deleteFileById = async (req, res) => {
    try {

      let id = req.body.fileId;
      let imagePath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${req.body.imageName}`);
      let thumbnailPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${req.body.thumnailName}`);
         
      console.log(imagePath);
      console.log(thumbnailPath);
      
      let result = await dbService.deleteByFileId(ImgData,id);
      if (result){
        fse.unlink(imagePath);
        fse.unlink(thumbnailPath);
        return  utils.successResponse(result, res);       
      }
      return utils.recordNotFound([],res);
    }
    catch (error){
      return utils.failureResponse(error.message,res);
    }
  };


  const deleteCollateral = async (req, res) => {
    try {
      let id = req.body.id;
         let result = dbService.deleteByPk(Collateral,id).then(() =>{
          dbService.deleteMany(Files, {
            imageName : req.body.imgList
          }).then(() => {
            req.body.imgList.forEach(element => {
              let imagePath = path.join(__dirname + UPLOADS_FOLDER + COLLATERAL_FOLDER + `/${element}`);
              fse.unlink(imagePath);
            });
            req.body.thumbList.forEach(element => {
              let thumbnailPath = path.join(__dirname + UPLOADS_FOLDER + COLLATERAL_FOLDER + `/${element}`);
              fse.unlink(thumbnailPath);
            });
          })
         })
          if (result){
        return  utils.successResponse(result, res);       
      }
      return utils.recordNotFound([],res);
    }
    catch (error){
      return utils.failureResponse(error.message,res);
    }
  };

  const deleteCobrand = async (req, res) => {
    try {
      let id = req.body.id;
      console.log("deleted request", req.body);
         let result = dbService.deleteByPk(Cobrand,id).then(() =>{
          dbService.deleteMany(Files, {
            imageName : req.body.imgList
          }).then(() => {
            req.body.imgList.forEach(element => {
              let imagePath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER + `/${element}`);
              fse.unlink(imagePath);
            });
            req.body.thumbList.forEach(element => {
              let thumbnailPath = path.join(__dirname + UPLOADS_FOLDER + COBRAND_FOLDER + `/${element}`);
              fse.unlink(thumbnailPath);
            });
          })
         })
          if (result){
        return  utils.successResponse(result, res);       
      }
      return utils.recordNotFound([],res);
    }
    catch (error){
      return utils.failureResponse(error.message,res);
    }
  };

  const downloadFileByPath = async (req, res) => {
    try {
        let fileName = path.join(__dirname + UPLOADS_FOLDER + COLLATERAL_FOLDER + '/' + req.params.name);
        console.log(fileName);
        res.download(fileName);
    }
    catch (error){
      return utils.failureResponse(error.message,res);
    }
  };

  const uploadFilesWithoutThumbnail =  async (req, res) => {
    try{
      const images = [];
      const files = req.files;
      if (!files) {
        const error = new Error("Please upload a file");
        error.httpStatusCode = 400;
        return next(error);
      }


      let pr = files.map(async (file) => {
        const newFileName = 'CobrandLogo'+ v4() + path.extname(file.originalname);
        console.log(file.originalname);
        await fse.writeFile(
          path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${newFileName}`),
          file.buffer,
          () => {
            var result = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${newFileName}`);
            return utils.successResponse(result, res);
          }
        ); 
      }); 
      await Promise.all(pr);
    }
    catch (error){
      console.log(error);
      return utils.failureResponse(error.message,res);
    }
  };

  async function base64ImageStore(base64Data,fileName) {
    try{
      fs.writeFile(
        path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${fileName}`),
        base64Data,
        async () => {
          var result = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${fileName}`);
          return result;
        }
      );
    }
    catch (error){
      console.log(error);
    }
  };

  const deleteMultipleCollateralFiles = async (req, res) => {
    try {

      let fileList = req.body.fileList;
      fileList.forEach(async(element) => {
          let id = element.fileId;
          let imagePath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${element.imageName}`);
          let thumbnailPath = path.join(__dirname + UPLOADS_FOLDER + TEMP_FOLDER + `/${element.thumnailName}`);
          
          let result = await dbService.deleteByFileId(ImgData,id);

          if (result){
            fse.unlink(imagePath);
            fse.unlink(thumbnailPath);     
          }
      });
      return  utils.successResponse("Records Deleted Successfully", res);       
    }
    catch (error){
      console.log("deleteMultipleCollateralFiles",error)
      return utils.failureResponse(error.message,res);
    }
  };
  
module.exports = {
    //getFileByUserId,
    getFileById,
    uploadFiles,
    uploadFilesWithoutThumbnail,
    deleteFileById,
    imageThumbnail,
    //imageThumbnailCobrand,
    videoThumbnail,
    downloadFileByPath,
    base64ImageStore,
    pdfThumbnail,
    deleteMultipleCollateralFiles,
    deleteCollateral,
    deleteCobrand
};