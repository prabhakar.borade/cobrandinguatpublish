
const model = require('../model');
const dbService = require('../utils/dbService');
const { replaceAll } = require('../utils/common');

async function seedRole () {
  const roles = [ 'ADMIN','USER' ];
  for (let i = 0; i < roles.length; i++) {
    let result = await dbService.findOne(model.role,{
      name: roles[i],
      isActive: true,
      isDeleted: false 
    });
    if (!result) {
      await dbService.createOne( model.role,{
        name: roles[i],
        code: roles[i].toUpperCase(),
        weight: 1
      });
    }
  };
  //console.info('Role model seeded 🍺');
}
async function seedProjectRoutes (routes) {
  if (routes && routes.length) {
    for (let i = 0; i < routes.length; i++) {
      const routeMethods = routes[i].methods;
      for (let j = 0; j < routeMethods.length; j++) {
        const routeObj = {
          uri: routes[i].path.toLowerCase(),
          method: routeMethods[j],
          route_name: `${replaceAll((routes[i].path).toLowerCase().substring(1), '/', '_')}`,
          isActive: true, 
          isDeleted: false
        };
        let result = await dbService.findOne(model.projectRoute,routeObj);
        if (!result) {
          await dbService.createOne(model.projectRoute,routeObj);
        }
      }
    }
    //console.info('ProjectRoute model seeded 🍺');
  }
}
async function seedRouteRole () {
  const routeRoles = [ 
    {
      route: '/client/api/v1/user/create',
      role: 'USER',
      method: 'POST' 
    },
    {
      route: '/client/api/v1/user/create',
      role: 'ADMIN',
      method: 'POST' 
    },
    {
      route: '/client/api/v1/user/list',
      role: 'USER',
      method: 'POST' 
    },
    {
      route: '/client/api/v1/user/list',
      role: 'ADMIN',
      method: 'POST' 
    },
    {
      route: '/client/api/v1/user/aggregate',
      role: 'USER',
      method: 'POST'
    },
    {
      route: '/client/api/v1/user/aggregate',
      role: 'ADMIN',
      method: 'POST'
    },
    {
      route: '/client/api/v1/user/:id',
      role: 'USER',
      method: 'GET' 
    },
    {
      route: '/client/api/v1/user/:id',
      role: 'ADMIN',
      method: 'GET' 
    },
    {
      route: '/client/api/v1/user/count',
      role: 'USER',
      method: 'POST' 
    },
    {
      route: '/client/api/v1/user/count',
      role: 'ADMIN',
      method: 'POST' 
    },
    {
      route: '/client/api/v1/user/update/:id',
      role: 'USER',
      method: 'PUT'
    },
    {
      route: '/client/api/v1/user/update/:id',
      role: 'ADMIN',
      method: 'PUT'
    },
    {
      route: '/client/api/v1/user/partial-update/:id',
      role: 'USER',
      method: 'PUT'
    },
    {
      route: '/client/api/v1/user/partial-update/:id',
      role: 'ADMIN',
      method: 'PUT'
    },
    {
      route: '/client/api/v1/user/softdelete/:id',
      role: 'USER',
      method: 'PUT'
    },
    {
      route: '/client/api/v1/user/softdelete/:id',
      role: 'ADMIN',
      method: 'PUT'
    },
    {
      route: '/client/api/v1/user/softdeletemany',
      role: 'USER',
      method: 'PUT'
    },
    {
      route: '/client/api/v1/user/softdeletemany',
      role: 'ADMIN',
      method: 'PUT'
    },
    //new routes start

      //productRoutes

        {
          route: '/client/api/v1/products/category/list',
          role: 'ADMIN',
          method: 'GET'
        },
        {
          route: '/client/api/v1/products/category/:Id',
          role: 'ADMIN',
          method: 'GET'
        },
        {
          route: '/client/api/v1/products/sub-category/list',
          role: 'ADMIN',
          method: 'POST'
        },
        {
          route: '/client/api/v1/products/sub-category/:Id',
          role: 'ADMIN',
          method: 'GET'
        },

        {
          route: '/client/api/v1/products/category/list',
          role: 'USER',
          method: 'GET'
        },
        {
          route: '/client/api/v1/products/category/:Id',
          role: 'USER',
          method: 'GET'
        },
        {
          route: '/client/api/v1/products/sub-category/list',
          role: 'USER',
          method: 'POST'
        },
        {
          route: '/client/api/v1/products/sub-category/:Id',
          role: 'USER',
          method: 'GET'
        },

      //filesRoutes
      {
        route: '/client/api/v1/collateral/files/upload',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/cobrand/files/upload',
        role: 'USER',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/files/:Id',
        role: 'ADMIN',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/files/:Id',
        role: 'USER',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/files/all/:userId',
        role: 'ADMIN',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/files/all/:userId',
        role: 'USER',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/files/delete',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/files/download/:name',
        role: 'ADMIN',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/files/download/:name',
        role: 'USER',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/files/multiple/delete',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/files/multiple/deletecollateral',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/cobrand/files/multiple/deletecobrand',
        role: 'USER',
        method: 'POST'
      },

      //collateralRoutes

      {
        route: '/client/api/v1/collateral/create',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/:id',
        role: 'ADMIN',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/:id',
        role: 'USER',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/update',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/submit',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/list',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/list',
        role: 'USER',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/filter',
        role: 'ADMIN',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/filter',
        role: 'USER',
        method: 'POST'
      },
      {
        route: '/client/api/v1/collateral/filelist/:collateralId',
        role: 'ADMIN',
        method: 'GET'
      },
      {
        route: '/client/api/v1/collateral/filelist/:collateralId',
        role: 'USER',
        method: 'GET'
      },

      //cobrandRoutes

      {
        route: '/client/api/v1/cobrand/create',
        role: 'USER',
        method: 'POST'
      },
      {
        route: '/client/api/v1/cobrand/list',
        role: 'USER',
        method: 'POST'
      },
      {
        route: '/client/api/v1/cobrand/files/download/:name',
        role: 'USER',
        method: 'GET'
      },
      {
        route: '/client/api/v1/cobrand/filter',
        role: 'USER',
        method: 'POST'
      },

    //new routes end

  ];
  if (routeRoles && routeRoles.length) {
    for (let i = 0; i < routeRoles.length; i++) {
      let route = await dbService.findOne(model.projectRoute,{
        uri: routeRoles[i].route.toLowerCase(),
        method: routeRoles[i].method,
        isActive: true,
        isDeleted: false 
      }, { attributes: ['id'] });
      let role = await dbService.findOne(model.role,{
        code: (routeRoles[i].role).toUpperCase(),
        isActive: true,
        isDeleted: false 
      }, { attributes: ['id'] });
      if (route && route.id && role && role.id) {
        let routeRoleObj = await dbService.findOne(model.routeRole,{
          roleId: role.id,
          routeId: route.id,
          isActive: true, 
          isDeleted: false
        });
        if (!routeRoleObj) {
          await dbService.createOne(model.routeRole,{
            roleId: role.id,
            routeId: route.id
          });
        }
      }
    };
    //console.info('RouteRole model seeded 🍺');
  }
}
async function seedUserRole (){
  
  let admin = await dbService.findOne(model.user,{
    'username':'Admin',
    'isActive':true,
    'isDeleted':false
  });
  let adminRole = await dbService.findOne(model.role,{
    code: 'ADMIN',
    isActive: true,
    isDeleted: false
  }, { attributes: ['id'] });
  if (admin && admin.isPasswordMatch('ABC') && adminRole){
    let count = await dbService.count(model.userRole, {
      userId: admin.id,
      roleId: adminRole.id,
      isActive: true, 
      isDeleted: false
    });
    if (count == 0) {
      await dbService.createOne(model.userRole, {
        userId: admin.id,
        roleId: adminRole.id
      });
      //console.info('admin seeded 🍺');
    }
  }
}
async function seedUser () {
  let admin = await dbService.findOne(model.user,{
    'username':'Admin',
    'isActive':true,
    'isDeleted':false
  });
  if (!admin || !admin.isPasswordMatch('ABC') ) {
    let admin = {
      'password':'ABC',
      'username':'Admin',
      'name': 'Admin',
      'role':1
    };
    await dbService.createOne(model.user,admin);
  }
  //console.info('user model seeded 🍺');

}
async function seedData (allRegisterRoutes){
  await seedUser();
  await seedRole();
  await seedProjectRoutes(allRegisterRoutes);
  await seedRouteRole();
  await seedUserRole();
}     

module.exports = seedData;