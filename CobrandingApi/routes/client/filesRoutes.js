const express = require('express');
const router = express.Router();

const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');
const filesController =  require('../../controller/client/filesController');
var multer = require("multer");
var upload = multer();

router.route('/client/api/v1/collateral/files/upload').post(auth(...['getByAdminInClientPlatform']),checkRolePermission,upload.any(),filesController.uploadFiles);
router.route('/client/api/v1/cobrand/files/upload').post(auth(...['getByUserInClientPlatform']),checkRolePermission,upload.any(),filesController.uploadFilesWithoutThumbnail);
router.route('/client/api/v1/collateral/files/:Id').get(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,filesController.getFileById);
//router.route('/client/api/v1/collateral/files/all/:userId').get(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,filesController.getFileByUserId);
router.route('/client/api/v1/collateral/files/delete').post(auth(...['getByAdminInClientPlatform']),checkRolePermission,filesController.deleteFileById);
router.route('/client/api/v1/collateral/files/download/:name').get(filesController.downloadFileByPath);
router.route('/client/api/v1/collateral/files/multiple/delete').post(auth(...['getByAdminInClientPlatform']),checkRolePermission,filesController.deleteMultipleCollateralFiles);
router.route('/client/api/v1/collateral/files/multiple/deletecollateral').post(auth(...['getByAdminInClientPlatform']),checkRolePermission,filesController.deleteCollateral);
router.route('/client/api/v1/cobrand/files/multiple/deletecobrand').post(auth(...['getByUserInClientPlatform']),checkRolePermission,filesController.deleteCobrand);

module.exports = router;
