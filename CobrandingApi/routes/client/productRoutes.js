const express = require('express');
const router = express.Router();

const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');
const productController =  require('../../controller/client/productController');

router.route('/client/api/v1/products/category/list').get(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,productController.getProduct);
router.route('/client/api/v1/products/category/:Id').get(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,productController.getProductByID);

router.route('/client/api/v1/products/sub-category/list').post(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,productController.getSubProduct);
router.route('/client/api/v1/products/sub-category/:Id').get(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,productController.getSubProductByID);


module.exports = router;
