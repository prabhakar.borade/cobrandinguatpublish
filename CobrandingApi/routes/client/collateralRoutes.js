const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');
const collateralController =  require('../../controller/client/collateralController');

router.route('/client/api/v1/collateral/create').post(auth(...['getByAdminInClientPlatform']),checkRolePermission,collateralController.addCollateral);
router.route('/client/api/v1/collateral/:id').get(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,collateralController.getCollateralById);
router.route('/client/api/v1/collateral/update').post(auth(...['getByAdminInClientPlatform']),checkRolePermission,collateralController.updateCollateralName);
router.route('/client/api/v1/collateral/submit').post(auth(...['getByAdminInClientPlatform']),checkRolePermission,collateralController.submitCollateral);
router.route('/client/api/v1/collateral/list').post(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,collateralController.getAllCollateral);
router.route('/client/api/v1/collateral/filter').post(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,collateralController.getCollateralByFilter);
//router.route('/client/api/v1/collateral/filelist/:collateralId').get(auth(...['getByAdminInClientPlatform', 'getByUserInClientPlatform']),checkRolePermission,collateralController.getAllFilesByCollateralId);


module.exports = router;
