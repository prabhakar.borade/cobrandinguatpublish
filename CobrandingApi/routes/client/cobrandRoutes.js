const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const checkRolePermission = require('../../middleware/checkRolePermission');
const coBrandingController =  require('../../controller/client/cobrandController');
const {fork} = require('child_process');
const utils = require('../../utils/messages');
const path = require('path');
const dbService = require('../../utils/dbService');
const Cobrands = require('../../model/cobrands');
// const { sendMessage, socketMsg } = require('../../utils/socket');
// const {server} = require("../../app")
//const {roomID} = require("../../constants/authConstant")
//const io  = require("../../utils/socket").io;
//let socket;
//Socket.Io starts

// socket = io();
//router.route('/client/api/v1/send-notification').get((req, res) => {
    // socket.on('watermarkingprocess',()=>{
    //     const notify = {"Wartermarking":"In process 1"};
    //     socket.emit('watermarkingprocess', notify);
    // })
    
    //return utils.successResponse({"Wartermarking":"In process 1"},res);
//});

//Socket.Io ends

router.route('/client/api/v1/cobrand/create').post(auth(...['getByUserInClientPlatform']),checkRolePermission, async (res,req) =>{
    const requestBody = req.req.body ;
    const requestUser = req.req.user;
    let cobrandID ;
    await cobrandDBentry(requestBody, requestUser);

    function cobrandDBentry(requestBody, requestUser ) {
        return new Promise ((resolve, reject) => {
            let coBrandDBData = [];
              coBrandDBData.push({
              collateralId : requestBody[0].collateralDetails.collateralfilemaps[0].collateralId,
              collateralName : requestBody[0].collateralDetails.collateralName,
              categoryName: requestBody[0].collateralDetails.categoryName,
              subcategoryName : requestBody[0].collateralDetails.subcategoryName,
              collateralType:requestBody[0].collateralDetails.collateralType,
              categoryId:requestBody[0].collateralDetails.categoryId,
              subcategoryId:requestBody[0].collateralDetails.subcategoryId,
              createdBy:requestUser.id,
              updatedBy:requestUser.id,
              isProcessed: false
            });
            dbService.createMany(Cobrands, coBrandDBData).then(async(result) => {
                if(result) {
                    cobrandID = result[0].id;
                    coBrandDBData.splice(0, coBrandDBData.length);
                resolve();
                }
                reject();
            });
        })
    }
    const child = fork(path.join(__dirname + '/../../controller/client/cobrandController.js'));
    child.send({
        "req":{"body":requestBody,"user":requestUser},
        "cobrandId" : {
            "id" : cobrandID
        }       
    });
    child.on('message',(response) =>{
         console.log("final response",response);
        //  return utils.successResponse({"Wartermarking":"In process"}, res.res);

        // console.log("stored roomid", roomID);
         // sendMessage(roomID, response)
        // io.to().emit('processing', response)
         //return utils.successResponse(response, res.res);
        //  res.res.status(responseStatusCode.success).send({
        //     status: 'SUCCESS',
        //     message: 'Your request is successfully executed',
        //     response,
        //   });
        //res.send(response)
        // const data = {
        //     watermarkingDone: true
        // }
        //  socketMsg(server, 'watermarking', response)
    });
    //return utils.successResponse(response, res);
    // child.on('close',(response)=>{
    //     //return utils.successResponse("success", 200);

    // });
    //socket.emit('message', {"Wartermarking":"In process"}); // Updates Live Notification
    //res.send(notify);
    return utils.successResponse({"Wartermarking":"In process"}, res.res);
});
router.route('/client/api/v1/cobrand/list').post(auth(...['getByUserInClientPlatform']),checkRolePermission,coBrandingController.getAllCoBrands);
router.route('/client/api/v1/cobrand/files/download/:name').get(coBrandingController.downloadFileByPath);
router.route('/client/api/v1/cobrand/filter').post(auth(...['getByUserInClientPlatform']),checkRolePermission,coBrandingController.getCoBrandsByFilter);


module.exports = router;
