const express =  require('express');
const router =  express.Router();
router.use('/client/auth',require('./auth'));
router.use(require('./userRoutes'));
router.use(require('./collateralRoutes'));
router.use(require('./cobrandRoutes'));
router.use(require('./productRoutes'));
router.use(require('./filesRoutes'));

module.exports = router;
