const express = require('express');
const cors = require('cors');
const path = require('path');
const dotenv = require('dotenv');
dotenv.config();
global.__basedir = __dirname;
const ejs = require('ejs');
var fs = require('fs');

let cookieParser = require('cookie-parser');
let logger = require('morgan');
const passport = require('passport');

const { clientPassportStrategy } = require('./config/clientPassportStrategy');

const app = express();
const corsOptions = { origin: process.env.ALLOW_ORIGIN, };
app.use(cors(corsOptions));

//template engine
app.set('view engine', 'ejs'); 
app.set('views', path.join(__dirname, 'views'));

//all routes 
const routes =  require('./routes/index');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(routes);

clientPassportStrategy(passport);

const uploadsDir = path.join(__dirname, "/uploads");
app.use(express.static(uploadsDir));
app.get('/:filename',(req,res)=>{
    res.sendFile(path.join(uploadsDir,req.params.filename));
});

var dirTemp = './uploads/temp';
var dirCollaterals = './uploads/collaterals';
var dirCoBrands = './uploads/cobrands';

if (!fs.existsSync(dirTemp)){
    fs.mkdirSync(dirTemp, { recursive: true });
}

if (!fs.existsSync(dirCollaterals)){
    fs.mkdirSync(dirCollaterals, { recursive: true });
}

if (!fs.existsSync(dirCoBrands)){
    fs.mkdirSync(dirCoBrands, { recursive: true });
}

module.exports = app;
