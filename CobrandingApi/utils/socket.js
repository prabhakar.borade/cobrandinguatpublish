let {roomID} = require("../constants/authConstant")
const { Roomdetails} = require("../model/roomidstore")
// const { Roomdetails} = require("../model/roomidstore")
// const {} = require("")
let io;
let socketInst;
exports.socketConn = (server) => {
  io = require('socket.io')(server, {
  cors:{origin:'*', methods: ["GET", "POST"]}
});
  io.on('connection', (socket) => {
    socketInst =  socket;
    console.info(`Client connected [id=${socket.id}]`);
    socket.emit('processing',  {"Data":socket.id});
    var rooms = io.sockets.adapter.rooms;
    console.log("All rooms", rooms)
    // socket.join(socket.request._query.id);
    socket.on('disconnect', (data) => {
      console.log("disconnect request", data);
      console.info(`Client disconnected [id=${socket.id}]`);
    });
    socket.on('join', (data) => {
      console.log("joining data", data);
      socket.join(socket.id)
      console.log("user joined in", rooms);
    })
  });
};

exports.io = io;
// exports.socketMsg = (server, event, message) => {
//   io = require('socket.io')(server, {
//   cors:{origin:'*', methods: ["GET", "POST"]}
// });
//   io.to().emit(event, message)
//   // io.on('connection', (socket) => {
//   //   console.info(`Client connected [id=${socket.id}]`);
    
//   //   socket.on('disconnect', () => {
//   //     console.info(`Client disconnected [id=${socket.id}]`);
//   //   });
//   // });
// };

// exports.sendMessage = (event, message) => io.sockets.emit(event, message);
exports.sendMessage = (roomId, message) => io.to(roomId).emit(message);
// exports.join = (roomId, user) => {
//   console.log("roomid to join", roomId);
  
//   console.log("user has joined ID-", user);
//   io.join(roomId);
//   // io.to(roomId).emit(message);
// }
// exports.leave = (roomId, user) => {
//   io.leave(roomId);
//   console.log("user has left ID-", user);
//   // io.to(roomId).emit(message);
// }

exports.getRooms = () => io.sockets.adapter.rooms;