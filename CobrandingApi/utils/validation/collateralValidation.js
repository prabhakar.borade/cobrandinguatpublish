const joi = require('joi');
 
exports.schemaKeys = joi.object({
    collateralType:joi.string(),
    categoryId:joi.number(),
    subcategoryId:joi.number(),
    categoryName:joi.string(),
    subcategoryName:joi.string(),
    isActive: joi.boolean()
}).unknown(true);

exports.updateSchemaKeys = joi.object({
    collateralType:joi.string(),
    categoryId:joi.string(),
    subcategoryId:joi.string(),
    isActive: joi.boolean()
}).unknown(true);

