const joi = require('joi');

exports.submitSchemaKeys = joi.object({
    logoXcordinate: joi.number(),
    logoYcordinate: joi.number(),
    logoWidthcordinate: joi.number(),
    logoHeightcordinate: joi.number(),
    textAreaXcordinate: joi.number(),
    textAreaYcordinate: joi.number(),
    textAreaWidthcordinate: joi.number(),
    textAreaHeightcordinate: joi.number(),
    textAreaFontText: joi.string(),
    textAreaFontSize: joi.number(),
    textAreaBold: joi.boolean(),
    textAreaItalic: joi.boolean(),
    maxCharacter:joi.number(),
}).unknown(true);