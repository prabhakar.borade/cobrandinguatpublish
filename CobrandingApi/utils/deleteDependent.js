let User = require('../model/user');
let UserAuthSettings = require('../model/userAuthSettings');
let UserToken = require('../model/userToken');
let Role = require('../model/role');
let ProjectRoute = require('../model/projectRoute');
let RouteRole = require('../model/routeRole');
let UserRole = require('../model/userRole');
let dbService = require('../utils/dbService');
const { Op } = require('sequelize');

const deleteUser = async (filter) =>{
  try {
    let user = await User.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (user?.length){
      user = user.map((obj) => obj.id);
      const userFilter0507 = { 'addedBy': { [Op.in]: user } };
      const user7615 = await deleteUser(userFilter0507);
      const userFilter6664 = { 'updatedBy': { [Op.in]: user } };
      const user9653 = await deleteUser(userFilter6664);
      const userAuthSettingsFilter6813 = { '': { [Op.in]: user } };
      const userAuthSettings6005 = await deleteUserAuthSettings(userAuthSettingsFilter6813);
      const userTokenFilter7853 = { '': { [Op.in]: user } };
      const userToken5416 = await deleteUserToken(userTokenFilter7853);
      const userRoleFilter9856 = { '': { [Op.in]: user } };
      const userRole0039 = await deleteUserRole(userRoleFilter9856);
      return await User.destroy({ where :filter });
    } else {
      return 'No user found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteUserAuthSettings = async (filter) =>{
  try {
    return await UserAuthSettings.destroy({ where: filter });
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteUserToken = async (filter) =>{
  try {
    return await UserToken.destroy({ where: filter });
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteRole = async (filter) =>{
  try {
    let role = await Role.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (role?.length){
      role = role.map((obj) => obj.id);
      const routeRoleFilter9685 = { '': { [Op.in]: role } };
      const routeRole3412 = await deleteRouteRole(routeRoleFilter9685);
      const userRoleFilter4880 = { '': { [Op.in]: role } };
      const userRole7828 = await deleteUserRole(userRoleFilter4880);
      return await Role.destroy({ where :filter });
    } else {
      return 'No role found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteProjectRoute = async (filter) =>{
  try {
    let projectRoute = await ProjectRoute.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (projectRoute?.length){
      projectRoute = projectRoute.map((obj) => obj.id);
      const routeRoleFilter2055 = { '': { [Op.in]: projectRoute } };
      const routeRole9415 = await deleteRouteRole(routeRoleFilter2055);
      return await ProjectRoute.destroy({ where :filter });
    } else {
      return 'No projectRoute found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteRouteRole = async (filter) =>{
  try {
    return await RouteRole.destroy({ where: filter });
  } catch (error){
    throw new Error(error.message);
  }
};

const deleteUserRole = async (filter) =>{
  try {
    return await UserRole.destroy({ where: filter });
  } catch (error){
    throw new Error(error.message);
  }
};

const countUser = async (filter) =>{
  try {
    let user = await User.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (user?.length){
      user = user.map((obj) => obj.id);
      const userFilter8550 = { 'addedBy': { [Op.in]: user } };
      const user9585Cnt = await countUser(userFilter8550);
      const userFilter5774 = { 'updatedBy': { [Op.in]: user } };
      const user8202Cnt = await countUser(userFilter5774);
      const userAuthSettingsFilter3990 = { '': { [Op.in]: user } };
      const userAuthSettings1574Cnt = await countUserAuthSettings(userAuthSettingsFilter3990);
      const userTokenFilter2447 = { '': { [Op.in]: user } };
      const userToken5696Cnt = await countUserToken(userTokenFilter2447);
      const userRoleFilter7118 = { '': { [Op.in]: user } };
      const userRole6713Cnt = await countUserRole(userRoleFilter7118);
      const userCnt =  await User.count(filter);
      let response = { user : userCnt  };
      response = {
        ...response,
        ...user9585Cnt,
        ...user8202Cnt,
        ...userAuthSettings1574Cnt,
        ...userToken5696Cnt,
        ...userRole6713Cnt,
      };
      return response;
    } else {
      return 'No user found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const countUserAuthSettings = async (filter) =>{
  try {
    const userAuthSettingsCnt =  await UserAuthSettings.count(filter);
    return { userAuthSettings : userAuthSettingsCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countUserToken = async (filter) =>{
  try {
    const userTokenCnt =  await UserToken.count(filter);
    return { userToken : userTokenCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countRole = async (filter) =>{
  try {
    let role = await Role.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (role?.length){
      role = role.map((obj) => obj.id);
      const routeRoleFilter5359 = { '': { [Op.in]: role } };
      const routeRole3622Cnt = await countRouteRole(routeRoleFilter5359);
      const userRoleFilter9313 = { '': { [Op.in]: role } };
      const userRole9903Cnt = await countUserRole(userRoleFilter9313);
      const roleCnt =  await Role.count(filter);
      let response = { role : roleCnt  };
      response = {
        ...response,
        ...routeRole3622Cnt,
        ...userRole9903Cnt,
      };
      return response;
    } else {
      return 'No role found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const countProjectRoute = async (filter) =>{
  try {
    let projectRoute = await ProjectRoute.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (projectRoute?.length){
      projectRoute = projectRoute.map((obj) => obj.id);
      const routeRoleFilter2242 = { '': { [Op.in]: projectRoute } };
      const routeRole7574Cnt = await countRouteRole(routeRoleFilter2242);
      const projectRouteCnt =  await ProjectRoute.count(filter);
      let response = { projectRoute : projectRouteCnt  };
      response = {
        ...response,
        ...routeRole7574Cnt,
      };
      return response;
    } else {
      return 'No projectRoute found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const countRouteRole = async (filter) =>{
  try {
    const routeRoleCnt =  await RouteRole.count(filter);
    return { routeRole : routeRoleCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const countUserRole = async (filter) =>{
  try {
    const userRoleCnt =  await UserRole.count(filter);
    return { userRole : userRoleCnt };
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteUser = async (filter) =>{
  try {
    let user = await User.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (user?.length){
      user = user.map((obj) => obj.id);
      const userFilter9721 = { 'addedBy': { [Op.in]: user } };
      const user7248 = await softDeleteUser(userFilter9721);
      const userFilter1886 = { 'updatedBy': { [Op.in]: user } };
      const user7519 = await softDeleteUser(userFilter1886);
      const userAuthSettingsFilter4697 = { '': { [Op.in]: user } };
      const userAuthSettings8377 = await softDeleteUserAuthSettings(userAuthSettingsFilter4697);
      const userTokenFilter1206 = { '': { [Op.in]: user } };
      const userToken4463 = await softDeleteUserToken(userTokenFilter1206);
      const userRoleFilter2134 = { '': { [Op.in]: user } };
      const userRole0739 = await softDeleteUserRole(userRoleFilter2134);
      return await User.update(
        {
          isDeleted:true,
          isActive:false
        },{
          fields: ['isDeleted', 'isActive'],
          where: filter ,
        });
    } else {
      return 'No user found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteUserAuthSettings = async (filter) =>{
  try {
    return await UserAuthSettings.update(
      {
        isDeleted:true,
        isActive:false
      },{
        fields: ['isDeleted', 'isActive'],
        where: filter,
      });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteUserToken = async (filter) =>{
  try {
    return await UserToken.update(
      {
        isDeleted:true,
        isActive:false
      },{
        fields: ['isDeleted', 'isActive'],
        where: filter,
      });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteRole = async (filter) =>{
  try {
    let role = await Role.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (role?.length){
      role = role.map((obj) => obj.id);
      const routeRoleFilter5869 = { '': { [Op.in]: role } };
      const routeRole8487 = await softDeleteRouteRole(routeRoleFilter5869);
      const userRoleFilter9231 = { '': { [Op.in]: role } };
      const userRole2553 = await softDeleteUserRole(userRoleFilter9231);
      return await Role.update(
        {
          isDeleted:true,
          isActive:false
        },{
          fields: ['isDeleted', 'isActive'],
          where: filter ,
        });
    } else {
      return 'No role found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteProjectRoute = async (filter) =>{
  try {
    let projectRoute = await ProjectRoute.findAll({
      where:filter,
      attributes:{ include:'id' }
    });
    if (projectRoute?.length){
      projectRoute = projectRoute.map((obj) => obj.id);
      const routeRoleFilter2546 = { '': { [Op.in]: projectRoute } };
      const routeRole2367 = await softDeleteRouteRole(routeRoleFilter2546);
      return await ProjectRoute.update(
        {
          isDeleted:true,
          isActive:false
        },{
          fields: ['isDeleted', 'isActive'],
          where: filter ,
        });
    } else {
      return 'No projectRoute found.';
    }
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteRouteRole = async (filter) =>{
  try {
    return await RouteRole.update(
      {
        isDeleted:true,
        isActive:false
      },{
        fields: ['isDeleted', 'isActive'],
        where: filter,
      });
  } catch (error){
    throw new Error(error.message);
  }
};

const softDeleteUserRole = async (filter) =>{
  try {
    return await UserRole.update(
      {
        isDeleted:true,
        isActive:false
      },{
        fields: ['isDeleted', 'isActive'],
        where: filter,
      });
  } catch (error){
    throw new Error(error.message);
  }
};

module.exports = {
  deleteUser,
  deleteUserAuthSettings,
  deleteUserToken,
  deleteRole,
  deleteProjectRoute,
  deleteRouteRole,
  deleteUserRole,
  countUser,
  countUserAuthSettings,
  countUserToken,
  countRole,
  countProjectRoute,
  countRouteRole,
  countUserRole,
  softDeleteUser,
  softDeleteUserAuthSettings,
  softDeleteUserToken,
  softDeleteRole,
  softDeleteProjectRoute,
  softDeleteRouteRole,
  softDeleteUserRole,
};
