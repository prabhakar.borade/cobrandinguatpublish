const { v4 } = require("uuid");
const fse = require("fs-extra");
const multer = require("multer");
const path = require("path");
const dbService = require('../../utils/dbService');
const ImgData = require('../../model/files');
const { UPLOADS_FOLDER,TEMP_FOLDER,COLLATERAL_FOLDER,COBRAND_FOLDER } = require('../../config/file')

const moveToPermanentFile =  (oldPath, newFilePath, newThumbName) => {
  return new Promise((resolve, reject) => {
     fse.move(oldPath, newFilePath, { overwrite: true }, (err) => {
    if (err) {
      reject(err);
      console.error(err);
    }
    let imageName = newFilePath.split('\\').pop();
    let imagePath = COLLATERAL_FOLDER + '/' + imageName;
    let thumbPath = COLLATERAL_FOLDER + '/' + newThumbName;
    let data = { path: imagePath , thumnailPath: thumbPath , isActive : false};
    dbService.updateOne(ImgData, imageName, data).then((result) => {
      console.log(result);
    });
    resolve();
  });
  })
 
};

const moveToPermanentThumbnail =  (oldPath, newFilePath) => {
   return new Promise((resolve, reject) => {
    fse.move(oldPath, newFilePath, { overwrite: true }, (err) => {
    if (err){
      reject(err);
      console.error(err);  
    }  
      resolve();   
  });
});
  
};

module.exports = {
  moveToPermanentFile,
  moveToPermanentThumbnail
};
