const path = require('path');
const dotenv = require("dotenv");
dotenv.config();
const { UPLOADS_FOLDER,TEMP_FOLDER,COLLATERAL_FOLDER,COBRAND_FOLDER } = require('../../config/file')
const jimp = require("jimp");
const ffmpeg = require("fluent-ffmpeg");
const PDFConvert = require("pdf2png-ts/dist/index").default;

function imageThumbnail(imagePath,newFileName) {
  return new Promise((resolve, reject) => {
    //const thumnailName = 'thumbnail'+newFileName;
    jimp
    .read(imagePath)
    .then((image) => {
       image
        .resize(150, 150)
        .quality(60)
        .write(newFileName);
        resolve();
    })
    .catch((err) => { 
      reject(err);
      console.error(err);
    });
  })
  
};

// function imageThumbnailCobrand(imagePath,newFilepath) {
//   return new Promise((resolve, reject) => {
//     jimp
//     .read(imagePath)
//     .then((image) => {
//        image
//         .resize(150, 150)
//         .quality(60)
//         .write(newFilepath);
//         resolve();
//     })
//     .catch((err) => { 
//       reject(err);
//       console.error(err);
//     });
//   })
  
// };

 async function videoThumbnail(videoInputPath, videoOutputPath,videoThumbnailName) {
   return new Promise((resolve, reject) => {
    try {
     ffmpeg(videoInputPath)
      .on("filenames", function (filenames) {
      })
      .on("end", function () {
         console.log("Screenshots taken");
         resolve();
      })
      .screenshots({
        count: 1,
        folder: videoOutputPath,
        filename:videoThumbnailName ,
        size: "150x150",
      });
      
  } catch (e) {
    reject(e.msg);
    console.log(e.code);
    console.log(e.msg);
  }
   });
}

async function pdfThumbnail(inputPDFPath, outputThumbnailPath) {
return new Promise(async (resolve, reject) => {
      try{
        const pdfConverter = new PDFConvert(inputPDFPath);
       await pdfConverter.getPageCount() 
          .then(pages => {
          console.log("PDF page count: ", pages);
          pdfConverter.convertPageToImage(1)
              .then(buffer => {
                jimp.read(buffer)
                      .then((image) => {
                        image
                          .resize(150, 150)
                          .quality(100)               
                          .write(outputThumbnailPath); 
                                        
                })
                .catch(error =>{
                  reject(error);
                  console.log(error)
                })
                  console.log("PDF Written to file as PNG Image")
                  pdfConverter.clean();
                  resolve();
              })
      })
      .catch(error => {
          console.error(error);
          pdfConverter.clean();
      })
 }
  catch (error) {
        console.log(error);
      }
    });
  }
module.exports = {
  imageThumbnail,
  //imageThumbnailCobrand,
  videoThumbnail,
  pdfThumbnail
};
