const jimp = require("jimp");
const fs = require("fs");
const { PDFDocument } = require("pdf-lib");
const {pdfThumbnail, videoThumbnail} =require("../../controller/client/filesController");
const { COBRAND_FOLDER , APP_HOST_URL} = require("../../config/file")
const ffmpegPath = require("@ffmpeg-installer/ffmpeg").path;
const fluentffmpeg = require("fluent-ffmpeg");
fluentffmpeg.setFfmpegPath(ffmpegPath);

const watermarkImage = (
  mainImage,
  logoImage,
  textImage,
  outputImagePath,
  Ximg,
  Yimg,
  logoWidth,
  logoHeight,
  Xtext,
  Ytext,
  textWidth,
  textHeight,
  thumnailPath
) => {
  return new Promise (async(resolve, reject )=> {
    const jimps = [jimp.read(mainImage), jimp.read(logoImage) , jimp.read(textImage)];
  Promise.all(jimps).then((data) => {
    if(!data) {
      reject();
    }
    data[1].resize(logoWidth, logoHeight, (cb) => {
      //data[1].write("./resize.png", () => console.log("Image resizes"));
      data[2].resize(textWidth, textHeight, () => {
      data[0].composite(data[1], Ximg, Yimg);
      data[0].composite(data[2], Xtext, Ytext);
      data[0].write(outputImagePath, () => console.log("Image created"));
      data[0].resize(150,150).quality(60).write(thumnailPath, () => {
        resolve();
      });
      })
    });
  });
  })
  
};

const waterMarkPDF =  (
  inputPDFPath,
  LogoimagePath,
  TextimagePath,
  outputPDFPath,
  Ximg,
  Yimg,
  Xtext,
  Ytext,
  logoWidth,
  logoHeight,
  textWidth,
  textHeight,
  outputPDFThumbnailPath
) => {
  return new Promise (async(resolve, reject )=> {
    let pngimg = LogoimagePath
    try {

  const pdfDoc = await PDFDocument.load(fs.readFileSync(inputPDFPath));
  let LogoimgData;
  if(LogoimagePath.split(".").pop() == "jpg") {LogoimgData = await pdfDoc.embedJpg(fs.readFileSync(pngimg));}
  if(LogoimagePath.split(".").pop() == "png") {LogoimgData = await pdfDoc.embedPng(fs.readFileSync(pngimg)); }
  const pages = pdfDoc.getPages();
  for (var i = 0; i < pages.length; i++) {
  const TextimgData = await pdfDoc.embedPng(TextimagePath[i].base64);

    pages[i].drawImage(LogoimgData, {
      x: Ximg[i],
      y: Yimg[i],
      width: logoWidth,
      height: logoHeight,
    });
    pages[i].drawImage(TextimgData, {
      x: Xtext[i],
      y: Ytext[i],
      width: textWidth,
      height: textHeight,
    });
  }
  const pdfBytes = await pdfDoc.save();
  
  fs.writeFile(outputPDFPath, pdfBytes, (error) => {
    if(error) {console.log("Error in pdf file writing", error); reject();}
    else {
    const pdfThumbURL = APP_HOST_URL+"/" + COBRAND_FOLDER +"/" + outputPDFPath.split("\\").pop();
     pdfThumbnail(pdfThumbURL, outputPDFThumbnailPath ).then(() => resolve());
    }

  });
 
  } catch (error) {
      reject();
      console.log(error);
    }
  })
};

 function watermarkVideo(
  inputVideo,
  inputimgpng,
  inputtextpng,
  outputVideo,
  duration,
  Ximg,
  Yimg,
  Xtext,
  Ytext,
  thumbnailVideoPath, 
  thumnailVideoName,
  LogoWidth,
  LogoHeight,
  videoStart,
  videoEnd,
  textStart,
  textEnd,
  TextWidth,
  TextHeight
) {
  return new Promise (async(resolve, reject )=> {
  try {
    await jimp.read(inputimgpng).then((image)=> 
    image
    .cover(LogoWidth, LogoHeight)
    .quality(100)
    .write(inputimgpng));
    await jimp.read(inputtextpng).then(image => image.cover(TextWidth, TextHeight).quality(100).write(inputtextpng));
    fluentffmpeg(inputVideo)
      .input(inputimgpng)
      .input(inputtextpng)
     .complexFilter(
    [
      {
        "filter": "overlay",
        "options": {
          "enable": `between(t,${videoStart},${videoEnd})`,
          "x": `${Ximg}`,
          "y":  `${Yimg}`,
        },
        "inputs": "[0:v][1:v]",
        "outputs": "tmp",
        
      },
      {
        "filter": "overlay",
        "options": {
          "enable": `between(t,${textStart},${textEnd})`,
          "x":  `${Xtext}`,
          "y": `${Ytext}`,
        },
        "inputs": "[tmp][2:v]",
        "outputs": "tmp",
      
      }
    ], 'tmp')
      .outputOptions(["-map 0:a?"])
      .output(outputVideo)
      .on("end", function (err) {
        if (!err) {
          videoThumbnail(outputVideo, thumbnailVideoPath, thumnailVideoName).then(() => {
            console.log("conversion Done");
            resolve();
          }) 
        }
      })
      .on("error", function (err) {
        console.log("error: ", err);
        reject(err);
      })
      .run();
  } catch (error) {
    reject(error);
    console.log(error);
  }
});
}
// const watermarkPPT = async () => {
//   try {
//     const pptx = new PPTX.Composer();
//     await pptx.load("./ppt/samplepptx.pptx");
//     await pptx.compose(async (pres) => {
//       await pres.addSlide((slide) => {
//         slide.addText((text) => {
//           text.value("Hello World");
//         });
//       });
//     });

//     await pptx.save(`./sampleppt-result.pptx`);
//   } catch (error) {
//     console.log(error);
//   }
// };

module.exports = {
  watermarkVideo,
  watermarkImage,
  // watermarkPPT,
  waterMarkPDF,
};

