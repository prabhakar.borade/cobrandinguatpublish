const dotenv = require('dotenv');
dotenv.config();
const listEndpoints = require('express-list-endpoints');
const app = require('./app_config');
const seeder = require('./seeders');
const models = require('./model');
const { socketConn, sendMessage, socketMsg } = require('./utils/socket');
models.sequelize.sync({}).then(()=>{
  const allRegisterRoutes = listEndpoints(app);
  seeder(allRegisterRoutes);
});


// const httpServer = require('http').createServer(app);
// var io = require('socket.io')(httpServer,{
//   cors:{origin:'*'}
// });

const server = require('http').createServer(app);
//socketConn(server);
// sendMessage('processing',  {"Data":" hello there!!"})
server.listen(process.env.PORT,()=>{
  console.log(`your application is running on port ${process.env.PORT}`);
});

//var io = socket(server);

// io.on('connection', (socket) => {
//   console.log('Socket: client connected');
//       socket.emit('processing', {"Data":" newel"});
//   // socket.on('processing', () =>{
//   //   console.log('Socket: client processing');

//   // });

// });

// function callUI(data) {
//   socket.emit('message', data);
// }
// io.emit('processing', {"Data":" akshay new"});
module.exports = {server};

// 
//     