const { DataTypes } = require("sequelize");
const sequelize = require("../config/dbConnection");
const sequelizePaginate = require("sequelize-paginate");
const sequelizeTransforms = require("sequelize-transforms");
const { convertObjectToEnum } = require("../utils/common");
const bcrypt = require("bcrypt");
// const Collaterals = require("./collaterals");
// const Files = require("./files");

let coUserActivity = sequelize.define("coUserActivity", {
  activityid: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
  activity : { type:DataTypes.JSONB , allowNull:true },
  createdBy: { type: DataTypes.STRING },
  userid : { type: DataTypes.INTEGER }
});


sequelizeTransforms(coUserActivity);
sequelizePaginate.paginate(coUserActivity);
module.exports = coUserActivity;
