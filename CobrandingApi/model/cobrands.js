const { DataTypes } = require("sequelize");
const sequelize = require("../config/dbConnection");
const sequelizePaginate = require("sequelize-paginate");
const sequelizeTransforms = require("sequelize-transforms");
const { convertObjectToEnum } = require("../utils/common");
const bcrypt = require("bcrypt");
const Collaterals = require("./collaterals");
const Files = require("./files");

let Cobrands = sequelize.define("cobrands", {
  id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
  collateralId: { type: DataTypes.INTEGER, allowNull: false },
  collateralName: { type: DataTypes.STRING, allowNull: true }, 
  categoryName: { type: DataTypes.STRING, allowNull: true }, 
  subcategoryName: { type: DataTypes.STRING, allowNull: true }, 
  createdBy: { type: DataTypes.INTEGER },
  updatedBy: { type: DataTypes.INTEGER },
  createdAt:{ type:DataTypes.DATE },
  updatedAt:{ type:DataTypes.DATE },
  collateralType:{ type:DataTypes.STRING ,allowNull: false},
  categoryId:{ type:DataTypes.INTEGER ,allowNull: false},
  subcategoryId:{ type:DataTypes.INTEGER ,allowNull: false},
  isProcessed:{ type:DataTypes.BOOLEAN },
});

sequelizeTransforms(Cobrands);
sequelizePaginate.paginate(Cobrands);
module.exports = Cobrands;
