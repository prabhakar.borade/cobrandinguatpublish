const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');

let Notifications = sequelize.define('notifications',{
  id:{type:DataTypes.INTEGER,autoIncrement:true,primaryKey:true},
  notification:{ type:DataTypes.STRING , allowNull: false},
  read:{ type:DataTypes.BOOLEAN , allowNull: false },
  createdBy:{ type:DataTypes.INTEGER , allowNull: false},
  createdAt:{ type:DataTypes.DATE },
});
sequelizeTransforms(Notifications);
sequelizePaginate.paginate(Notifications);
module.exports = Notifications;
