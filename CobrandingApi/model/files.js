const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { convertObjectToEnum } = require('../utils/common');
const bcrypt = require('bcrypt');

let Files = sequelize.define('files',{
  id:{type:DataTypes.INTEGER,autoIncrement:true},
  fileId: {
    type: DataTypes.STRING,
    allowNull: false,
    primaryKey:true
  },
  // userId: {
  //   type: DataTypes.STRING,
  //   allowNull: false,
  // },
  // collateralId: {
  //   type: DataTypes.INTEGER,
  //   allowNull: false,
  // },
  path: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  originalFileName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  imageName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  mimetype: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  // encoding: {
  //   type: DataTypes.STRING,
  //   allowNull: true,
  // },
  size: {
    type: DataTypes.INTEGER,
    allowNull: true,
  },
  
  thumnailName : { type: DataTypes.STRING,allowNull: false,  },
  thumnailPath : { type: DataTypes.STRING,allowNull: false,  },

  isActive:{ type:DataTypes.BOOLEAN },
  createdBy:{ type:DataTypes.INTEGER },
  updatedBy:{ type:DataTypes.INTEGER }
});
sequelizeTransforms(Files);
sequelizePaginate.paginate(Files);
module.exports = Files;
