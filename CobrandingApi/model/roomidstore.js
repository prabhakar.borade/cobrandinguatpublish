const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');

let Roomdetails = sequelize.define('roomdetails',{
  id:{type:DataTypes.INTEGER,autoIncrement:true,primaryKey:true},
  roomId:{ type:DataTypes.STRING , allowNull: false},
  createdBy:{ type:DataTypes.INTEGER , allowNull: false},
  createdAt:{ type:DataTypes.DATE },
});
sequelizeTransforms(Roomdetails);
sequelizePaginate.paginate(Roomdetails);
module.exports = Roomdetails;
