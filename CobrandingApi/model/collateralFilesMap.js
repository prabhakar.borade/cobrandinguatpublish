const { DataTypes } = require("sequelize");
const sequelize = require("../config/dbConnection");
const sequelizePaginate = require("sequelize-paginate");
const sequelizeTransforms = require("sequelize-transforms");
const { convertObjectToEnum } = require("../utils/common");
const bcrypt = require("bcrypt");
const Collaterals = require("./collaterals");
const Files = require("./files");

let CollateralFileMap = sequelize.define("collateralfilemaps", {
  id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
  collateralId: { type: DataTypes.INTEGER, allowNull: false },
  fileId: { type: DataTypes.STRING, allowNull: false },
  watermarkDetails : { type:DataTypes.JSONB , allowNull:true },
  // logoXcordinate: { type: DataTypes.INTEGER },
  // logoYcordinate: { type: DataTypes.INTEGER },
  // logoWidthcordinate: { type: DataTypes.INTEGER },
  // logoHeightcordinate: { type: DataTypes.INTEGER },
  // textAreaXcordinate: { type: DataTypes.INTEGER },
  // textAreaYcordinate: { type: DataTypes.INTEGER },
  // textAreaWidthcordinate: { type: DataTypes.INTEGER },
  // textAreaHeightcordinate: { type: DataTypes.INTEGER },
  // textAreaFontText: { type: DataTypes.STRING },
  // textAreaFontSize: { type: DataTypes.INTEGER },
  // textAreaBold: { type: DataTypes.BOOLEAN },
  // textAreaItalic: { type: DataTypes.BOOLEAN },
  // maxCharacter: { type: DataTypes.INTEGER },
  createdBy: { type: DataTypes.INTEGER },
  updatedBy: { type: DataTypes.INTEGER },
});

// Collaterals.belongsTo(CollateralFileMap, {
//   foreignKey: "categoryId",
//   constraints: false,
// });

sequelizeTransforms(CollateralFileMap);
sequelizePaginate.paginate(CollateralFileMap);
module.exports = CollateralFileMap;
