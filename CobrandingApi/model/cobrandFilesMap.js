const { DataTypes } = require("sequelize");
const sequelize = require("../config/dbConnection");
const sequelizePaginate = require("sequelize-paginate");
const sequelizeTransforms = require("sequelize-transforms");
const { convertObjectToEnum } = require("../utils/common");
const bcrypt = require("bcrypt");
const Collaterals = require("./collaterals");
const Files = require("./files");

let CobrandFilesMap = sequelize.define("cobrandfilemaps", {
  id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
  fileId: { type: DataTypes.STRING, allowNull: false },
  cobrandId: { type: DataTypes.INTEGER, allowNull: false },
  createdBy: { type: DataTypes.INTEGER },
  updatedBy: { type: DataTypes.INTEGER },
});

sequelizeTransforms(CobrandFilesMap);
sequelizePaginate.paginate(CobrandFilesMap);
module.exports = CobrandFilesMap;
