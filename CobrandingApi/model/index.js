const dbConnection = require('../config/dbConnection');
const db = {};
db.sequelize = dbConnection;

db.user = require('./user');
db.collateral = require('./collaterals');
db.files = require('./files');
db.collateralFilesMap = require("./collateralFilesMap");
db.cobrands = require("./cobrands");
db.cobrandsfilesmaps = require("./cobrandFilesMap");
db.coUserActivity =require("./cobrandingActivity")


db.userAuthSettings = require('./userAuthSettings');
db.userToken = require('./userToken');
db.role = require('./role');
db.projectRoute = require('./projectRoute');
db.routeRole = require('./routeRole');
db.userRole = require('./userRole');


//db.collateralFilesMap.belongsToMany(db.collateral, { foreignKey: "collateralId" });
//db.collateralFilesMap.hasMany(db.files , { foreignKey: "fileId" })
//db.collateralFilesMap.hasMany(db.collateral,{ foreignKey: "collateralId" });
db.collateral.hasMany(db.collateralFilesMap,{ foreignKey: "collateralId", onDelete: 'cascade' });
db.collateralFilesMap.belongsTo(db.files , { foreignKey: "fileId" });

// db.cobrands.belongsTo(db.collateral , { foreignKey:"collateralId"});
db.cobrands.hasMany(db.cobrandsfilesmaps , { foreignKey:"cobrandId",  onDelete: 'cascade'});
db.cobrandsfilesmaps.belongsTo(db.files , { foreignKey: "fileId"});
//db.collateralFilesMap.belongsTo(db.files,{ foreignKey: "fileId" });

//db.collateral.belongsToMany(db.files,{through:db.collateralFilesMap})
//db.files.belongsToMany(db.collateral,{through:db.collateralFilesMap})

//db.collateral.hasMany(db.collateralFilesMap,{ foreignKey: "collateralId" });

//db.files.hasOne(db.collateral);
// db.collateralFilesMap.hasMany(db.collateral);
// db.collateralFilesMap.hasMany(db.files);
//db.collateralFilesMap.belongsTo(db.files, { foreignKey: "fileId" });

// db.user.belongsTo(db.user, { foreignKey: 'addedBy' });
// db.user.belongsTo(db.user, { foreignKey: 'updatedBy' });

db.user.belongsTo(db.user, { foreignKey: 'addedBy' });
db.user.belongsTo(db.user, { foreignKey: 'updatedBy' });
db.userAuthSettings.belongsTo(db.user, { foreignKey: '' });
db.userToken.belongsTo(db.user, { foreignKey: '' });
db.userRole.belongsTo(db.user, { foreignKey: '' });
db.routeRole.belongsTo(db.role, { foreignKey: '' });
db.userRole.belongsTo(db.role, { foreignKey: '' });
db.routeRole.belongsTo(db.projectRoute, { foreignKey: '' });



module.exports = db;