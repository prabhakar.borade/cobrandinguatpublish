const { DataTypes } = require('sequelize');
const sequelize = require('../config/dbConnection');
const sequelizePaginate = require('sequelize-paginate');
const sequelizeTransforms = require('sequelize-transforms');
const { convertObjectToEnum } = require('../utils/common');
const bcrypt = require('bcrypt');

let Collaterals = sequelize.define('collateral',{
  id:{type:DataTypes.INTEGER,autoIncrement:true,primaryKey:true},
  collateralName:{ type:DataTypes.STRING },
  collateralType:{ type:DataTypes.STRING },
  categoryId:{ type:DataTypes.INTEGER },
  categoryName:{ type:DataTypes.STRING },
  subcategoryId:{ type:DataTypes.INTEGER },
  subcategoryName:{ type:DataTypes.STRING },
  statusAsDraft:{ type:DataTypes.BOOLEAN },
  //numberOfCollateral:{ type:DataTypes.INTEGER },
  isActive:{ type:DataTypes.BOOLEAN },
  createdBy:{ type:DataTypes.INTEGER },
  updatedBy:{ type:DataTypes.INTEGER }
});
sequelizeTransforms(Collaterals);
sequelizePaginate.paginate(Collaterals);
module.exports = Collaterals;
